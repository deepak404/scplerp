$(document).ready(function(){
    $('#filter-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'mm-yy',
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });

    $('#filter-date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });

    // $('.committed-date').datepicker({dateFormat : 'dd-mm-yy'});

    $(document).on('input','#total_order,.weight' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });

    var currentTime = new Date()
    var minDate = '1-'+currentTime.getMonth()+'-'+currentTime.getFullYear();
    var maxDate =  new Date(currentTime.getFullYear(), currentTime.getMonth() +1, +0);

    console.log(minDate);

    $('#order_date, #order_update').datepicker({
        dateFormat : 'dd-mm-yy',
        minDate: minDate,
        // maxDate: maxDate,
    });

    $('#customer_date, #customer_date_update').datepicker({
        dateFormat : 'dd-mm-yy',
        minDate: minDate
    });

	$.ajaxSetup({
	    headers:
	    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});

    $('#create-sale').on('click',function(){
    	$('#sales-modal').modal('show');
    });

    $('#add-sales-form').on('submit',function(e){

    	e.preventDefault();

    	var formData = $(this).serialize();

		var saleEntries = [];

        $('.order-group').each(function(){

            saleEntries.push({
                'material': $(this).find('.material').val(),
                'weight': $(this).find('.weight').val(),
                'customerDate': $(this).find('.customer-date').val(),
            })

        });

        saleEntries = JSON.stringify(saleEntries);

        var formData = new FormData();
        formData.append('customer_name', $(this).find('#customer_name').val());
        formData.append('order_date', $(this).find('#order_date').val());
        formData.append('sale_entries', saleEntries);
        formData.append('id', 0);

        $.ajax({
            type: "POST",
            url: "/create-sale-order",
            data: formData,
            cache: !1,
            contentType: !1,
            processData: !1,
            beforeSend: function() {},
            success: function(data, status, xhr) {
                if(xhr.status == 200){
                    location.reload();
                }

                if(xhr.status == 500){
                    alert(data.msg);
                }
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {

                	errorHandler(xhr.responseJSON.errors)

                }
			},
        })

    });

    $('#update-sales-form').on('submit',function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "/update-sale-order",
            data: formData,
            success: function(data, status, xhr) {
                if(xhr.status == 200){
                    location.reload();
                }

                if(xhr.status == 500){
                    alert(data.msg);
                }
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {

                    errorHandler(xhr.responseJSON.errors)

                }
            },
        })
    })

    function errorHandler(errors) {
    	$.each(errors, function(key,value){
			$("#"+key).next(".feedback").text(value[0]);
    	});
    }

	$('.text-input').on('keyup',function(){
		$(this).next(".feedback").text('');
	});

	$('.delete-sales').on('click',function(e){
        e.preventDefault();
        var data_id = $(this).data('id');

        $('#confirmationModal').modal('show');

        $('.delete-sale-btn').click(function(e){

            if ($(this).val() == 'Yes'){
                $.ajax({
                    type: 'POST',
                    url: '/delete-sales',
                    processData : false,
                    cache : false,
                    data: 'id='+data_id,
                    success:function(data){
                        if (data.status) {
                            $('#confirmationModal').modal('hide');
                            location.reload(true);
                        }
                    },
                    error:function(data, status, xhr){
                        if(xhr.status == 500){
                            alert(data.msg);
                        }
                    }
                });
            } else{


            }


        })

	});

	$('.edit-sales').on('click',function(){
		var data_id = $(this).data('id');
		$.ajax({
			type: 'POST',
            url: '/get-sale-order',
            processData : false,
            cache : false,
            data: 'id='+data_id,
            success:function(data){
                $('#update_id').val(data_id);
                if (data.status) {

                    if(data.data.edit_state == 1 || data.data.delete_state == 1){

                        alert('Production planning has started for the sale order. The sale Order cannot be edited anymore.');
                    }else{

                        $('#material_update').empty();

                        var raw = data.data;
                        var orderDate = (raw.order_date).split('-');
                        orderDate = orderDate[2]+'-'+orderDate[1]+'-'+orderDate[0];
                        var customerDate = (raw.customer_date).split('-');
                        customerDate = customerDate[2]+'-'+customerDate[1]+'-'+customerDate[0];
                        $('#customer_name_update').val(raw.bp_id);
                        $("#customer_name_update").attr('readonly','readonly');
                        $('#weight_update').val(raw.order_quantity);
                        $('#order_update').val(orderDate);
                        $('#customer_date_update').val(customerDate);

                        $.each(data.bpMaterials, function(k,v){
                            $('#material_update').append(
                                '<option value="'+v.material_id+'|'+v.descriptive_name+'">'+v.descriptive_name+'</option>'
                            );
                        })
                        $('#material_update').val(raw.material_id);

                        $('#sales-modal-update').modal('show');
                    }

            	}
            },
            error:function(data, status, xhr){

			    if(xhr.status == 500){
			        alert(data.data)
                }
            }
    	});
	});

    $('#export').on('click',function(){
        // window.location.href = "/generate-sales-report";
        var win = window.open("/generate-sales-report/"+$(this).data('date'), '_blank');
        win.focus();
    })

    $(document).on('click','.ui-datepicker-close',function(){
        window.location.href = '/sale-order/'+$('#filter-date').val();
    });

	$('#total_order').on('blur', function(){
		var totalOrders = $(this).val();
		var i;

		if($('#order_date').val() != '' ){
            if(totalOrders > 10){

                $(this).next().text('Order cannot be greater than 10.');

            }else{

                // totalOrders = $(document).find('.order-group').length;


                $('.order-group').not(':first').remove();

                for(i=1; i<totalOrders; i++){
                    $( ".order-group" ).first().clone().insertAfter($('.order-group').last());

                    $(document).find(".order-group").last().find('.weight').val('');
                    $(document).find(".order-group").last().find('.customer-date').val('');
                }

            }
        }else{
		    alert('Please Input the order Date before entering the No of items.');
        }





	});


	$(document).on('click', '.delete-sale-item', function(e){

	    if($('.order-group').length > 1){
            $(this).closest('.order-group').remove();
            $('#total_order').val($('#total_order').val() - 1);
        }else{
	        alert('Cannot Delete. Entry Should have atleast one sale Item');
        }

    });



    $('body').on('focus',".customer-date", function() {
        $(this).datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: 0,
            onSelect: function(date){
                var date1 = $('#order_date').datepicker('getDate');
                var date = new Date( Date.parse( date1 ) );
                date.setDate( date.getDate());
                var newDate = date.toDateString();
                newDate = new Date( Date.parse( newDate ) );
                $(this).datepicker("option","minDate",newDate);
            },

        });
    });


    $(document).on('keyup','input[name="weight"]',function(){
        var totalSaleQty = 0;

        var weights = $(document).find('input[name="weight"]');

        weights.each(function(){

            console.log($(this).val());
            if($(this).val() != ''){
                totalSaleQty += parseInt($(this).val());
            }
        });

        $('#total-sale-qty span').text(totalSaleQty);
    });


    $('#customer_name').on('change', function () {

        $.ajax({
            type: 'POST',
            url: '/get-bp-materials',
            processData : false,
            cache : false,
            data: 'id='+$(this).val(),
            success:function(data){
                $('select.material').empty();
                $.each(data.materials, function(k,v){
                    $('select.material').append(
                        '<option value="'+v.material_id+'|'+v.descriptive_name+'">'+v.descriptive_name+'</option>'
                    );
                })
            },
            error:function(xhr){

            }
        });
    });
    $('input[name="committed_date"]').attr('autocomplete','off');
    $('.add-date').on('click',function() {
      // alert($(this).data('id'));
      $('input[name="committed_date"]').val($(this).data('date'));
      $('input[name="id"]').val($(this).data('id'));
      $('#updateDate').modal('show');
    });
});
