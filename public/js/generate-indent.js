$(document).ready(function(){
    $('.due_date').datepicker({dateFormat : 'dd-mm-yy'});
    $('#date').datepicker({dateFormat : 'dd-mm-yy'});
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $(document).on('input','.quantity,.rate,#tax,#handling-charges' ,function() {
      $(this).val($(this).val().replace(/[^0-9.]/gi, ''));
    });

    $(document).on('change','input[type="checkbox"]',function(){
        if ($(this).prop('checked')) {
            var count = $(this).data('count');
            var quantity = $(this).data('quantity');
            $(this).parent().find('i').text('check_box').addClass('green');
            $(this).parent().parent().find("input[type='text']").attr('disabled',false);
            $(this).parent().parent().find("input[type='text']").css({'background-color':'white'});
        }else{
            $(this).parent().find('i').text('check_box_outline_blank').removeClass('green');
            $(this).parent().parent().find("input[type='text']").attr('disabled',true);
            $(this).parent().parent().find("input[type='text']").css({'background-color':'rgba(156, 156, 156, 0.23)'});
        }
    });


    $('#customer_name').on('change',function(){
        var bp_id = $(this).val();
        $.ajax({
            type:'POST',
            url:'/get-bp-order',
            data:'bp_id='+bp_id,
            success:function(data){
                if (data.status) {
                    $('.checkbox').empty();
                    $('#bank-name').empty();
                    var orders = data.data;
                    var banks = data.bankDetail;
                    if (orders.length > 0) {
                        appendOrderList(data.businessPartner,orders,banks)
                    }

                    $('#tax').val(data.businessPartner.transit_insurance);

                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
    });

    $('#add-order').on('click',function(){
        var bp_id = $(this).data('bpid');
        var master_id = $(this).data('masterid');
        $.ajax({
            type:'POST',
            url:'/get-bp-order',
            data:'bp_id='+bp_id,
            success:function(data){
                if (data.status) {
                    $('#data-input').empty();
                    var orders = data.data;
                    if (orders.length > 0) {
                        var count = 0;
                        $('#data-input').append('<input type="hidden" name="id" value="'+master_id+'">')
                        $.each(orders,function(key,value){
                            $('#data-input').append('<div class="form-group col-sm-12">'+
                                                        '<div class="col-sm-3" style="margin-top: 20px;">'+
                                                            '<label for="c'+count+'" class="inline-label">'+
                                                                '<i class="material-icons">'+
                                                                    'check_box_outline_blank'+
                                                                '</i>'+
                                                            '</label>'+
                                                            '<input type="checkbox" name="order[]" id="c'+count+'" value="'+value.id+'">'+
                                                            '<span>'+value.material_name+'</span>'+
                                                        '</div>'+
                                                        '<div class="col-sm-3">'+
                                                            '<label for="due_date" class="inline-label">Due on</label>'+
                                                            '<input type="text" class="order-input due_date" name="due_date[] "value="'+formatDate(value.customer_date)+'" autocomplete="off" disabled>'+
                                                        '</div>'+
                                                        '<div class="col-sm-3">'+
                                                            '<label for="quantity" class="inline-label">Quantity (Kgs)</label>'+
                                                            '<input type="text" class="order-input quantity" name="quantity[]" autocomplete="off" value="'+value.order_quantity+'" disabled readonly>'+
                                                        '</div>'+
                                                        '<div class="col-sm-3">'+
                                                            '<label for="rate" class="inline-label">Rate</label>'+
                                                            '<input type="text" class="order-input rate" name="rate[]" autocomplete="off" disabled required>'+
                                                        '</div>'+
                                                    '</div>');
                            $('.due_date').datepicker({dateFormat : 'dd-mm-yy'});
                            count++;
                        });
                        $('#data-input').append('<div class="form-group col-sm-12">'+
                                                    '<input type="submit" class="btn btn-primary" value="Update Indent">'+
                                                '</div>');
                    }else{
                        $('#data-input').append('<p>No Order to Add..!</p>');
                    }
                    $('#add-order-modal').modal('show');
                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
    });

    function appendOrderList(businessPartner, data,banks) {
        var count = 0;

        $('#destination').val(businessPartner.dispatch_destination);
        $('#dispatch').val(businessPartner.dispatcher);
        //console.log(data.businessPartner.mode_of_transport);
        $('#mode_of_transport').val(businessPartner.mode_of_transport);

        $.each(data,function(key,value){


            $('.checkbox').append('<div class="form-group ">'+
                                '<div class="order-list" style="width: 24%;">'+
                                    '<label for="c'+count+'" class="inline-label"><i class="material-icons">'+
                                        'check_box_outline_blank'+
                                    '</i></label>'+
                                    '<input type="checkbox" name="order[]" id="c'+count+'" value="'+value.id+'"> <span>'+value.material_name+'</span>'+
                                '</div>'+
                                '<div class="order-list list-input-spacing" style="width: 20%;">'+
                                    '<label for="due_date" class="inline-label">Due on</label>'+
                                    '<input type="text" class="order-input due_date" value="'+formatDate(value.customer_date)+'" name="due_date[]" autocomplete="off" disabled>'+
                                '</div>'+
                                '<div class="order-list list-input-spacing" style="width: 25%;">'+
                                    '<label for="quantity" class="inline-label">Quantity (Kgs)</label>'+
                                    '<input type="text" class="order-input quantity" name="quantity[]" autocomplete="off" value="'+value.order_quantity+'" disabled>'+
                                '</div>'+
                                '<div class="order-list list-input-spacing" style="width: 20%;">'+
                                    '<label for="rate" class="inline-label">Rate</label>'+
                                    '<input type="text" class="order-input rate" name="rate[]" autocomplete="off" disabled required>'+
                                '</div>'+
                            '</div>');
            $('.due_date').datepicker({dateFormat : 'dd-mm-yy'});
            count++;
        });
    }


    function formatDate(date){
        var date = new Date(date);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();


        return day+'-'+month+'-'+year;
    }

});
