$(document).ready(function(){

    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $("input[type='text']").attr("autocomplete", "off");

    $('#create-memo').on('click',function(){
        $("#lot-id").remove();
        $("input[type=text]").val("");
        $("select").val("");
        $('#createFilamentModal').modal('show');
    });

	$('#filament-form').on('submit',function(e){
		e.preventDefault();
		var formData = $(this).serialize();
		$.ajax({
            type:'POST',
            url:'/create-filament-memo',
            data:formData,
            success:function(data){
                if (data.status == '1') {
                    location.reload();
                }else{
                    alert(data.msg);
                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
	});

    $('.edit').on('click',function(){
        var id = $(this).data('id');
        $.ajax({
            type:'POST',
            url:'/get-filament-memo',
            data:'id='+id,
            success:function(data){
                if (data.status == '1') {
                    $("#lot-id").remove();
                    var lot = data.lot;
                    $('#filament-form').append('<input type="hidden" name="id" id="lot-id" value="'+lot.id+'">');
                    if (lot.twisting != null) {
                        $('#twisting').val(lot.twisting);
                    }
                    if (lot.type != null) {
                        $('#type').val(lot.type);
                    }
                    if (lot.denier != null) {
                        $('#denier').val(lot.denier);
                    }
                    if (lot.net_wt != null) {
                        $('#weight').val(lot.net_wt);
                    }
                    if (lot.tube_color != null) {
                        $('#tube-color').val(lot.tube_color);
                    }
                    if (lot.tube_id != null) {
                        $('#tube-id').val(lot.tube_id);
                    }
                    if (lot.arrival_date != null) {
                        $('#doa').val(lot.arrival_date);
                    }
                    if (lot.inv_no != null) {
                        $('#inv-no').val(lot.inv_no);
                    }
                    if (lot.inv_date != null) {
                        $('#inv-date').val(lot.inv_date);
                    }
                    if (lot.pallet_count != null) {
                        $('#pallet-no').val(lot.pallet_count);
                    }
                    if (lot.material_code != null) {
                        $('#material-code').val(lot.material_code);
                    }
                    if (lot.note != null) {
                        $('#note').val(lot.note);
                    }
                    if (lot.lot_batch_no != null) {
                        $('#lot-no').val(lot.lot_batch_no);
                    }
                    if (lot.gate_entry_no != null) {
                        $('#gate-no').val(lot.gate_entry_no);
                    }
                    if (lot.gate_entry_date != null) {
                        $('#gate-date').val(lot.gate_entry_date);
                    }
                    if(lot.supplier_filament_type != null){
                      $('#supplier_filament_type').val(lot.supplier_filament_type);
                    }
                    $('#createFilamentModal').modal('show');
                }else{
                    alert(data.msg);
                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
    });

    $('#inv-date,#gate-date,#doa').datepicker({
        dateFormat : 'yy-mm-dd'
    })

    $(document).on('input','#gate-no,#pallet-no' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });

    $(document).on('change','#type',function(){
      var filament = "filament="+$(this).val();
      $.ajax({
              type:'POST',
              url:'/get-filament-code',
              data:filament,
              success:function(data){
                  if (data.status == '1') {
                    $('#material-code').val(data.material_code);
                  }else{
                      alert(data.msg);
                  }
              },
              error:function(xhr){
                  console.log(xhr.status);
              }
          });
    });
});
