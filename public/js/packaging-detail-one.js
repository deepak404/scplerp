$(document).ready(function(){ 
	$.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });


    $('#package-form').on('submit',function(e){
		e.preventDefault();
        var bobbinSelection = [];
		$('.bobbin-selection').each(function(){

		    if($(this).val() == null){
		        $(this).parent().append('<p class="text-danger empty-bobbin" style="margin-top: 5px;">Select Bobbin</p>')
                throw new Error("Spindle Selection Pending");

            }else{
		        bobbinSelection.push($(this).val())
            }
        });

        var duplicates = hasDuplicates(bobbinSelection);

        if(duplicates.length > 0){
            $('#second-div').append('<p class="text-danger duplicate-spindle">Duplicate Spindle Found</p>')
            throw new Error("Duplicate Spindle Found");

        }


		var formData = $(this).serialize();
		$.ajax({
			type:'POST',
			url:'/add-package-details',
			data:formData,
			success:function(data){
				if (data.status) {
					// $('#package-form').trigger("reset");
                    location.reload();
				}
			},
			error:function(xhr){
                if (xhr.status == 422) {
                    // errorHandler(xhr.responseJSON.errors)
                    console.log(xhr.responseJSON.errors);
                }
			}
		})
    });



    $('#doff_no').on('change',function(e){
        e.preventDefault();

        var formData = 'id='+$(this).val();
        $.ajax({
            type:'POST',
            url:'/get-doff-weightlog-details',
            data:formData,
            success:function(data){
                $('.bobbin-selection').empty();
                $.each(data.weightLogDetails, function(k, v){
                    $('.bobbin-selection').append(
                        '<option value="'+v.id+'">Spindle '+v.spindle+'</option>'
                    );
                });

                $('.bobbin-selection').append(
                    '<option value="empty">empty</option>'
                );


            },
            error:function(xhr){

            }
        })
    });


    $('.bobbin-selection').on('change', function(){

        $(this).parent().find('.empty-bobbin').remove();
        $('.duplicate-spindle').remove();
    })


    function hasDuplicates(array) {
        var object = {};
        var result = [];

        array.forEach(function (item) {
            if(!object[item])
                object[item] = 0;
            object[item] += 1;
        })

        for (var prop in object) {
            if(object[prop] >= 2) {
                result.push(prop);
            }
        }

        return result;

    }
});