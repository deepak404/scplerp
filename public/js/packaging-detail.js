$(document).ready(function(){
	$(document).on('input','#bobbin_weight,#box_weight,#moisture,#bobbin_count' ,function() {
		$(this).val($(this).val().replace(/[^0-9.]/gi, ''));
	});

	$(document).on('input','#bobbin_count' ,function() {
		$(this).val($(this).val().replace(/[^0-9]/gi, ''));
	});

	$(document).on('click','#add-details',function(){
		$('#package_id').val('0');
		$('#package-modal').modal('show');
	});

	$.ajaxSetup({
	    headers:
	    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
	});

    $('#date').datepicker({dateFormat : 'dd-mm-yy'});

	$('#packaging-detail-form').on('submit',function(e){
		e.preventDefault();
		var formData = $(this).serialize();
		$.ajax({
			type:'POST',
			url:'/add-packaging-master',
			data:formData,
            success: function(data, status, xhr) {
                if(xhr.status == 200){
                    location.reload();
                }
            },
			error:function(xhr){
                if (xhr.status == 422) {
                    errorHandler(xhr.responseJSON.errors)
                }
			}
		});
	});

    function errorHandler(errors) {
    	$.each(errors, function(key,value){
    		console.log(key);
			$("#"+key).next(".feedback").text(value[0]);
    	});
    }

	$('.text-input').on('keyup',function(){
		$(this).next(".feedback").text('');
	});



    $('.delete-package').on('click', function(e){
    	e.preventDefault();

    	var packingId = $(this).data('id');
		$('#confirmationModal').modal('show');

        $('.delete-pack-btn').on('click', function(){
            if($(this).val() == 'Yes'){
                window.location.href = "/delete-packaging/"+packingId;

            }else{
                $('#confirmationModal').modal('hide');

            }
        });
	});


    $('.edit-package').on('click', function(e){

        e.preventDefault();

        $.ajax({
            type:'GET',
            url:'/get-package-details/'+$(this).data('id'),
            success: function(data, status, xhr) {
                if(xhr.status == 200){
                	var data = data.data;
					var parent = $('#update-packaging-detail-form');
					console.log(data,parent.find('#package-type').val(), data.package_type);
                    parent.find('#package_id').val(data.id);
                    parent.find('#package-type').val(data.package_type);
                    parent.find('#date').val(data.packed_date);
                    parent.find('#box_type').val(data.box_type);
                    parent.find('#bobbin_weight').val(data.bobbin_weight);
                    parent.find('#box_weight').val(data.box_weight);
                    parent.find('#moisture').val(data.moisture_weight);
                    parent.find('#bobbin_count').val(data.bobbin_count);
                    parent.find('#quality').val(data.material_id).change();
                }
            },
            error:function(xhr){
                if (xhr.status == 500) {
                    errorHandler(xhr.responseJSON.errors)
                }
            }
        });

    	$('#update-package-modal').modal('show');

	});


    $('#filter-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'mm-yy',
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });

    $('#filter-date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });

    $(document).on('click','.ui-datepicker-close',function(){
        window.location.href = '/packaging/'+$('#filter-date').val();
    });

});
