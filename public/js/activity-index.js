$(document).ready(function(){

    $('.show-activity-change').on('click', function(e){

        var id = $(this).data('id');
        console.log(id);

        $.ajax({
            type: "GET",
            url: "/get-activity-changes/"+id,
            data : '',
            success: function(data, status, xhr) {
                if(xhr.status == 200){

                    console.log(data)

                    var changedColumns = data.activity.changed_columns;
                    var sortedColumns = data.activity.sorted_columns;
                    var previousData = JSON.parse(data.activity.previous_data);
                    var updatedData = JSON.parse(data.activity.updated_data);


                    $('#changes-table').find('tbody').empty();

                    $.each(changedColumns, function(k, v){
                        if(data.activity.activity_type == 'edit'){

                            $('#changes-table').find('tbody').append('<tr><td>'+$.trim(sortedColumns[k])+'</td><td>'+previousData[v]+'</td><td>'+updatedData[v]+'</td></tr>');

                        }else{

                            $('#changes-table').find('tbody').append('<tr><td>'+$.trim(sortedColumns[k])+'</td><td>'+previousData[v]+'</td><td>-</td></tr>');

                        }
                    });

                    $('#myModal').modal('show');


                }
            },
            error: function(xhr, status, error) {

            },
        })
    });


})