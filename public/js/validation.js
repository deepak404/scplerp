$(document).ready(function(){

	$(document).on('keydown','.text-field',function(e){
		console.log(e.keyCode);
		if ((e.keyCode > 64 && e.keyCode < 91) || e.keyCode == 8 || e.keyCode == 32 || e.keyCode == 9 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
			return true;
		}else{
			return false;
		}
	});

	$(document).on('keydown','.num-field',function(e){
		console.log(e.keyCode);
		if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 95 && e.keyCode < 106)  || e.keyCode == 8 || e.keyCode == 9 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
			return true;
		}else{
			return false;
		}
	});

});