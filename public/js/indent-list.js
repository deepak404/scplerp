$(document).ready(function(){

    $.ajaxSetup({
        headers:
        {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#filter-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'mm-yy',
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });

    $('#filter-date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });
    $(document).on('click','.ui-datepicker-close',function(){
        window.location.href = '/indent-list/'+$('#filter-date').val();
    });
    $(document).on('click','.delete-indent',function(){
    	console.log($(this).data('id'));
    });

    $('.delete-indent').on('click',function(e){
        e.preventDefault();
        var data_id = $(this).data('id');

        $('#confirmationModal').modal('show');

        $('.delete-sale-btn').click(function(e){

            if ($(this).val() == 'Yes'){
                window.location.href = "/delete-indent/"+data_id;
            } else{
                $('#confirmationModal').modal('hide');
            }

        });

    });

});
