$(document).ready(function(){

    var palletId = '';
    var selectedTag = '';
    var lotId = '';
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $('input[type="checkbox"]').on('change', function(){
        if($(this).prop('checked')){
            $(this).parent().parent().find("input[type='text']").attr('disabled',false);
            $(this).parent().parent().find("input[type='hidden']").attr('disabled',false);

            $(this).prev().empty().append('<i class="material-icons checkbox_selected">check_box</i>');
        }else{
            $(this).prev().empty().append('<i class="material-icons">check_box_outline_blank</i>');

            $(this).parent().parent().find("input[type='text']").attr('disabled',true);
            $(this).parent().parent().find("input[type='hidden']").attr('disabled',true);
        }
    });

    $('#tested-pallet-count').on('keyup',function(){
        var count = $(this).val();
        var rowCount = $('.tested-pallet tr').length;
        if(Math.floor(count) == count && $.isNumeric(count)){
            if ($('#tested-pallet-count').data('count') == 0) {
                $('.tested-pallet').empty();
                if (count > 20) {
                  count = 20;
                  $(this).val('20');
                }
                for (var i = 0; i < count; i++) {
                    $('.tested-pallet').append('<tr>'+
                                '<td></td>'+
                                '<td><input type="text" name="ps-no[]"></td>'+
                                '<td><input type="text" name="code[]"></td>'+
                                '<td><input type="text" name="pallet-no[]"></td>'+
                                '<td><input type="text" name="weight[]"></td>'+
                                '<td><input type="text" name="denier[]"><i class="material-icons delete">delete</i><i class="material-icons add_info"data-id="'+i+'">playlist_add</i></td>'+
                                '<input type="hidden" name="result[]" id="result-'+i+'">'+
                            '</tr>');
                }
            }else{
                for (var i = 1; i <= count; i++) {
                  rowCount = rowCount+1;
                  if (rowCount <= 20) {
                    $('.tested-pallet').append('<tr>'+
                    '<td></td>'+
                    '<td><input type="text" name="ps-no[]"></td>'+
                    '<td><input type="text" name="code[]"></td>'+
                    '<td><input type="text" name="pallet-no[]"></td>'+
                    '<td><input type="text" name="weight[]"></td>'+
                    '<td><input type="text" name="denier[]"><i class="material-icons delete">delete</i><i class="material-icons add_info"data-id="'+i+'">playlist_add</i></td>'+
                    '<input type="hidden" name="result[]" id="result-'+rowCount+'">'+
                    '</tr>');
                  }
                }
            }
        }
    });

    // $(document).on('click','.delete',function(){
    //     $(this).parent().parent().remove();
    // });

    // $(document).on('click','.add_info',function(){
    //     var id = $(this).data('id');
    //     var val = $(this).data('val');
    //     $("input[name='actual[]']").val("");
    //     $("input[name='parameter[]']").prev().empty().append('<i class="material-icons">check_box_outline_blank</i>');
    //     $("input[name='parameter[]']").prop('checked', false);
    //     $("input[name='actual[]']").attr('disabled','true');
    //     $("input[name='scpl[]']").attr('disabled','true');
    //     $('#form-btn').data('id',id);
    //     if (typeof val === "undefined") {
    //         // skip
    //     }else{
    //         $.each(val.data_id,function(key,value){
    //            $('#ft-'+value).prev().empty().append('<i class="material-icons checkbox_selected">check_box</i>');
    //            $('#ft-'+value).prop('checked', true);
    //            $('#ft-'+value).parent().next().next().children().attr('disabled',false);
    //            $('#ft-'+value).parent().next().next().next().children().attr('disabled',false);
    //            $('#ft-'+value).parent().next().next().next().next().attr('disabled',false);
    //            $('#ft-'+value).parent().next().next().children().val(val.actual[key]);
    //         });
    //     }
    //     $('#parameterModal').modal('show');
    // });

    $('#parameter-form').on('submit',function(e){
        e.preventDefault();

        // var parameter = $("input[name='parameter[]']:checked").map(function(){return $(this).val();}).get();
        // var actual = $("input[name='actual[]']:enabled").map(function(){return $(this).val();}).get();
        // var scpl = $("input[name='scpl[]']:enabled").map(function(){return $(this).val();}).get();
        // var data_id = $("input[name='id[]']:enabled").map(function(){return $(this).val();}).get();
        // var values = {'data_id':data_id, 'parameter':parameter,'actual':actual,'scpl':scpl};
        // console.log($(this).serialize());
        // $('#result-'+$('#form-btn').data('id')).val(JSON.stringify(values));
        // $('#parameterModal').modal('hide');
        var formData = $(this).serialize();
        console.log(formData);
        $.ajax({
            type:'POST',
            url:'/filament-pallet-result',
            data:formData,
            success:function(data){
                if (data.code == '1') {
                  clearForm($('#parameter-form'));
                  $('.status-note').removeClass('error').addClass('success');
                  $('.status-note').text(data.msg);
                  $('.status-note').css("display", "inline-block");
                  setTimeout(closeNote, 5000);
                }else if (data.code == '2') {
                  $('.status-note').removeClass('success').addClass('error');
                  $('.status-note').text(data.msg);
                  $('.status-note').css("display", "inline-block");
                }else if (data.code == '3') {
                  $('.status-note').removeClass('error').addClass('success');
                  $('.status-note').text(data.msg);
                  $('.status-note').css("display", "inline-block");
                  location.reload();
                }else{
                    alert(data);
                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
    });

    $('#quality-update-form').on('submit',function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        console.log(formData);
        $.ajax({
            type:'POST',
            url:'/filament-quality-status',
            data:formData,
            success:function(data){
                if (data.status == '1') {
                    // window.location.href = "/filament-clearance-qc";
                    location.reload();
                }else{
                    alert(data.msg);
                }
            },
            error:function(xhr){
                console.log(xhr.status);
            }
        });
    });

    // $('#report-update').on('submit',function(e){
    //     e.preventDefault();
    //     var formData = $(this).serialize();
    //     $.ajax({
    //         type:'POST',
    //         url:'/filament-report-update',
    //         data:formData,
    //         success:function(data){
    //             if (data.status == '1') {
    //                 window.location.href = "/filament-clearance-qc";
    //             }else{
    //                 alert(data.msg);
    //             }
    //         },
    //         error:function(xhr){
    //             alert(xhr.status);
    //         }
    //     });
    // });


    $('#tested-date, #tested-arrival-date').datepicker({
        dateFormat : 'dd-mm-yy',
    });

    $('#add-result').on('click',function(){
      $('#parameterModal').modal('show');
    });

    function closeNote() {
      $('.status-note').css("display", "none");
    }
    function clearForm($form)
    {
        $form.find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $form.find(':checkbox, :radio').prop('checked', false);
        $form.find(':checkbox').prev().empty().append('<i class="material-icons">check_box_outline_blank</i>');
        $form.find(':checkbox').parent().parent().find("input[type='text']").attr('disabled',true);
        $form.find(':checkbox').parent().parent().find("input[type='hidden']").attr('disabled',true);
    }

    $('#done-btn').on('click',function(){
      console.log($(this).data('id'));
    });

    $('.delete-pallet').on('click',function(){
      palletId = $(this).data('id');
      $('.confirmation-info').text('Are you surely want to delete this Tested Result ? ');
      $('#note-p').text('NOTE : The tested pallet will be removed from the Filament Lot.');
      $('input[value="Yes"]').val('Delete');
      $('#confirmationModal').modal('show');
    });

    $('#done-btn').on('click',function(){
      lotId = $(this).data('id');
      $('.confirmation-info').text('Are you surely want to Complete this Tested Result ? ');
      $('#note-p').text("NOTE : Once you complete the test results you can't update in future");
      $('input[value="Delete"]').val('Yes');
      $('#confirmationModal').modal('show');
    });

    $('.confirm-btn').on('click',function(){
      if ($(this).val() == 'Delete') {
        deletePallet(palletId)
      }else if ($(this).val() == 'Yes') {
        window.location.href = "/filament-clearance-update/"+lotId;
      }else{
        $('#confirmationModal').modal('hide');
      }
    });

    function deletePallet(id) {
      $.ajax({
          type:'GET',
          url:'/filament-pallet-delete/'+id,
          success:function(data){
              if (data.code == '1') {
                location.reload();
              }else{
                  alert(data.msg);
              }
          },
          error:function(xhr){
              alert(xhr.status);
          }
      });
    }

});
