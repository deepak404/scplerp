<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceMaster extends Model
{
    protected $fillable = [

        'invoice_no',
        'indent_id',
        'date',
        'bp_id',
        'consignee_id',
        'handling_charges',
    ];

    public function packageMaster()
    {
        return $this->hasMany(PackageMaster::class, 'invoice_master_id');
    }


    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'bp_id');
    }

    public function indent()
    {
        return $this->belongsTo(IndentMaster::class, 'indent_id');
    }

    public function Weightlogs()
    {
        return $this->hasManyThrough(WeightLog::class, PackageMaster::class);
    }

    public function indentDetail()
    {
        return $this->hasManeyThrough(IndentDetail::class, IndentMaster::class);
    }
}
