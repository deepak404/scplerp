<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChemicalClearance extends Model
{
    protected $fillable = [
//        "supplier",
//        "received_on",
//        "received_from",
//        "chemical",
//        "chemical_code",
//        "lot_no",
//        "manufactured_date",
//        "total_weight",
//        "expiry_date",
//        "no_of_packs",
//        "no_of_samples",
//        "grn_remarks",
//        "status"

        "received_on",
        "supplier_type",
        "chemical",
        "chemical_code",
        "gate_in_no",
        "gate_date",
        "lot_no",
        "manufactured_date",
        "total_weight",
        "expiry_date",
        "no_of_packs",
        "grn_remarks",
        "status"
    ];


    public function chemicalClearanceResult(){

        return $this->hasOne(ChemicalClearanceResult::class, 'clearance_id');
    }


    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }


}
