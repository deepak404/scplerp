<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DipCordSpecification extends Model
{
  protected $fillable = [
    'material_id',
    'material',
    'floor_code',
    'fhs',
    'adhesion',
    'grade',
  ];

  public function itemMaster()
  {
      return $this->belongsTo(ItemMaster::class);
  }

}
