<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewindWeightLog extends Model
{
    protected $fillable = [
        'doff_no',
        'doff_date',
        'op_name',
        'material_id',
        'material',
        'new_material',
        'tare_weight',
        'floor_code',
        'material_weight',
        'total_weight',
        'weight_status',
        'ncr_status',
        'reason',
        'wl_time',
        'spindle',
        'wl_id',
    ];

}
