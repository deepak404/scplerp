<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionPlanning extends Model
{

    protected $fillable = [
        'order_id',
        'feasibility_id',
        'start_date',
        'end_date',
        'batch',
        'material',
        'order_kg',
        'filament_type',
        'delete_status',
    ];

    public function productionFeasibility(){

        return $this->belongsTo(ProductionFeasibility::class , 'feasibility_id');
    }

    public function saleOrder(){
        return $this->belongsTo(SaleOrder::class, 'order_id');
    }

    public function itemMaster()
    {
        return $this->belongsTo(ItemMaster::class, 'material');
    }


    public static function getEntry($id){

        return self::where('id', $id)->first()->toArray();

    }
}
