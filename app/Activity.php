<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'user_id',
        'dept_name',
        'activity_type',
        'previous_data',
        'updated_data',
        'log_details',
        'changed_columns',
        'log_time',
        'reason'
    ];
}
