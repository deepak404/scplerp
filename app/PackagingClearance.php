<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackagingClearance extends Model
{

    protected $table = 'packaging_material_clearances';

    protected $fillable = [
        'item',
        'item_code',
        'supplier_id',
        'supplier_type',
        'packing_lot_no',
        'gate_in_no',
        'gate_date',
        'invoice_quantity',
        'quantity_received',
        'received_on',
        'status'
    ];


    public function packageMaterialMaster(){
        return $this->belongsTo(PackagingMaterial::class,'item');
    }


    public function packingClearanceResult(){

        return $this->hasOne(PackingClearanceResult::class,'clearance_id');
    }


    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
}
