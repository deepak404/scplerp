<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilamentClearance extends Model
{
    protected $fillable = [
      'supplier_id',
      'gate_entry_no',
      'gate_entry_date',
    	'supplier_type',
      'type',
      'denier',
      'net_wt',
      'tube_color',
      'tube_id',
      'inv_no',
      'inv_date',
    	'arrival_date',
      'pallet_count',
      'material_code',
      'note',
      'producte_code',
      'supplier',
      'description',
      'sample_date',
      'test_date',
      'lot_batch_no',
      'clearance_status',
      'report_result',
      'tested_by',
      'edit_delete',
      'supplier_filament_type',
    ];

    public function palletEntry()
    {
    	return $this->hasOne(PalletEntry::class,'fl_id');
    }


    public function testedPallet()
    {
        return $this->hasMany(TestedPallet::class, 'fl_id');
    }
}
