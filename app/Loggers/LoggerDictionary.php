<?php


namespace App\Loggers;


class LoggerDictionary
{

    private $dictionary = [
        'SALES' => [
            'id' => 'ID',
            'bp_id' => 'Business Partner ID',
            'material_id' => 'Material ID',
            'material_name' => 'Material Name',
            'order_quantity' => 'Order Quantity',
            'order_date' => 'Order Date',
            'total_order' => 'Total Order',
            'customer_date' => 'Customer Date',
            'last_month_pending' => 'Last Month Pending',
            'committed_date' => 'Committed Date',
            'scpl_confirmation_date' => 'SCPL Confirmation Date',
            'dispatch_date' => 'Dispatch Date',
            'dispatch_quantity' => 'Dispatch Quantity',
            'pending_quantity' => 'Pending Quantity',
            'indent_status' => 'Indent Status',
            'edit_state' => 'Edit State',
            'delete_state' => 'Delete State',
            'pre_close' => 'Pre-Close Quantity',
            'pre_close_reason' => 'Pre-Close Reason',
            'created_at' => 'Created At',
        ],
        'PRODUCTION PLAN' => [
            'order_id' => 'Order Id',
            'material' => 'Material',
            'machine' => 'Machine',
            'existing_filament' => 'Existing Filament',
            'changed_filament' => 'Filament Updated',
            'consumption' => 'Consumption',
            'no_of_spls' => 'No of Spindles',
            'cord_weight' => 'Cord Weight',
            'speed_in_mpm' => 'Speed in MPM',
            'production_per_hr' => 'Production Per Hour',
            'running_hrs' => 'Running Hours',
            'no_of_changes' => 'No of Changes',
            'changes_per_hr' => 'Changes Per hour',
            'update_state' => 'Update State',
            'in_stock' => 'Instock',
            'created_at' => 'Created At',
        ],
    ];

    public function getColumnNameInProperForm(String $dept, Array $columns, String $changeType){

        $response = $this->buildLogDetails($dept, $columns, $changeType);

        return $response;

    }


    /**
     * @param String $dept
     * @param array $columns
     * @return bool|string
     */


    private function buildLogDetails(String $dept, Array $columns, String $response){

//        $response = 'Changed ';

        $columns = array_keys($columns);

        if(count($columns) > 1){

            $end = $columns[count($columns) - 2];


            foreach ($columns as $column){


                if($column == $end){

                    $response .= $this->dictionary[$dept][$column].' and ';


                }else{

                    $response .= $this->dictionary[$dept][$column].', ';

                }

            }

            $response = substr($response, 0, -2);


        }else{

            $response .= $this->dictionary[$dept][$columns[0]];

        }



        return $response;

    }

}