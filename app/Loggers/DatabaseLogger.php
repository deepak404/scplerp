<?php
/**
 * Created by PhpStorm.
 * User: WELCOME
 * Date: 8/2/2019
 * Time: 3:12 PM
 */

namespace App\Loggers;

use App\Activity;
use App\Contracts\Logger;
use DateTime;
use App\Loggers\LoggerDictionary;

class DatabaseLogger implements Logger
{

    protected $beforeUpdate;
    protected $afterUpdate;
    protected $activityType;
    protected $time;
    protected $reason;
    protected $dept;




    public function __construct(array $beforeUpdate, array $afterUpdate, string $activityType, String $time, String $reason, String $dept)
    {
        $this->beforeUpdate = $beforeUpdate;
        $this->afterUpdate = $afterUpdate;
        $this->activityType = $activityType;
        $this->time = $time;
        $this->reason = $reason;
        $this->dept = $dept;
    }

    public function saveDeleteEntry()
    {
        $changedColumns = array_diff($this->beforeUpdate, $this->afterUpdate);



        unset($changedColumns['updated_at']);

        if(count($changedColumns) == 0){
            return true;
        }

        $logSentence = (new LoggerDictionary())->getColumnNameInProperForm($this->dept, $changedColumns, 'Deleted ');

//        dd($logSentence);
//        dd(json_encode($this->beforeUpdate), 'hey');

        Activity::create([
            'user_id' => auth()->user()->id,
            'dept_name' => $this->dept,
            'activity_type' => $this->activityType,
            'previous_data' => json_encode($this->beforeUpdate),
            'updated_data' => null,
            'changed_columns' => implode(',',array_keys($changedColumns)),
            'log_time' => date('Y-m-d H:i:s'),
            'log_details' => $logSentence,
            'reason' => $this->reason
        ]);


        return true;
    }


    public function saveEditEntry()
    {

        $changedColumns = array_diff($this->beforeUpdate, $this->afterUpdate);
        unset($changedColumns['updated_at']);

        if(count($changedColumns) == 0){
            return true;
        }

        $logSentence = (new LoggerDictionary())->getColumnNameInProperForm($this->dept, $changedColumns, 'Changed ');

//        dd(json_encode($this->beforeUpdate), 'hey');

        Activity::create([
            'user_id' => auth()->user()->id,
            'dept_name' => $this->dept,
            'activity_type' => $this->activityType,
            'previous_data' => json_encode($this->beforeUpdate),
            'updated_data' => json_encode($this->afterUpdate),
            'changed_columns' => implode(',',array_keys($changedColumns)),
            'log_time' => date('Y-m-d H:i:s'),
            'log_details' => $logSentence,
            'reason' => $this->reason
        ]);


        return true;

    }


//    private function

}