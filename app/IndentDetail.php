<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndentDetail extends Model
{
    protected $fillable = [
        'indent_id', 'order_id', 'hsn_sac','gst_rate','due_on','quantity','rate_per_kg','material_id',
    ];

    public function saleOrder(){
        return $this->belongsTo(SaleOrder::class, 'order_id');
    }

    public function indentMaster(){
        return $this->belongsTo(IndentMaster::class, 'indent_id');
    }

    public function itemMaster(){
    	return $this->belongsTo(ItemMaster::class, 'material_id');
    }

}
