<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DipCordSample extends Model
{
    protected $fillable = [
      'material_id',
      'sample_date',
      'process',
      'doff_no',
      'spindles',
      'result',
      'status',
      'cleared_by'
    ];

    public function dipCordClearance()
    {
      $this->belongsTo(DipCordClearance::class);
    }
}
