<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    public function productionFeasibility(){
        return $this->hasMany(ProductionFeasibility::class, 'machine');
    }
}
