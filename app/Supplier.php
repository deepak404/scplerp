<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ['vendor_code'];

    public function chemicalClearance(){

        return $this->hasMany(ChemicalClearance::class, 'supplier_id');
    }

    public function packingMaterialClearance(){
        return $this->hasMany(PackagingClearance::class,'supplier_id');
    }
}
