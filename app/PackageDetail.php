<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    protected $fillable = [
        'master_id','case_no','doff_no', 'weight','invoice_status'
    ];

    public function packagingMaster()
    {
    	return $this->belongsTo(PackageMaster::class, 'master_id');
    }

	public function packagingBobbin()
    {
    	return $this->hasMany(PackedBobbin::class,'detail_id');
    }

}
