<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPartner extends Model
{
    protected $fillable = [
        'mode_of_transport','bp_name', 'address', 'pincode','email','phone_number','district','state','ar_no','code','contact_person','contact_no','dispatcher','dispatch_destination','gst_code', 'transit_insurance'
    ];

    public function saleOrder()
    {
    	return $this->hasMany(SaleOrder::class, 'bp_id');
    }

    public function bankAccount()
    {
    	return $this->hasMany(BankAccount::class,'bp_id');
    }

    public function indentMaster()
    {
        return $this->hasMany(IndentMaster::class, 'bp_id');
    }

    public function invoice(){
        return $this->hasMany(InvoiceMaster::class, 'bp_id');
    }


    public function bpMaterials(){
        return $this->hasMany(BpMaterial::class, 'bp_id');
    }

}
