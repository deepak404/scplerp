<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BusinessPartner;
use App\SaleOrder;
use App\ItemMaster;

class PendingOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pendingOrder:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pending Orders create as sale orders and it only shown in Production Planing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        $saleOrders = SaleOrder::where('customer_date', '<', $date)
                                ->where('pending_quantity', '>', 0)
                                ->where('order_quantity','>',0)
                                ->where('bp_id','!=',114)
                                ->get();
        $oldOrders = [];

        foreach ($saleOrders as $value) {
            if (empty($oldOrders[$value->bp_id][$value->material_id]['material_name'])) {
              $oldOrders[$value->bp_id][$value->material_id]['material_name'] =  $value->material_name;
              $oldOrders[$value->bp_id][$value->material_id]['value'] =  $value->pending_quantity;
            }else{
              $oldOrders[$value->bp_id][$value->material_id]['value'] += $value->pending_quantity;
            }
        }

        foreach ($oldOrders as $bpId => $materials) {
          $businessPartner = BusinessPartner::find($bpId);
          foreach ($materials as $materialId => $value) {
            $existingFilament = null;

            $existing = $businessPartner->saleOrder()
            ->where('material_id', $materialId)
            ->orderBy('order_date')
            ->with('productionFeasibility')
            ->get()
            ->last();

            if (!is_null($existing)) {
                $existingFilament = $existing->productionFeasibility->changed_filament;
            }

            $createOrder = $businessPartner->saleOrder()->create([

                'material_id'=>$materialId,
                'material_name' => $value['material_name'],
                'order_quantity'=>0,
                'order_date'=>$date,
                'customer_date'=>$date,
                'last_month_pending'=>$value['value'],
                'total_order'=>$value['value'],
                'pending_quantity'=>$value['value']

            ]);

            $itemMaster = ItemMaster::where('id', $materialId)->first();

            $createOrder->productionFeasibility()->create([
                'material' => $materialId,
                'cord_weight'=>$itemMaster->cord_wt,
                'existing_filament'=>$existingFilament,
            ]);

          }

        }
    }
}
