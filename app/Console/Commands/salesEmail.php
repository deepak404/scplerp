<?php

namespace App\Console\Commands;

use App\Mail\SaleOrders;
use App\SaleOrder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class salesEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saleOrders:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all the new Sale Orders to Production.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $senderTo = [['name' => 'Production', 'email'=>'ganeshscpl@gmail.com']];
        $saleOrders = SaleOrder::whereBetween('created_at',[Carbon::yesterday(), Carbon::today()])
            ->where('edit_state',0)
            ->get();

        if($saleOrders->count() != 0){
            foreach ($senderTo as $send){
                $sendMail = Mail::to($send['email'])->send(new SaleOrders($saleOrders, $send));
            }

        }





//        $to_name = 'Naveen PWM';
//        $to_email = 'naveen@pwm-india.com';
//        $data = array('name'=>"Naveen Kumar", "body" => "Test mail");
//
//
//        Mail::send('email.sale-noti', $data, function($message) use ($to_name, $to_email) {
//            $message->to($to_email, $to_name)
//                ->subject('New Sale Order');
//            $message->from('scplmdu@gmail.com','Artisans Web');
//        });

    }
}
