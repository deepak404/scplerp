<?php
/**
 * Created by PhpStorm.
 * User: rightfunds
 * Date: 17/09/18
 * Time: 4:41 PM
 */

namespace App;


class CurrencyToWords
{

    /**
     * Function to convert a number to a the string literal for the number
     */
    public function convert($num) {


        $count = 0;
        global $ones, $tens, $triplets;
        $ones = array(
            '',
            ' One',
            ' Two',
            ' Three',
            ' Four',
            ' Five',
            ' Six',
            ' Seven',
            ' Eight',
            ' Nine',
            ' Ten',
            ' Eleven',
            ' Twelve',
            ' Thirteen',
            ' Fourteen',
            ' Fifteen',
            ' Sixteen',
            ' Seventeen',
            ' Eighteen',
            ' Nineteen'
        );
        $tens = array(
            '',
            '',
            ' Twenty',
            ' Thirty',
            ' Forty',
            ' Fifty',
            ' Sixty',
            ' Seventy',
            ' Eighty',
            ' Ninety'
        );

        $triplets = array(
            '',
            ' Thousand',
            ' Million',
            ' Billion',
            ' Trillion',
            ' Quadrillion',
            ' Quintillion',
            ' Sextillion',
            ' Septillion',
            ' Octillion',
            ' Nonillion'
        );

//        dd($this->convertNum($num));


        return $this->convertNum($num);


    }



    public function convertIndianCurrency($number){
        //A function to convert numbers into Indian readable words with Cores, Lakhs and Thousands.
        $words = array(
            '0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five',
            '6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten',
            '11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen',
            '16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty',
            '30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy',
            '80' => 'eighty','90' => 'ninty');

        //First find the length of the number
        $number_length = strlen($number);

        //Initialize an empty array
        $number_array = array(0,0,0,0,0,0,0,0,0);
        $received_number_array = array();

        //Store all received numbers into an array
        for($i=0;$i<$number_length;$i++){
            $received_number_array[$i] = substr($number,$i,1);
        }
        //Populate the empty array with the numbers received - most critical operation
        for($i=9-$number_length,$j=0;$i<9;$i++,$j++){
            $number_array[$i] = $received_number_array[$j];
        }
        $number_to_words_string = "";
        //Finding out whether it is teen ? and then multiply by 10, example 17 is seventeen, so if 1 is preceeded with 7 multiply 1 by 10 and add 7 to it.
        for($i=0,$j=1;$i<9;$i++,$j++){
            //"01,23,45,6,78"
            //"00,10,06,7,42"
            //"00,01,90,0,00"
            if($i==0 || $i==2 || $i==4 || $i==7){
                if($number_array[$j]==0 || $number_array[$i] == "1"){
                    $number_array[$j] = intval($number_array[$i])*10+$number_array[$j];
                    $number_array[$i] = 0;
                }

            }
        }
        $value = "";
        for($i=0;$i<9;$i++){
            if($i==0 || $i==2 || $i==4 || $i==7){
                $value = $number_array[$i]*10;
            }
            else{
                $value = $number_array[$i];
            }
            if($value!=0)         {    $number_to_words_string.= $words["$value"]." "; }
            if($i==1 && $value!=0){    $number_to_words_string.= "Crores "; }
            if($i==3 && $value!=0){    $number_to_words_string.= "Lakhs ";    }
            if($i==5 && $value!=0){    $number_to_words_string.= "Thousand "; }
            if($i==6 && $value!=0){    $number_to_words_string.= "Hundred & "; }
        }
        if($number_length>9){ $number_to_words_string = "Sorry This does not support more than 99 Crores"; }
        return ucwords(strtolower($number_to_words_string));


    }

    /**
     * Function to dislay tens and ones
     */
    public function commonloop($val, $str1 = '', $str2 = '') {
        global $ones, $tens;
        $string = '';
        if ($val == 0)
            $string .= $ones[$val];
        else if ($val < 20)
            $string .= $str1.$ones[$val] . $str2;
        else
            $string .= $str1 . $tens[(int) ($val / 10)] . $ones[$val % 10] . $str2;
        return $string;
    }

    /**
     * returns the number as an anglicized string
     */
    public function convertNum($num) {
        $num = (int) $num;    // make sure it's an integer

        if ($num < 0)
            return 'negative' . $this->convertTri(-$num, 0);

        if ($num == 0)
            return 'Zero';
        return $this->convertTri($num, 0);
    }

    /**
     * recursive fn, converts numbers to words
     */
    public function convertTri($num, $tri) {
        global $ones, $tens, $triplets, $count;
        $test = $num;
        $count++;
        // chunk the number, ...rxyy
        // init the output string
        $str = '';
        // to display hundred & digits
        if ($count == 1) {
            $r = (int) ($num / 1000);
            $x = ($num / 100) % 10;
            $y = $num % 100;
            // do hundreds
            if ($x > 0) {
                $str = $ones[$x] . ' Hundred';
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', '');
            }
            else if ($r > 0) {
                // do ones and tens
                $str .= $this->commonloop($y, ' and ', '');
            }
            else {
                // do ones and tens
                $str .= $this->commonloop($y);
            }
        }
        // To display lakh and thousands
        else if($count == 2) {
            $r = (int) ($num / 10000);
            $x = ($num / 100) % 100;
            $y = $num % 100;
            $str .= $this->commonloop($x, '', ' Lakh ');
            $str .= $this->commonloop($y);
            if ($str != '')
                $str .= $triplets[$tri];
        }
        // to display till hundred crore
        else if($count == 3) {
            $r = (int) ($num / 1000);
            $x = ($num / 100) % 10;
            $y = $num % 100;
            // do hundreds
            if ($x > 0) {
                $str = $ones[$x] . ' Hundred';
                // do ones and tens
                $str .= $this->commonloop($y,' and ',' Crore ');
            }
            else if ($r > 0) {
                // do ones and tens
                $str .= $this->commonloop($y,' and ',' Crore ');
            }
            else {
                // do ones and tens
                $str .= $this->commonloop($y);
            }
        }
        else {
            $r = (int) ($num / 1000);
        }
        // add triplet modifier only if there
        // is some output to be modified...
        // continue recursing?
        if ($r > 0)
            return $this->convertTri($r, $tri+1) . $str;
        else
            return $str;
    }
}