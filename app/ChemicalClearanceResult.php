<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChemicalClearanceResult extends Model
{
    protected $fillable = [
        'tested_by',
        'tested_date',
        'test_arrival_date',
        'result',
        'sample',
    ];


    public function clearance(){
        return $this->belongsTo(ChemicalClearance::class, 'clearance_id');
    }
}
