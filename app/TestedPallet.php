<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestedPallet extends Model
{
    protected $fillable = ['result','code','ps_no','pallet_no','lea_weight','actual',];

    public function filamentClearance()
    {
    	return $this->belongTo(FilamentClearance::class);
    }
}
