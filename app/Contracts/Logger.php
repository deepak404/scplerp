<?php

namespace App\Contracts;

interface Logger{

    public function saveEditEntry();

    public function saveDeleteEntry();

}