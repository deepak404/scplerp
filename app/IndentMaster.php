<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndentMaster extends Model
{

    protected $fillable = [
        'bp_id', 'quotation_number', 'transit_insurance','handling_charges','dated_on','order_ref_no','other_ref_no','dispatcher','destination','terms_of_delivery','bank_id','edit_state','delete_status',
        'delivery_type', 'payment_mode', 'indent_type', 'mode_of_transport'
    ];

    public function indentDetails(){
        return $this->hasMany(IndentDetail::class, 'indent_id');
    }

    public function bankAccount(){
        return $this->hasOne(BankAccount::class, 'bank_id');
    }

    public function businessPartner()
    {
    	return $this->belongsTo(BusinessPartner::class, 'bp_id');
    }

    public function invoice(){
        return $this->hasMany(InvoiceMaster::class, 'indent_id');
    }

}
