<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DipSolutionClearance extends Model
{
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'prepared_by',
        'collected_by',
        'expiry_date',
        'batch_no',
        'status'
    ];


    public function dipSolutionClearanceResult(){

        return $this->hasOne(DipSolutionClearanceResult::class, 'clearance_id');
    }
}
