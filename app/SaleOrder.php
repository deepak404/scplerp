<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    protected $fillable = [
        'bp_id', 'material_id','material_name' ,'order_quantity','order_date','customer_date','last_month_pending','committed_date','scpl_confirmation_date','dispatch_date','dispatch_quantity','pending_quantity','edit_state','delete_state','total_order','indent_status',
    ];


    public function productionFeasibility(){
        return $this->hasOne(ProductionFeasibility::class, 'order_id');
    }


    public function productionPlanning(){
        return $this->hasMany(ProductionPlanning::class,'order_id');
    }


    public function indentDetails(){
        return $this->hasOne(IndentDetail::class,'order_id');
    }

    public function businessPartner()
    {
        return $this->belongsTo(BusinessPartner::class, 'bp_id');
    }


    public function itemMaster(){
        return $this->belongsTo(ItemMaster::class, 'material_id');
    }



    public static function getEntry($id){

        return self::where('id', $id)->first()->toArray();

    }



}
