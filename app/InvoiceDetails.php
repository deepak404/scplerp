<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{

    protected $fillable = ['package_master_id'];

    public function invoiceMaster(){
        return $this->belongsTo(InvoiceMaster::class, 'master_id');
    }


    public function packageMaster(){
        return $this->hasMany(PackageMaster::class);
    }
}
