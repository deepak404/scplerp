<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DipCordClearance extends Model
{
  protected $fillable = [
    'material_id',
    'customer',
    'filament_type',
    'lot_no',
    'tpm',
    'machine',
    'temp_c',
    'stretch_tension',
    'damper_opening',
    'dewebber_suction',
    'speed_mpm',
    'solution',
    'batch_no',
    'scrapper_squeeze',
    'no_cords',
    'nip_roll_pressure',
    'no_of_pass',
    'fan_usage',
    'created_by',
    'reason'
  ];

  public function itemMaster()
  {
      return $this->belongsTo(ItemMaster::class);
  }

  public function dipCordSample()
  {
      return $this->hasMany(DipCordSample::class, 'master_id');
  }
}
