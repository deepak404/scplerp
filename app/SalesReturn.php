<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReturn extends Model
{
    protected $fillable = [
        "intimation_by",
        "customer",
        "received_on",
        "docs_received",
        "grn",
        "shortage",
        "reason",
        "scpl_invoice",
        "condition",
        "frieght_details",
        "sales_folio",
        "attachment_details",
        "credit_note_details"
    ];


    public function salesReturnDetails(){
        return $this->hasMany(SalesReturnDetails::class);
    }
}
