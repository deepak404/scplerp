<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMaster extends Model
{
    protected $fillable = [
        'cord_wt',
        'descriptive_name',
        'material',
        'zell_a_spls_no',
        'zell_a_speed',
        'zell_b_spls_no',
        'zell_b_speed',
        'kidde_spls_no',
        'kidde_speed',
        'hsn_sac',
        'sgst',
        'cgst',
        'igst',
        'package',
    ];

    public function saleOrder()
    {
        return $this->hasMany(SaleOrder::class, 'material_id');
    }

    public function weightLogMaster()
    {
        return $this->hasMany(WeightLogMaster::class, 'material_id');
    }

    public function packageMaster()
    {
        return $this->hasMany(PackageMaster::class, 'material_id');
    }

    public function indentDetail()
    {
    	return $this->hasMany(IndentMaster::class, 'material_id');
    }

    public function productionPlanning()
    {
        return $this->hasMany(ProductionPlanning::class);
    }

    public function dipCordSpecification()
    {
        return $this->hasOne(DipCordSpecification::class, 'material_id');
    }

    public function dipCordClearance()
    {
        return $this->hasMany(DipCordClearance::class, 'material_id');
    }
}
