<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionFeasibility extends Model
{

    protected $fillable = ['material','no_of_spls','existing_filament','speed_in_mpm','production_per_hr','running_hrs','cord_weight','update_state','in_stock'];


    /*
     * The below function can be used to override the model updating event.
     *
     * NOTE : When issuing a mass update via Eloquent, the saved and updated model events will not be fired for the updated models.
     * This is because the models are never actually retrieved when issuing a mass update.
     *
     *
     * */


//    protected static function boot(){
//
//        static::updating(function ($model) {
//
//            $model->existing_filament = $model->existing_filament ?? null;
//            $model->changed_filament = $model->changed_filament ?? null;
//            $model->cord_weight = $model->cord_weight ?? null;
//            $model->speed_in_mpm = $model->speed_in_mpm ?? null;
//            $model->production_per_hr = $model->production_per_hr ?? null;
//            $model->running_hrs = $model->running_hrs ?? null;
//            $model->no_of_changes = $model->no_of_changes ?? null;
//            $model->changes_per_hr = $model->changes_per_hr ?? null;
//
//
//        });
//    }

    public function saleOrder(){
        return $this->belongsTo(SaleOrder::class, 'order_id');
    }

    public function productionPlanning(){
        return $this->hasMany(ProductionPlanning::class, 'feasibility_id');
    }


    public function machine(){
        return $this->belongsTo(Machine::class, 'machine');
    }



    public static function getEntry($id){

        return self::where('id', $id)->first()->toArray();

    }
}
