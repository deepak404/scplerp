<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageMaster extends Model
{
    protected $fillable = [
       'material_type','package_type','packed_date','case_no','reason','material_id','box_type','packing_box','box_weight','moisture_weight','bobbin_count','dispatch_date', 'expiry_date', 'inset_weight'
    ];

    public function itemMaster()
    {
        return $this->belongsTo(ItemMaster::class, 'material_id');
    }

    public function weightLogs()
    {
    	return $this->hasMany(WeightLog::class,'package_master_id');
    }

    public function invoiceMaster(){
        return $this->belongsTo(InvoiceMaster::class, 'invoice_master_id');
    }


    public function indentDetails(){
        return $this->belongsTo(IndentDetail::class, 'package_master_id');
    }
}
