<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingClearanceResult extends Model
{

    protected $fillable = [
        'tested_by',
        'tested_date',
        'test_arrival_date',
        'result',
        'sample',
    ];


    public function packingClearance(){

        return $this->belongsTo(PackagingClearance::class, 'clearance_id');

    }
}
