<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BpMaterial extends Model
{
  protected $fillable = [
      'bp_id', 'descriptive_name', 'material',
  ];


  public function businessPartner(){
      return $this->belongsTo(BusinessPartner::class);
  }
}
