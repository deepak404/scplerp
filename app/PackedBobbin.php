<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackedBobbin extends Model
{
    protected $fillable = [
        'detail_id','bobbin','b_no','b_wt',
    ];

    public function packagingDetail()
    {
    	return $this->belongsTo(PackageDetail::class, 'detail_id');
    }
}
