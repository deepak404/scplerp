<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PalletEntry extends Model
{
    protected $fillable = [
        'fl_id', 'pallets',
    ];

    public function filamentClearance()
    {
    	return $this->belongTo(FilamentClearance::class);
    }
}
