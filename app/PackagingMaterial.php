<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackagingMaterial extends Model
{


    public function packageClearance(){
        return $this->hasMany(PackagingClearance::class);
    }
}
