<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DipSolutionClearanceResult extends Model
{

    protected $fillable = [
        'tested_by',
        'tested_date',
        'test_arrival_date',
        'result',
        'sample',
    ];

    public function dipSolutionClearance(){

        return $this->belongsTo(DipSolutionClearance::class,  'clearance_id');
    }
}
