<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeightLog extends Model
{
        protected $fillable = [

            'unique_id',
            'material_id',
            'material',
            'machine',
            'wl_time',
            'op_name',
            'doff_no',
            'spindle',
            'tare_weight',
            'material_weight',
            'total_weight',
            'weight_status',
            'rw_status',
            'doff_date',
            'reason',
            'ncr_status',
            'floor_code',
            'filament',
            'package_master_id',
            'packing_name',
            'wl_id',
            'rewind_process',
            'final_winding_reason',
            'inspection_date',
            'packed_date',
            'inspection_status'

        ];


        public function packageMaster(){
            return $this->belongsTo(PackageMaster::class, 'package_master_id');
        }
}
