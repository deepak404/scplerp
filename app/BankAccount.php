<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
        'bp_id', 'account_name', 'account_number','ifsc_code','bank_name','bank_address',
    ];

    public function businessPartner()
    {
    	$this->belongsTo(BusinessPartner::class, 'bp_id');
    }
}
