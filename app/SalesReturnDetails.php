<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReturnDetails extends Model
{
    public function saleReturn(){
        return $this->belongsTo(SalesReturn::class);
    }
}
