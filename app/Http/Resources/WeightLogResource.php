<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WeightLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'doff_date' => $this->doff_date,
            'doff_no' => $this->doff_no,
            'floor_code' => $this->floor_code,
            'filament' => $this->filament,
            'material' => $this->material,
            'unique_id' => $this->unique_id,
            'spindle' => $this->spindle
        ];
    }
}
