<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FinalWindingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('final_winding');
    }

    public function index()
    {
        return view('final-winding.index');
    }
}
