<?php

namespace App\Http\Controllers;

use App\ItemMaster;
use App\WeightLog;
use App\WeightLogDetails;
use App\WeightLogMaster;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WeightLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('production');
    }

    public function index()
    {
        $items = ItemMaster::all();
        return view('weight_log.weight-log')->with(compact(['items']));
    }


    public function insertWeightLogDetails(Request $request)
    {
        $request->validate([

            'spindle_details' => 'required',
            'machine' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'tare' => 'required',
            'material' => 'required',
            'doff_no' => 'required',
            'done_by' => 'required',

        ]);


        try {
            $weightLogMaster = WeightLogMaster::create([

                'machine_id' => $request['machine'],
                'from_date' => \DateTime::createFromFormat('d-m-Y H:i', $request['from_date']),
                'to_date' => \DateTime::createFromFormat('d-m-Y H:i', $request['to_date']),
                'tare_weight' => $request['tare'],
                'material_id' => $request['material'],
                'doff_no' => $request['doff_no'],
                'done_by' => $request['done_by'],
            ]);


            $spindleDetails = json_decode($request['spindle_details']);


            foreach ($spindleDetails as $spindle) {
                $weightLogMaster->weightLogDetails()->create([
                    'spindle' => $spindle->no,
                    'weight' => $spindle->weight,
                    'quality_check' => $spindle->quality,
                ]);
            }

            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            http_response_code(500);
            dd($e->getMessage());
            return response()->json(['status' => false], 500);
        }
    }


    public function showWeightLogList($date = null)
    {
        if ($date == null) {
            $today = date('Y-m-01');
        } else {
            $today = date('Y-m-d', strtotime('01-'.$date));
        }




        $weightLog = WeightLog::where('doff_date','LIKE',date('Y-m-d').'%')
//                                            ->where('wl_time','<=',date('Y-m-t',strtotime($today)))
                                            // ->where('packed_status', null)
                                            // ->whereIn('machine', ['ZellA','Zell A'])
                                            ->where('active_status', '1')
                                            ->get()
                                            ->sortBy('doff_date');


//        dd($weightLog->last(), $today, date('Y-m-t',strtotime($today)));

        return view('weight_log.weight-log-list')->with(compact('weightLog'));
    }


    public function updateWeightLogDetails(Request $request)
    {
        $request->validate([

            'spindle_details' => 'required',
            'machine' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'tare' => 'required',
            'material' => 'required',
            'doff_no' => 'required',
            'done_by' => 'required',
            'master_id' => 'required',
        ]);




        try {
            $weightLogMaster = WeightLogMaster::find($request['master_id'])->update([

                'machine_id' => $request['machine'],
                'from_date' => \DateTime::createFromFormat('d-m-Y H:i', $request['from_date']),
                'to_date' => \DateTime::createFromFormat('d-m-Y H:i', $request['to_date']),
                'tare_weight' => $request['tare'],
                'material_id' => $request['material'],
                'doff_no' => $request['doff_no'],
                'done_by' => $request['done_by'],

            ]);


            if ($weightLogMaster) {
                WeightLogDetails::where('master_id', $request['master_id'])->delete();

                $weightLogMaster = WeightLogMaster::where('id', $request['master_id'])->first();

                $spindleDetails = json_decode($request['spindle_details']);

//                http_response_code(500);
//                dd($weightLogMaster, $spindleDetails);

                foreach ($spindleDetails as $spindle) {
                    $weightLogMaster->weightLogDetails()->create([
                        'spindle' => $spindle->no,
                        'weight' => $spindle->weight,
                        'quality_check' => $spindle->quality,
                    ]);
                }

                return response()->json(['status' => true], 200);
            } else {
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }


    public function editWeightLogDetails($id)
    {
        $items = ItemMaster::all();
        $weightLogMaster = WeightLogMaster::where('id', $id)->with('weightLogDetails')->first();

        return view('weight_log.edit-weight-log')->with(compact('weightLogMaster', 'items'));
    }


    public function deleteWeightLogDetails($id)
    {
        $wm = WeightLogMaster::where('id', $id)->delete();
        $wd = WeightLogDetails::where('master_id', $id)->delete();

        return redirect()->back();
    }
}
