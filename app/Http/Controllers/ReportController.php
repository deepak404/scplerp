<?php

namespace App\Http\Controllers;

use App\ChemicalClearance;
use App\DipSolutionClearance;
use App\IndentMaster;
use App\InvoiceMaster;
use App\PackagingClearance;
use App\PackingClearanceResult;
use App\SaleOrder;
use App\WeightLog;
use App\WeightLogDetails;
use App\WeightLogMaster;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Http\Request;
use App\BusinessPartner;
use App\BankAccount;
use App\ItemMaster;
use App\ProductionFeasibility;
use App\ProductionPlanning;
use Carbon\Carbon;
use App\Filament;
use App\PackageMaster;
use App\FilamentClearance;
use App\NcrMaster;
use ZipArchive;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dateCommitmentReport(Request $request)
    {
        $today = $request['filter-date'];

        $filter = date('F Y', strtotime($today));

        $filter = strtoupper($filter);

        $salesList = SaleOrder::where('order_date', 'LIKE', date('Y-m', strtotime($today)).'%')
                                ->where('order_quantity', '>', 0)
                                ->where('bp_id', '!=', 114)
                                ->with('indentDetails')
                                ->get()
                                ->groupby('bp_id');

        $preOrders = SaleOrder::where('order_date', '<', date('Y-m-01', strtotime($today)))
                                ->where('order_quantity', '>', 0)
                                ->where('pending_quantity', '>', 0)
                                ->where('bp_id', '!=', 114)
                                ->with('indentDetails')
                                ->get()
                                ->groupby('bp_id');
        // dd($salesList->first()->first()->indentDetails->indentMaster,$salesList->first()->first());
        $updateList = [];
        $count = 0;
        foreach ($salesList as $bpId => $sales) {
            $bpname = BusinessPartner::where('id', $bpId)->value('bp_name');
            foreach ($sales as $sale) {
                if ($sale->order_quantity != $sale->pre_close) {
                    $updateList[$bpname]['current_orders'][$count]['material_name'] = $sale->material_name;
                    $updateList[$bpname]['current_orders'][$count]['last_month_pending'] = $sale->last_month_pending;
                    $updateList[$bpname]['current_orders'][$count]['order_quantity'] = $sale->order_quantity;
                    $updateList[$bpname]['current_orders'][$count]['total_order'] = $sale->total_order;
                    $updateList[$bpname]['current_orders'][$count]['dispatch_quantity'] = round($sale->dispatch_quantity, 1);
                    $updateList[$bpname]['current_orders'][$count]['pending_quantity'] = round($sale->pending_quantity, 1);
                    $updateList[$bpname]['current_orders'][$count]['customer_date'] = isset($sale->customer_date) ? date('d.m.Y', strtotime($sale->customer_date)) : ' ';
                    $updateList[$bpname]['current_orders'][$count]['dispatch_date'] = isset($sale->dispatch_date) ? date('d.m.Y', strtotime($sale->dispatch_date)) : ' ';
                    if (empty($sale->indentDetails)) {
                        $updateList[$bpname]['current_orders'][$count]['order_ref_no'] = '';
                        $updateList[$bpname]['current_orders'][$count]['dated_on'] = '';
                        $updateList[$bpname]['current_orders'][$count]['rate_per_kg'] = '';
                    } else {
                        $updateList[$bpname]['current_orders'][$count]['order_ref_no'] = $sale->indentDetails->indentMaster->order_ref_no;
                        $updateList[$bpname]['current_orders'][$count]['dated_on'] = date('d.m.Y', strtotime($sale->indentDetails->indentMaster->dated_on));
                        $updateList[$bpname]['current_orders'][$count]['rate_per_kg'] = $sale->indentDetails->rate_per_kg;
                    }
                    $updateList[$bpname]['current_orders'][$count]['scpl_confirmation_date'] = $sale->scpl_confirmation_date;
                    $count++;
                }
            }
        }
        foreach ($preOrders as $bpId => $sales) {
            $bpname = BusinessPartner::where('id', $bpId)->value('bp_name');
            foreach ($sales as $sale) {
                $updateList[$bpname]['pending_order'][$count]['material_name'] = $sale->material_name;
                $updateList[$bpname]['pending_order'][$count]['last_month_pending'] = $sale->last_month_pending;
                $updateList[$bpname]['pending_order'][$count]['order_quantity'] = $sale->order_quantity;
                $updateList[$bpname]['pending_order'][$count]['total_order'] = $sale->total_order;
                $updateList[$bpname]['pending_order'][$count]['dispatch_quantity'] = round($sale->dispatch_quantity, 1);
                $updateList[$bpname]['pending_order'][$count]['pending_quantity'] = round($sale->pending_quantity, 1);
                $updateList[$bpname]['pending_order'][$count]['customer_date'] = isset($sale->customer_date) ? date('d.m.Y', strtotime($sale->customer_date)) : ' ';
                $updateList[$bpname]['pending_order'][$count]['dispatch_date'] = isset($sale->dispatch_date) ? date('d.m.Y', strtotime($sale->dispatch_date)) : ' ';
                if (empty($sale->indentDetails)) {
                    $updateList[$bpname]['pending_order'][$count]['order_ref_no'] = '';
                    $updateList[$bpname]['pending_order'][$count]['dated_on'] = '';
                    $updateList[$bpname]['pending_order'][$count]['rate_per_kg'] = '';
                } else {
                    $updateList[$bpname]['pending_order'][$count]['order_ref_no'] = $sale->indentDetails->indentMaster->order_ref_no;
                    $updateList[$bpname]['pending_order'][$count]['dated_on'] = date('d.m.Y', strtotime($sale->indentDetails->indentMaster->dated_on));
                    $updateList[$bpname]['pending_order'][$count]['rate_per_kg'] = $sale->indentDetails->rate_per_kg;
                }
                $updateList[$bpname]['pending_order'][$count]['scpl_confirmation_date'] = $sale->scpl_confirmation_date;
                $count++;
            }
        }
        ksort($updateList);
        // dd($updateList);
        return PDF::loadView('reports.sales.date-commitment-report', ['salesList' =>$updateList,'filter'=>$filter])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Date Commitment Report.pdf');
    }



    public function indentReport($id)
    {
        $indentMaster = IndentMaster::where('id', $id)->first();

        $indentDetails = $indentMaster->indentDetails;

        $businessPartner = [];

        $businessPartner['name'] = $indentMaster->businessPartner->bp_name;
        $businessPartner['address'] = $indentMaster->businessPartner->address;
        $businessPartner['pincode'] = $indentMaster->businessPartner->pincode;
        $businessPartner['district'] = $indentMaster->businessPartner->district;
        $businessPartner['gst'] = $indentMaster->businessPartner->gst;
        $businessPartner['state'] = $indentMaster->businessPartner->state;


        return PDF::loadView('reports.sales.indent', ['indentMaster' => $indentMaster, 'indentDetails' => $indentDetails, 'businessPartner' => $businessPartner])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Indent.pdf');

        //        return view('reports.sales.indent')->with(compact('indentMaster','indentDetails', 'businessPartner'));
    }

    public function tentativeProductionPlanOne(Request $request)
    {
        // dd($request->all());

        $date = date('Y-m-d', strtotime($request['filter-date']));

        $lastDate = date('Y-m-d', strtotime("-1 months", strtotime($date)));


        // $saleOrders = SaleOrder::with(['productionFeasibility','businessPartner','itemMaster']);

        $masterData = SaleOrder::whereBetween('order_date', [$date,date('Y-m-t', strtotime($date))])->with(['productionFeasibility','businessPartner','itemMaster'])->get();

        $lastMonth = SaleOrder::whereBetween('order_date', [$lastDate,date('Y-m-t', strtotime($lastDate))])->with(['productionFeasibility','businessPartner','itemMaster'])->get();

        $filament = Filament::all()->groupBy('name')->toArray();

        // $filament = [];

        // foreach (array_keys($filaments) as $key => $value) {
        //     $data = explode('-', $value);
        //     $filament[] = trim($data[1]);
        // }
        //
        // dd($filament);

        $tentativePlan = [];

        $consumption = [];

        $overAllConsuption = [];

        $consumptionByParty = [];

        $total = ['1'=>0,'2'=>0,'3'=>0];

        $count = 0;

        $start= $date;

        $end = date('Y-m-t', strtotime($date));

        foreach ($filament as $key => $value) {
            $consumption['1'][$key]['current'] = 0;
            $consumption['1'][$key]['change'] = 0;
            $overAllConsuption[$key]['current'] = 0;
            $overAllConsuption[$key]['change'] = 0;

            $consumption['2'][$key]['current'] = 0;
            $consumption['2'][$key]['change'] = 0;
            $overAllConsuption[$key]['current'] = 0;
            $overAllConsuption[$key]['change'] = 0;

            $consumption['3'][$key]['current'] = 0;
            $consumption['3'][$key]['change'] = 0;
            $overAllConsuption[$key]['current'] = 0;
            $overAllConsuption[$key]['change'] = 0;
        }

        $lastMonthCurrent = [];
        foreach ($lastMonth as $key => $value) {
            if (!is_null($value->productionFeasibility->changed_filament)) {
                $changedFilament = explode(',', $value->productionFeasibility->changed_filament);

                $changedConsumption = explode(',', $value->productionFeasibility->consumption);
                foreach ($changedFilament as $key2 => $two) {
                    if (empty($consumption[$value->productionFeasibility->machine][$two]['current'])) {
                        $consumption[$value->productionFeasibility->machine][$two]['current'] = $changedConsumption[$key2];
                        $overAllConsuption[$two]['current'] = $changedConsumption[$key2];
                        $lastMonthCurrent[$value->productionFeasibility->machine][$value->itemMaster->material]['current'] = $two;
                    } else {
                        $consumption[$value->productionFeasibility->machine][$two]['current'] += $changedConsumption[$key2];
                        $overAllConsuption[$two]['current'] += $changedConsumption[$key2];
                        $lastMonthCurrent[$value->productionFeasibility->machine][$value->itemMaster->material]['current'] = $two;
                    }
                }
            }
        }

        // dd($lastMonthCurrent[1]['2x3 ST 001']['current']);

        foreach ($masterData as $key => $value) {
            $machine_id = $value->productionFeasibility['machine'];
            if (!is_null($machine_id)) {

                // $tentativePlan[$machine_id][$count]['current_filament'] = '-';

                // $lastMonth = SaleOrder::where('bp_id',$value->bp_id)
                //                         ->where('material_id',$value->material_id)
                //                         ->where('order_date','<',$value->order_date)
                //                         ->orderBy('customer_date','DESC')
                //                         ->first();
                //
                // if (!empty($lastMonth)) {
                //
                //     $tentativePlan[$machine_id][$count]['current_filament'] = $lastMonth->productionFeasibility['changed_filament'];
                //
                //     $currentFilament = explode(',', $lastMonth->productionFeasibility['changed_filament']);
                //
                //     $currentConsumption = explode(',', $lastMonth->productionFeasibility['consumption']);
                //
                //     foreach ($currentFilament as $key1 => $one) {
                //       if (empty($consumption[$lastMonth->productionFeasibility['machine']][$one]['current'])) {
                //         if (!empty($currentConsumption[$key1])) {
                //           $consumption[$lastMonth->productionFeasibility['machine']][$one]['current'] = $currentConsumption[$key1];
                //           $overAllConsuption[$one]['current'] = $currentConsumption[$key1];
                //         }
                //       }else{
                //         $consumption[$lastMonth->productionFeasibility['machine']][$one]['current'] += $currentConsumption[$key1];
                //         $overAllConsuption[$one]['current'] += $currentConsumption[$key1];
                //       }
                //
                //     }
                //
                // }

                $changedFilament = explode(',', $value->productionFeasibility['changed_filament']);

                $changedConsumption = explode(',', $value->productionFeasibility['consumption']);


                foreach ($changedFilament as $key2 => $two) {
                    $overAllConsuption[$two]['change'] += $changedConsumption[$key2];

                    if (empty($consumption[$machine_id][$two]['change'])) {
                        $consumption[$machine_id][$two]['change'] = $changedConsumption[$key2];
                    } else {
                        $consumption[$machine_id][$two]['change'] += $changedConsumption[$key2];
                    }


                    if (empty($consumptionByParty[$machine_id][$value['bp_id']])) {
                        $consumptionByParty[$machine_id][$value['bp_id']] = $changedConsumption[$key2];
                    } else {
                        $consumptionByParty[$machine_id][$value['bp_id']] += $changedConsumption[$key2];
                    }
                }

                $tentativePlan[$machine_id][$count]['material'] = $value->itemMaster['material'];

                $tentativePlan[$machine_id][$count]['customer'] = $value->businessPartner['bp_name'];

                $tentativePlan[$machine_id][$count]['order_quantity'] = array_sum($changedConsumption);
                // $tentativePlan[$machine_id][$count]['order_quantity'] = $value['order_quantity'];

                $tentativePlan[$machine_id][$count]['changed_filament'] = $value->productionFeasibility['changed_filament'];

                $total[$value->productionFeasibility['machine']] += array_sum($changedConsumption);

                $count += 1;
            }
        }
        // dd($overAllConsuption);
        ksort($tentativePlan);
        $month = date('F " Y', strtotime($date));
        $finalPlan = [];
        foreach ($tentativePlan as $key => $value) {
            foreach ($value as $data) {
                if ($data['order_quantity'] > 0) {
                    if (empty($finalPlan[$key][$data['material']][$data['changed_filament']])) {
                        $finalPlan[$key][$data['material']][$data['changed_filament']]['order_quantity'] = $data['order_quantity'];
                        $finalPlan[$key][$data['material']][$data['changed_filament']]['current'] = $lastMonthCurrent[$key][$data['material']]['current'] ?? "-";
                    } else {
                        $finalPlan[$key][$data['material']][$data['changed_filament']]['order_quantity'] += $data['order_quantity'];
                        $finalPlan[$key][$data['material']][$data['changed_filament']]['current'] = $lastMonthCurrent[$key][$data['material']]['current'] ?? "-";
                    }
                }
            }
        }
        // dd($finalPlan);
        return PDF::loadView('reports.production-plan.tentative-production-plan-one', ['planes' => $finalPlan,'consumption'=>$consumption,'party'=>$consumptionByParty,'overAllConsuption'=>$overAllConsuption,'month'=>$month,'total'=>$total])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Tentative Production Plan.pdf');

        // return view('reports.production-plan.tentative-production-plan-one',['planes' => $tentativePlan,'consumption'=>$consumption,'party'=>$consumptionByParty,'overAllConsuption'=>$overAllConsuption,'total'=>$total]);
    }

    public function tentativeProductionPlanTwo(Request $request)
    {
        $date = date('Y-m-d', strtotime($request['filter-date']));

        $saleOrders = SaleOrder::with(['productionFeasibility','businessPartner','itemMaster']);

        $masterData = $saleOrders->whereBetween('order_date', [$date,date('Y-m-t', strtotime($date))])->orderBy('material_id')->get();

        $finalArray = [];

        $total = [];

        $count = 0;

        foreach ($masterData as $key => $value) {
            $machine_id = $value->productionFeasibility['machine'];
            if (!is_null($machine_id)) {
                $changedConsumption = explode(',', $value->productionFeasibility['consumption']);

                $finalArray[$machine_id][$count]['material'] = $value->itemMaster['material'];
                $finalArray[$machine_id][$count]['customer'] = $value->businessPartner['bp_name'];
                $finalArray[$machine_id][$count]['order_quantity'] = array_sum($changedConsumption);
                $finalArray[$machine_id][$count]['cord_wt'] = $value->productionFeasibility['cord_weight'];
                $finalArray[$machine_id][$count]['no_of_spls'] = $value->productionFeasibility['no_of_spls'];
                $finalArray[$machine_id][$count]['speed_mpm'] = $value->productionFeasibility['speed_in_mpm'];
                $finalArray[$machine_id][$count]['prod_hr'] = $value->productionFeasibility['production_per_hr'];
                $finalArray[$machine_id][$count]['running_hrs'] = $value->productionFeasibility['running_hrs'];
                $finalArray[$machine_id][$count]['no_of_changes'] = $value->productionFeasibility['no_of_changes'];
                $finalArray[$machine_id][$count]['changes_per_hr'] = $value->productionFeasibility['changes_per_hr'];

                    


                // $test[$machine_id][$value->itemMaster['material']][] = $value->productionFeasibility['running_hrs'];

                $count += 1;
            }
        }


        ksort($finalArray);
        $testArray = [];
        $month = date('F " Y', strtotime($date));
        foreach ($finalArray as $machine => $data) {
            foreach ($data as $plan) {
                if ($plan['order_quantity'] > 0) {
                    $testArray[$machine][$plan['material']]['order_quantity'][] = $plan['order_quantity'];
                    $testArray[$machine][$plan['material']]['cord_wt'][] = $plan['cord_wt'];
                    $testArray[$machine][$plan['material']]['no_of_spls'][] = $plan['no_of_spls'];
                    $testArray[$machine][$plan['material']]['speed_mpm'][] = $plan['speed_mpm'];
                    $testArray[$machine][$plan['material']]['prod_hr'][] = $plan['prod_hr'];
                    $testArray[$machine][$plan['material']]['running_hrs'][] = $plan['running_hrs'];
                    $testArray[$machine][$plan['material']]['no_of_changes'][] = $plan['no_of_changes'];
                    $testArray[$machine][$plan['material']]['changes_per_hr'][] = $plan['changes_per_hr'];

                    if (empty($total[$machine]['order_quantity'])) {
                        $total[$machine]['order_quantity'] = $plan['order_quantity'];
                    } else {
                        $total[$machine]['order_quantity'] += $plan['order_quantity'];
                    }
    
                    if (empty($total[$machine]['total_running_hr'])) {
                        $total[$machine]['total_running_hr'] = $plan['running_hrs'];
                    } else {
                        $total[$machine]['total_running_hr'] += $plan['running_hrs'];
                    }
    
                    
                    if (empty($total[$machine]['no_of_changes'])) {
                        $total[$machine]['no_of_changes'] = $plan['no_of_changes'];
                    } else {
                        $total[$machine]['no_of_changes'] += $plan['no_of_changes'];
                    }
                    
                    if (empty($total[$machine]['changes_per_hr'])) {
                        $total[$machine]['changes_per_hr'] = $plan['changes_per_hr'];
                    } else {
                        $total[$machine]['changes_per_hr'] += $plan['changes_per_hr'];
                    }
                }
            }
        }
        return PDF::loadView('reports.production-plan.tentative-production-plan-two', ['master'=>$testArray,'month'=>$month,'total'=>$total])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Tentative Production Plan.pdf');
        // return view('reports.production-plan.tentative-production-plan-two',['master'=>$finalArray,'month'=>$month,'total'=>$total]);
    }

    public function packagingDetail($id)
    {
        $packageMasters = PackageMaster::where('id', $id)->first();

        $bobbin_count = $packageMasters['bobbin_count'];
        $empty_weight = $packageMasters['bobbin_weight'];
        $moisture_weight = $packageMasters['moisture_weight'];
        $box_weight = $packageMasters['box_weight'];

        $packagingDetail = $packageMasters->packagingDetails()->get();
        $finalArray = [];


        foreach ($packagingDetail as $key => $value) {
            $packedBobbin = $value->packagingBobbin->toArray();
            $total_weight = 0;
            foreach ($packedBobbin as $bobbin) {
                $total_weight += $bobbin['b_wt'];
            }
            $net_weight = $total_weight - $empty_weight;
            $finalArray[] = [
                'case_no' => $value['case_no'],
                'doff_no' => $value['doff_no'],
                'bobbin_count' => $bobbin_count,
                'bobbin_weight' => $empty_weight,
                'total_weight' => $total_weight,
                'gross_weight' => $total_weight + $box_weight,
                'net_weight' => $net_weight,
                'commercial_weight' => $net_weight + $moisture_weight,
                'packed_bobbin' => $packedBobbin
            ];
        }
        // dd($finalArray);
        return PDF::loadView('reports.packaging.packaging-detail', ['master' => $packageMasters,'data'=>$finalArray])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Finished Goods Packing Details.pdf');
        // return view('reports.packaging.packaging-detail');
    }
    public function generateWeightLogReport($id, $machine)
    {
        $weightLogMaster = WeightLogMaster::where('id', $id)->with('WeightLogDetails')->first();

        $totalWeight = $weightLogMaster->WeightLogDetails->sum('weight');

        if ($machine == 1) {
            /*
            * Below if for ZELL A Machine
            *
            * */

            $totalOkWeight = $weightLogMaster->WeightLogDetails->where('quality_check', 0)->sum('weight');
            $totalNotOkWeight = $weightLogMaster->WeightLogDetails->where('quality_check', 1)->sum('weight');
            $totalEntries = $weightLogMaster->WeightLogDetails->count();

            // dd($weightLogMaster);
            return PDF::loadView('reports.weight-log.zell-a', [

                'master'=>$weightLogMaster,
                'machine' => $machine,
                'totalWeight' => $totalWeight,
                'totalOkWeight' => $totalOkWeight,
                'totalNotOkWeight' => $totalNotOkWeight,
                'totalEntries' => $totalEntries,
                'totalWeight' => $totalWeight
            ])
                ->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->stream('ZELL A Weight Log Report.pdf');
        } elseif ($machine == 2) {

        /*
         * Below if for ZELL B Machine
         *
         * */

            $eastEntries = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'E%')->where('weight', '!=', 0)->get();
            $westEntries = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'W%')->where('weight', '!=', 0)->get();

            $eastEntriesCount = $eastEntries->count();
            $westEntriesCount = $westEntries->count();

            $eastEntriesOkWeight = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'E%')->where('quality_check', 0)->sum('weight');
            $westEntriesOkWeight = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'W%')->where('quality_check', 0)->sum('weight');

            $eastEntriesNotOkWeight = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'E%')->where('quality_check', 1)->sum('weight');
            $westEntriesNotOkWeight = WeightLogDetails::where('master_id', $id)->where('spindle', 'LIKE', 'W%')->where('quality_check', 1)->sum('weight');


            return PDF::loadView('reports.weight-log.zell-b', [
                'master'=>$weightLogMaster,
                'machine' => $machine,
                'totalWeight' => $totalWeight,
                'eastEntryCount' => $eastEntriesCount,
                'westEntryCount' => $westEntriesCount,
                'eastEntryOkWeight' => $eastEntriesOkWeight,
                'westEntryOkWeight' => $westEntriesOkWeight,
                'eastEntryNotOkWeight' => $eastEntriesNotOkWeight,
                'westEntryNotOkWeight' => $westEntriesNotOkWeight,
                'totalWeight' => $totalWeight
                ])
                ->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->stream('ZELL B Weight Log Report.pdf');
        } else {
            $totalOkWeight = $weightLogMaster->WeightLogDetails->where('quality_check', 0)->sum('weight');
            $totalNotOkWeight = $weightLogMaster->WeightLogDetails->where('quality_check', 1)->sum('weight');
            $totalEntries = $weightLogMaster->WeightLogDetails->count();


            return PDF::loadView('reports.weight-log.kidde', [

                'master'=>$weightLogMaster,
                'machine' => $machine,
                'totalWeight' => $totalWeight,
                'totalOkWeight' => $totalOkWeight,
                'totalNotOkWeight' => $totalNotOkWeight,
                'totalEntries' => $totalEntries,
                'totalWeight' => $totalWeight
            ])
                ->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->stream('ZELL A Weight Log Report.pdf');
        }
    }


    public function generateInvoice(Request $request)
    {
        $request->validate([
            'no_of_copies' => 'required',
            'invoice_id' => 'required',
        ]);



        $totalInvoiceWeight = 0;
        $invoiceMaster = InvoiceMaster::where('id', $request['invoice_id'])->first();
        $indentMaster = $invoiceMaster->indent;

        $packageMastersId = PackageMaster::where('invoice_master_id', $request['invoice_id'])->pluck('id');

//        dd($request['invoice_id']);

        $packageMasters = PackageMaster::whereIn('id', $packageMastersId)->get()->groupBy('material_id');
//        dd($packageMasters);
        $orderDetails = [];
        $taxDetails = [];

        foreach ($packageMasters as $material => $packageMaster) {
            $orderDetails[$material]['weight'] = 0;
//            dd($indentDetails);
            foreach ($packageMaster as $package) {
                $indentDetails =  $indentMaster->indentDetails()->where('material_id', $material)->first();

//                dd(SaleOrder::where('id', $indentDetails->order_id)->value('material_name'));
                $orderDetails[$material]['name'] = SaleOrder::where('id', $indentDetails->order_id)->value('material_name');
                $orderDetails[$material]['weight'] += $package->weightLogs->sum('material_weight') + $package->moisture_weight;
                $orderDetails[$material]['hsn'] = $indentDetails->hsn_sac;
                $orderDetails[$material]['count'] = $package->weightLogs->count();
                $orderDetails[$material]['gst'] = $indentDetails->gst_rate;
                $orderDetails[$material]['rate'] = $indentDetails->rate_per_kg;
                $orderDetails[$material]['no_box'] = $packageMaster->count();

//                $taxDetails[$indentDetails->hsn_sac]['totalAmount'] =

                $totalInvoiceWeight += $package->weightLogs->sum('material_weight') + $package->moisture_weight;
            }
        }


        $businessPartner['name'] = $indentMaster->businessPartner->bp_name;
        $businessPartner['address'] = $indentMaster->businessPartner->address;
        $businessPartner['pincode'] = $indentMaster->businessPartner->pincode;
        $businessPartner['district'] = $indentMaster->businessPartner->district;
        $businessPartner['gst'] = $indentMaster->businessPartner->gst_code;
        $businessPartner['state'] = $indentMaster->businessPartner->state;


        $this->clearDirectory('Invoice - '.$invoiceMaster->invoice_no);
        mkdir('Invoice - '.$invoiceMaster->invoice_no, 0777, true);

//        return view(
//            'invoice.invoice-dummy',
//            ['master'=>$invoiceMaster,
//                'businessPartner' => $businessPartner,
//                'indentMaster' => $indentMaster,
//                'orderDetails' => $orderDetails,
//                'invoiceName' => 'test',
//                'totalInvoiceWeight' => $totalInvoiceWeight,
//            ]
//        );
        //return PDF::loadView('invoice.invoice-dummy', ['master'=>$invoiceMaster, 'businessPartner' => $businessPartner, 'indentMaster' => $indentMaster, 'orderDetails' => $orderDetails, 'invoiceName' => 'test', 'totalInvoiceWeight' => $totalInvoiceWeight])->stream();
//        return view('reports.invoice.invoice', ['master'=>$invoiceMaster, 'businessPartner' => $businessPartner, 'indentMaster' => $indentMaster, 'orderDetails' => $orderDetails, 'invoiceName' => 'test', 'totalInvoiceWeight' => $totalInvoiceWeight]);

        for ($i=0; $i< (3 +$request['no_of_copies']); $i++) {
            if ($i == 0) {
                $invoiceName = 'ORIGINAL';
                $fileName = $invoiceName.'.pdf';
            } elseif ($i == 1) {
                $invoiceName = 'DUPLICATE FOR TRANSPORT';
                $fileName = $invoiceName.'.pdf';
            } elseif ($i == 2) {
                $invoiceName = 'TRIPLICATE FOR SUPPLIER';
                $fileName = $invoiceName.'.pdf';
            } else {
                $invoiceName = 'EXTRA COPY';
                $fileName = $invoiceName.'-'.$i.'.pdf';
            }



            PDF::loadView(
                'invoice.invoice-dummy',
                ['master'=>$invoiceMaster,
                    'businessPartner' => $businessPartner,
                    'indentMaster' => $indentMaster,
                    'orderDetails' => $orderDetails,
                    'invoiceName' => $invoiceName,
                    'totalInvoiceWeight' => $totalInvoiceWeight,
                ]
            )
                ->setPaper('a4', 'portrait')->setWarnings(false)->save('Invoice - '.$invoiceMaster->invoice_no.'/'.$fileName);
        }

        $response = $this->makeZip('Invoice - '.$invoiceMaster->invoice_no);

        $headers = array(
            'Content-Type: application/zip',
        );


        return response()->download($response[0], $response[1], $headers);
    }


    private function makeZip($folder)
    {
        if (file_exists($folder.'.zip')) {
            unlink($folder.'.zip');
        }

        $zip = new ZipArchive;
        if ($zip->open($folder.'.zip', ZipArchive::CREATE) === true) {
            $files = glob($folder . '/*');


            foreach ($files as $value) {
                $zip->addFile($value);
            }
            $zip->close();
        }


        $file= public_path(). "/".$folder.'.zip';

        return [$file, $folder.'.zip' ];
    }

    private function clearDirectory($dir)
    {
        $files = glob($dir . '/*');

        foreach ($files as $file) {
            is_dir($file) ? clearDirectory($file) : unlink($file);
        }

        if (file_exists($dir)) {
            rmdir($dir);
        } else {
        }
        return;
    }

    public function filamentClearanceReportOne($id)
    {
        $filamentLot = FilamentClearance::where('id', $id)->first();

        $testedPallets = $filamentLot->testedPallet;

        $reportResult = null;
        if (!is_null($filamentLot->report_result)) {
            $reportResult = json_decode($filamentLot->report_result);
        }

        $palletEntrys = $filamentLot->palletEntry;


        if (!is_null($palletEntrys)) {
            $palletEntrys = json_decode($palletEntrys['pallets']);
        }


        return PDF::loadView('reports.quality.filament-clearance-report-one', ['filamentLot'=>$filamentLot,'testedPallets'=>$testedPallets,'palletEntrys'=>$palletEntrys,'reportResult'=>$reportResult])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Filament Clearance Report.pdf');
    }

    public function filamentClearanceReportTwo($id)
    {
        $filamentLot = FilamentClearance::where('id', $id)->first();
        $testedPallets = $filamentLot->testedPallet;
        // dd($testedPallets);
        $testedResult = [];

        foreach ($testedPallets as $testedPallet) {
            $results = json_decode($testedPallet->result);
            foreach ($results->parameter as $key2 => $data) {
                $umo = Filament::where('parameter', $data)->where('name', $filamentLot->type)->first();
                $testedResult[$data]['actual'][] = $results->actual[$key2];
                $testedResult[$data]['scpl'] = $results->scpl[$key2];
                $testedResult[$data]['uom'] = $umo->unit;
            }
        }

        // dd($testedResult);
        foreach ($testedResult as $key => $value) {
            $min = "";
            $max = "";
            $avg = "";
            $rang = 0.0;
            $test = "";
            if ($key != 'Variety') {
                if (strpos($value['scpl'], '±')) {
                    $data = explode('±', $value['scpl']);
                    $min = $data[0]-$data[1];
                    $max = $data[0]+$data[1];
                } else {
                    $min = $value['scpl'];
                }
                $avg = (array_sum($value['actual'])/count($value['actual']));
                $avg = round($avg, 2);
                $rangeMax = max($value['actual']);
                $rangeMin = min($value['actual']);
                if (is_numeric($rangeMax)) {
                    $rang = round(($rangeMax-$rangeMin), 2);
                    if (($min <= $avg) && ($max >= $avg)) {
                        $test = 'Ok';
                    } elseif ($max == "") {
                        if ($min <= $avg) {
                            $test = 'Ok';
                        } else {
                            $test = 'Not Ok';
                        }
                    } else {
                        $test = 'Not Ok';
                    }
                } else {
                    $test = 'Not Ok';
                }
            } else {
                $min = $value['scpl'];
                $max = $value['scpl'];
                $test = 'Ok';
            }
            $testedResult[$key]['min'] = $min;
            $testedResult[$key]['max'] = $max;
            $testedResult[$key]['avg'] = $avg;
            $testedResult[$key]['range'] = $rang;
            $testedResult[$key]['status'] = $test;
        }

        return PDF::loadView('reports.quality.filament-clearance-report-two', ['filamentLot'=>$filamentLot,'result'=>$testedResult])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Filament Clearance Report.pdf');
    }

    public function generateChemicalClearance($id)
    {
        $chemicalClearance = ChemicalClearance::where('id', $id)->first();
        $chemicalClearanceResult = $chemicalClearance->chemicalClearanceResult;

//        dd($chemicalClearance, $chemicalClearanceResult);
        return PDF::loadView(
            'reports.quality.chemical-clearance',
            [
                'chemicalClearance' => $chemicalClearance,
                'chemicalClearanceResult' => $chemicalClearanceResult
            ]
        )
            ->setPaper('a4', 'landscape')->setWarnings(false)->stream('Chemical Clearance.pdf');
    }

    public function planActualReport(Request $request)
    {
        if ($request['machine'] == 1) {
            $machine = ['ZellA','Zell A'];
            $myMc = 'ZellA';
        } elseif ($request['machine'] == 2) {
            $machine = ['ZellB','Zell B'];
            $myMc = 'ZellB';
        } else {
            $machine = ['Kidde'];
            $myMc = 'Kidde';
        }


        $month = date('F - Y', strtotime($request['filter-date']));
        $start = date('Y-m-01 07:00:00', strtotime($request['filter-date']));
        $end = date('Y-m-t 23:59:59', strtotime($request['filter-date']));

        $productionPlan = ProductionPlanning::where('start_date', '>=', $start)
            ->where('start_date', '<=', $end)
            ->join('production_feasibilities', 'production_plannings.feasibility_id', '=', 'production_feasibilities.id')
            ->where('production_feasibilities.machine', $request['machine'])
            ->join('item_masters', 'production_plannings.material', '=', 'item_masters.id')
            ->join('sale_orders', 'production_feasibilities.order_id', '=', 'sale_orders.id')
            ->join('business_partners', 'sale_orders.bp_id', '=', 'business_partners.id')
            ->get(
                [
                        'production_plannings.start_date',
                        'production_plannings.end_date',
                        'production_plannings.filament_type',
                        'production_plannings.batch',
                        'production_plannings.order_kg',
                        'item_masters.material',
                        'business_partners.bp_name'
                      ]
            );

        $data = [];
        foreach ($productionPlan as $matrialId => $plan) {
            if (empty($data[$plan->start_date][$plan->material][$plan->filament_type]['sum'])) {
                $data[$plan->start_date][$plan->material][$plan->filament_type]['sum'] = $plan->order_kg;
                $data[$plan->start_date][$plan->material][$plan->filament_type]['customer'] = $plan->bp_name;
                $data[$plan->start_date][$plan->material][$plan->filament_type]['end_date'] = $plan->end_date;
            } else {
                $data[$plan->start_date][$plan->material][$plan->filament_type]['sum'] += $plan->order_kg;
            }
        }

        ksort($data);
        $actual = WeightLog::whereIn('machine', $machine)
                            ->where('spindle', '!=', 'ALL')
                            ->where('doff_date', '>=', $start)
                            ->where('doff_date', '<=', $end)
                            ->where('rw_status', 0)
                            ->get()
                            ->sortBy('doff_date')
                            ->groupBy('doff_no');
        // dd($actual->toArray());
        $finalActual = [];
        $count = 0;
        $oldMaterial = "";
        // $oldFilament = "";
        $uniqueId = [];
        foreach ($actual as $value) {
            $material = ItemMaster::where('id', $value->first()->material_id)->value('material');
            foreach ($value->sortBy('id') as $entry) {
                if (!in_array($entry->unique_id, $uniqueId)) {
                    $uniqueId[] = $entry->unique_id;
                    // if ($oldMaterial != "") {
                    if ($oldMaterial == $material) {
                        if (!in_array($entry->filament, $finalActual[$count]['filament'])) {
                            $finalActual[$count]['filament'][] = $entry->filament;
                        }
                        $finalActual[$count]['end_date'] = $entry->doff_date;
                        $finalActual[$count]['sum'] += $entry->material_weight;
                    } else {
                        $count++;
                        $oldMaterial = $material;
                        $finalActual[$count]['start'] = $entry->doff_date;
                        $finalActual[$count]['end_date'] = $entry->doff_date;
                        $finalActual[$count]['material'] = $material;
                        $finalActual[$count]['sum'] = $entry->material_weight;
                        $finalActual[$count]['filament'][] = $entry->filament;
                    }
                }
            }
        }
        // dd($finalActual);
        $planActualArray = [];
        $masterCount = 0;
        foreach ($data as $from => $materials) {
            foreach ($materials as $material => $filements) {
                foreach ($filements as $filement => $value) {
                    $planActualArray[$masterCount]['plane_start'] = $from;
                    $planActualArray[$masterCount]['plane_end'] = $value['end_date'];
                    $planActualArray[$masterCount]['plane_material'] = $material;
                    $planActualArray[$masterCount]['plane_qty'] = $value['sum'];
                    $planActualArray[$masterCount]['plane_filement'] = $filement;
                    foreach ($finalActual as $key => $actualData) {
                        if ($material == $actualData['material']) {
                            $planActualArray[$masterCount]['actual_start'] = $actualData['start'];
                            $planActualArray[$masterCount]['actual_end'] = $actualData['end_date'];
                            $planActualArray[$masterCount]['actual_material'] = $actualData['material'];
                            $planActualArray[$masterCount]['actual_qty'] = $actualData['sum'];
                            $planActualArray[$masterCount]['actual_filement'] = implode(',', $actualData['filament']);
                            unset($finalActual[$key]);
                            break;
                        }
                    }
                    $masterCount++;
                }
            }
        }
        foreach ($finalActual as $key => $actualData) {
            $planActualArray[$masterCount]['actual_start'] = $actualData['start'];
            $planActualArray[$masterCount]['actual_end'] = $actualData['end_date'];
            $planActualArray[$masterCount]['actual_material'] = $actualData['material'];
            $planActualArray[$masterCount]['actual_qty'] = $actualData['sum'];
            $planActualArray[$masterCount]['actual_filement'] = implode(',', $actualData['filament']);
            $masterCount++;
        }
        // dd($planActualArray, $finalActual);

        // return PDF::loadView('reports.weight-log.plan-vs-actual',['machine'=>$machine,'month'=>$month,'plan'=>$planData,'actual'=>$actualData])->setPaper('a4','portrait')->setWarnings(false)->stream('Plan Vs Actual Report.pdf');
        return PDF::loadView('reports.weight-log.v2.plan-vs-actual-new', ['machine'=>$myMc,'month'=>$month,'planActualArray'=>$planActualArray])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Plan Vs Actual Report.pdf');
    }

    public function pendingContracts($group = null)
    {
        $date = Carbon::now();
        $pendingOrders = SaleOrder::where('pending_quantity', '>', 0)->where('order_quantity', '>', 0)->orderBy('customer_date')->get();
        $finalArray = [];
        $count = 0;
        foreach ($pendingOrders as $key => $value) {
            $finalArray[$count]['name'] = $value->businessPartner['bp_name'];
            $finalArray[$count]['code'] = $value->material_name;
            if (!is_null($value->indentDetails)) {
                $finalArray[$count]['rate'] = $value->indentDetails['rate_per_kg'];
                $finalArray[$count]['date'] = $value->indentDetails->indentMaster['dated_on'];
                $finalArray[$count]['contract'] = $value->indentDetails->indentMaster['order_ref_no'];
            } else {
                $finalArray[$count]['rate'] = null;
                $finalArray[$count]['contract'] = null;
                $finalArray[$count]['date'] = null;
            }
            $finalArray[$count]['qty'] = $value['order_quantity'];
            $finalArray[$count]['delivered'] = $value['dispatch_quantity'];
            $finalArray[$count]['balance'] = $value['pending_quantity'];
            $finalArray[$count]['state'] = $value->businessPartner['state'];
            $count++;
        }
        // dd($finalArray);
        if ($group == null) {
            return PDF::loadView('reports.sales.pending-contracts', ['pendingOrders'=>$finalArray,'date'=>$date])->setPaper('a4', 'portrait')->setWarnings(false)->stream('pending Contracts.pdf');
        } else {
            $groupArray = [];
            if ($group == 'count') {
                foreach ($finalArray as $key => $value) {
                    if ($value['name'] != 'buffer') {
                        $groupArray[$value['code']][] = $value;
                    }
                }
                ksort($groupArray);
                return PDF::loadView('reports.sales.pending-contracts-by-group-two', ['pendingOrders'=>$groupArray,'date'=>$date,'type'=>'group'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('pending Contracts.pdf');
            } else {
                foreach ($finalArray as $key => $value) {
                    if ($value['name'] != 'buffer') {
                        $groupArray[$value['name']][] = $value;
                    }
                }
                ksort($groupArray);
                return PDF::loadView('reports.sales.pending-contracts-by-group', ['pendingOrders'=>$groupArray,'date'=>$date,'type'=>'group'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('pending Contracts.pdf');
            }
            // dd($groupArray);
            // return view('reports.sales.pending-contracts-by-group',['pendingOrders'=>$groupArray,'date'=>$date,'type'=>'group']);
        }
    }

    public function generatePackingClearance($id)
    {
        $packingClearance = PackagingClearance::where('id', $id)->first();
        $packingClearanceResult = $packingClearance->packingClearanceResult;

        if ($packingClearanceResult == null) {
            return abort(404);
        }

//            return view('reports.quality.packing-clearance')->with(compact('packingClearance','packingClearanceResult'));

        return PDF::loadView(
            'reports.quality.packing-clearance',
            [
                'packingClearance' => $packingClearance,
                'packingClearanceResult' => $packingClearanceResult,

            ]
        )
            ->setPaper('a4', 'landscape')->setWarnings(false)->stream('Packing Clearance.pdf');
    }

    public function customerSummary(Request $request)
    {
        $date = Carbon::now();
        // $start = date('Y-m-d',strtotime($request['from-date']));
        // $end = date('Y-m-d',strtotime($request['to-date']));
        $saleOrders = SaleOrder::where('bp_id', $request['bp'])
                                ->where('order_quantity', '>', 0)
                                // ->whereBetween('order_date', [$start, $end])
                                ->get();
        $finalArray = [];
        $count = 0;
        foreach ($saleOrders as $key => $value) {
            if (($value->order_quantity != $value->pre_close) && !empty($value->indentDetails->indentMaster['dated_on'])) {
                $state = 'dispatched';
                if ($value['pending_quantity'] > 0) {
                    $state = 'pending';
                }
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['code'] = $value->material_name;
                if (!is_null($value->indentDetails)) {
                    $finalArray[$value->businessPartner['bp_name']][$state][$count]['rate'] = $value->indentDetails['rate_per_kg'];
                    $finalArray[$value->businessPartner['bp_name']][$state][$count]['contract'] = $value->indentDetails->indentMaster['order_ref_no'];
                } else {
                    $finalArray[$value->businessPartner['bp_name']][$state][$count]['rate'] = null;
                    $finalArray[$value->businessPartner['bp_name']][$state][$count]['contract'] = null;
                }
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['qty'] = $value['order_quantity'];
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['delivered'] = $value['dispatch_quantity'];
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['balance'] = $value['pending_quantity'];
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['date'] = $value->indentDetails->indentMaster['dated_on'] ?? null;
                $finalArray[$value->businessPartner['bp_name']][$state][$count]['state'] = $value->businessPartner['state'];
                $count++;
            }
        }
        // dd($finalArray);
        return PDF::loadView('reports.sales.pending-contracts-by-group', ['pendingOrders'=>$finalArray,'date'=>$date,'type'=>'customer'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Customer Summary.pdf');
        // return view('reports.sales.pending-contracts-by-group',['pendingOrders'=>$finalArray,'date'=>$date,'type'=>'customer']);
    }


    public function getPackageSlip($id)
    {
        $invoice = InvoiceMaster::where('id', $id)->first();

        $packages = $invoice->packageMaster()->get()->groupBy('material_id');

        return PDF::loadView('reports.invoice.package-slip', ['invoice' => $invoice, 'packages' =>$packages])->setPaper('a4', 'portrait')->setWarnings(false)->stream('pending Contracts.pdf');
    }


    public function dipSolution($id)
    {
        $dipSolutionClearance = DipSolutionClearance::where('id', $id)->first();
        $dipSolutionClearanceResult = $dipSolutionClearance-> dipSolutionClearanceResult;



        return PDF::loadView(
            'reports.quality.dip-solution-clearance',
            [
                'clearance' => $dipSolutionClearance,
                'clearanceResult' => $dipSolutionClearanceResult
            ]
        )
            ->setPaper('a4', 'portrait')->setWarnings(false)->stream('Dip Solution Clearance.pdf');
    }

    public function getDayWisePack(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $packageMaster = PackageMaster::where('packed_date', $start)->with('weightLogs')->get();

        $boxCount = $packageMaster->count();
        $packages = [];
        $count = 0;
        $totalNetWeight = 0;
        $totalGrossWeight = 0;
        foreach ($packageMaster as $value) {
            $netWeight = round(($value->weightLogs->sum('material_weight') + $value->moisture_weight), 1);
            $grossWeight = round(($value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight), 1);

            $totalNetWeight += $netWeight;
            $totalGrossWeight += $grossWeight;

            $first = $value->weightLogs->first();

            $packages[$count]['case_no']=$value->case_no;
            $packages[$count]['floor_code']=$first->floor_code;
            $packages[$count]['doff_no']=$first->doff_no;
            $packages[$count]['net_weight']=$netWeight;
            $packages[$count]['gross_weight']=$grossWeight;
            $packages[$count]['bobbin_count']=$value->bobbin_count;
            $packages[$count]['machine']=$first->machine;
            $packages[$count]['box_type']=$value->box_type;
            $packages[$count]['package_type']=$value->package_type;
            foreach ($value->weightLogs as $weight) {
                $packages[$count]['spindle'][]=$weight->spindle;
            }
            $count++;
        }
        // dd($packages);
        return PDF::loadView('reports.invoice.day-wise-pack', [
        'packageMaster'=>$packages,
        'start'=>$request['start_date'],
        'boxCount'=>$boxCount,
        'netWeight'=>$totalNetWeight,
        'grossWeight'=>$totalGrossWeight
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Day Wise Packed Entry.pdf');
    }

    public function getBoxUsage(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $end = date('Y-m-d', strtotime($request['end_date']));
        $packageMaster = PackageMaster::whereBetween('packed_date', [$start,$end])->get();
        $boxUsage = [];

        foreach ($packageMaster as $value) {
            if (empty($boxUsage[$value->box_type])) {
                $boxUsage[$value->box_type] = 1;
            } else {
                $boxUsage[$value->box_type] += 1;
            }
        }

        ksort($boxUsage);

        return PDF::loadView('reports.invoice.box-usage', [
        'boxUsage'=>$boxUsage,
        'start'=>$request['start_date'],
        'end'=>$request['end_date']
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Box Usage Summary.pdf');
    }

    public function dispatchByDate(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $end = date('Y-m-d', strtotime($request['end_date']));
        $packId = InvoiceMaster::whereBetween('date', [$start,$end])
                              ->join('package_masters', 'invoice_masters.id', '=', 'package_masters.invoice_master_id')
                              ->pluck('package_masters.id');

        $packageMasters = PackageMaster::whereIn('id', $packId)->get();


//        dd($packageMasters);


        $dispatch = [];
        $total = 0;

        foreach ($packageMasters as $pack) {

//            dd($pack->weightLogs);

            $materialWeight = $pack->weightLogs->sum('material_weight');

//            $moisture = round($pack->moisture_weight/$pack->weightLogs->count(), 2);
            $total += $materialWeight + $pack->moisture_weight;

            $material = $pack->weightLogs->first()->material;

            if (!empty($dispatch[$material]['value'])) {
                $dispatch[$material]['value'] += $materialWeight + $pack->moisture_weight;
            } else {
                $dispatch[$material]['value'] = $materialWeight + $pack->moisture_weight;
                $dispatch[$material]['material'] = $pack->weightLogs->first()->floor_code;
            }



//            foreach ($pack->weightLogs as $weightLog) {
//
//                $moisture = round($pack->moisture_weight/$pack->weightLogs->count(), 2);
//                $total += $weightLog->material_weight + $moisture;
//
//                if (!empty($dispatch[$weightLog->material]['value'])) {
//                    $dispatch[$weightLog->material]['value'] += $weightLog->material_weight + $moisture;
//                } else {
//                    $dispatch[$weightLog->material]['value'] = $weightLog->material_weight + $moisture;
//                    $dispatch[$weightLog->material]['material'] = $weightLog->floor_code;
//                }
//            }
        }

        ksort($dispatch);






//        $weightLogs = WeightLog::whereIn('package_master_id', $packId)->get();
//
//        $dispatch = [];
//        $total = 0;
//
//        foreach ($weightLogs as $weightLog) {
//
//            $moisture = round($weightLog->packageMaster->moisture_weight/$weightLog->packageMaster->weightLogs->count(), 2);
//            $total += $weightLog->material_weight + $moisture;
//
//            if (!empty($dispatch[$weightLog->material]['value'])) {
//                $dispatch[$weightLog->material]['value'] += $weightLog->material_weight + $moisture;
//            } else {
//                $dispatch[$weightLog->material]['value'] = $weightLog->material_weight + $moisture;
//                $dispatch[$weightLog->material]['material'] = $weightLog->floor_code;
//            }
//        }
//        ksort($dispatch);





        return PDF::loadView('reports.dispatch.dispatch-summary', [
        'dispatch'=>$dispatch,
        'total'=>$total,
        'start'=>$request['start_date'],
        'end'=>$request['end_date']
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Dispatch Summary.pdf');
    }


    public function dispatchByCustomer(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'customer' => 'required'
        ]);


//        $this->allInvoiceDetails($request);

        $businessPartner = BusinessPartner::where('id', $request['customer'])->first();


        $invoices = $businessPartner->invoice()->whereBetween('date', [
            date('Y-m-d', strtotime($request['start_date'])),
            date('Y-m-d', strtotime($request['end_date']))
        ])->with('indent')->orderBy('date')->get();

        $finalResult = [];

        foreach ($invoices as $invoice) {
            $indentMaster = $invoice->indent;


            $packageMastersId = PackageMaster::where('invoice_master_id', $invoice->id)->pluck('id');

            $packageMasters = PackageMaster::whereIn('id', $packageMastersId)->get()->groupBy('material_id');

            $orderDetails = [];
            $taxDetails = [];

            foreach ($packageMasters as $material => $packageMaster) {
                $orderDetails[$material]['weight'] = 0;

                foreach ($packageMaster as $package) {
                    $indentDetails =  $indentMaster->indentDetails()->where('material_id', $material)->first();

                    $orderDetails[$material]['name'] = SaleOrder::where('id', $indentDetails->order_id)->value('material_name');
                    $orderDetails[$material]['weight'] += $package->weightLogs->sum('material_weight') + $package->moisture_weight;
                    $orderDetails[$material]['hsn'] = $indentDetails->hsn_sac;
                    $orderDetails[$material]['count'] = $package->weightLogs->count();
                    $orderDetails[$material]['gst'] = $indentDetails->gst_rate;
                    $orderDetails[$material]['rate'] = $indentDetails->rate_per_kg;
                    $orderDetails[$material]['no_box'] = $packageMaster->count();
                }

                $orderDetails[$material]['value'] = $orderDetails[$material]['weight'] * $orderDetails[$material]['rate'];
            }

            $finalResult[$invoice->id] = $orderDetails;
        }

        $totalBilled = 0;
        $masterArray = [];

        foreach ($finalResult as $invoiceId => $dispatch) {
            $invoice = InvoiceMaster::where('id', $invoiceId)->first();


            $totalAmount = array_sum(array_column($dispatch, 'value'));


            foreach ($dispatch as $dis) {
                $tempHandlingCharges = ($invoice->handling_charges / $totalAmount) * $dis['value'];
                $insuranceCost = ((($invoice->indent->transit_insurance / 100) * $totalAmount) / $totalAmount) * $dis['value'];

                $gst = $dis['gst'] / 100 * ($dis['value'] + $tempHandlingCharges + $insuranceCost);
                $grantTotal = $dis['value'] + $tempHandlingCharges + $insuranceCost + $gst;
                $totalBilled += $grantTotal;


                $masterArray[] = [
                        'invoice_no' => $invoice->invoice_no,
                        'invoice_date' => date('d-m-Y', strtotime($invoice->date)),
                        'item_name' => $dis['name'],
                        'weight' => $dis['weight'],
                        'rate' => $dis['rate'],
                        'value' => $dis['value'],
                        'insurance' => $insuranceCost,
                        'handling_charges' => $tempHandlingCharges,
                        'gst' => $gst,
                        'grant_total' => $grantTotal,
                        'ref_no' => $invoice->indent->order_ref_no,
                    ];
            }
        }



        $keys = array_column($masterArray, 'item_name');
        array_multisort($keys, SORT_ASC, $masterArray);

        $newArray = [];

        foreach ($masterArray as $key => $array) {
            $newArray[$array['item_name']][$key] = $array;
        }

        ksort($newArray, SORT_NUMERIC);

        $subDetails = [];

        foreach ($newArray as $material => $group) {
            $subDetails[$material]['total_weight'] = array_sum(array_column($group, 'weight'));
            $subDetails[$material]['sub_total'] = array_sum(array_column($group, 'grant_total'));
        }


        return PDF::loadView('reports.dispatch.customer-wise-dispatch', [
            'masterArray'=>$newArray,
            'subDetails' => $subDetails,
            'start'=>$request['start_date'],
            'end'=>$request['end_date'],
            'businessPartner' => $businessPartner,
            'totalBilled' => $totalBilled
        ])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Dispatch Summary.pdf');
    }

    public function productionSummary(Request $request)
    {
        $start = date('Y-m-d 00:00:00', strtotime($request['start_date']));
        $end = date('Y-m-d 23:59:59', strtotime($request['end_date']));

        $weightLog = WeightLog::whereBetween('doff_date', [$start,$end])->get();
        $productionSummary = [];
        $total = [];
        foreach ($weightLog as $value) {
            $floor_code = trim($value->floor_code);
            if (empty($total[$value->machine])) {
                $total[$value->machine] = $value->material_weight;
            } else {
                $total[$value->machine] += $value->material_weight;
            }
            if (empty($productionSummary[$value->machine][$value->material][$floor_code])) {
                $productionSummary[$value->machine][$value->material][$floor_code] = $value->material_weight;
            } else {
                $productionSummary[$value->machine][$value->material][$floor_code] += $value->material_weight;
            }
        }

        return PDF::loadView('reports.dispatch.production-summary', [
        'productionSummary'=>$productionSummary,
        'total'=>$total,
        'start'=>$request['start_date'],
        'end'=>$request['end_date']
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Production Summary.pdf');
    }


    public function getCumulativeDispatch(Request $request)
    {
        set_time_limit(0);
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);



        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',

        ]);


        $finalArray = [];
        $totalBilled = 0;

        $invoices = InvoiceMaster::whereBetween('date', [
            date('Y-m-d', strtotime($request['start_date'])),
            date('Y-m-d', strtotime($request['end_date']))
        ])->get()->groupBy('bp_id')->toArray();

        $businessPartners = BusinessPartner::whereIn('id', array_keys($invoices))->get();

        foreach ($businessPartners as $businessPartner) {
            $invoices = $businessPartner->invoice()->whereBetween('date', [
                date('Y-m-d', strtotime($request['start_date'])),
                date('Y-m-d', strtotime($request['end_date']))
            ])->with('indent')->orderBy('date')->get();

            $finalResult = [];


            foreach ($invoices as $invoice) {
                $indentMaster = $invoice->indent;


                $packageMastersId = PackageMaster::where('invoice_master_id', $invoice->id)->pluck('id');

                $packageMasters = PackageMaster::whereIn('id', $packageMastersId)->get()->groupBy('material_id');

                $orderDetails = [];

                foreach ($packageMasters as $material => $packageMaster) {
                    $orderDetails[$material]['weight'] = 0;

                    foreach ($packageMaster as $package) {
                        $indentDetails = $indentMaster->indentDetails()->where('material_id', $material)->first();

                        $orderDetails[$material]['item_name'] = SaleOrder::where('id', $indentDetails->order_id)->value('material_name');
                        $orderDetails[$material]['weight'] += $package->weightLogs->sum('material_weight') + $package->moisture_weight;
                        $orderDetails[$material]['hsn'] = $indentDetails->hsn_sac;
                        $orderDetails[$material]['count'] = $package->weightLogs->count();
                        $orderDetails[$material]['gst_value'] = $indentDetails->gst_rate;
                        $orderDetails[$material]['rate'] = $indentDetails->rate_per_kg;
                        $orderDetails[$material]['no_box'] = $packageMaster->count();
                    }

                    $totalAmount = $orderDetails[$material]['weight'] * $orderDetails[$material]['rate'];

                    $orderDetails[$material]['value'] = $totalAmount;



                    $orderDetails[$material]['invoice_no'] = $invoice->invoice_no;
                    $orderDetails[$material]['invoice_date'] = date('d-m-Y', strtotime($invoice->date));
                    $orderDetails[$material]['ref_no'] = $invoice->indent->order_ref_no;
                }



                foreach ($orderDetails as $material => $od) {
                    $invoiceValue = array_sum(array_column($orderDetails, 'value'));

                    $tempHandlingCharges = ($invoice->handling_charges / $invoiceValue) * $od['value'];
                    $insuranceCost = ((($invoice->indent->transit_insurance / 100) * $invoiceValue) / $invoiceValue) * $od['value'];

                    $gst = $orderDetails[$material]['gst_value'] / 100 * ($od['value'] + $tempHandlingCharges + $insuranceCost);
                    $grantTotal = $od['value'] + $tempHandlingCharges + $insuranceCost + $gst;
                    $totalBilled += $grantTotal;

                    $orderDetails[$material]['handling_charges'] = $tempHandlingCharges;
                    $orderDetails[$material]['insurance'] = $insuranceCost;
                    $orderDetails[$material]['gst'] = $gst;
                    $orderDetails[$material]['grant_total'] = $grantTotal;
                }

                if (!empty($orderDetails)) {
                    $finalResult[] = $orderDetails;
                }
            }


            if (!empty($finalResult)) {
                $finalArray[$businessPartner->bp_name] = $finalResult;
            }
        }


        $masterArrays = $finalArray;
        $start = $request['start_date'];
        $end = $request['end_date'];


        return view('reports.test.test')->with(compact('masterArrays', 'start', 'end'));


        // return PDF::loadView('reports.test.test', [
        //     'masterArrays' => $finalArray,
        //     'start' => $request['start_date'],
        //     'end' => $request['end_date'],
        // ])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Cumulative Dispatch Summary'.date('d-m-Y', strtotime($request['start_date'])).'-'.date('d-m-Y', strtotime($request['end_date'])).'.pdf');
    }


    public function getCumulativeDispatchTwo(Request $request)
    {
        set_time_limit(0);
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 0);
        
        $master = InvoiceMaster::whereBetween('date', [date('Y-m-d', strtotime($request['start_date'])),date('Y-m-d', strtotime($request['end_date']))])
                                ->with('Weightlogs', 'indent', 'businessPartner', 'packageMaster')
                                ->get();
        $finalArray = [];
        $totalBilled = 0;

        foreach ($master as $invoice) {
            $packageMasters = $invoice->packageMaster;
            $weightLogs = $invoice->Weightlogs;
            $indentMater = $invoice->indent;
            $businessPartner = $invoice->businessPartner;
            $indentDetails = $indentMater->indentDetails;


            foreach ($indentDetails as $indent) {
                $finalData = [];
                $saleOrder = $indent->saleOrder;
                $finalData[$saleOrder->material_id]['item_name'] = $saleOrder->material_name;
                $finalData[$saleOrder->material_id]['weight'] = 0;
                $finalData[$saleOrder->material_id]['count'] = 0;
                $finalData[$saleOrder->material_id]['no_box'] = 1;
                $finalData[$saleOrder->material_id]['hsn'] = $indent->hsn_sac;
                $finalData[$saleOrder->material_id]['gst_value'] = $indent->gst_rate;
                $finalData[$saleOrder->material_id]['rate'] = $indent->rate_per_kg;
                $finalData[$saleOrder->material_id]['invoice_no'] = $invoice->invoice_no;
                $finalData[$saleOrder->material_id]['invoice_date'] = date('d-m-Y', strtotime($invoice->date));
                $finalData[$saleOrder->material_id]['ref_no'] = $indentMater->order_ref_no;
                $finalData[$saleOrder->material_id]['handling_charges'] = $invoice->handling_charges;
                $finalData[$saleOrder->material_id]['transit_insurance'] = $indentMater->transit_insurance;


                foreach ($packageMasters as $package) {
                    $inBox = $weightLogs->where('package_master_id', $package->id)->where('material_id', $saleOrder->material_id);

                    if ($inBox->count() > 0) {
                        $finalData[$saleOrder->material_id]['weight'] += $inBox->sum('material_weight');
                        $finalData[$saleOrder->material_id]['count'] += $inBox->count();
                        $finalData[$saleOrder->material_id]['no_box']++;
                    }
                }

                $totalAmount = $finalData[$saleOrder->material_id]['weight'] * $finalData[$saleOrder->material_id]['rate'];
                $finalData[$saleOrder->material_id]['value'] = $totalAmount;

                if ($finalData[$saleOrder->material_id]['weight'] > 0) {
                    foreach ($finalData as $material => $od) {
                        $invoiceValue = array_sum(array_column($finalData, 'value'));
    
                        $tempHandlingCharges = ($finalData[$material]['handling_charges'] / $invoiceValue) * $od['value'];
                        $insuranceCost = ((($finalData[$material]['transit_insurance'] / 100) * $invoiceValue) / $invoiceValue) * $od['value'];
    
                        $gst = $finalData[$material]['gst_value'] / 100 * ($od['value'] + $tempHandlingCharges + $insuranceCost);
                        $grantTotal = $od['value'] + $tempHandlingCharges + $insuranceCost + $gst;
                        $totalBilled += $grantTotal;
    
                        $finalData[$material]['handling_charges'] = $tempHandlingCharges;
                        $finalData[$material]['insurance'] = $insuranceCost;
                        $finalData[$material]['gst'] = $gst;
                        $finalData[$material]['grant_total'] = $grantTotal;
                    }
                    if (!empty($finalData)) {
                        $finalResult[$businessPartner->bp_name][] = $finalData;
                    }
                }
            }
        }

        ksort($finalResult);
        $masterArrays = $finalResult;
        $start = $request['start_date'];
        $end = $request['end_date'];


        return view('reports.test.test')->with(compact('masterArrays', 'start', 'end'));
    }

    public function getCountPack(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $packageMaster = PackageMaster::where('packed_date', $start)->with('weightLogs')->get();

        $boxCount = $packageMaster->count();
        $packages = [];
        $count = 0;
        $totalNetWeight = 0;
        $totalGrossWeight = 0;
        foreach ($packageMaster as $value) {
            $netWeight = round(($value->weightLogs->sum('material_weight') + $value->moisture_weight), 1);
            $grossWeight = round(($value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight), 1);

            $totalNetWeight += $netWeight;
            $totalGrossWeight += $grossWeight;

            $first = $value->weightLogs->first();

            $packages[$first->floor_code][$count]['case_no']=$value->case_no;
            $packages[$first->floor_code][$count]['floor_code']=$first->floor_code;
            $packages[$first->floor_code][$count]['doff_no']=$first->doff_no;
            $packages[$first->floor_code][$count]['net_weight']=$netWeight;
            $packages[$first->floor_code][$count]['gross_weight']=$grossWeight;
            $packages[$first->floor_code][$count]['bobbin_count']=$value->bobbin_count;
            $packages[$first->floor_code][$count]['machine']=$first->machine;
            $packages[$first->floor_code][$count]['box_type']=$value->box_type;
            $packages[$first->floor_code][$count]['package_type']=$value->package_type;
            foreach ($value->weightLogs as $weight) {
                $packages[$first->floor_code][$count]['spindle'][]=$weight->spindle;
            }
            $count++;
        }
        ksort($packages);
        // dd($packages);
        return PDF::loadView('reports.invoice.count-wise-pack', [
        'packageMaster'=>$packages,
        'start'=>$request['start_date'],
        'boxCount'=>$boxCount,
        'netWeight'=>$totalNetWeight,
        'grossWeight'=>$totalGrossWeight
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Count Wise Packed Entry.pdf');
    }


    public function getAsOnPacked(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $packageMaster = PackageMaster::where('packed_date', '<=', $start)->where('material_id', $request['material'])->where('invoice_status', 0)->with('weightLogs')->get();


        $boxCount = $packageMaster->count();
        $packages = [];
        $count = 0;
        $totalNetWeight = 0;
        $totalGrossWeight = 0;
        foreach ($packageMaster as $value) {
            $netWeight = round(($value->weightLogs->sum('material_weight') + $value->moisture_weight), 1);
            $grossWeight = round(($value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight), 1);

            $totalNetWeight += $netWeight;
            $totalGrossWeight += $grossWeight;

            $first = $value->weightLogs->first();
//            dd($first);

            if (!is_null($first)) {
                $packages[$first->floor_code][$count]['case_no']=$value->case_no;
                $packages[$first->floor_code][$count]['floor_code']=$first->floor_code;
                $packages[$first->floor_code][$count]['packed_date'] = date('d-m-Y', strtotime($value->packed_date));
                $packages[$first->floor_code][$count]['doff_no']=$first->doff_no;
                $packages[$first->floor_code][$count]['net_weight']=$netWeight;
                $packages[$first->floor_code][$count]['gross_weight']=$grossWeight;
                $packages[$first->floor_code][$count]['bobbin_count']=$value->bobbin_count;
                $packages[$first->floor_code][$count]['machine']=$first->machine;
                $packages[$first->floor_code][$count]['box_type']=$value->box_type;
                $packages[$first->floor_code][$count]['package_type']=$value->package_type;
                foreach ($value->weightLogs as $weight) {
                    $packages[$first->floor_code][$count]['spindle'][]=$weight->spindle;
                }
                $count++;
            }
        }
        ksort($packages);
        // dd($packages);


        return PDF::loadView('reports.invoice.as-on-packed', [
            'packageMaster'=>$packages,
            'start'=>$request['start_date'],
            'boxCount'=>$boxCount,
            'netWeight'=>$totalNetWeight,
            'grossWeight'=>$totalGrossWeight
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('As on Packed Entry.pdf');
    }

    public function getMaterialPack(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $end = date('Y-m-d', strtotime($request['end_date']));
        $packageMaster = PackageMaster::whereBetween('packed_date', [$start,$end])
                                    ->where('material_id', $request['material'])
                                    ->with('weightLogs')
                                    ->get();

        $boxCount = $packageMaster->count();
        $packages = [];
        $count = 0;
        $totalNetWeight = 0;
        $totalGrossWeight = 0;
        foreach ($packageMaster as $value) {
            $netWeight = round(($value->weightLogs->sum('material_weight') + $value->moisture_weight), 1);
            $grossWeight = round(($value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight), 1);

            $totalNetWeight += $netWeight;
            $totalGrossWeight += $grossWeight;

            $first = $value->weightLogs->first();

            $packages[$count]['case_no']=$value->case_no;
            $packages[$count]['floor_code']=$first->floor_code;
            $packages[$count]['doff_no']=$first->doff_no;
            $packages[$count]['net_weight']=$netWeight;
            $packages[$count]['gross_weight']=$grossWeight;
            $packages[$count]['bobbin_count']=$value->bobbin_count;
            $packages[$count]['machine']=$first->machine;
            $packages[$count]['box_type']=$value->box_type;
            $packages[$count]['package_type']=$value->package_type;
            foreach ($value->weightLogs as $weight) {
                $packages[$count]['spindle'][]=$weight->spindle;
            }
            $count++;
        }
        // ksort($packages);
        // dd($packages);
        return PDF::loadView('reports.invoice.material-packs', [
        'packageMaster'=>$packages,
        'start'=>$request['start_date'],
        'end'=>$request['end_date'],
        'boxCount'=>$boxCount,
        'netWeight'=>$totalNetWeight,
        'grossWeight'=>$totalGrossWeight
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Material Packed Entry.pdf');
    }



    public function dispatchByCount(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'customer' => 'required'
        ]);
    }

    public function getDayProductionSummary(Request $request)
    {
        $start = date('Y-m-d', strtotime($request['start_date']));
        $end = date('Y-m-d', strtotime($request['end_date']));
        $packageMaster = PackageMaster::whereBetween('packed_date', [$start,$end])
                                    ->where('material_id', $request['material'])
                                    ->where('invoice_status', 0)
                                    ->with('weightLogs')
                                    ->get();
        $packSummary = [];
        foreach ($packageMaster as $info) {
            $doff = '';
            $firstWeight = $info->weightLogs->first();
            if (is_null($info->reason)) {
                $doff = $firstWeight->doff_no;
            } else {
                $doff =  $info->reason;
            }
            // dd($info->weightLogs->sum('total_weight') + $info->moisture_weight + $info->inset_weight, $info->weightLogs->sum('total_weight'));
            if (empty($packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament])) {
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['material_weight'] = $info->weightLogs->sum('material_weight') + $info->moisture_weight;
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['total_weight'] = ($info->weightLogs->sum('total_weight') + $info->moisture_weight + $info->inset_weight + $info->box_weight);
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['spindel'] = $info->weightLogs->count();
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['box'] = 1;
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['packed_date'] = $info->packed_date;
            } else {
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['material_weight'] += $info->weightLogs->sum('material_weight') + $info->moisture_weight;
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['total_weight'] += ($info->weightLogs->sum('total_weight') + $info->moisture_weight + $info->inset_weight + $info->box_weight);
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['spindel'] += $info->weightLogs->count();
                $packSummary[$firstWeight->floor_code][$doff][$firstWeight->filament]['box'] += 1;
            }
        }
        // dd($packSummary);
        return PDF::loadView('reports.dispatch.day-wise-production-summary', [
        'packSummary'=>$packSummary,
        'start'=>$request['start_date'],
        'end'=>$request['end_date'],
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Defect Reports.pdf');
    }


    public function generateDefectReport(Request $request)
    {
        $request->validate([
            'start-date' => 'required',
            'end-date' => 'required',
            'defect' => 'required',
        ]);

        $startDate = date('Y-m-d H:i:s', strtotime($request['start-date']));
        $endDate = date('Y-m-d H:i:s', strtotime($request['end-date']));

        if ($request['defect'] == 'all') {
            $weightLogs = WeightLog::where('weight_status', 0)
                                    ->where('machine', $request['machine'])
                                    ->whereBetween('doff_date', [$startDate, $endDate])
                                    ->where(function ($q) {
                                        $q->where('reason', '!=', null)
                                        ->orWhere('inspection_reason', '!=', null);
                                    })
                                    ->orderBy('doff_date')
                                    ->get()
                                    ->groupBy('reason');


//            dd($weightLogs);
        } else {
            $defect = $request['defect'];
            $weightLogs = WeightLog::where('weight_status', 0)
                                    ->where('machine', $request['machine'])
                                    ->whereBetween('doff_date', [$startDate, $endDate])
                                    ->where(function ($q) use ($defect) {
                                        $q->where('reason', $defect)
                                        ->orWhere('inspection_reason', $defect);
                                    })
                                    // ->where('reason', $request['defect'])
                                    ->orderBy('doff_date')
                                    ->get()
                                    ->groupBy('reason');
        }


//        dd($weightLogs);


        $result = [];



        foreach ($weightLogs as $reason => $weightLog) {
            foreach ($weightLog as $wl) {

//                $weightLogsCount = WeightLog::where('unique_id', $wl->unique_id)->get();


                if ($wl->reason != null || $wl->inspection_reason) {
                    if (empty($result[$reason][$wl->material])) {
                        $result[$reason][$wl->material][$wl->unique_id] = $wl;
                    } else {
                        if (!array_key_exists($wl->unique_id, $result[$reason][$wl->material])) {
                            $result[$reason][$wl->material][$wl->unique_id] = $wl;
                        }
                    }
                }
            }
        }

        unset($result['RE-WINDING']);
//        dd($result);


        return PDF::loadView('reports.production.defect', [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'defects' => $result,
            'machine' => $request['machine'],
        ])->setPaper('a4', 'portrait')->setWarnings(false)
            ->stream('Defects Report ('.date('d-m-Y', strtotime($startDate)).' to '.date('d-m-Y', strtotime($endDate)).') -'.$request['machine'].'-'.'.pdf');
    }


    public function generateNcrReport(Request $request)
    {
        $request->validate([
            'start-date' => 'required',
            'end-date' => 'required',
            'machine' => 'required',
        ]);


        $startDate = date('Y-m-d 00:00:00', strtotime($request['start-date']));
        $endDate = date('Y-m-d 23:59:59', strtotime($request['end-date']));


        $weightLogs = WeightLog::where('weight_status', 0)
            ->where('machine', $request['machine'])
            ->whereBetween('doff_date', [$startDate, $endDate])
            ->where('ncr_status', 1)
            ->where('active_status', 1)
            ->where('reason', '!=', null)
            ->orderBy('doff_date')
            ->get()
            ->groupBy('reason');





        $result = [];



        foreach ($weightLogs as $reason => $weightLog) {
            foreach ($weightLog as $wl) {
                $result[$reason][$wl->material][$wl->unique_id] = $wl;
            }
        }


        return PDF::loadView('reports.production.defect', [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'defects' => $result,
            'machine' => $request['machine'],
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('NCR Report.pdf');
    }

    public function packedCase(Request $request)
    {
        $startDate = date('Y-m-d', strtotime($request['start_date']));
        $endDate = date('Y-m-d', strtotime($request['end_date']));
        $packedMaster = PackageMaster::where('material_type', $request['type'])
                                    ->whereBetween('packed_date', [$startDate,$endDate])
                                    ->where('invoice_status', 0)
                                    ->with('weightLogs')
                                    ->get();
        $processed = [];

        foreach ($packedMaster as $value) {
            if ($value->weightLogs->count() > 1) {
                // dd($value->toArray());
            }
//            foreach ($value->weightLogs as $weightLog) {
//                if (empty($processed[$weightLog->packing_name][$value->case_no][$value->packed_date])) {
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['doff_no'] = $weightLog->doff_no;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['doff_date'] = $weightLog->doff_date;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['spindle'] = $value->bobbin_count;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['box_type'] = $value->box_type;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['info'] = $value->reason;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['net_wt'] = $weightLog->material_weight + $value->moisture_weight;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['gw_wt'] = round(($weightLog->total_weight + $value->box_weight + $value->inset_weight), 1);
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['moisture_weight'] = $value->moisture_weight;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['box_weight'] = $value->box_weight;
//                } else {
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['net_wt'] += $weightLog->material_weight + $value->moisture_weight;
//                    $processed[$weightLog->packing_name][$value->case_no][$value->packed_date]['gw_wt'] += $weightLog->total_weight + $value->box_weight + $value->inset_weight;
//                }
//
            ////                if($value->case_no == '35367'){
            ////                    dd($value->weightLogs->sum('material_weight')+ $value->moisture_weight, $value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight);
            ////                }
//            }



//            foreach ($value->weightLogs as $weightLog) {
            $first = $value->weightLogs->first();

//                dd($first);
            if (empty($processed[$first->packing_name][$value->case_no][$value->packed_date])) {
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['doff_no'] = $first->doff_no;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['doff_date'] = $first->doff_date;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['spindle'] = $value->bobbin_count;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['box_type'] = $value->box_type;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['info'] = $value->reason;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['net_wt'] = $value->weightLogs->sum('material_weight') + $value->moisture_weight;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['gw_wt'] = round(($value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight), 1);
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['moisture_weight'] = $value->moisture_weight;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['box_weight'] = $value->box_weight;
            } else {
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['net_wt'] += $value->weightLogs->sum('material_weight') + $value->moisture_weight;
                $processed[$first->packing_name][$value->case_no][$value->packed_date]['gw_wt'] += $value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight;
            }

//                if($value->case_no == '35367'){
//                    dd($value->weightLogs->sum('material_weight')+ $value->moisture_weight, $value->weightLogs->sum('total_weight') + $value->box_weight + $value->inset_weight);
//                }
//            }
        }


//        dd($processed);

        return view('reports.production.packed-case', [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'type' => $request['type'],
            'processed' => $processed,
        ]);


        // dd($processed);

        return PDF::loadView('reports.production.packed-case', [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'type' => $request['type'],
            'processed' => $processed,
        ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Packed Stock Report.pdf');
    }


    public function productionReportOk()
    {
        $weightLogs = WeightLog::where('active_status', 1)
                                ->where('machine', '!=', 'kidde')
                                ->where('weight_status', 1)
                                ->where('package_master_id', null)
                                ->get();
        
        $productionStatus  = [];


        foreach ($weightLogs as $weightLog) {
            if ($weightLog->inspection_status == 1) {
                $productionStatus[$weightLog->material][$weightLog->doff_no]['avp'][] = $weightLog;
            } elseif ($weightLog->weight_status == 1) {
                $productionStatus[$weightLog->material][$weightLog->doff_no]['avi'][] = $weightLog;
            }
        }
        $count = $weightLogs->count();
        // dd($productionStatus);
        // return PDF::loadView('reports.production.production-stock', [
        //     'productionStatus' => $productionStatus
        // ])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Production Report.pdf');
        return view('reports.production.production-stock')->with(compact('productionStatus', 'count'));
    }


    public function productionReportNotOk()
    {
        $weightLogs = WeightLog::where('active_status', 1)
                                ->where('machine', '!=', 'kidde')
                                ->where('package_master_id', null)
                                ->where('weight_status', 0)
                                ->where('ncr_status', 0)
                                ->get();
        foreach ($weightLogs as $weightLog) {
            $productionStatus[$weightLog->material][$weightLog->doff_no][] = $weightLog;
        }
        // dd($productionStatus);
        $count = $weightLogs->count();

        return view('reports.production.production-stock-not-ok')->with(compact('productionStatus', 'count'));
    }

    public function productionReportNcr()
    {
        $weightLogs = WeightLog::where('active_status', 1)
                                ->where('machine', '!=', 'kidde')
                                ->where('package_master_id', null)
                                ->where('weight_status', 0)
                                ->where('ncr_status', 1)
                                ->get();
        foreach ($weightLogs as $weightLog) {
            $productionStatus[$weightLog->material][$weightLog->doff_no][] = $weightLog;
        }
        // dd($productionStatus);
        $count = $weightLogs->count();

        return view('reports.production.production-stock-not-ok')->with(compact('productionStatus', 'count'));
    }


    public function generateDoffwiseDefect(Request $request)
    {
        $request->validate([
            'start-date' => 'required',
            'end-date' => 'required',
            'material' => 'required',
            'machine' => 'required',
        ]);

        $startDate = date('Y-m-d 00:00:00', strtotime($request['start-date']));
        $endDate = date('Y-m-d 23:59:59', strtotime($request['end-date']));


        $weightLogs = WeightLog::where('machine', $request['machine'])
            ->where('material_id', $request['material'])
            ->whereBetween('doff_date', [$startDate, $endDate])
            ->orderBy('doff_date')
            ->get()
            ->groupBy('doff_no');


//        dd($weightLogs);

        $result =[];

        $direction = ['E','W'];

        $totalSpindles = [];

        $finalResult = [];

        foreach ($direction as $d) {
            for ($i=1;$i<=30;$i++) {
                $wl = new WeightLog();
                $wl->spindle = $d.$i;
                $wl->reason = '';
                $wl->inspection_reason = '';
                $wl->material_weight = 0;
                $totalSpindles[$d.$i] = $wl;
            }
        }

//        dd($totalSpindles);

        foreach ($weightLogs as $doffNo => $weightLog) {
            foreach ($weightLog as $wl) {
//                dd($wl->toArray());
                $result[$doffNo][$wl->spindle] = $wl;
            }

            $emptySpindles = array_diff_key($totalSpindles, $result[$doffNo]);

            $merged = array_merge($result[$doffNo], $emptySpindles);

            $keys = array_keys($merged);

            natsort($keys);


            foreach ($keys as $key) {
                $finalResult[$doffNo][$key] = $merged[$key];
            }
        }

//        return view('reports.production.qualitywise-defect', [
//            'startDate' => $startDate,
//            'endDate' => $endDate,
//            'result' => $finalResult,
//            'machine' => $request['machine'],
//            'material' => ItemMaster::where('id', $request['material'])->value('descriptive_name')
//        ]);

        $totalDoffs = count($finalResult);




        return PDF::loadView('reports.production.qualitywise-defect', [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'totalDoff' => $totalDoffs,
            'result' => $finalResult,
            'machine' => $request['machine'],
            'material' => ItemMaster::where('id', $request['material'])->value('descriptive_name')
        ])->setPaper('a4', 'portrait')->setWarnings(false)
            ->stream('Defects Report ('.date('d-m-Y', strtotime($startDate)).' to '.date('d-m-Y', strtotime($endDate)).') -'.$request['machine'].'-'.'.pdf');
    }


    public function generateChallanList($challanNo)
    {
        $challanId = \DB::select('select id from challans where challan_no ='.$challanNo);

        $ids = array_column($challanId, 'id');

        $packageMastersId = DB::select('
            select id from package_masters where challan_id in ('.implode(',', $ids).')
        ');

        $packageMastersId = array_column($packageMastersId, 'id');




        $packages = DB::select('
                select package_masters.case_no as case_no, test.spindle_count, test.material_name, test.doff_no, test.net_weight, test.gross_wt, package_masters.box_type from package_masters
                inner join
                  
                (select distinct(package_master_id), 
                count(*) as spindle_count,
                MAX(packing_name) as material_name,
                group_concat(distinct(doff_no)) as doff_no,
                round(sum(material_weight),2) as net_weight,
                sum(total_weight) as gross_wt 
                from weight_logs where package_master_id in ('.implode(',', $packageMastersId).') group by package_master_id) as test
                
                on package_masters.id = test.package_master_id
                where challan_id in ('.implode(',', $ids).')
            ');

//        dd($packages);


        return PDF::loadView('reports.invoice.repack-slip', ['packages' =>$packages, 'challanNo' => $challanNo])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Repacking Slip - '.$challanNo.'.pdf');
    }

    public function productionStockReport(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'filter-date' => 'required',
            'rpt_type' => 'required',
            'machine' => 'required',
        ]);
        
        $weightStatus = 0;
        $inspectionStatus = [0];
        $ncrStatus = 0;
        $machine = [];
        $doffDate = date('Y-m-d H:i:s', strtotime($request['filter-date']));
        $title = '';

        if ($request['machine'] == 1) {
            $machine = ['Zell A', 'ZellA'];
        } elseif ($request['machine'] == 2) {
            $machine = ['Zell B', 'ZellB'];
        } elseif ($request['machine'] == 3) {
            $machine = ['kidde'];
        }

        if ($request['rpt_type'] == 1) {
            $title = 'AWAIT INSPECTION';
            $weightStatus = 1;
        } elseif ($request['rpt_type'] == 2) {
            $title = 'AWAIT PACKING';
            $weightStatus = 1;
            $inspectionStatus = [1];
        } elseif ($request['rpt_type'] == 3) {
            $title = 'AWAIT REWINDING';
            $weightStatus = 0;
            $inspectionStatus = [1,0];
        } elseif ($request['rpt_type'] == 4) {
            $title = 'NCR';
            $inspectionStatus = [1,0];
            $ncrStatus = 1;
        }

        $weightLogs = WeightLog::whereIn('machine', $machine)
                                ->where('weight_status', $weightStatus)
                                ->whereIn('inspection_status', $inspectionStatus)
                                ->where('package_master_id', null)
                                ->where('active_status', 1)
                                ->where('ncr_status', $ncrStatus)
                                ->where('doff_date', '<=', $doffDate)
                                ->orderby('doff_date')
                                ->get()
                                ->groupBy('doff_no');

        if ($request['rpt_type'] == 1 || $request['rpt_type'] == 2) {
            return PDF::loadView('reports.production.production-ok', ['weightLogs'=>$weightLogs,'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Repacking Slip - Production Ok.pdf');
        } else {
            return PDF::loadView('reports.production.production-not-ok', ['weightLogs'=>$weightLogs,'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Repacking Slip - Production Not Ok.pdf');
        }
    }

    public function productionStockReportTwo(Request $request)
    {
        $request->validate([
            'filter-date' => 'required',
            'rpt_type' => 'required',
            'machine' => 'required',
            'group_by' => 'required'
        ]);

        $doffDate = date('Y-m-d H:i:s', strtotime($request['filter-date']));
        $groupBy = $request['group_by'];

        if ($request['machine'] == 1) {
            $machine = 'Zell A';
        } elseif ($request['machine'] == 2) {
            $machine = 'Zell B';
        } elseif ($request['machine'] == 3) {
            $machine = 'kidde';
        }elseif($request['machine'] == 4){
            $machine = 'UDFW';
        }

        if ($request['rpt_type'] == 1) {
            $title = 'AWAIT INSPECTION';
            $weightLogs = WeightLog::where(function ($q) {
                $q->where('weight_status', 1)
                                        ->orWhere('inspection_reason', '!=', null);
            })
                                    ->where('wl_time', '<', $doffDate)
                                    ->where('machine', $machine)
                                    ->where(function ($q) use ($doffDate) {
                                        $q->where('inspection_status', 0)
                                        ->orWhere('inspection_date', '>', $doffDate);
                                    })
                                    ->get();
        } elseif ($request['rpt_type'] == 2) {
            $title = 'AWAIT PACKING';
            $weightLogs = WeightLog::where('weight_status', 1)
                                    ->where('wl_time', '<', $doffDate)
                                    ->where('machine', $machine)
                                    ->where('inspection_status', 1)
                                    ->where('inspection_date', '<', $doffDate)
                                    ->where('active_status', 1)
                                    ->where(function ($q) use ($doffDate) {
                                        $q->where('packed_status', null)
                                        ->orWhere('packed_date', '>', $doffDate);
                                    })
                                    ->get();
        } elseif ($request['rpt_type'] == 3) {
            $title = 'AWAIT REWINDING (REWORK 1)';
            $reason = NcrMaster::where('ncr_check', 'no')->pluck('reason')->toArray();
            $rewinded = WeightLog::where('wl_time', '<', $doffDate)
                                    ->where('rw_status', 1)
                                    ->where('machine', $machine)
                                    ->pluck('unique_id');
            $weightLogs = WeightLog::whereNotIn('unique_id', $rewinded)
                                    ->where('wl_time', '<', $doffDate)
                                    ->where('weight_status', 0)
                                    ->where('machine', $machine)
                                    ->where(function ($q) use ($reason) {
                                        $q->whereIn('reason', $reason)
                                        ->orWhereIn('inspection_reason', $reason);
                                    })
                                    ->get();
        } elseif ($request['rpt_type'] == 4) {
            $title = 'NCR (D) (REWORK 2)';
            $reason = NcrMaster::where('ncr_check', 'yes')->pluck('reason')->toArray();
            $rewinded = WeightLog::where('wl_time', '<', $doffDate)
                                    ->where('rw_status', 1)
                                    ->where('machine', $machine)
                                    ->pluck('unique_id');
            $weightLogs = WeightLog::whereNotIn('unique_id', $rewinded)
                                    ->where('wl_time', '<', $doffDate)
                                    ->where('weight_status', 0)
                                    ->where('machine', $machine)
                                    ->where(function ($q) use ($reason) {
                                        $q->whereIn('reason', $reason)
                                        ->orWhereIn('inspection_reason', $reason);
                                    })
                                    ->get();
        } else {
            $title = 'NCR';
            $weightLogs = weightLog::where('wl_time', '<', $doffDate)
                                    ->where('machine', $machine)
                                    ->where('ncr_status', 1)
                                    ->where('active_status', 1)
                                    ->where('reason', '!=', null)
                                    ->where(function ($q) use ($doffDate) {
                                        $q->where('packed_status', null)
                                        ->orWhere('packed_date', '>', $doffDate);
                                    })
                                    ->get();
        }
        $weightLog = $weightLogs->where('floor_code', '!=', null)->where('filament', '!=', null)->sortBy('doff_date');
        if ($request['rpt_type'] < 3) {
            if ($groupBy == 'doff_no') {
                return PDF::loadView('reports.production.production-ok-doff', ['weightLogs'=>$weightLog->groupBy($groupBy),'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            } elseif ($groupBy == 'material_id') {
                return PDF::loadView('reports.production.production-ok-material', ['weightLogs'=>$weightLog->groupBy($groupBy),'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            } else {
                return PDF::loadView('reports.production.production-ok', ['weightLogs'=>$weightLog,'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            }
        } else {
            if ($groupBy == 'doff_no') {
                return PDF::loadView('reports.production.production-ok-doff', ['weightLogs'=>$weightLog->groupBy($groupBy),'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            } elseif ($groupBy == 'material_id') {
                return PDF::loadView('reports.production.production-ok-material', ['weightLogs'=>$weightLog->groupBy($groupBy),'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            } else {
                return PDF::loadView('reports.production.production-not-ok', ['weightLogs'=>$weightLog,'title'=>$title,'machine'=>$machine,'date'=>$doffDate])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Stock - '.$title.'.pdf');
            }
        }
    }




    public function productionReport(Request $request)
    {
        $request->validate([
            'from-date' => 'required',
            'from-date' => 'required',
            'rpt_type' => 'required',
            'machine' => 'required',
            'status' => 'required',
        ]);
        $from = date('Y-m-d H:i:s', strtotime($request['from-date']));
        $to = date('Y-m-d H:i:s', strtotime($request['to-date']));
        $machine = $request['machine'];
        $wl_status = [];
        $ncr = [];
        if ($request['status'] == 'ok') {
            $wl_status = [1];
            $ncr = [0];
            $rw_status = 0;
        } elseif ($request['status'] == 'not-ok') {
            $wl_status = [0];
            $rw_status = 0;
            $ncr = [0];
        } elseif ($request['status'] == 'ncr'){
            $wl_status = [0];
            $rw_status = 1;
            $ncr = [1];
        } else {
            $wl_status = [0,1];
            $rw_status = 0;
            $ncr = [0];
        }
        
        if ($machine == 'ALL') {
            $weightLog = WeightLog::whereBetween('doff_date', [$from,$to])
                                    ->whereIn('machine', ['Zell A','Zell B','KIDDE'])
                                    ->where('doff_no', '!=', '0')
                                    ->whereIn('weight_status', $wl_status)
                                    ->whereIn('ncr_status', $ncr)
                                    ->where('rw_status', $rw_status)
                                    ->where('final_winding_reason', null)
                                    ->orderBy('wl_time')
                                    ->get();
            $master = $this->stockSummary($weightLog, $request['rpt_type']);
        } else {
            $weightLog = WeightLog::whereBetween('doff_date', [$from,$to])
                                    ->where('machine', $machine)
                                    ->where('doff_no', '!=', '0')
                                    ->whereIn('weight_status', $wl_status)
                                    ->whereIn('ncr_status', $ncr)
                                    ->where('rw_status', $rw_status)
                                    ->where('final_winding_reason', null)
                                    ->orderBy('wl_time')
                                    ->get();
            $master = $this->stockSummary($weightLog, $request['rpt_type']);
        }

        if ($request['rpt_type'] == 'doff') {
            return PDF::loadView('reports.production.production-summary-doff', [
                'title' => $machine,
                'master' => $master,
                'from' => date('d-m-Y H:i', strtotime($from)),
                'to' => date('d-m-Y H:i', strtotime($to)),
                'status' => strtoupper($request['status'])
                ])->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->stream('Production Summary Doff Wise.pdf');
        } else {
            return PDF::loadView('reports.production.production-summary-material', [
                    'title' => $machine,
                    'master' => $master,
                    'from' => date('d-m-Y H:i', strtotime($from)),
                    'to' => date('d-m-Y H:i', strtotime($to)),
                    'status' => strtoupper($request['status'])
                ])->setPaper('a4', 'portrait')
                ->setWarnings(false)
                ->stream('Production Summary Quality Wise.pdf');
        }
    }

    public function stockSummary($weightLog, $type)
    {
        $master = [];
        $uniqueId = [];

        if ($type == 'doff') {
            foreach ($weightLog as $value) {
                if (!in_array($value->unique_id, $uniqueId)) {
                    $uniqueId[] = $value->unique_id;
                    if (empty($master[$value->doff_no][$value->material][$value->floor_code][$value->doff_date])) {
                        $master[$value->doff_no][$value->material][$value->floor_code][$value->doff_date]['spl'] = 1;
                        $master[$value->doff_no][$value->material][$value->floor_code][$value->doff_date]['net_wt'] = $value->material_weight;
                    } else {
                        $master[$value->doff_no][$value->material][$value->floor_code][$value->doff_date]['spl'] += 1;
                        $master[$value->doff_no][$value->material][$value->floor_code][$value->doff_date]['net_wt'] += $value->material_weight;
                    }
                }
            }
        } else {
            foreach ($weightLog as $value) {
                if (!in_array($value->unique_id, $uniqueId)) {
                    $uniqueId[] = $value->unique_id;
                    if (empty($master[$value->material][$value->floor_code])) {
                        $master[$value->material][$value->floor_code]['spl'] = 1;
                        $master[$value->material][$value->floor_code]['net_wt'] = $value->material_weight;
                    } else {
                        $master[$value->material][$value->floor_code]['spl'] += 1;
                        $master[$value->material][$value->floor_code]['net_wt'] += $value->material_weight;
                    }
                }
            }
        }

        return $master;
    }

    public function stickerChange(Request $request){

        $request->validate([
            'doff' => 'required'
        ]);

        $weightLogs = WeightLog::where('doff_no', $request['doff'])->where('weight_status',0)->where('rw_status',0)->where('active_status',0)->get();
        if($weightLogs->count() == 0){
           return redirect()->back()->withErrors(['error' => 'No Sticker Changes in this doff']);
        }
        $output = [];

        foreach ($weightLogs as $wl){
//            dd($wl);

            $afterWeightLog = WeightLog::where('unique_id',$wl->unique_id)->where('rw_status',1)->where('active_status',1)->first();

            if(!empty($afterWeightLog)){
                $output[$wl->unique_id]['before'] = $wl;
                $output[$wl->unique_id]['after'] = $afterWeightLog;
            }
        }

//        dd($output);
//        return view('reports.production.sticker-change', ['spindles' => $output]);
        return PDF::loadView('reports.production.sticker-change', ['spindles' => $output, 'doff' => $request['doff']])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Sticker Change.pdf');
    }

    public function FinalWindingReport(Request $request)    
    {
        $request->validate([
            'machine' => 'required',
            'from-date' => 'required',
            'to-date' => 'required'
        ]);

        // $weightLog = WeightLog::where('machine',$request['machine'])
        //                         ->whereBetween('')
    }
}
