<?php

namespace App\Http\Controllers;

use App\IndentMaster;
use App\InvoiceMaster;
use App\ItemMaster;
use App\PackageDetail;
use App\PackageMaster;
use App\SaleOrder;
use App\WeightLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\BusinessPartner;
use PDF;

class InvoiceController extends Controller
{
    public function createInvoice($indentId)
    {
        $orderIds = [];
        $indentMaster = IndentMaster::where('id', $indentId)->first();

        if (!is_null($indentMaster)) {
            $orderIds = $indentMaster->indentDetails()->pluck('order_id');
        }

        $materialIds = SaleOrder::whereIn('id', $orderIds)->pluck('material_id');

        $packed = PackageMaster::whereIn('material_id', $materialIds)
            ->where('invoice_status', 0)
            ->with('weightLogs')->get()->groupBy('material_id');


        $fiscalYear = (date('m') > 3) ? date('Y') + 1 : date('Y');

        $fiscalStart = date(''.($fiscalYear - 1).'-04-01');

        $fiscalEnd = date(''.($fiscalYear).'-03-31');

        if ($indentMaster->delivery_type == 2) {
            //Not within TamilNadu
            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'IC'.($fiscalYear-2001).($fiscalYear-2000).'-')->pluck('invoice_no')->toArray();
        } elseif ($indentMaster->delivery_type == 3) {
            //Export

            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'EC'.($fiscalYear-2001).($fiscalYear-2000).'-')->pluck('invoice_no')->toArray();
        } else {
            //Within TamilNadu
            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'SC'.($fiscalYear-2001).($fiscalYear-2000).'-')->orderBy('invoice_no')->pluck('invoice_no')->toArray();
        }

        natsort($invoice);
        if (count($invoice) == 0) {
            if ($indentMaster->delivery_type == 2) {
                //Not within TamilNadu
                $latestInvoiceNo = 'IC'.($fiscalYear-2001).($fiscalYear-2000).'-01';
            } elseif ($indentMaster->delivery_type == 3) {
                //Export
                $latestInvoiceNo = 'EC'.($fiscalYear-2001).($fiscalYear-2000).'-01';
//                dd($invoice);
            } else {
                //Within TamilNadu
                $latestInvoiceNo = 'SC'.($fiscalYear-2001).($fiscalYear-2000).'-01';
            }
        } else {
            $latestInvoiceNo = explode('-', end($invoice));
            $latestInvoiceNo = $latestInvoiceNo[0].'-'.($latestInvoiceNo[1] + 1);
        }






        if ($packed->count() == 0) {
            return redirect()->back()->withErrors(['no_packaging' => 'Packaging is not available for the materials in the indent.']);
        }

        return view('invoice.invoice-lot')->with(compact('packed', 'materialIds', 'indentMaster', 'latestInvoiceNo'));
    }

    public function index()
    {
        $pendingSales = SaleOrder::where('pending_quantity', '>', 0)->get()->groupBy('bp_id');

        // dd($pendingSales);

        return view('invoice.invoice-lot')->with(compact('pendingSales'));
    }


    public function createInvoiceDetails(Request $request)
    {
        $request->validate([
            'indent_id' => 'required',
            'invoice_no' => 'required',
            'invoice_date' => 'required',
            'customer' => 'required',
            'packing_details' => 'required',
            'handling_charges' => 'required',
        ]);

        //Packing details contains ids of packing details

        $invoiceMaster = InvoiceMaster::create([
            'invoice_no' => $request['invoice_no'],
            'indent_id' => $request['indent_id'],
            'consignee_id' => $request['consignee'],
            'date' => date('Y-m-d', strtotime($request['invoice_date'])),
            'bp_id' => $request['customer'],
            'handling_charges' => $request['handling_charges'],
        ]);


        $indentDetails = IndentMaster::where('id', $request['indent_id'])->first()->indentDetails;

        if ($invoiceMaster) {
            foreach ($request['packing_details'] as $packageMasterId) {
                WeightLog::where('package_master_id', $packageMasterId)->update(['invoice_status'=>1]);
            }


            foreach ($request['packing_details'] as $masterId) {
                foreach ($indentDetails as $indentDetail) {
                    $pMaster = PackageMaster::where('id', $masterId)->first();
                    if ($indentDetail->material_id == $pMaster->material_id) {
                        $saleOrder = SaleOrder::where('id', $indentDetail->order_id)->first();
                        $pendingQty = ($saleOrder->pending_quantity - ($pMaster->moisture_weight + $pMaster->weightLogs()->sum('material_weight')));
                        if ($pendingQty < 0) {
                            $pendingQty = 0;
                        }
                        SaleOrder::where('id', $indentDetail->order_id)->update([
                               'edit_state'=> 1,
                               'delete_state'=> 1,
                               'pending_quantity' => round($pendingQty, 1),
                               'dispatch_quantity' => round(($saleOrder->dispatch_quantity + $pMaster->moisture_weight + $pMaster->weightLogs()->sum('material_weight')), 1),
                               'dispatch_date' =>  date('Y-m-d', strtotime($request['invoice_date']))

                           ]);
                    }
                }

                $weightLogsCount = WeightLog::where('package_master_id', $masterId)->where('invoice_status', 0)->count();

                if ($weightLogsCount == 0) {
                    PackageMaster::where('id', $masterId)->update(['invoice_status' => 1,'invoice_master_id' => $invoiceMaster->id]);
                }
            }

            return redirect('invoice-list');
        } else {
            return redirect()->back()->withErrors(['error' => 'Cannot Create the Invoice right now. Please Try again later']);
        }
    }

    public function invoiceList($date = null)
    {
        if ($date == null) {
            $today = date('Y-m-01');
        } else {
            $today = date('Y-m-d', strtotime('01-'.$date));
        }

        $invoiceList = InvoiceMaster::where('date', '>=', date('Y-m-01', strtotime($today)))
                                      ->where('date', '<=', date('Y-m-t', strtotime($today)))
                                      ->get();

        return view('invoice.invoice-list')->with(compact('invoiceList'));
    }

    public function indentList($bp_id = null)
    {
        if ($bp_id == null) {
            $saleOrders = [];
        } else {
            $saleOrders = SaleOrder::where('bp_id', $bp_id)
                                ->where('pending_quantity', '>', 0)
                                ->with('indentDetails', 'businessPartner')
                                ->get();
        }

        $businessPartner = BusinessPartner::all();

        // dd($saleOrders->first()->indentDetails);
        return view('invoice.customer-indent-list')->with(compact('businessPartner', 'saleOrders'));
    }


    public function deleteInvoice($id)
    {
        $invoiceMaster = InvoiceMaster::where('id', $id)->first();
    }

    public function instock($exp = null)
    {
        $weightLog = WeightLog::where('packed_status', 1)
                          ->where('inspection_status', 1)
                          ->where('active_status', 1)
                          ->where('invoice_status', 0)
                          ->where('ncr_status', 0)
                          ->with('packageMaster')
                          ->get()
                          ->groupBy('package_master_id');
        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];
        foreach ($weightLog as $value) {
            $first = $value->first();
            $info = "";
            $reason = $first->packageMaster->reason;
            $doff = preg_replace("/[^A-Z]+/", "", $first->doff_no);
            if (is_null($reason)) {
                $info = $doff;
            } else {
                $info = $reason;
            }
            $sum = $value->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->count();
            if (empty($instock[$first->material][$info])) {
                $instock[$first->material][$info]['spindle'] = $value->count();
                $instock[$first->material][$info]['box'] = 1;
                $instock[$first->material][$info]['material'] = $value->first()->floor_code;
                $instock[$first->material][$info]['weight'] = $sum;
            } else {
                $instock[$first->material][$info]['spindle'] += $value->count();
                $instock[$first->material][$info]['box'] += 1;
                $instock[$first->material][$info]['weight'] += $sum;
            }
        }
        ksort($instock);
        if (is_null($exp)) {
            return view('invoice.instock')->with(compact('instock', 'total'));
        } else {
            return PDF::loadView('reports.dispatch.doff-wise-stock', ['instock'=>$instock,'total'=>$total,'type'=>'Sound'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function instockNcr($exp = null)
    {
        $weightLog = WeightLog::where('package_master_id', '!=', null)
                              ->whereIn('inspection_status', [0,1])
                              ->where('active_status', 1)
                              ->where('invoice_status', '!=', 1)
                              ->where('ncr_status', 1)
                              ->with('packageMaster')
                              ->get()
                              ->groupBy('package_master_id');
        // dd($weightLog->toArray());
        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];
        foreach ($weightLog as $value) {
            $first = $value->first();
            $info = "";
            if (!is_null($first->packageMaster)) {
                $reason = $first->packageMaster->reason;
                $doff = preg_replace("/[^A-Z]+/", "", $first->doff_no);
                if (is_null($reason)) {
                    $info = $doff;
                } else {
                    $info = $reason;
                }
                $sum = $value->sum('material_weight');
                $total['weight'] += $sum;
                $total['box'] += 1;
                $total['spindle'] += $value->count();
                if (empty($instock[$first->material][$info])) {
                    $instock[$first->material][$info]['spindle'] = $value->count();
                    $instock[$first->material][$info]['box'] = 1;
                    $instock[$first->material][$info]['material'] = $value->first()->floor_code;
                    $instock[$first->material][$info]['weight'] = $sum;
                } else {
                    $instock[$first->material][$info]['spindle'] += $value->count();
                    $instock[$first->material][$info]['box'] += 1;
                    $instock[$first->material][$info]['weight'] += $sum;
                }
            }
        }
        ksort($instock);
        if (is_null($exp)) {
            return view('invoice.instock-ncr')->with(compact('instock', 'total'));
        } else {
            return PDF::loadView('reports.dispatch.doff-wise-stock', ['instock'=>$instock,'total'=>$total,'type'=>'NCR'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function instockLeader($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'leader')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();
        // dd($packageMaster->first());
        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];
        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            $doff = preg_replace("/[^A-Z]+/", "", $first->weightLogs->first()->doff_no);
            if (is_null($reason)) {
                $info = $doff;
            } else {
                $info = $reason;
            }
            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            if (empty($instock[$first->weightLogs->first()->material][$info])) {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] = 1;
                $instock[$first->weightLogs->first()->material][$info]['material'] = $value->weightLogs->first()->floor_code;
                $instock[$first->weightLogs->first()->material][$info]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$info]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'Leader';
        if (is_null($exp)) {
            return view('invoice.instock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.doff-wise-stock', ['instock'=>$instock,'total'=>$total,'type'=>'Leader'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function getRefNo($ref = null)
    {
        if ($ref == null) {
            $indentMasters = [];
        } else {
            $indentMasters = IndentMaster::where('order_ref_no', $ref)->with('indentDetails', 'businessPartner')->get();
        }
        return view('invoice.ref-no-indent-list')->with(compact('indentMasters'));
    }

    public function finshedGoods($exp = null)
    {
        $weightLog = WeightLog::where('packed_status', 1)
                          ->where('inspection_status', 1)
                          ->where('active_status', 1)
                          ->where('invoice_status', 0)
                          ->where('ncr_status', 0)
                          ->with('packageMaster')
                          ->get()
                          ->groupBy('package_master_id');

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];

        foreach ($weightLog as $value) {
            $first = $value->first();
            $info = "";
            $reason = $first->packageMaster->reason;
            $doff = preg_replace("/[^A-Z]+/", "", $first->doff_no);

            if (is_null($reason)) {
                $info = $doff;
            } else {
                $info = $reason;
            }

            $sum = $value->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->count();
            $material = trim($value->first()->floor_code);

            if (empty($instock[$first->material][$material])) {
                $instock[$first->material][$material]['spindle'] = $value->count();
                $instock[$first->material][$material]['box'] = 1;
                // $instock[$first->material]['material'] = $value->first()->floor_code;
                $instock[$first->material][$material]['weight'] = $sum;
            } else {
                $instock[$first->material][$material]['spindle'] += $value->count();
                $instock[$first->material][$material]['box'] += 1;
                $instock[$first->material][$material]['weight'] += $sum;
            }
        }
        ksort($instock);
        if (is_null($exp)) {
            return view('invoice.finshed-goods-stock')->with(compact('instock', 'total'));
        } else {
            return PDF::loadView('reports.dispatch.finshed-goods-stock', ['instock'=>$instock,'total'=>$total,'type'=>'Sound'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function finshedGoodsNcr($exp = null)
    {
        $weightLog = WeightLog::where('package_master_id', '!=', null)
                          ->where('active_status', 1)
                          ->where('invoice_status', 0)
                          ->where('ncr_status', 1)
                          ->with('packageMaster')
                          ->get()
                          ->groupBy('package_master_id');

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];

        foreach ($weightLog as $value) {
            $first = $value->first();
            $info = "";
            if (!is_null($first->packageMaster)) {
                $reason = $first->packageMaster->reason;
                $doff = preg_replace("/[^A-Z]+/", "", $first->doff_no);
    
                if (is_null($reason)) {
                    $info = $doff;
                } else {
                    $info = $reason;
                }
    
                $sum = $value->sum('material_weight');
                $total['weight'] += $sum;
                $total['box'] += 1;
                $total['spindle'] += $value->count();
                $material = trim($value->first()->floor_code);
    
                if (empty($instock[$first->material][$material])) {
                    $instock[$first->material][$material]['spindle'] = $value->count();
                    $instock[$first->material][$material]['box'] = 1;
                    // $instock[$first->material]['material'] = $value->first()->floor_code;
                    $instock[$first->material][$material]['weight'] = $sum;
                } else {
                    $instock[$first->material][$material]['spindle'] += $value->count();
                    $instock[$first->material][$material]['box'] += 1;
                    $instock[$first->material][$material]['weight'] += $sum;
                }
            }
        }
        ksort($instock);
        if (is_null($exp)) {
            return view('invoice.finshed-goods-stock-ncr')->with(compact('instock', 'total'));
        } else {
            return PDF::loadView('reports.dispatch.finshed-goods-stock', ['instock'=>$instock,'total'=>$total,'type'=>'NCR'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function finshedGoodsLeader($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'leader')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];

        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            // $doff = preg_replace("/[^A-Z]+/", "", $first->weightLog->first()->doff_no);

            // if (is_null($reason)) {
            //     $info = $doff;
            // } else {
            //     $info = $reason;
            // }

            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            $material = trim($value->weightLogs->first()->floor_code);

            if (empty($instock[$first->weightLogs->first()->material][$material])) {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] = 1;
                // $instock[$first->weightLogs->first()->material][$material]['material'] = $value->weightLogs->first()->floor_code;
                $instock[$first->weightLogs->first()->material][$material]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$material]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'Leader';
        if (is_null($exp)) {
            return view('invoice.finshed-goods-stock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.finshed-goods-stock', ['instock'=>$instock,'total'=>$total,'type'=>'Leader'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function dayWisePack()
    {
        return view('invoice.day-wise-pack');
    }

    public function boxUsage()
    {
        return view('invoice.box-usage-summary');
    }

    public function dispatchByDate()
    {
        return view('invoice.dispatch-by-date');
    }


    public function dispatchByMaterial()
    {
        return view('invoice.dispatch-by-date');
    }

    public function productionSummary()
    {
        return view('invoice.production-summary');
    }

    public function countPacked($item = null)
    {
        if ($item == null) {
            $model = 'count';
        } else {
            $model = 'item';
        }
        return view('invoice.day-wise-pack')->with(compact('model'));
    }



    public function dispatchByCount(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'material' => 'required'
        ]);


        $materialName = ItemMaster::where('id', $request['material'])->value('descriptive_name');
//        dd($materialName);


        $invoices = InvoiceMaster::whereBetween('date', [
            date('Y-m-d', strtotime($request['start_date'])),
            date('Y-m-d', strtotime($request['end_date']))
        ])->with('indent')->orderBy('date')->get();

        $finalResult = [];

        foreach ($invoices as $invoice) {
            $indentMaster = $invoice->indent;


            $packageMastersId = PackageMaster::where('invoice_master_id', $invoice->id)->pluck('id');

            $packageMasters = PackageMaster::whereIn('id', $packageMastersId)->where('material_id', $request['material'])->get()->groupBy('material_id');


            $orderDetails = [];


            foreach ($packageMasters as $material => $packageMaster) {
                $orderDetails[$material]['weight'] = 0;

                foreach ($packageMaster as $package) {
                    $indentDetails =  $indentMaster->indentDetails()->where('material_id', $material)->first();

                    $orderDetails[$material]['name'] = SaleOrder::where('id', $indentDetails->order_id)->value('material_name');
                    $orderDetails[$material]['weight'] += $package->weightLogs->sum('material_weight') + $package->moisture_weight;
                    $orderDetails[$material]['hsn'] = $indentDetails->hsn_sac;
                    $orderDetails[$material]['count'] = $package->weightLogs->count();
                    $orderDetails[$material]['gst'] = $indentDetails->gst_rate;
                    $orderDetails[$material]['rate'] = $indentDetails->rate_per_kg;
                    $orderDetails[$material]['no_box'] = $packageMaster->count();
                }

                $orderDetails[$material]['value'] = $orderDetails[$material]['weight'] * $orderDetails[$material]['rate'];
            }

            $finalResult[$invoice->id] = $orderDetails;
        }

//        dd($finalResult);

        $totalBilled = 0;
        $masterArray = [];

        foreach ($finalResult as $invoiceId => $dispatch) {
            $invoice = InvoiceMaster::where('id', $invoiceId)->first();


            $totalAmount = array_sum(array_column($dispatch, 'value'));


            foreach ($dispatch as $dis) {
                $tempHandlingCharges = ($invoice->handling_charges / $totalAmount) * $dis['value'];
                $insuranceCost = ((($invoice->indent->transit_insurance / 100) * $totalAmount) / $totalAmount) * $dis['value'];

                $gst = $dis['gst'] / 100 * ($dis['value'] + $tempHandlingCharges + $insuranceCost);
                $grantTotal = $dis['value'] + $tempHandlingCharges + $insuranceCost + $gst;
                $totalBilled += $grantTotal;


                $masterArray[] = [
                    'invoice_no' => $invoice->invoice_no,
                    'bp_name' => $invoice->businessPartner->bp_name,
                    'invoice_date' => date('d-m-Y', strtotime($invoice->date)),
                    'item_name' => $dis['name'],
                    'weight' => $dis['weight'],
                    'rate' => $dis['rate'],
                    'value' => $dis['value'],
                    'insurance' => $insuranceCost,
                    'handling_charges' => $tempHandlingCharges,
                    'gst' => $gst,
                    'grant_total' => $grantTotal,
                    'ref_no' => $invoice->indent->order_ref_no,
                ];
            }
        }



        return PDF::loadView('reports.dispatch.count-wise', [
            'masterArray'=>$masterArray,
            'start'=>$request['start_date'],
            'end'=>$request['end_date'],
            'totalBilled' => $totalBilled,
            'materialName' => $materialName,
        ])->setPaper('a4', 'landscape')->setWarnings(false)->stream('Dispatch Summary.pdf');
    }

    public function dayWiseProduction()
    {
        return view('invoice.day-wise-pack-summary');
    }

    public function instockWeast($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'waste')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];
        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            $doff = preg_replace("/[^A-Z]+/", "", $first->weightLogs->first()->doff_no);
            if (is_null($reason)) {
                $info = $doff;
            } else {
                $info = $reason;
            }
            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            if (empty($instock[$first->weightLogs->first()->material][$info])) {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] = 1;
                $instock[$first->weightLogs->first()->material][$info]['material'] = $value->weightLogs->first()->floor_code;
                $instock[$first->weightLogs->first()->material][$info]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$info]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'WASTE';
        if (is_null($exp)) {
            return view('invoice.instock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.doff-wise-stock', ['instock'=>$instock,'total'=>$total,'type'=>'Leader'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function instockSlb($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'SLB')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];
        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            $doff = preg_replace("/[^A-Z]+/", "", $first->weightLogs->first()->doff_no);
            if (is_null($reason)) {
                $info = $doff;
            } else {
                $info = $reason;
            }
            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            if (empty($instock[$first->weightLogs->first()->material][$info])) {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] = 1;
                $instock[$first->weightLogs->first()->material][$info]['material'] = $value->weightLogs->first()->floor_code;
                $instock[$first->weightLogs->first()->material][$info]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$info]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$info]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$info]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'SLB';
        if (is_null($exp)) {
            return view('invoice.instock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.doff-wise-stock', ['instock'=>$instock,'total'=>$total,'type'=>'SLB'])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function finshedGoodsWaste($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'waste')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];

        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            $material = trim($value->weightLogs->first()->floor_code);

            if (empty($instock[$first->weightLogs->first()->material][$material])) {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] = 1;
                $instock[$first->weightLogs->first()->material][$material]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$material]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'WASTE';
        if (is_null($exp)) {
            return view('invoice.finshed-goods-stock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.finshed-goods-stock', ['instock'=>$instock,'total'=>$total,'type'=>$title])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }

    public function finshedGoodsSlb($exp = null)
    {
        $packageMaster = PackageMaster::where('material_type', 'SLB')
                                        ->where('invoice_status', '!=', 1)
                                        ->with('weightLogs')
                                        ->get();

        $total = ['weight'=>0,'box'=>0,'spindle'=>0];
        $instock = [];

        foreach ($packageMaster as $value) {
            $first = $value;
            $info = "";
            $reason = $first->reason;
            $sum = $value->weightLogs->sum('material_weight');
            $total['weight'] += $sum;
            $total['box'] += 1;
            $total['spindle'] += $value->bobbin_count;
            $material = trim($value->weightLogs->first()->floor_code);

            if (empty($instock[$first->weightLogs->first()->material][$material])) {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] = $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] = 1;
                $instock[$first->weightLogs->first()->material][$material]['weight'] = $sum;
            } else {
                $instock[$first->weightLogs->first()->material][$material]['spindle'] += $value->bobbin_count;
                $instock[$first->weightLogs->first()->material][$material]['box'] += 1;
                $instock[$first->weightLogs->first()->material][$material]['weight'] += $sum;
            }
        }
        ksort($instock);
        $title = 'SLB';
        if (is_null($exp)) {
            return view('invoice.finshed-goods-stock-leader')->with(compact('instock', 'total', 'title'));
        } else {
            return PDF::loadView('reports.dispatch.finshed-goods-stock', ['instock'=>$instock,'total'=>$total,'type'=>$title])->setPaper('a4', 'portrait')->setWarnings(false)->stream('Doff wise stock.pdf');
        }
    }


    public function createInvoiceTwo($indentId)
    {
        $orderIds = [];
        $indentMaster = IndentMaster::where('id', $indentId)->first();

        // dd($indentMaster);
        if (!is_null($indentMaster)) {
            $orderIds = $indentMaster->indentDetails()->pluck('order_id');
        }

        $materialIds = SaleOrder::whereIn('id', $orderIds)->pluck('material_id');

        $packed = PackageMaster::whereIn('material_id', $materialIds)
            ->where('invoice_status', 0)
            ->with('weightLogs')->get()->groupBy('material_id');

        $fiscalYear = (date('m') > 3) ? date('Y') + 1 : date('Y');

        $fiscalStart = date(''.($fiscalYear - 1).'-04-01');

        $fiscalEnd = date(''.($fiscalYear).'-03-31');

        if ($indentMaster->delivery_type == 2) {
            //Not within TamilNadu
            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'IC'.($fiscalYear-2001).($fiscalYear-2000).'-')->pluck('invoice_no')->toArray();
        } elseif ($indentMaster->delivery_type == 3) {
            //Export

            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'EC'.($fiscalYear-2001).($fiscalYear-2000).'-')->pluck('invoice_no')->toArray();
        } else {
            //Within TamilNadu
            $invoice = InvoiceMaster::whereBetween('date', [$fiscalStart, $fiscalEnd])->where('invoice_no', 'REGEXP', 'SC'.($fiscalYear-2001).($fiscalYear-2000).'-')->orderBy('invoice_no')->pluck('invoice_no')->toArray();
        }

        natsort($invoice);
        $latestInvoiceNo = explode('-', end($invoice));
        $latestInvoiceNo = $latestInvoiceNo[0].'-'.($latestInvoiceNo[1] + 1);



        if ($packed->count() == 0) {
            return redirect()->back()->withErrors(['no_packaging' => 'Packaging is not available for the materials in the indent.']);
        }

        return view('invoice.invoice-lot-two')->with(compact('packed', 'materialIds', 'indentMaster', 'latestInvoiceNo'));
    }
}
