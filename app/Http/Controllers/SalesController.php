<?php

namespace App\Http\Controllers;

use App\BpMaterial;
use App\Loggers\DatabaseLogger;
use App\Mail\SaleOrders;
use Illuminate\Http\Request;
use App\SaleOrder;
use Carbon\Carbon;
use App\BusinessPartner;
use App\ItemMaster;
use App\ProductionFeasibility;
use App\BankAccount;
use App\IndentMaster;
use App\IndentDetail;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('sales');
    }

    public function index($date = null)
    {
        if ($date == null) {
            $today = Carbon::now();
        } else {
            $today = Carbon::parse('01-'.$date);
        }

        $filter = $today->format('F Y');

        $saleOrder = SaleOrder::where('order_date', 'LIKE', $today->format('Y-m').'%')
                                ->where('bp_id', '!=', 114)
                                ->where('order_quantity', '>', 0)
                                ->orderBy('customer_date')->get();
        $overAllPending = SaleOrder::where('pending_quantity', '>', 0)
                                    ->where('order_quantity', '>', 0)
                                    ->where('bp_id', '!=', 114)
                                    ->sum('pending_quantity');

        $itemMaster = ItemMaster::all();

        $businessPartner = BusinessPartner::orderBy('bp_name')->get();


        $totalOrders = $saleOrder->sum('order_quantity');
        $pendingOrders = $saleOrder->sum('pending_quantity');
        $dispatchedOrders = $saleOrder->sum('dispatch_quantity');
        // $thisMonthOrders = $saleOrder->sum('order_quantity');
        // dd($totalOrders, $pendingOrders,$saleOrder->pluck('order_quantity'));

        return view('sales.sale_order')->with(compact(
            'saleOrder',
            'itemMaster',
            'businessPartner',
            'filter',
            'totalOrders',
            'pendingOrders',
            'dispatchedOrders',
            'overAllPending'
        ));
    }

    public function createSaleOrder(Request $request)
    {
        $request->validate([
            'customer_name' => 'required|numeric',
            'order_date' => 'required|date',
            'sale_entries' => 'required'
        ]);

        $businessPartner = BusinessPartner::find($request['customer_name']);

        $saleEntries = json_decode($request['sale_entries']);



        foreach ($saleEntries as $saleEntry) {
            try {
                $existingFilament = null;

                // $lastMonthPending = $businessPartner->saleOrder()
                //     ->where('material_id', $saleEntry->material)
                //     ->where('pending_quantity', '>', 0)
                //     ->where('customer_date', '<', Carbon::now()->toDateString('Y-m-d'))
                //     ->sum('pending_quantity');
                // if ($lastMonthPending > 0) {
                //     $thisMonthOrders = $businessPartner->saleOrder()
                //                     ->where('material_id', $saleEntry->material)
                //                     ->where('last_month_pending', '>', 0)
                //                     ->where('order_date', '>=', date('Y-m-01'))
                //                     ->get()
                //                     ->count();
                //     if ($thisMonthOrders > 0) {
                //         $lastMonthPending = 0;
                //     }
                // }

                $existing = $businessPartner->saleOrder()
                    ->where('material_id', $saleEntry->material)
                    ->orderBy('order_date')
                    ->with('productionFeasibility')
                    ->get()
                    ->last();

                if (!is_null($existing)) {
                    $existingFilament = $existing->productionFeasibility->changed_filament;
                }

                // if (!is_null($lastOrder)) {
                //   if (date('Y-m-d',strtotime($lastOrder->customer_date)) >= date('Y-m-01',strtotime(Carbon::now()))) {
                //     $lastMonthPending = 0;
                //   }else{
                //     $lastMonthPending = $lastOrder->pending_quantity;
                //   }
                // }else{
                //   $lastMonthPending = 0;
                // }
                // dd($existingFilament);
                $totalOrder = $saleEntry->weight;
                $material = explode('|', $saleEntry->material);
                $createOrder = $businessPartner->saleOrder()->create([

                    'material_id'=>$material[0],
                    'material_name' => $material[1],
                    'order_quantity'=>$saleEntry->weight,
                    'order_date'=>date('Y-m-d', strtotime($request['order_date'])),
                    'customer_date'=>date('Y-m-d', strtotime($saleEntry->customerDate)),
                    'last_month_pending'=>0,
                    'total_order'=>$totalOrder,
                    'pending_quantity'=>$saleEntry->weight

                ]);

                $itemMaster = ItemMaster::where('id', $saleEntry->material)->first();


                //Below is to create a empty Production feasibility when a sale order is created.

                $createOrder->productionFeasibility()->create([
                    'material' => $material[0],
                    'cord_weight'=>$itemMaster->cord_wt,
                    'existing_filament'=>$existingFilament,
                ]);
            } catch (\Exception $e) {
                return response()->json(['status'=> false,'msg'=>$e->getMessage()], 500);
            }
        }

        return response()->json(['status'=> true,'msg'=>'Sale Order Created.']);
    }

    public function updateSaleOrder(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'customer_name_update' => 'required',
            'material_update' => 'required',
            'weight_update' => 'required|numeric',
            'order_update' =>'required|date',
            'customer_date_update' => 'required|date|after_or_equal:order_update',
        ]);

        try {

            http_response_code(500);



            $businessPartner = BusinessPartner::where('id', $request['customer_name_update'])->first();

            $material = explode('|', $request['material_update']);

            // $lastMonthPending = $businessPartner->saleOrder()
            //     ->where('id', '!=', $request['id'])
            //     ->where('material_id', $material[0])
            //     ->where('customer_date', '<', Carbon::now()->toDateString('Y-m-d'))
            //     ->where('pending_quantity', '>', 0)
            //     ->sum('pending_quantity');

            // if ($lastMonthPending > 0) {
            //     $thisMonthOrders = $businessPartner->saleOrder()
            //                     ->where('id', '!=', $request['id'])
            //                     ->where('material_id', $material[0])
            //                     ->where('last_month_pending', '>', 0)
            //                     ->where('order_date', '>=', date('Y-m-01'))
            //                     ->get()
            //                     ->count();
            //     if ($thisMonthOrders > 0) {
            //         $lastMonthPending = 0;
            //     }
            // }

            $totalOrder = $request['weight_update'];

            $beforeUpdate = SaleOrder::getEntry($request['id']);

            SaleOrder::where('id', $request['id'])->update([

                'material_id'=> $material[0],
                'material_name' => $material[1],
                'order_quantity'=>$request['weight_update'],
                'order_date'=>date('Y-m-d', strtotime($request['order_update'])),
                'customer_date'=>date('Y-m-d', strtotime($request['customer_date_update'])),
                'last_month_pending'=>0,
                'total_order'=>$totalOrder,
                'pending_quantity'=>$request['weight_update']

            ]);

            $afterUpdate = SaleOrder::getEntry($request['id']);

            (new DatabaseLogger($beforeUpdate, $afterUpdate, 'edit', Date('Y-m-d H:i:s'), '', auth()->user()->name))->saveEditEntry();



            $itemMaster = ItemMaster::where('id', $request['material_update'])->first();
            // $runningHrs = round(($request['weight_update']/$itemMaster->prod_hr), 1);

            ProductionFeasibility::where('order_id', $request['id'])->update(['material' => $material[0],
                'cord_weight'=>$itemMaster->cord_wt,
                // 'speed_in_mpm'=>$itemMaster->speed_in_mpm,
                // 'production_per_hr'=>$itemMaster->prod_hr,
                // 'no_of_spls'=>$itemMaster->no_of_spls,
                // 'running_hrs'=>$runningHrs,
            ]);
        } catch (\Exception $e) {
            return response()->json(['status'=> false,'msg'=>$e->getMessage()]);
        }

        return response()->json(['status'=> true,'msg'=>'Sale Order Updated.']);
    }

    public function deleteSaleOrder(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        try {
            $order = SaleOrder::where('id', $request['id'])->first();

            $deletedOrder = $order->toArray();

            (new DatabaseLogger($deletedOrder, [], 'delete', Date('Y-m-d H:i:s'), '', auth()->user()->name))->saveDeleteEntry();

            if (!is_null($order->productionFeasibility)) {
                $deleteFeasibility = $order->productionFeasibility->delete();
            }

            $order->delete();
        } catch (\Exception $e) {
            return response()->json(['status'=>true,'msg'=>$e->getMessage()], 500);
        }

        return response()->json(['status'=>true,'msg'=>'Sale Order deleted.']);
    }

    public function getSaleOrder(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        try {
            $order = SaleOrder::where('id', $request['id'])->first();
            $bpMaterials = $order->businessPartner->bpMaterials;
//            $bpMaterials = ItemMaster::whereIn('id', array_keys($bpMaterials))->get();

            return response()->json(['status'=>true,'data'=>$order, 'bpMaterials' => $bpMaterials]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'data' => $e->getMessage()], 500);
        }
    }



    public function getBusinessPartnerMaterials(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $businesspartnerMaterials = BpMaterial::where('bp_id', $request['id'])->get();

//        $itemMaster = ItemMaster::whereIn('id', array_keys($businesspartnerMaterials))->get();


        return response()->json(['materials' => $businesspartnerMaterials], 200);
    }





    public function generateIndent()
    {
        $lastIndent = IndentMaster::get()->last();

        if (!is_null($lastIndent)) {
            $quotationNo = explode('/', $lastIndent->quotation_number);
            $quotationNo = (date('Y').'/'.($quotationNo[1] + 1));
        } else {
            $quotationNo = (date('Y').'/'.(0 + 1));
        }


        $saleOrders = saleOrder::where('indent_status', 0)
                                ->where('order_quantity', '>', 0)
                                ->get()
                                ->groupBy('bp_id')
                                ->toArray();

        $businessPartner = BusinessPartner::whereIn('id', array_keys($saleOrders))
                                            ->get();

        return view('sales.create_indent')->with(compact('businessPartner', 'quotationNo'));
    }

    public function getBpOrder(Request $request)
    {
        $request->validate([
            'bp_id' => 'required',
        ]);

        $businessPartner = BusinessPartner::where('id', $request['bp_id'])
            ->first();

        $orderList = $businessPartner->saleOrder()
                                    ->where('indent_status', 0)
                                    ->where('order_quantity', '>', 0)
                                    ->with('itemMaster')
                                    ->get();

        $bankDetail = BankAccount::where('bp_id', $request['bp_id'])->get();



        return response()->json(['status'=>true, 'data'=>$orderList, 'bankDetail'=>$bankDetail,
            'businessPartner' => $businessPartner]);
    }

    public function createIndent(Request $request)
    {
        $request->validate([
              "customer_name" => "required",
              "quotation-number" => "required",
              "tax" => "required",
              "date" => "required",
              "reference-no" => "required",
              "dispatch" => "required",
              "destination" => "required",
              "order" => "required",
              "delivery-type" => 'required',
              "indent-type" => 'required',
              "payment-mode" => 'required',
              "mode-of-transport" => "required",
        ]);



        try {
            $businessPartner = BusinessPartner::where('id', $request['customer_name'])
                ->get()
                ->first();

            $createIndentMaster = $businessPartner->indentMaster()
                ->create([
                    'quotation_number'=>$request['quotation-number'],
                    'transit_insurance'=>$request['tax'],
                    'handling_charges' => '0',
                    'dated_on'=>date('Y-m-d', strtotime($request['date'])),
                    'order_ref_no'=>$request['reference-no'],
                    'other_ref_no'=>$request['other-reference-no'],
                    'dispatcher'=>$request['dispatch'],
                    'destination'=>$request['destination'],
                    'terms_of_delivery'=>$request['terms'],
                    'payment_mode' => $request['payment-mode'],
                    'delivery_type' => $request['delivery-type'],
                    'indent_type' => $request['indent-type'],
                    'mode_of_transport' => $request['mode-of-transport'],
                ]);

            foreach ($request['order'] as $key => $value) {
                $saleOrder = SaleOrder::where('id', $value)->first();

                $itemMaster = $saleOrder->itemMaster;

                if ($request['delivery-type'] == 1) {

                    //Within TamilNadu
                    $gstRate = $itemMaster->sgst + $itemMaster->cgst;
                } else {

                    //outside TamilNadu
                    $gstRate = $itemMaster->igst;
                }

                $createIndent =  $createIndentMaster->indentDetails()
                    ->create(['order_id'=>$value,
                        'material_id'=>$saleOrder['material_id'],
                        'hsn_sac'=>$itemMaster['hsn_sac'],
                        'gst_rate'=>$gstRate,
                        'due_on'=>date('Y-m-d', strtotime($request['due_date'][$key])),
                        'quantity'=>$saleOrder['order_quantity'],
                        'rate_per_kg'=>$request['rate'][$key]
                    ]);

                $orderUpdate = $saleOrder->update(['indent_status'=>1]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }

        return redirect('/indent-list');
    }

    public function indentList($date = null)
    {
        if ($date == null) {
            $today = Carbon::now();
        } else {
            $today = Carbon::parse('01-'.$date);
        }

        $indentMaster = IndentMaster::where('dated_on', 'LIKE', $today->format('Y-m').'%')->with(['businessPartner','invoice'])->get();
        // dd($indentMaster[0]->invoice->count());

        return view('sales.indent_list')->with(compact('indentMaster'));
    }

    public function deleteIndent($id)
    {
        $indentMaster = IndentMaster::where('id', $id)->first();

        $indentDetails = $indentMaster->indentDetails;

        try {
            foreach ($indentDetails as $key => $value) {
                $updateStatus = $value->saleOrder->update(['indent_status'=>0]);

                $value->delete();
            }

            $indentMaster->delete();

            return redirect('/indent-list');
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }
    }

    public function editIndent($id)
    {
        $indentMaster = IndentMaster::where('id', $id)->first();

        if (is_null($indentMaster)) {
            return abort(404);
        }

        return view('sales.edit_indent', ['indent'=>$indentMaster]);
    }

    public function deleteIndentDetail($id)
    {
        $indentDetail = IndentDetail::where('id', $id)->first();

        if (is_null($indentDetail)) {
            return abort(404);
        }

        try {
            $indentId = $indentDetail['indent_id'];

            $indentDetail->saleOrder->update(['indent_status'=>0]);

            $indentDetail->delete();

            return redirect("/edit-indent/".$indentId);
        } catch (\Exception $e) {
            return redirect("/edit-indent/".$indentId)->withErrors(['errors' => $e->getMessage()]);
        }
    }

    public function addIndentDetail(Request $request)
    {
        $indentMaster = IndentMaster::where('id', $request['id'])->first();

        try {
            if (array_key_exists('order', $request->all())) {
                if (count($request['order']) > 0) {
                    foreach ($request['order'] as $key => $value) {
                        $saleOrder = SaleOrder::where('id', $value)->first();

                        $itemMaster = $saleOrder->itemMaster;

                        $createIndent =  $indentMaster->indentDetails()
                            ->create(['order_id'=>$value,
                                'material_id' => $saleOrder['material_id'],
                                'hsn_sac' => $itemMaster['hsn_sac'],
                                'gst_rate' => $itemMaster['igst'],
                                'due_on' => date('Y-m-d', strtotime($request['due_date'][$key])),
                                'quantity' => $saleOrder['order_quantity'],
                                'rate_per_kg' => $request['rate'][$key]
                            ]);

                        $orderUpdate = $saleOrder->update(['indent_status'=>1]);
                    }
                }
            }

            return redirect("/edit-indent/".$request['id']);
        } catch (\Exception $e) {
            return redirect("/edit-indent/".$request['id'])->withErrors(['error' => 'Cannot add the indent Details now.']);
        }
    }

    public function updateIndent(Request $request)
    {
        $indentMaster = IndentMaster::where('id', $request['indent_id'])->first();

        if (!is_null($indentMaster)) {
            $updateIndentMster = $indentMaster->update([
                'quotation_number'=>$request['quotation-number'],
                'transit_insurance'=>$request['tax'],
                'dated_on'=>date('Y-m-d', strtotime($request['date'])),
                'order_ref_no'=>$request['reference-no'],
                'other_ref_no'=>$request['other-reference-no'],
                'dispatcher'=>$request['dispatch'],
                'destination'=>$request['destination'],
                'terms_of_delivery'=>$request['terms'],
            ]);

            if (count($request['detail_id']) > 0) {
                foreach ($request['detail_id'] as $key => $value) {
                    $indentDetail = IndentDetail::where('id', $value)
                        ->update(['due_on'=>date('Y-m-d', strtotime($request['due_date'][$key])),
                            'rate_per_kg'=>$request['rate'][$key]
                        ]);
                }
            }

            return redirect('/indent-list');
        }
    }


    public function showItemMaster()
    {
        $businessPartners = BusinessPartner::all();
        return view('sales.business-partner-list')->with(compact('businessPartners'));
    }

    public function getSalesCsv()
    {
        $date = Carbon::now();
        $saleOrder = SaleOrder::where('pending_quantity', '>', 0)->orderBy('customer_date')->get();
        $finalArray = [];
        $count = 0;
        foreach ($saleOrder as $key => $value) {
            $finalArray[$count][] = str_replace(',', '', $value->businessPartner['bp_name']);
            $finalArray[$count][] = $value->material_name;
            $finalArray[$count][] = $value['order_quantity'];
            $finalArray[$count][] = $value['customer_date'];
            $finalArray[$count][] = $value['scpl_confirmation_date'];
            $finalArray[$count][] = $value['dispatch_date'];
            $finalArray[$count][] = $value['dispatch_quantity'];
            $finalArray[$count][] = $value['pending_quantity'];
            $count++;
        }
        return response()->json(['orders'=>$finalArray]);
    }

    public function salesSummary()
    {
        return view('sales.sales_summary');
    }

    public function getSalesSummary(Request $request)
    {
        $from = date('Y-m-d', strtotime($request['from-date']));
        $to = date('Y-m-d', strtotime($request['to-date']));

        $saleOrder = SaleOrder::whereBetween('order_date', [$from,$to])->orderBy('order_date')->get();
        $finalArray = [];
        $count = 0;
        foreach ($saleOrder as $key => $value) {
            $finalArray[$count][] = str_replace(',', '', $value->businessPartner['bp_name']);
            $finalArray[$count][] = $value->material_name;
            $finalArray[$count][] = $value['order_quantity'];
            $finalArray[$count][] = $value['order_date'];
            $finalArray[$count][] = $value['customer_date'];
            $finalArray[$count][] = $value['scpl_confirmation_date'];
            $finalArray[$count][] = $value['dispatch_date'];
            $finalArray[$count][] = $value['dispatch_quantity'];
            $finalArray[$count][] = $value['pending_quantity'];
            $count++;
        }
        return response()->json(['orders'=>$finalArray]);
    }

    public function customerSummary()
    {
        $bp = BusinessPartner::orderBy('bp_name')->get();
        return view('sales.customer_summary')->with(compact('bp'));
    }

    public function getSalesReport()
    {
        return view('sales.get-sales-report');
    }

    public function updateDateCommitted(Request $request)
    {
        $request->validate([
          "id" => "required",
          "committed_date" => "required",
      ]);
        try {
            $Date = $request['committed_date'];
            $committedDate =  date('d-m-Y', strtotime($Date));
            $bufferDate = date('d-m-Y', strtotime($Date. ' + 3 days'));
            if (is_null($request['type'])) {
                $committedDate =  $committedDate.'/'.$request['quantity'];
                $bufferDate = $bufferDate.'/'.$request['quantity'];
            } else {
                $committedDate =  $committedDate.'/'.$request['quantity'].'/'.$request['type'];
                $bufferDate = $bufferDate.'/'.$request['quantity'].'/'.$request['type'];
            }
            $saleOrder = SaleOrder::where('id', $request['id'])->first();
            if (!is_null($saleOrder->committed_date)) {
                $oldCommitted = explode(',', $saleOrder->committed_date);
                $oldBuffer = explode(',', $saleOrder->scpl_confirmation_date);
                $oldCommitted[] = $committedDate;
                $oldBuffer[] = $bufferDate;
            } else {
                $oldCommitted[] = $committedDate;
                $oldBuffer[] = $bufferDate;
            }
            $saleOrder->update(['committed_date'=>implode(",", $oldCommitted),'scpl_confirmation_date'=>implode(",", $oldBuffer)]);
            return redirect('/');
        } catch (\Exception $e) {
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    public function preCloseOrder($id = null)
    {
        $pendingOrders = \DB::table('indent_details')
                          ->join('sale_orders', 'indent_details.order_id', 'sale_orders.id')
                          ->join('indent_masters', 'indent_details.indent_id', 'indent_masters.id')
                          ->join('business_partners', 'sale_orders.bp_id', 'business_partners.id')
                          ->where('sale_orders.pending_quantity', '>', 0)
                          ->where('sale_orders.order_quantity', '>', 0)
                          ->orderBy('sale_orders.customer_date')
                          ->get([
                            'sale_orders.id AS id',
                            'business_partners.id AS bp_id',
                            'business_partners.bp_name AS bp_name',
                            'indent_masters.order_ref_no AS ref_no',
                            'sale_orders.pending_quantity AS pending_quantity',
                            'sale_orders.order_date AS customer_date',
                            'sale_orders.material_name AS material_name',
                            'indent_details.rate_per_kg AS price',
                            'sale_orders.order_quantity AS order_quantity',
                            'sale_orders.dispatch_quantity AS dispatch_quantity'
                          ]);
        $businessPartners = BusinessPartner::all();
        // foreach ($businessPartners as $key => $value) {
        //     # code...
        //     dd($value);
        // }
        if (!is_null($id)) {
            $pendingOrders =  $pendingOrders->where('bp_id', $id);
        }
        return view('sales.pre_close_order')->with(compact('pendingOrders', 'businessPartners', 'id'));
    }

    public function preCloseData(Request $request)
    {
        $id = $request['id'];
        try {
            $saleOrder = SaleOrder::where('id', $id)->first();
            SaleOrder::where('id', $id)->update(['pre_close'=>$saleOrder->pending_quantity,'pending_quantity'=>0,'pre_close_reason'=>$request['reason']]);
            return \Redirect::back();
        } catch (\Exception $e) {
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

    public function businessPartner()
    {
        $businessPartners = BusinessPartner::where('bp_name', '!=', 'buffer')->get();
        return view('sales.business_partner')->with(compact('businessPartners'));
    }

    public function createBusinessPartner(Request $request)
    {
        $request->validate([
          'password' => 'required|regex:"master@sales"',
//          'bp_name'=>'required',
//          'gst_code'=>'required',
//          'address'=>'required',
//          'state'=>'required',
//          'district'=>'required',
//          'code'=>'required',
//          'ar_no'=>'required',
//          'pincode'=>'required|numeric',
//          'contact_no'=>'required|numeric',
//          'phone_number'=>'required|numeric',
//          'contact_person'=>'required',
//          'dispatcher'=>'required',
//          'dispatch_destination'=>'required',
//          'email'=>'required|email',
      ]);
        try {
            $bpData = $request->all();
            unset($bpData['password']);
            if ($request['id'] == "") {
                unset($bpData['id']);
                $businessPartner = BusinessPartner::create($bpData);
                return response()->json(['status'=>true]);
            } else {
                $id = $bpData['id'];
                unset($bpData['id']);
                $businessPartner = BusinessPartner::where('id', $id)->update($bpData);
                return response()->json(['status'=>true]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function getBp(Request $request)
    {
        try {
            $businessPartner = BusinessPartner::where('id', $request['id'])->first();
            return response()->json(['status'=>true,'bp'=>$businessPartner]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }


    public function showPrecloseReport(Request $request)
    {
//        dd($request->all());
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $startDate = date('Y-m-d', strtotime($request['start_date']));
        $endDate = date('Y-m-d', strtotime($request['end_date']));

        $saleOrders = SaleOrder::where('pre_close_reason', '!=', null)
            ->whereBetween('order_date', [$startDate, $endDate])
            ->orderBy('order_date')
            ->get();


        $results = [];


        foreach ($saleOrders as $saleOrder) {
            $results[$saleOrder->id]['customer_name'] = $saleOrder->businessPartner->bp_name;
            $results[$saleOrder->id]['order_date'] = date('d-m-Y', strtotime($saleOrder->order_date));
            $results[$saleOrder->id]['contract_no'] = $saleOrder->indentDetails->indentMaster->order_ref_no;
            $results[$saleOrder->id]['contract_date'] = date('d-m-Y', strtotime($saleOrder->indentDetails->indentMaster->dated_on));
            $results[$saleOrder->id]['order_qty'] = $saleOrder->order_quantity;
            $results[$saleOrder->id]['dispatch_qty'] = $saleOrder->dispatch_quantity;
            $results[$saleOrder->id]['preclose_qty'] = $saleOrder->pre_close;
            $results[$saleOrder->id]['preclose_reason'] = $saleOrder->pre_close_reason;
        }

        return view('sales.pre-close-report-view', compact('results', 'startDate', 'endDate'));
    }
}
