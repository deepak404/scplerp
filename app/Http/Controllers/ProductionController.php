<?php

namespace App\Http\Controllers;

use App\DipSolution;
use App\DipSolutionClearance;
use App\InvoiceMaster;
use App\Loggers\DatabaseLogger;
use App\SalesReturn;
use App\SalesReturnDetails;
use App\WeightLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\FilamentClearance;
use App\PalletEntry;
use App\Filament;
use App\DipCordSpecification;
use App\ItemMaster;
use App\PackageMaster;
use App\Http\Controllers\ActivityController;

class ProductionController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('production');

    }

    public function filamentClearance()
    {
        $lots = FilamentClearance::all();

        return view('production.filament_clearance_production')->with(compact('lots'));
    }

    public function palletEntry($id)
    {
        $data = PalletEntry::where('fl_id', $id)->first();

        $pallets = json_decode($data['pallets']);

        return view('production.filament_pallet_entry')->with(compact('id', 'pallets'));
    }

    public function updatePalletEntry(Request $request)
    {
        $filamentClearance = FilamentClearance::where('id', $request['id'])->first();

        $finalData = [];

        foreach ($request['pallet-no'] as $key => $value) {
            if (!is_null($value)) {
                $finalData['pallet_no'][] = $value;
                $finalData['weight'][] = $request['pallet-weight'][$key];
            }
        }
        if (is_null($filamentClearance->palletEntry)) {
            $filamentClearance->palletEntry()->create(['pallets'=>json_encode($finalData)]);
        } else {
            $filamentClearance->palletEntry()->update(['pallets'=>json_encode($finalData)]);
        }
        $filamentClearance->update(['edit_delete'=>1]);
        return redirect('/filament-clearance-production');
    }



    public function planVsActual()
    {
        return view('production.plan-vs-actual');
    }


    public function dipSolution()
    {
        $solutions = array_keys(DipSolution::all()->groupBy('name')->toArray());
        $dipSolutionResult = DipSolutionClearance::all();


        return view('production.dip-solution-clearance')->with(compact('solutions', 'dipSolutionResult'));
    }


    public function createDipSolutionClearance(Request $request)
    {
        $request->validate([
            'solution-name' => 'required',
            'batch-no' => 'required',
            'start-date' => 'required',
            'completion-date' => 'required',
            'prepared-by' => 'required',
            'sample-collected' => 'required',
            'master-date' => 'required',
        ]);



        try {
            $create = DipSolutionClearance::create([
                'name' => $request['solution-name'],
                'batch_no' => $request['batch-no'],
                'start_date' => Carbon::make($request['start-date'])->toDateTimeString(),
                'end_date' => Carbon::make($request['completion-date'])->toDateTimeString(),
                'prepared_by' => $request['prepared-by'],
                'collected_by' => $request['sample-collected'],
                'expiry_date' => Carbon::make($request['master-date'])->addDays(10)->toDateTimeString(),
                'status' => 0,
            ]);


            $dipSolutionResult = DipSolutionClearance::where('status', 0)->get();


            return redirect('/dip-solution-clearance-production')->with(['message' => 'Dip Solution Clearance Lot successfully created', 'dipSolutionResult' => $dipSolutionResult]);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }

    public function deleteDipSolutionLot($id)
    {
        try {
            $delete = DipSolutionClearance::where('id', $id)->delete();

            if ($delete) {
                return redirect()->back()->with('message', 'Dip Solution Clearance Lot is successfully Deleted.');
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['error' => 'Cannot Delete the Dip Solution Lot Right now. Please refresh and try again.']);
        }
    }

    public function dipCordsList($date = null)
    {
        return view('production.dip_cord_list');
    }

    public function dipCordsClearance()
    {
        $expDate = date('Y-m-d H:i:s');
        $itemMaster = DipCordSpecification::orderBy('material')->get();
        $filaments = Filament::distinct()->get(['name'])->sortBy('name');
        $solutions = DipSolutionClearance::where('status', 1)->where('expiry_date', '>=', $expDate)->get()->toArray();

        return view('production.dip_cord_create')->with(compact('itemMaster', 'filaments', 'solutions'));
    }

    public function dipCordsClearanceCreate(Request $request)
    {
        $request->validate([
          'machine' => 'required',
          'material' => 'required',
          'filament' => 'required',
          'lot_no' => 'required',
          'customer' => 'required',
          'speed' => 'required',
          'no_cords' => 'required',
          'created_by' => 'required',
          'tpm' => 'required'
      ]);
        dd($request->all());
    }

    public function packedCase()
    {
        $packedType = PackageMaster::distinct('material_type')->get(['material_type']);
        return view('production.packed-case')->with(compact('packedType'));
        // dd($packedType);
    }

    public function productionStock()
    {
        return view('production.production-stock');
    }


    public function productionReport()
    {
        return view('production.production-all');
    }

    // public function sample(){

    //     $controller = new ActivityController(new DatabaseLogger());

    //     dd($controller->sample());

    // }


    public function stickerChange(){

        $fiscalYear = (date('m') > 3) ? date('Y') + 1 : date('Y');
        $fiscalStart = date(''.($fiscalYear - 1).'-04-01');
        $fiscalEnd = date(''.($fiscalYear).'-03-31');

        $doffs = WeightLog::whereBetween('doff_date',[$fiscalStart, $fiscalEnd])->where('weight_status',0)->where('active_status',0)->pluck('doff_no')->toArray();

        $doffs = array_unique($doffs);
        sort($doffs);

        return view('production.sticker-changes')->with(compact('doffs'));
    }

    public function salesReturn(){
        $salesReturns = SalesReturn::where('update_status',0)->get();

        return view('production.sales_return')->with(compact('salesReturns'));
    }

    public function updateSalesReturnSpindles($id){
        $salesReturnDetails = SalesReturnDetails::where('sales_return_id',$id)->get();
        $result = [];
        foreach ($salesReturnDetails as $sr){
            $packages = PackageMaster::where('invoice_master_id', $sr->invoice_id)->pluck('id')->toArray();
            $spindles = WeightLog::whereIn('package_master_id',$packages)->where('invoice_status',1)->get();
            $invoiceNo = InvoiceMaster::where('id',$sr->invoice_id)->value('invoice_no');
            $result[$invoiceNo] = $spindles;
        }


        return view('production.sales_return_list')->with(compact('result','id'));

    }

    public function createSalesReturnSpindles(Request $request){

        $request->validate([
            'spindle_details' => 'required',
            'sales_return_id' => 'required',
        ]);

        $result = [];

        foreach ($request['spindle_details'] as $spindle){
            $invoiceId = InvoiceMaster::where('invoice_no',explode('/',$spindle)[1])->value('id');
            $weightLogId = explode('/',$spindle)[0];

            $result[$invoiceId][] = $weightLogId;
        }

        foreach ($result as $invoiceId => $spindles){
            SalesReturnDetails::where('sales_return_id',$request['sales_return_id'])->where('invoice_id',$invoiceId)->update([
                'spindles' => (implode(',', $spindles))
            ]);
            WeightLog::whereIn('id',$spindles)->update(['sales_return_status' => 1]);

        }

        SalesReturn::where('id', $request['sales_return_id'])->update(['update_status' => 1]);

        return redirect('/production-sales-return');
    }
}
