<?php

namespace App\Http\Controllers;

use App\PackageDetail;
use App\PackedBobbin;
use App\WeightLog;
use App\WeightLogDetails;
use App\WeightLogMaster;
use Illuminate\Http\Request;
use App\ItemMaster;
use App\PackageMaster;
use Carbon\Carbon;

class PackagingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('packaging');
    }

	public function index($date = null)
	{


        $unpackedWeightLog = WeightLog::where('packed_status', 0)->orWhere('packed_status',null)->get();

        $unpackedMaterialMaster = ItemMaster::whereIn('material',
            array_keys($unpackedWeightLog->groupBy('material')->toArray())
            )->get();


//        dd($unpackedWeightLog, $unpackedMaterialMaster);


        if ($date == null) {
            $today = Carbon::now();
        }else{
            $today = Carbon::parse('01-'.$date);
        }


		$itemMaster = ItemMaster::all();


        $packageMaster = PackageMaster::where('packed_date','LIKE',$today->format('Y-m').'%')
                ->orderBy('packed_date')->get();


		return view('packaging.packaging-details', [
                'itemMaster'=>$itemMaster,
                'packageMaster'=>$packageMaster,
                'unpackedMaterialMaster' => $unpackedMaterialMaster,
            ]);

	}

	public function packagingDetail($id)
	{
		$packageMasters = PackageMaster::where('id',$id)->first();
		$bobbin_count = $packageMasters['bobbin_count'];
		$empty_weight = $packageMasters['bobbin_weight'];
		$moisture_weight = $packageMasters['moisture_weight'];
		$box_weight = $packageMasters['box_weight'];
		$packagingDetail = $packageMasters->packagingDetails()->get();
		$finalArray = [];

		$material = ItemMaster::where('id', $packageMasters->material_id)->value('material');


		$availableDoff = WeightLog::where('packed_status',0)->orWhere('packed_status',null)->where('material', $material)->get();

//        dd($availableDoff);

		foreach ($packagingDetail as $key => $value) {
			$packedBobbin = $value->packagingBobbin->toArray();
			$total_weight = 0;
			foreach ($packedBobbin as $bobbin) {
				$total_weight += $bobbin['b_wt'];
			}
			$net_weight = $total_weight-$empty_weight;
			$finalArray[] = [
									'case_no'=>$value['case_no'],
									'doff_no'=>$value['doff_no'],
									'bobbin_count'=>$bobbin_count,
									'bobbin_weight'=>$empty_weight,
									'total_weight'=>$total_weight,
									'gross_weight'=>$total_weight+$box_weight,
									'net_weight'=>$net_weight,
									'commercial_weight'=>$net_weight+$moisture_weight,
									'packed_bobbin'=>$packedBobbin
								];
		}


		return view('packaging.packaging-details-one',[
		    'id'=>$id,
            'bobbin_count'=>$bobbin_count,
            'data'=>$finalArray,
            'availableDoff' => $availableDoff,
            ]);
	}

	public function addPackagingMaster(Request $request)
	{

		$request->validate([
			'package_type' => 'required',
			'quality' => 'required|numeric',
			'date' => 'required|date',
			'box_type' => 'required',
			'bobbin_weight' => 'required|numeric',
			'box_weight' => 'required|numeric',
			'moisture' => 'required|numeric',
            'bobbin_count' => 'required|integer'
        ]);

		if ($request['id'] == 0) {

			$createPackageMaster = PackageMaster::create([
									'package_type' => $request['package_type'],
									'material_id' => $request['quality'],
									'packed_date' => date('Y-m-d',strtotime($request['date'])),
									'box_type' => $request['box_type'],
									'box_weight' => $request['box_weight'],
									'moisture_weight' => $request['moisture'],
									'bobbin_count' => $request['bobbin_count'],
									'bobbin_weight' => $request['bobbin_weight']
								]);

			if ($createPackageMaster) {

				return response()->json(['status'=>true,'msg'=>'PackageMaster successfully Added.']);

			}else{

				return response()->json(['status'=>false,'msg'=>'Adding Package has some issue.']);

			}

		}else{

			$createPackageMaster = PackageMaster::where('id',$request['id'])->update([
									'package_type' => $request['package_type'],
									'material_id' => $request['quality'],
									'packed_date' => date('Y-m-d',strtotime($request['date'])),
									'box_type' => $request['box_type'],
									'box_weight' => $request['box_weight'],
									'moisture_weight' => $request['moisture'],
									'bobbin_count' => $request['bobbin_count'],
									'bobbin_weight' => $request['bobbin_weight']
								]);

			if ($createPackageMaster) {

				return response()->json(['status'=>true,'msg'=>'PackageMaster successfully updated.']);

			}else{

				return response()->json(['status'=>false,'msg'=>'Updating Package has some issue.']);

			}

		}
	}

	public function addPackageDetails(Request $request)
	{


	    //DOFF No is weight Log Master ID
        // ID is Packing Master ID

	    $request->validate([
	        'id' => 'required',
            'doff_no' => 'required',
            'case_no' => 'required',
            'b_no' => 'required',
        ]);


		$packageMaster = PackageMaster::where('id',$request['id'])->first();


		$weight = WeightLogDetails::whereIn('id',$request['b_no'])->sum('weight');

		$createPackageDetail = $packageMaster
								->packagingDetails()
								->create([
									'case_no'=>$request['case_no'],
									'doff_no'=> WeightLogMaster::where('id', $request['doff_no'])->value('doff_no'),
                                    'weight' => round($weight, 2),
                                    'invoice_status' => 0
								]);
		$count = 1;
 		foreach ($request['b_no'] as $key => $value) {

 		    if($value != 'empty'){
                $bobbin = WeightLogDetails::where('id', $value)->first();

                $bobbin->packed_status = 1;
                $bobbin->save();

                $createPackage = $createPackageDetail
                    ->packagingBobbin()
                    ->create([
                        'bobbin' => 'B'.$count,
                        'b_no' => $bobbin->spindle,
                        'b_wt' => $bobbin->weight,
                    ]);
                $count += 1;
            }

		}


 		$weightLogDetail = WeightLogDetails::where('master_id',$request['doff_no'])->where('quality_check',0)->where('packed_status',0)->count();

 		if($weightLogDetail == 0){

    		$weightLogMaster = WeightLogMaster::where('id', $request['doff_no'])->update(['packed_status' => 1]);

        }



		return response()->json(['status'=>true]);
	}


	public function deletePackagingDetail($id){

        try{
            $packagingMaster = PackageMaster::where('id',$id)->first();

            $packagingDetails = PackageDetail::where('master_id', $packagingMaster->id)->pluck('id')->toArray();

            $packedBobbins = PackedBobbin::whereIn('detail_id', $packagingDetails)->delete();


            $packagingMaster->packagingDetails()->delete();

            $packagingMaster->delete();

            return redirect('/packaging');


        }catch(\Exception $e){

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }
    }


    public function getPackageDetails($id){

        $packageDetails = PackageMaster::where('id', $id)->first();

        return response()->json(['data' => $packageDetails], 200);
    }


    public function updatePackagingMaster(Request $request){

        $request->validate([

            'id' => 'required',
            'package_type' => 'required',
            'quality' => 'required|numeric',
            'date' => 'required|date',
            'box_type' => 'required',
            'bobbin_weight' => 'required|numeric',
            'box_weight' => 'required|numeric',
            'moisture' => 'required|numeric',
            'bobbin_count' => 'required|integer'

        ]);


        try{
            $packageMaster = PackageMaster::where('id', $request['id'])->first();

            $packageMaster->package_type = $request['package_type'];
            $packageMaster->material_id = $request['quality'];
            $packageMaster->packed_date = date('Y-m-d', strtotime($request['date']));
            $packageMaster->box_type = $request['box_type'];
            $packageMaster->bobbin_weight = $request['bobbin_weight'];
            $packageMaster->box_weight = $request['box_weight'];
            $packageMaster->moisture_weight = $request['moisture'];
            $packageMaster->bobbin_count = $request['bobbin_count'];

            $update = $packageMaster->save();

            if($update){
                return redirect('/packaging');
            }


        }catch(\Exception $e){
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }
    }


    public function getDoffWeightLogDetails(Request $request){
        $request->validate([
            'id' => 'required'
        ]);


        $weightLogDetails = WeightLogDetails::where('master_id', $request['id'])->where('packed_status', 0)
            ->where('quality_check',0)
            ->get();


        return response()->json(['weightLogDetails' => $weightLogDetails], 200);


    }
}
