<?php

namespace App\Http\Controllers;

use App\ChemicalClearance;
use App\ChemicalClearanceResult;
use App\Chemicals;
use App\DipSolution;
use App\DipSolutionClearance;
use App\PackagingClearance;
use App\PackagingMaterial;
use App\PackingClearanceResult;
use Illuminate\Http\Request;
use App\FilamentClearance;
use App\Filament;
use App\TestedPallet;

class QualityController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
       $this->middleware('quality_check');
    }

    public function showChemicalLot(){

        $chemicalClearance = ChemicalClearance::where('status',0)->get();


        return view('qc.quality-chemical-clearance-list')->with(compact('chemicalClearance'));

    }


    public function updateChemicalLot($id){
        $chemicalLot = ChemicalClearance::where('id', $id)->first();

        if($chemicalLot->status != 0 ){

            return redirect('/edit-updated-chemical-details/'.$id);

        }

        $chemicalProperties = Chemicals::where('material_code',$chemicalLot->chemical_code)->get();
//        dd($chemicalLot, $chemicalProperties);

        return view('qc.chemical-quality-update')->with(compact('chemicalLot','chemicalProperties'));
    }


    public function insertChemicalClearanceResult(Request $request){

        $request->validate([
            'id' => 'required',
            'type' => 'required',
            'tested-by' => 'required',
            'tested-date' => 'required',
            'test-arrival-date' => 'required',
            'final-result' => 'required',
            'parameter-sample' => 'required',
        ]);

        $clearance = ChemicalClearance::where('id', $request['id'])->first();

        if($request->type == 'create'){

            $save = $clearance->chemicalClearanceResult()->create([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);

        }else{

            $save = $clearance->chemicalClearanceResult()->update([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);
        }



        if($save){

            if($request['final-result'] == 'cleared'){
                $clearance->update([
                    'status' => 1
                ]);
            }else{
                $clearance->update([
                    'status' => 2
                ]);
            }

            return response()->json(['message' => 'success'], 200);

        }


        return response()->json(['message' => 'failed'], 500);





    }


    public function showUpdatedChemicalClearance(){
        $chemicalClearance = ChemicalClearance::where('status','!=',0)->get();

        return view('qc.chemical-clearance-updated-list')->with(compact('chemicalClearance'));
    }


    public function editUpdatedChemical($id){

        $chemicalLot = ChemicalClearance::where('id', $id)->first();

        $chemicalClearanceResult = ChemicalClearanceResult::where('clearance_id', $chemicalLot->id)->first();

        $chemicalProperties = Chemicals::where('material_code',$chemicalLot->chemical_code)->get();

//        $chemicalProperties = Chemicals::where('name',$chemicalLot->chemical)->get();

        return view('qc.edit-updated-chemical-quality')->with(compact('chemicalLot','chemicalProperties','chemicalClearanceResult'));

    }


    public function showPackingMaterialClearance(){

        $packageClearance = PackagingClearance::where('status',0)->get();
        $packagingMaterial = PackagingMaterial::all()->groupBy('name');

       // dd($packageClearance);

        return view('qc.packing-quality-list')->with(compact('packagingMaterial','packageClearance'));
    }


    public function updatePackingMaterialLot($id){

        $packingMaterialClearance = PackagingClearance::where('id',$id)->first();
       // dd($packingMaterialClearance);

        if($packingMaterialClearance->status != 0){

            return redirect('/edit-updated-packing-details/'.$id);
        }


        $packingMaterial = PackagingMaterial::where('name', $packingMaterialClearance->item)->get();

//        dd($packingMaterial);

        return view('qc.packing-quality-update')->with(compact('packingMaterial','packingMaterialClearance'));
    }


    public function insertPackingClearanceResult(Request $request){

        $request->validate([
            'id' => 'required',
            'type' => 'required',
            'tested-by' => 'required',
            'tested-date' => 'required',
            'test-arrival-date' => 'required',
            'final-result' => 'required',
            'parameter-sample' => 'required',
        ]);
//        http_response_code(500);

        $clearance = PackagingClearance::where('id', $request['id'])->first();

//        dd($clearance);

        if($request->type == 'create'){

            $save = $clearance->packingClearanceResult()->create([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);

        }else{

            $save = $clearance->packingClearanceResult()->update([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);
        }



        if($save){

            if($request['final-result'] == 'cleared'){
                $clearance->update([
                    'status' => 1
                ]);
            }else{
                $clearance->update([
                    'status' => 2
                ]);
            }

            return response()->json(['message' => 'success'], 200);

        }


        return response()->json(['message' => 'failed'], 500);


    }


    public function showUpdatedPackingClearance(){
        $packingClearance = PackagingClearance::where('status','!=',0)->get();

        return view('qc.packing-clearance-updated-list')->with(compact('packingClearance'));
    }


    public function editUpdatedPacking($id){

        $packingLot = PackagingClearance::where('id', $id)->first();
        $packingClearanceResult = PackingClearanceResult::where('clearance_id', $packingLot->id)->first();
        $packingMaterialProperties = PackagingMaterial::where('name',$packingLot->item)->get();

        return view('qc.edit-updated-packing-quality')->with(compact('packingLot','packingMaterialProperties','packingClearanceResult'));

    }

    public function filamentClearanceQc()
    {
        $lots = FilamentClearance::all();

        return view('qc.filament_clearance_qc')->with(compact('lots'));
    }

    public function filamentQualityUpdate($id)
    {
        $filamentLot = FilamentClearance::where('id',$id)->first();

        $filamentTest = Filament::where('name',$filamentLot['type'])->get();

        $testedPallet = $filamentLot->testedPallet;

        // $testedPalletData = null;
        //
        //
        // if (count($testedPallet) > 0) {
        //
        //     $testedPalletData = json_decode($testedPallet['result']);
        //
        // }
        // dd($testedPallet);
        return view('qc.filament-quality-update-two')->with(compact('filamentTest','id','filamentLot','testedPallet'));
    }

    public function filamentPalletDelete($id)
    {
      $testedPallet = TestedPallet::where('id', $id)->first();

      if (!empty($testedPallet)) {
        $testedPallet->delete();
      }

      return response()->json(['code'=>1]);
    }

    public function filamentClearanceUpdate($id)
    {
      if (!is_null(FilamentClearance::where('id', $id)->value('clearance_status'))) {
        return redirect('/filament-clearance-qc');
      }

      $testedPallet = TestedPallet::where('fl_id', $id)->get();
      if (count($testedPallet) > 0) {
        $testedResult = [];
        foreach ($testedPallet as $pallet) {
          $result = json_decode($pallet->result);
          foreach ($result->parameter as $key => $value) {
            $testedResult[$value]['actual'][] = $result->actual[$key];
            $testedResult[$value]['scpl'] = $result->scpl[$key];
          }
        }
        // dd($testedResult);
        $finalResult = [];
        foreach ($testedResult as $key => $value) {
            $min = "";
            $max = "";
            $avg = "";
            $rang = 0.0;
            $test = "";
            if ($key != 'Variety') {
                if (strpos($value['scpl'], '±')) {
                    $data = explode('±', $value['scpl']);
                    $min = $data[0]-$data[1];
                    $max = $data[0]+$data[1];
                }else{
                    /** To remove non-numeric characters from string **/
                    $min = preg_replace("/[^0-9.]/","", $value['scpl']);
                }
                $avg = (array_sum($value['actual'])/count($value['actual']));
                $avg = round($avg, 2);
                $rangeMax = max($value['actual']);
                $rangeMin = min($value['actual']);
                if (is_numeric($rangeMax)) {
                  $rang = round(($rangeMax-$rangeMin), 2);
                  if (($min <= $avg) && ($max >= $avg)) {
                    $test = 'Ok';
                  }elseif ($max == "") {
                    if ($min <= $avg) {
                      $test = 'Ok';
                    }else{
                      $test = 'Not Ok';
                    }
                  }else{
                    $test = 'Not Ok';
                  }
                }else{
                  $test = 'Not Ok';
                }
            }else{
                $min = $value['scpl'];
                $max = $value['scpl'];
                $test = 'Ok';
                $avg = $value['scpl'];
            }
            $finalResult[$key]['scpl'] = $value['scpl'];
            $finalResult[$key]['min'] = $min;
            $finalResult[$key]['max'] = $max;
            $finalResult[$key]['avg'] = $avg;
            $finalResult[$key]['range'] = $rang;
            $finalResult[$key]['status'] = $test;
        }
        // dd($finalResult);
        return view('qc.filament-report-update')->with(compact('finalResult','id'));
      }
      return redirect('/filament-clearance-qc');
    }



    public function filamentQualityStatus(Request $request)
    {
        $id = $request['id'];
        $filamentLot = FilamentClearance::where('id',$id)->first();

        $updateFilamentLot = $filamentLot->update(['tested_by'=>$request['tested-by'],
                                                    'test_date'=>date('Y-m-d',strtotime($request['tested-date'])),
                                                    'sample_date'=>date('Y-m-d',strtotime($request['tested-arrival-date'])),
                                                    'edit_delete' => 1
                                                ]);

        return response()->json(['status'=>1]);
    }

    public function filamentPalletResult(Request $request)
    {
      if (is_null($request['pallet-id'])) {
        $filamentClearance = FilamentClearance::where('id', $request['id'])->first();
        if (empty($request['parameter'])) {
          return response()->json(['code'=>2,'msg'=>'Select parameters to create entry.']);
        }
        $result = [
                    'id'=>$request['data-id'],
                    'parameter'=>$request['parameter'],
                    'actual'=>$request['actual'],
                    'scpl'=>$request['scpl']
                  ];
        $filamentClearance->testedPallet()->create([
                                                    'ps_no'=>$request['ps_no'],
                                                    'code'=>$request['code'],
                                                    'pallet_no'=>$request['pallet_no'],
                                                    'lea_weight'=>$request['lea_weight'],
                                                    'actual'=>$request['actual-denier'],
                                                    'result'=>json_encode($result),
                                                  ]);
        return response()->json(['code'=>1,'msg'=>'Successfully Create!!! Add Next Pallet.']);
      }else{
        return response()->json(['code'=>3,'msg'=>'Successfully updated!!!']);

      }
    }

    // public function filamentReportUpdate($id)
    // {
    //     $filamentLot = FilamentClearance::where('id',$id)->first();
    //
    //     $filamentTest = Filament::where('name',$filamentLot['type'])->get();
    //
    //     $testedPallet = $filamentLot->testedPallet;
    //     $testedPalletData = null;
    //     $reportResult = null;
    //     if (!is_null($testedPallet['report_result'])) {
    //         $reportResult = json_decode($testedPallet['report_result']);
    //     }
    //     // dd(in_array('Strength', $reportResult->parameter),array_search ('Strength', $reportResult->parameter));
    //
    //     $testedResult = [];
    //
    //     if (!is_null($testedPallet)) {
    //
    //         $testedPalletData = json_decode($testedPallet['result']);
    //         foreach ($testedPalletData->result as $key => $value) {
    //             $result = json_decode($value);
    //             foreach ($result->parameter as $key2 => $data) {
    //                 $umo = Filament::where('parameter',$data)->first();
    //                 $testedResult[$data]['actual'][] = $result->actual[$key2];
    //                 $testedResult[$data]['scpl'] = $result->scpl[$key2];
    //                 $testedResult[$data]['uom'] = $umo['unit'];
    //             }
    //         }
    //         foreach ($testedResult as $key => $value) {
    //             $avg = "";
    //             if ($key != 'Variety') {
    //                 $avg = array_sum($value['actual'])/count($value);
    //                 $avg = round($avg, 2);
    //             }else{
    //                 $avg = $value['scpl'];
    //             }
    //             $testedResult[$key]['avg'] = $avg;
    //         }
    //     }
    //
    //     return view('qc.filament-report-update')->with(compact('filamentTest','testedResult','id','reportResult'));
    //
    // }

    public function ReportUpdate(Request $request)
    {
        $lotSpecification = [];
        $clearance = 1;
        $filamentClearance = FilamentClearance::where('id',$request['id'])->first();
        $data = $request->all();
        if (array_key_exists('parameter', $request->all())) {
            foreach ($data['parameter'] as $key => $value) {
                $lotSpecification['parameter'][] = $value;
                $lotSpecification['actual'][] = $data['actual'][$key];
                $lotSpecification['scpl'][] = $data['scpl'][$key];
                $lotSpecification['status'][] = $data['status'][$key];
                if ($request['status'][$key] == 'Not Ok') {
                  $clearance = 0;
                }
            }
            $filamentClearance->update(['report_result'=>json_encode($lotSpecification),'clearance_status'=>$clearance]);
        }



        return redirect('/filament-clearance-qc');
    }


    public function showDipSolutionClearance(){

        $dipSolutionClearances = DipSolutionClearance::where('status',0)->get();

        return view('qc.dip-solution-clearance-list')->with(compact('dipSolutionClearances'));

    }


    public function updateDipSolutionLot($id){

        $dipSolution = DipSolutionClearance::where('id',$id)->first();

        $dipSolutionData = DipSolution::where('name', $dipSolution->name)->get();

        return view('qc.dip-solution-update')->with(compact('dipSolution','dipSolutionData'));

    }


    public function insertdipSolutionClearanceResult(Request $request){
        $request->validate([
            'id' => 'required',
            'type' => 'required',
            'tested-by' => 'required',
            'tested-date' => 'required',
            'test-arrival-date' => 'required',
            'final-result' => 'required',
            'parameter-sample' => 'required',
        ]);



        $clearance = DipSolutionClearance::where('id', $request['id'])->first();

//        dd($clearance);

        if($request->type == 'create'){

            $save = $clearance->dipSolutionClearanceResult()->create([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);

        }else{

            $save = $clearance->packingClearanceResult()->update([

                'tested_by' => $request['tested-by'],
                'tested_date' => date('Y-m-d', strtotime($request['tested-date'])),
                'test_arrival_date' => date('Y-m-d', strtotime($request['test-arrival-date'])),
                'result' => $request['final-result'],
                'sample' => $request['parameter-sample'],
            ]);
        }



        if($save){

            if($request['final-result'] == 'cleared'){
                $clearance->update([
                    'status' => 1
                ]);
            }else{
                $clearance->update([
                    'status' => 2
                ]);
            }

            return response()->json(['message' => 'success'], 200);

        }


        return response()->json(['message' => 'failed'], 500);

    }


    public function showClearedDsList(){

        $dipSolutionClearances = DipSolutionClearance::where('status','!=',0)->get();

        return view('qc.ds-cleared-list')->with(compact('dipSolutionClearances'));
    }

}
