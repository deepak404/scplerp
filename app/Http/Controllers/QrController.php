<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use LaravelQRCode\Facades\QRCode;
use QR_Code\QR_Code;

class QrController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    public function generateQr(){

        $pixQrArray =
            [
                'RCORPSO00000'	=> 'CORD HMLS PCC SOFT 2X5',
                'RCORPSO00003'	=> 'CORD HMLS PCC SOFT 3X5',
                'RCORPSO00005'	=> 'CORD HMLS PCC SOFT 5X4',
                'RCORPSO00006'	=> 'CORD HMLS PCC SOFT 9X3',
                'RCORPSO00001'	=> 'CORD HMLS PCC SOFT 9X6',
                'RCORPSO00004'	=> 'CORD HMLS PCC SOFT 2X3',
                'RCORESTZZ001'	=> 'CORD HMLS EPDM STIFF 2X3',
                'RCORESTZZ007'	=> 'CORD HMLS EPDM STIFF 2X5',
                'RCORPST00001'	=> 'CORD HMLS PCC STIFF 2X5',
                'RCORPST00002'	=> 'CORD HMLS PCC STIFF 3X5',
                'RCORPST00003'	=> 'CORD HMLS PCC STIFF 9X3',
                'RCORPST00004'	=> 'CORD HMLS PCC STIFF 5X4',
                'RCORPST00005'	=> 'CORD HMLS PCC STIFF 2X3',
                'RCORKSO00000'	=> 'CORD KEVLAR SOFT 1670X2X3'

            ];

        $materials = array_values($pixQrArray);


        return view('generate-qr')->with(compact('materials'));
    }

    public function generateQrCode(Request $request){
        $request->validate([
            'count' => 'required',
            'doff' => 'required',
            'dop' => 'required',
            'from' => 'required',
            'to' => 'required',
//            'direction' => 'required',
        ]);

        if(file_exists('output.prn')){
            unlink('output.prn');

        }


        $direction = $request['direction'];
        $dop = $request['dop'];
        $count = $request['count'];
        $doffNo = $request['doff'];



        $pixQrArray =
            [
                'RCORPSO00000'	=> 'CORD HMLS PCC SOFT 2X5',
                'RCORPSO00003'	=> 'CORD HMLS PCC SOFT 3X5',
                'RCORPSO00005'	=> 'CORD HMLS PCC SOFT 5X4',
                'RCORPSO00006'	=> 'CORD HMLS PCC SOFT 9X3',
                'RCORPSO00001'	=> 'CORD HMLS PCC SOFT 9X6',
                'RCORPSO00004'	=> 'CORD HMLS PCC SOFT 2X3',
                'RCORESTZZ001'	=> 'CORD HMLS EPDM STIFF 2X3',
                'RCORESTZZ007'	=> 'CORD HMLS EPDM STIFF 2X5',
                'RCORPST00001'	=> 'CORD HMLS PCC STIFF 2X5',
                'RCORPST00002'	=> 'CORD HMLS PCC STIFF 3X5',
                'RCORPST00003'	=> 'CORD HMLS PCC STIFF 9X3',
                'RCORPST00004'	=> 'CORD HMLS PCC STIFF 5X4',
                'RCORPST00005'	=> 'CORD HMLS PCC STIFF 2X3',
                'RCORKSO00000'	=> 'CORD KEVLAR SOFT 1670X2X3'

            ];

        $bottomText = array_search($count, $pixQrArray);
        $bodyText = '';


        $fileUpperPart = "<xpml><page quantity='0' pitch='25.0 mm'></xpml>'Seagull:2.1:DP
INPUT OFF
VERBOFF
INPUT ON
SYSVAR(48) = 0
ERROR 15,\"FONT NOT FOUND\"
ERROR 18,\"DISK FULL\"
ERROR 26,\"PARAMETER TOO LARGE\"
ERROR 27,\"PARAMETER TOO SMALL\"
ERROR 37,\"CUTTER DEVICE NOT FOUND\"
ERROR 1003,\"FIELD OUT OF LABEL\"
SYSVAR(35)=0
OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
PRINT#1,\"Printing,Media,Clip Default,On\"
CLOSE #1
SETUP \"tmp:setup.sys\"
KILL \"tmp:setup.sys\"";









        $qrFiles = [];


        $loopCount = 0;
        $subcount = [];

        for ($i = $request['from']; $i<=$request['to'];$i++) {

            if($i < $request['to']){
                $subcount[] = [$i, $i+1];
                $i++;
            }else{
                $subcount[] = [$i];
            }

        }


        foreach($subcount as $scount){

            if(count($scount) == 2){
                $barCodeInnertextFirst = $count.' | '. $doffNo .' | '.$dop.' | '.$request['direction'].$scount[0].' | '.$bottomText;
                $barCodeInnertextSecond = $count.' | '. $doffNo .' | '.$dop.' | '.$request['direction'].$scount[1].' | '.$bottomText;

                $QrSticker1 = "<xpml></page></xpml><xpml><page quantity='1' pitch='25.0 mm'></xpml>CLL
OPTIMIZE \"BATCH\" ON
PP28,179:AN7
BARSET \"QRCODE\",1,1,3,2,1
PB \"$barCodeInnertextFirst\"
PP143,180:NASC 8
FT \"Univers Bold\",6,0,99
PT \"$count\"
PP143,154:FT \"Univers Bold\",6,0,99
PT \"DOFF NO : $doffNo\"
PP143,131:FT \"Univers Bold\",6,0,99
PT \"DOP : $dop\"
PP143,107:FT \"Univers Bold\",6,0,99
PT \"Spindle No : $direction$scount[0]\"
PP100,55:FT \"Univers\",10,0,99
PT \"$bottomText\"
LAYOUT RUN \"\"
PF
PRINT KEY OFF
<xpml></page></xpml><xpml><end/></xpml>";




                $QrSticker2 = "<xpml></page></xpml><xpml><page quantity='1' pitch='25.0 mm'></xpml>CLL
OPTIMIZE \"BATCH\" ON
PP28,179:AN7
BARSET \"QRCODE\",1,1,3,2,1
PB \"$barCodeInnertextSecond\"
PP143,180:NASC 8
FT \"Univers Bold\",6,0,99
PT \"$count\"
PP143,154:FT \"Univers Bold\",6,0,99
PT \"DOFF NO : $doffNo\"
PP143,131:FT \"Univers Bold\",6,0,99
PT \"DOP : $dop\"
PP143,107:FT \"Univers Bold\",6,0,99
PT \"Spindle No : $direction$scount[1]\"
PP100,55:FT \"Univers\",10,0,99
PT \"$bottomText\"
LAYOUT RUN \"\"
PF
PRINT KEY OFF
<xpml></page></xpml><xpml><end/></xpml>";

                $bodyText .= $QrSticker1.PHP_EOL;
                $bodyText .= $QrSticker2.PHP_EOL;

                $exportText = $fileUpperPart.$bodyText;

                file_put_contents('output.prn', $exportText);


            }else{

                $barCodeInnertextFirst = $count.' | '. $doffNo .' | '.$dop.' | '.$request['direction'].$scount[0].' | '.$bottomText;
//                $barCodeInnertextSecond = $count.' | '. $doffNo .' | '.$dop.' | '.$request['direction'].$scount[1].' | '.$bottomText;


                $QrSticker = "<xpml></page></xpml><xpml><page quantity='1' pitch='25.0 mm'></xpml>CLL
OPTIMIZE \"BATCH\" ON
PP28,179:AN7
BARSET \"QRCODE\",1,1,3,2,1
PB \"$barCodeInnertextFirst\"
PP143,180:NASC 8
FT \"Univers Bold\",6,0,99
PT \"$count\"
PP143,154:FT \"Univers Bold\",6,0,99
PT \"DOFF NO : $doffNo\"
PP143,131:FT \"Univers Bold\",6,0,99
PT \"DOP : $dop\"
PP143,107:FT \"Univers Bold\",6,0,99
PT \"Spindle No : $direction$scount[0]\"
PP100,55:FT \"Univers\",10,0,99
PT \"$bottomText\"
LAYOUT RUN \"\"
PF
PRINT KEY OFF
<xpml></page></xpml><xpml><end/></xpml>";

                $bodyText .= $QrSticker.PHP_EOL;
                $exportText = $fileUpperPart.$bodyText;

                file_put_contents('output.prn', $exportText);
            }



        }



        return response()->download('output.prn');

        //for ($i = $request['from']; $i<=$request['to'];$i++){



//            $filename = trim($request['count']).'-'.trim($request['doff']).'-'.trim($request['dop']).'-'.$request['direction'].$i;
//            $filename = str_replace('/','-',$filename);
//            $filename = str_replace(' ','',$filename);

//            $qrFiles[] = $filename;
//            $qrFiles[$filename] = $filename.'|'.$request['direction'].$i;

//            QRCode::text('Count : '.$request['count'].' | Doff No :'.$request['doff'].' | DOP : '.$request['dop'].' | Spindle No : '.$request['direction'].$i)->setOutFile('qrcode/'.$filename.'.png')->png();

            $loopCount++;
        }



//        $files = $qrFiles;
//        $count = $request['count'];
//        $doff = $request['doff'];
//        $dop = $request['dop'];
//
//        $files = array_chunk($files, 2);
////        dd(array_chunk($files,3));
//
////        return view('reports.qr-code')->with(compact('files','count','doff','dop'));
//        return PDF::loadView('reports.qr-code',
//            [
//                'files' => $files,
//                'count' => $count,
//                'doff' => $doff,
//                'dop' => $dop
//            ])
//            ->setPaper('a4', 'portrait')->setWarnings(false)->stream('qr-code.pdf');

    //}


    public function generateFabricQr(){

        return view('fabric-qr-code');
    }


    public function generateFabricQrCode(Request $request){

        if($request->hasFile('fabric-csv')){

            $csv_file = file($request['fabric-csv']);
            unset($csv_file[0]);

            $qrFiles = [];

            if(count($csv_file) > 12){
                return redirect()->back()->withErrors(['error' => 'More than 12 Entries found. Max Allowed entries is 12.']);
            }
            foreach ($csv_file as $line) {

                $line = preg_replace('~[\r\n]+~', '', $line);
                $line = explode(',',$line);

               $filename = $line[0].'|'.$line[1].'|'.$line[2];
            $filename = str_replace('/','-',$filename);
            $filename = str_replace(' ','',$filename);
               $qrFiles[] = $filename;

                QRCode::text('Quality : '.$line[0].' | Quantity :'.$line[1].' | Roll No : '.$line[2])->setOutFile('fabricqrcode/'.$filename.'.png')->png();

            }


            $files = array_chunk($qrFiles, 2);
//        dd(array_chunk($files,3));

//        return view('reports.qr-code')->with(compact('files','count','doff','dop'));
            return PDF::loadView('reports.fabric-qr-code',
                [
                    'files' => $files,
                ])
                ->setPaper('a4', 'portrait')->setWarnings(false)->stream('fabric-qr-code.pdf');
        }else{
            return "No files Found";
        }
    }



}
