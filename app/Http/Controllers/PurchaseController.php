<?php

namespace App\Http\Controllers;

use App\ChemicalClearance;
use App\Chemicals;
use App\PackagingClearance;
use App\PackagingMaterial;
use App\Supplier;
use Illuminate\Http\Request;
use App\FilamentClearance;
use App\Filament;

class PurchaseController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
       $this->middleware('purchase');
    }

    public function filamentClearance()
    {

    	$lots = FilamentClearance::all();

    	$filaments = Filament::all()->groupBy('name');

      $suppliers = Supplier::all();

  		return view('qc.purchase-filament-clearance')->with(compact('lots','filaments','suppliers'));

    }

    public function createFilamentMemo(Request $request)
    {
    	try {

        $request['inv_date'] = date('Y-m-d', strtotime($request['inv_date']));
        $request['arrival_date'] = date('Y-m-d', strtotime($request['arrival_date']));
        $request['gate_entry_date'] = date('Y-m-d', strtotime($request['gate_entry_date']));

    		if (array_key_exists('id', $request->all())) {

          if (FilamentClearance::where('lot_batch_no',$request['lot_batch_no'])->where('id','!=' ,$request['id'])->get()->count() > 0) {
            return response()->json(['status'=>0,'msg'=>'Lot Number already taken.']);
          }

    			$data = $request->all();

    			unset($data['id']);

    			$update = FilamentClearance::where('id',$request['id'])
    										->update($data);

    			return response()->json(['status'=>1,'msg'=>'Created updated.']);

    		}else{

          if (FilamentClearance::where('lot_batch_no',$request['lot_batch_no'])->get()->count() > 0) {
            return response()->json(['status'=>0,'msg'=>'Lot Number already taken.']);
          }

		    	$create = FilamentClearance::create($request->all());

	    		return response()->json(['status'=>1,'msg'=>'Created successfully.']);

    		}

    	} catch (Exception $e) {

			return response()->json(['status'=>0,'msg'=>$e->getMessage()]);

    	}

    }

    public function getFilamentMemo(Request $request)
    {
      $id = $request['id'];

    	try {

    		$lot = FilamentClearance::where('id',$id)->first();

    		return response()->json(['status'=>1,'lot'=>$lot]);

    	} catch (Exception $e) {

    		return response()->json(['status'=>0,'msg'=>$e->getMessage()]);

    	}
    }

    public function deleteFilamentMemo($id)
    {
    	try {

    		FilamentClearance::where('id',$id)->delete();

    		return redirect('/filament-clearance');

    	} catch (Exception $e) {

    		dd($e->getMessage());

    	}
    }


    public function chemicalClearance(){

        $chemicals = Chemicals::all()->groupBy('descriptive_name')->toArray();
        //dd($chemicals);
        $chemicals = array_keys($chemicals);
        $chemicalClearance = ChemicalClearance::all();



        return view('purchase.chemical-clearance-purchase')->with(compact('chemicals','chemicalClearance'));
    }


    public function createChemicalLot(Request $request){

//        dd($_POST);



        $request->validate([

            "supplier" => 'required',
            "received_on" => 'required',
            "supplier_type" => 'required',
            "chemical" => 'required',
            "chemical_code" => 'required',
            "gate_in_no" => 'required',
            "gate_date" => 'required',
            "lot_no" => 'required',
            "manufactured_date" => 'required',
            "total_weight" => 'required',
            "expiry_date" => 'required',
            "no_of_packs" => 'required',
            "grn_remarks" => 'required',

        ]);


        try{


            $supplier = Supplier::where('id', $request['supplier'])->first();

            $request['manufactured_date'] = date('Y-m-d', strtotime($request['manufactured_date']));
            $request['received_on'] = date('Y-m-d', strtotime($request['received_on']));
            $request['expiry_date'] = date('Y-m-d', strtotime($request['expiry_date']));
            $request['gate_date'] = date('Y-m-d', strtotime($request['gate_date']));

            if($request['update-type'] == 1){

                $chemicalClearance = ChemicalClearance::where('id', $request['id'])->first();
                $chemicalClearance->supplier_id = $request['supplier'];
                $chemicalClearance->supplier_type = $request['supplier_type'];
                $chemicalClearance->gate_in_no = $request['gate_in_no'];
                $chemicalClearance->gate_date = $request['gate_date'];
                $chemicalClearance->received_on = $request['received_on'];
                $chemicalClearance->chemical = $request['chemical'];
                $chemicalClearance->chemical_code = $request['chemical_code'];
                $chemicalClearance->lot_no = $request['lot_no'];
                $chemicalClearance->manufactured_date = $request['manufactured_date'];
                $chemicalClearance->total_weight = $request['total_weight'];
                $chemicalClearance->expiry_date = $request['expiry_date'];
                $chemicalClearance->no_of_packs = $request['no_of_packs'];
                $chemicalClearance->grn_remarks = $request['grn_remarks'];

                $createEntry = $chemicalClearance->save();

            }else{

                $request->validate([
                    'lot_no' => 'required|unique:chemical_clearances'

                    ]);

                $request['status'] = 0;
                $createEntry = $supplier->chemicalClearance()->create($request->all());

            }

            if($createEntry){

                return redirect('/chemical-clearance')->with('message','Chemical Clearance Updated Successfully.');

            }else{
                return redirect()->back()->withErrors(['error' => 'Cannot Create Chemical Clearance Entry. Please Try again later']);
            }

        }catch(\Exception $e){

                return redirect()->back()->withErrors(['error' => $e->getMessage()]);

        }


    }


    public function deleteChemicalLot($id){


        try{

            $delete = ChemicalClearance::where('id',$id)->delete();

            if($delete){
                return redirect()->back()->with('message','Chemical Clearance Lot is successfully Deleted.');
            }

        }catch(\Exception $e){

            return redirect()->back()->withErrors(['error' => 'Cannot Delete the Chemical Lot Right now. Please refresh and try again.']);
        }
    }


    public function packageClearance(){

        $packagingMaterial = PackagingMaterial::all()->groupBy('name');
        $packageClearance = PackagingClearance::all();

        return view('purchase.purchase-packaging-clearance')->with(compact('packagingMaterial','packageClearance'));

    }



    public function createPackagingLot(Request $request){


        //dd($request->all());

        $request->validate([
            'item' => 'required',
            'item_code' => 'required',
            'supplier_name' => 'required',
            'supplier_type' => 'required',
            'goods_received' => 'required',
            'gate_in_no'=>'required',
            'gate_date' => 'required',
            'packing_lot_no' => 'required',
            'invoice_quantity' => 'required',
            'goods_received' => 'required',
        ]);





        try{

            if($request['form-type'] == 'update'){

                $packingClearance = PackagingClearance::where('id', $request['packing-id'])->first();
                $createPackagingClearance = $packingClearance->update([
                    'item' => $request['item'],
                    'item_code' => $request['item_code'],
                    'supplier_id' => $request['supplier_name'],
                    'supplier_type' => $request['supplier_type'],
                    'packing_lot_no' => $request['packing_lot_no'],
                    'gate_in_no'=> $request['gate_in_no'],
                    'gate_date' => date('Y-m-d', strtotime($request['gate_date'])),
                    'invoice_quantity' => $request['invoice_quantity'],
                    'quantity_received' => $request['quantity_ordered'],
                    'received_on' => date('Y-m-d', strtotime($request['goods_received'])),
                    'status' => 0,
                ]);

                return redirect()->back()->with('message','Package Clearance Successfully updated.');


            }else{

                $createPackagingClearance = PackagingClearance::create([


                    'item' => $request['item'],
                    'item_code' => $request['item_code'],
                    'supplier_id' => $request['supplier_name'],
                    'supplier_type' => $request['supplier_type'],
                    'packing_lot_no' => $request['packing_lot_no'],
                    'gate_in_no'=> $request['gate_in_no'],
                    'gate_date' => date('Y-m-d', strtotime($request['gate_date'])),
                    'invoice_quantity' => $request['invoice_quantity'],
                    'quantity_received' => $request['quantity_ordered'],
                    'received_on' => date('Y-m-d', strtotime($request['goods_received'])),
                    'status' => 0,

                ]);

                return redirect()->back()->with('message','Package Clearance Successfully created.');

            }



        }catch(\Exception $e){

            return redirect()->back()->withErrors(['error' => 'Cannot Create Packaging Material Lot Right now. Please refresh and try again.'.$e->getMessage()]);

        }


    }


    public function deletePackageMaterialLot($id){
        try{
            $clearance = PackagingClearance::where('id',$id)->first();
            if($clearance->status == 0){

                $delete = $clearance->delete();

                if($delete){
                    return redirect()->back()->with('message','Packing Clearance Lot is successfully Deleted.');
                }
            }else{
                return redirect()->back()->withErrors(['error' => 'Cannot Delete the Package Material Lot Right now.']);
            }
        }catch(\Exception $e){

            return redirect()->back()->withErrors(['error' => 'Cannot Delete the Package Material Lot Right now. Please refresh and try again.']);

        }

    }


    public function findPackingMaterialLot(Request $request){
        $request->validate([
            'id' => 'required',
        ]);


        try{
            $packingClearance = PackagingClearance::where('id',$request['id'])->first();
            $packingClearance->received_on = date('d-m-Y', strtotime($packingClearance->received_on));
            $packingClearance->gate_date = date('d-m-Y', strtotime($packingClearance->gate_date));

            return response()->json(['data' => $packingClearance], 200);

        }catch(\Exception $e){
            return response()->json(['error' => 'Cannot fetch the details Right now.'], 500);
        }

    }


    public function getChemicalClearance(Request $request){

        $request->all([
            'id' => 'required'
        ]);


        $chemicalClearance = ChemicalClearance::where('id',$request['id'])->first();

        if($chemicalClearance->status == 0){
            return $chemicalClearance;

        }

        return [];


    }

    public function getFilamentCode(Request $request)
    {
      $materialCode = Filament::where('name',$request['filament'])->value('material_code');
      if (is_null($materialCode)) {
        return response()->json(['status'=>0,'msg'=>'Invalid Filament Type.']);
      }
      return response()->json(['status'=>1,'material_code'=>$materialCode]);
    }
}
