<?php

namespace App\Http\Controllers;

use App\BusinessPartner;
use App\ItemMaster;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function createCustomer(Request $request){
        $request->validate([
            'bp_name' => 'required',
            'gst_code' => 'required',
            'address' => 'required',
            'district' => 'required',
            'state' => 'required',
            'code' => 'required',
            'ar_no' => 'required',
            'contact_person' => 'required',
            'contact_no' => 'required',
            'dispatcher' => 'required',
            'dispatch_destination' => 'required',
            'pincode' => 'required',
            'email' => 'required',
            'phone_number' => 'required'
        ]);


        try{

            $createCustomer = BusinessPartner::create($request->toArray());

            if($createCustomer){
                return response()->json(['status' => true], 200);
            }

        }catch(\Exception $e){

            return response()->json(['status' => false], 500);


        }
    }


    public function uploadFromCsv(){
//        $csv_file = file(storage_path()."/Item Master - Business Partner.csv");
//        $industry = [];
//
//        $filteredStock = [];
//
//
////        dd($csv_file);
////        BusinessPartner::truncate();
//        unset($csv_file[0]);
//        foreach ($csv_file as $line) {
//            $line = preg_replace('~[\r\n]+~', null, $line);
//            $line = explode(',',$line);
////            dd($line);
//            BusinessPartner::create([
//                'bp_name' => $line[0],
//                'address' => $line[1],
//                'pincode' => $line[5],
//                'email' => $line[7],
//                'phone_number' => $line[6],
//                'district' => $line[4],
//                'state' => $line[3],
//                'ar_no' => $line[9],
//                'code' => $line[8],
//                'contact_person' => $line[10],
//                'contact_no' => $line[11],
//                'dispatcher' => $line[12],
//                'dispatch_destination' => $line[13],
//                'gst_code' => $line[2]
//            ]);
//        }


        foreach(ItemMaster::all() as $item){
            $item->update([
                'prod_hr' => round(($item->cord_wt * $item->no_of_spls * $item->speed_in_mpm) * 60/1000, 2)
            ]);
        }
    }
}
