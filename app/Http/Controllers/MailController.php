<?php

namespace App\Http\Controllers;

use App\SaleOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendMail(){



        $to_name = 'Naveen PWM';
        $to_email = 'naveen@pwm-india.com';
        $data = array('name'=>"Naveen Kumar", "body" => "Test mail");

        Mail::send('email.sale-noti', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('New Sale Order');
            $message->from('nknaveen328@gmail.com','Artisans Web');
        });
    }
}
