<?php

namespace App\Http\Controllers;

use App\Filament;
use App\Machine;
use App\ProductionFeasibility;
use App\ProductionPlanning;
use App\SaleOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ItemMaster;
use App\BusinessPartner;
use App\BpMaterial;
use App\Loggers\DatabaseLogger;

class ProductionPlanningController extends Controller
{
    /**
     * @return `production_feasibility page with sales order entry to create feasibility`.
     *
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('production_plan');
    }

    public function index($date = null)
    {
        $zellAChangeHour = Machine::where('name', 'ZELL A')->value('change_hr');
        $zellBChangeHour = Machine::where('name', 'ZELL B')->value('change_hr');
        $kiddeChangeHour = Machine::where('name', 'KIDDE')->value('change_hr');

        $itemMaster = ItemMaster::orderBy('material')->get();

        if ($date == null) {
            $date = date('Y-m-01');
        } else {
            $date = date('Y-m-d', strtotime('01-'.$date));
        }
        $salesList = SaleOrder::whereBetween('order_date', [$date,date('Y-m-t', strtotime($date))])->with(['productionFeasibility','businessPartner','itemMaster'])->get();

        $filaments = array_keys(Filament::all()->groupBy('name')->toArray());

        // dd($salesList);
        return view('production.production_feasibility')->with(compact('salesList', 'zellAChangeHour', 'zellBChangeHour', 'kiddeChangeHour', 'filaments', 'itemMaster'));
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * The function `updateProductionFeasibility` entry updates the PF entries. The arguments are not required because
     * not all details will be updated at once. Once Updated, the function redirects to the same page.
     *
     */


    public function updateProductionFeasibility(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'machine' => 'required',
            'changed_filament' => 'required',
            'cord_weight' => 'required',
            'speed_in_mpm' => 'required',
            'production_per_hr' => 'required',
            'running_hrs' => 'required',
            'no_of_changes' => 'required',
            'changes_per_hr' => 'required'
        ]);
        // dd($request->all());

        try {
            unset($_POST['_token']);
            $inStock = null;
            $existingFilament = null;
            $changeFilament = $request['changed_filament'];
            $consumption = $request['consumption'];
            $noChanges = $request['no_of_changes'];
            $changesPerHr = $request['changes_per_hr'];

            $productionFeasibility = ProductionFeasibility::where('id', $request['id'])->first();

            $beforeUpdate = ProductionFeasibility::getEntry($request['id']);

            $totalConsumption = 0;
            $machine = $request['machine'];

            if (strpos($changeFilament, ',') !== false) {
                if (count(explode(',', $consumption)) != count(explode(',', $changeFilament))) {
                    return redirect()->back()->withErrors(['error' => 'Consumption='.$consumption.' and Change Filament='.$changeFilament.' both must be same level of separation.']);
                }
                $kgs = explode(',', $consumption);
                $totalConsumption = array_sum($kgs);
            } else {
                $totalConsumption = $consumption;
            }
            if (empty($request['in_stock'])) {
                if ($totalConsumption != $productionFeasibility->saleOrder->total_order) {
                    return redirect()->back()->withErrors(['error' => 'Consumption='.$totalConsumption.' and OrderWeight='.$productionFeasibility->saleOrder->total_order.' both must be same.']);
                }
            } else {
                $inStock = 1;
                if ($totalConsumption >= $productionFeasibility->saleOrder->total_order) {
                    return redirect()->back()->withErrors(['error' => 'Consumption='.$totalConsumption.' and OrderWeight='.$productionFeasibility->saleOrder->total_order.' Consumption should be less then OrderWeight on In-stock.']);
                }
            }
            if (is_null($productionFeasibility->existing_filament)) {
                $orderDetail = $productionFeasibility->saleOrder;
                $oldOrders = \DB::table('sale_orders')
                                    ->where('bp_id', $orderDetail->bp_id)
                                    ->where('material_id', $orderDetail->material_id)
                                    ->where('order_date', '<', $orderDetail->order_date)
                                    ->leftJoin('production_feasibilities', 'sale_orders.id', '=', 'production_feasibilities.order_id')
                                    ->where('production_feasibilities.machine', $productionFeasibility->machine)
                                    ->orderBy('sale_orders.order_date', 'DESC')
                                    ->first();
                if (!is_null($oldOrders)) {
                    $existingFilament = $oldOrders->changed_filament;
                }
            } else {
                $existingFilament = $productionFeasibility->existing_filament;
            }
            if ($productionFeasibility->update_state == 1) {
                return redirect()->back()->withErrors(['error' => 'Production Planning has started for the entry. Cannot Update the feasibility Now.']);
            }
            $productionFeasibility->machine = $machine;
            $productionFeasibility->existing_filament = $existingFilament;
            $productionFeasibility->changed_filament = $changeFilament;
            $productionFeasibility->consumption = $consumption;
            $productionFeasibility->cord_weight = $request['cord_weight'] ?? null;
            $productionFeasibility->no_of_spls = $request['no_of_spls'] ?? null;
            $productionFeasibility->speed_in_mpm = $request['speed_in_mpm'] ?? null;
            $productionFeasibility->production_per_hr = $request['production_per_hr'] ?? null;
            $productionFeasibility->running_hrs = $request['running_hrs'] ?? null;
            $productionFeasibility->no_of_changes = $noChanges;
            $productionFeasibility->changes_per_hr = $changesPerHr;
            $productionFeasibility->in_stock = $inStock;
            // dd($productionFeasibility);
            $productionFeasibility->save();

            $afterUpdate = ProductionFeasibility::getEntry($request['id']);

            (new DatabaseLogger($beforeUpdate, $afterUpdate, 'edit', Date('Y-m-d H:i:s'), '', auth()->user()->name))->saveEditEntry();


            if ($productionFeasibility) {

                //Once the Production feasibility is updated, The sale order cannot be edited or deleted.

                $productionFeasibility->saleOrder->update(['edit_state'=>1, 'delete_state' => 1]);

                return redirect()->back()->with('status', 'Feasibility updated!');
            }
        } catch (\Exception $e) {
            dd($e);
            return redirect()->back()->withErrors(['error' => $e->getMessage()]);
        }
    }


    /**
     *
     *
     * @return `production_planning` view with production feasibility and sales entries.
     */


    public function productionPlanning(Request $request)
    {
        $request->validate([
            'machine' => 'required',
            'filter-date' => 'required',
        ]);

        $month = $request['filter-date'];
        $machine = $request['machine'];
        $filaments = Filament::all('name')->groupBy('name')->toArray();
        $filaments = json_encode(array_keys($filaments));

        $sales = SaleOrder::where('order_date', '>=', date('Y-m-01', strtotime($request['filter-date'])))
            ->where('order_date', '<=', date('Y-m-t', strtotime($request['filter-date'])))
            ->pluck('id');

//        $productions = ProductionFeasibility::whereIn('order_id', $sales)->where('machine', $request['machine'])->with('productionPlanning')->get();
        $productionFeasibilities = ProductionFeasibility::whereIn('order_id', $sales)
                                                        ->where('machine', $request['machine'])
                                                        ->where('update_state', null)
                                                        ->where('consumption', '>', 0)
                                                        ->get()
                                                        ->groupBy('material');
        $feasibility = [];

        // dd($productionFeasibilities->toArray());
        foreach (array_keys($productionFeasibilities->toArray()) as $materialId) {
            $feasibility[$materialId] = [];
        }

        foreach ($productionFeasibilities as $material => $pfs) {
            foreach ($pfs as $pf) {
                if (count(explode(',', $pf->changed_filament)) > 1) {
                    foreach (explode(',', $pf->changed_filament) as $key => $filament) {
                        if (explode(',', $pf->consumption)[$key] != ProductionPlanning::where('feasibility_id', $pf->id)->where('filament_type', $filament)->sum('order_kg')) {
                            if (array_key_exists($filament, $feasibility[$material])) {
                                $feasibility[$material][$filament]['filament'] = $filament;
                                $feasibility[$material][$filament]['consumption'] += explode(',', $pf->consumption)[$key];
                                $feasibility[$material][$filament]['material'] = $pf->material;
                            } else {
                                $feasibility[$material][$filament]['filament'] = $filament;
                                $feasibility[$material][$filament]['consumption'] = explode(',', $pf->consumption)[$key];
                                $feasibility[$material][$filament]['material'] = $pf->material;
                            }
                        }
                    }
                } else {
                    if ($pf->consumption != ProductionPlanning::where('feasibility_id', $pf->id)->where('filament_type', $pf->changed_filament)->sum('order_kg')) {
                        if (array_key_exists($pf->changed_filament, $feasibility[$material])) {
                            $feasibility[$material][$pf->changed_filament]['filament'] = $pf->changed_filament;
                            $feasibility[$material][$pf->changed_filament]['consumption'] += $pf->consumption;
                            $feasibility[$material][$pf->changed_filament]['material'] = $pf->material;
                        } else {
                            $feasibility[$material][$pf->changed_filament]['filament'] = $pf->changed_filament;
                            $feasibility[$material][$pf->changed_filament]['consumption'] = $pf->consumption;
                            $feasibility[$material][$pf->changed_filament]['material'] = $pf->material;
                        }
                    }
                }
            }
        }
        // dd($feasibility);
        return view('production.production_planning')->with(compact('month', 'machine', 'filaments', 'feasibility'));
    }


    public function plannedProduction(Request $request)
    {
        $request->validate([
          'machine' => 'required',
          'filter-date' => 'required',
      ]);

        $month = $request['filter-date'];
        $machine = $request['machine'];

        $productionPlan = ProductionPlanning::where('start_date', '>=', date('Y-m-01', strtotime($request['filter-date'])))
          ->where('start_date', '<=', date('Y-m-t 23:59:59', strtotime($request['filter-date'])))
          ->join('production_feasibilities', 'production_plannings.feasibility_id', '=', 'production_feasibilities.id')
          ->where('production_feasibilities.machine', $request['machine'])
          ->join('item_masters', 'production_plannings.material', '=', 'item_masters.id')
          ->get(
              [
                'production_plannings.id AS id',
                'production_plannings.start_date AS start_date',
                'production_plannings.end_date AS end_date',
                'production_plannings.batch AS batch',
                'production_plannings.order_kg AS consumption',
                'item_masters.material AS material',
                'production_plannings.filament_type AS filament_type',
                'production_feasibilities.id AS feasibility_id',
                'item_masters.cord_wt AS cord_wt',
                'item_masters.zell_a_spls_no AS zell_a_spls_no',
                'item_masters.zell_a_speed AS zell_a_speed',
                'item_masters.zell_b_spls_no AS zell_b_spls_no',
                'item_masters.zell_b_speed AS zell_b_speed',
                'item_masters.kidde_spls_no AS kidde_spls_no',
                'item_masters.kidde_speed AS kidde_speed',
                'production_feasibilities.machine AS machine'
                ]
          );
        // dd($productionPlan->toArray());
        $finalArray = [];

    
        foreach ($productionPlan as $plan) {
            if (empty($finalArray[$plan->material][$plan->filament_type][$plan->batch])) {
                $ProdHr = 0;

                if ($plan->machine == 1) {
                    $ProdHr = ($plan->cord_wt*$plan->zell_a_spls_no*$plan->zell_a_speed*60)/1000;
                } elseif ($plan->machine == 2) {
                    $ProdHr = ($plan->cord_wt*$plan->zell_b_spls_no*$plan->zell_b_speed*60)/1000;
                } elseif ($plan->machine == 3) {
                    $ProdHr = ($plan->cord_wt*$plan->kidde_spls_no*$plan->kidde_speed*60)/1000;
                }
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['start_date'] = $plan->start_date;
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['end_date'] = $plan->end_date;
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['consumption'] = $plan->consumption;
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['prod_hr'] = round($ProdHr, 2);
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['id'][] = $plan->id;
            } else {
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['consumption'] += $plan->consumption;
                $finalArray[$plan->material][$plan->filament_type][$plan->batch]['id'][] = $plan->id;
            }
            $finalArray[$plan->material][$plan->filament_type]['feasibility_id'][] = $plan->feasibility_id;
            
            if (empty($finalArray[$plan->material][$plan->filament_type]['sum'])) {
                $finalArray[$plan->material][$plan->filament_type]['sum'] = $plan->consumption;
            } else {
                $finalArray[$plan->material][$plan->filament_type]['sum'] += $plan->consumption;
            }
        }

        // dd($finalArray);

        return view('production.planned_production')->with(compact('month', 'machine', 'finalArray'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * The `createProductionBatch` function creates all the production batches for the production feasibility entry.
     *
     */
    public function createProductionBatch(Request $request)
    {
        $request->validate([

            'batch_details' => 'required',
            'filament' => 'required',
            'material' => 'required',
            'month' => 'required',

        ]);


        try {
            $batchDetails = json_decode($request['batch_details']);
            $batchArray = [];
            foreach ($batchDetails as $batchDetail) {
                $batchArray[] = (array)$batchDetail;
            }

            $start = date('Y-m-01', strtotime($request['month']));
            $end = date('Y-m-t', strtotime($request['month']));
            // $productionFeasibility = ProductionFeasibility::where('id',$request['pf_id'])->first();
            $productionFeasibility = ProductionFeasibility::where('changed_filament', 'LIKE', '%'.$request['filament'].'%')
                                                            ->where('material', $request['material'])
                                                            ->where('update_state', null)
                                                            ->where('machine', $request['machine'])
                                                            ->join('sale_orders', 'production_feasibilities.order_id', '=', 'sale_orders.id')
                                                            ->where('sale_orders.order_date', '>=', $start)
                                                            ->where('sale_orders.order_date', '<=', $end)
                                                            ->orderBy('sale_orders.customer_date')
                                                            ->get(['sale_orders.id AS order_id',
                                                                   'production_feasibilities.id as fs_id',
                                                                   'sale_orders.material_id as material_id',
                                                                   'sale_orders.order_quantity as order_quantity',
                                                                   'production_feasibilities.changed_filament as filament_type',
                                                                   'production_feasibilities.consumption as consumption'
                                                                  ])
                                                            ->toArray();

            // dd($productionFeasibility);
            foreach ($productionFeasibility as $key => $value) {
                if (count(explode(',', $value['filament_type'])) > 1) {
                    foreach (explode(',', $value['filament_type']) as $key2 => $value2) {
                        if ($request['filament'] == $value2) {
                            $productionFeasibility[$key]['filament_type'] = $value2;
                            $productionFeasibility[$key]['consumption'] = explode(',', $value['consumption'])[$key2];
                        }
                    }
                }
            }
            // dd($productionFeasibility);
            $a = 10;
            while ($a > 0) {
                $a=0;
                foreach ($productionFeasibility as $key => $value) {
                    $a += $value['consumption'];
                }
                foreach ($batchArray as $arrayCount => $batch) {
                    $batchCount = (int)$arrayCount+1;
                    if ($batch['weight'] > 0) {
                        $bal = 0;
                        foreach ($productionFeasibility as $count => $orderEntry) {
                            if ($orderEntry['consumption'] > 0) {
                                if ($bal > 0) {
                                    if ($orderEntry['consumption'] > $bal) {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$bal,
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $batchArray[$arrayCount]['weight'] = 0;
                                        $productionFeasibility[$count]['consumption'] = $orderEntry['consumption'] - $bal;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    } elseif ($orderEntry['consumption'] < $bal) {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$orderEntry['consumption'],
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $productionFeasibility[$count]['consumption'] = 0;
                                        $bal = $bal - $orderEntry['consumption'];
                                        $batchArray[$arrayCount]['weight'] = $bal;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    } else {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$orderEntry['consumption'],
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $productionFeasibility[$count]['consumption'] = 0;
                                        $batchArray[$arrayCount]['weight'] = 0;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    }
                                } else {
                                    if ($orderEntry['consumption'] > $batch['weight']) {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$batch['weight'],
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $productionFeasibility[$count]['consumption'] = $orderEntry['consumption'] - $batch['weight'];
                                        $batchArray[$arrayCount]['weight'] = 0;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    } elseif ($orderEntry['consumption'] < $batch['weight']) {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$orderEntry['consumption'],
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $productionFeasibility[$count]['consumption'] = 0;
                                        $bal = $batch['weight'] - $orderEntry['consumption'];
                                        $batchArray[$arrayCount]['weight'] = $bal;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    } else {
                                        $data = ProductionFeasibility::where('id', $orderEntry['fs_id'])->first();
                                        $data->productionPlanning()->create([
                                                                            'order_id'=>$orderEntry['order_id'],
                                                                            'start_date'=>date('Y-m-d H:i', strtotime($batch['start_date'])),
                                                                            'end_date'=>date('Y-m-d H:i', strtotime($batch['end_date'])),
                                                                            'batch'=>'Batch '.$batchCount,
                                                                            'material'=>$orderEntry['material_id'],
                                                                            'order_kg'=>$orderEntry['consumption'],
                                                                            'filament_type'=>$orderEntry['filament_type']
                                                                            ]);
                                        $productionFeasibility[$count]['consumption'] = 0;
                                        $batchArray[$arrayCount]['weight'] = 0;
                                        $data->update(['update_state'=>1]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return response()->json(['message'=> 'Production batches Successfully created'], 200);
        } catch (\Exception $e) {
            return response()->json(['message'=> $e->getMessage()], 500);
        }
    }


    /**
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     *
     *
     * The `deleteproductionPlanning` function gets the Production feasibility ID and gets all the production planning
     * for the particular feasibility and deletes them.
     *
     */


    public function deleteproductionPlanning(Request $request)
    {
        $ids = explode(',', $request['id']);
        try {
            ProductionPlanning::whereIn('feasibility_id', $ids)
                                            ->delete();

            ProductionFeasibility::whereIn('id', $ids)->update(['update_state'=>null]);

            return response()->json(['code'=>1]);
            // dd($productionPlan);

        //     $data = ProductionFeasibility::where('id',$id)->with('saleOrder')->first();
        //     $date = date('Y-m-01',strtotime($data->saleOrder->order_date));
        //     $machine = $data->machine;
        //     // dd($date, $machine);
        //     $delete = $data->productionPlanning()->delete();
        //
        //     if($delete){
        //
        //         ProductionFeasibility::where('id',$id)->update(['update_state' => null]);
        //
        //         // return redirect('machine-selection');
        //     }
        } catch (\Exception $e) {
            return response()->json(['code'=>1,'error' => 'Cannot Delete the Production Plan. Please Reload and Try again later.']);
            // return redirect()->back()->withErrors(['error' => 'Cannot Delete the Production Plan. Please Reload and Try again later.']);
        }
    }


    public function machineSelection()
    {
        return view('production.machine_selection');
    }

    public function machineSelectionTwo()
    {
        return view('production.machine_selection_two');
    }

    public function productionFeasibilityReport()
    {
        return view('production.production_feasibility_report');
    }

    public function productionPlanningReport()
    {
        return view('production.production_planning_report');
    }


    public function getProductionFeasibility(Request $request)
    {
        $request->validate([
            'material' => 'required',
            'filament' => 'required',
            'month' => 'required',
        ]);


        $month = date('Y-m-d', strtotime($request['month']));


        $productions = DB::table('production_feasibilities')
            ->join('sale_orders', 'production_feasibilities.order_id', '=', 'sale_orders.id')
            ->join('business_partners', 'sale_orders.bp_id', '=', 'business_partners.id')
            ->whereBetween('sale_orders.customer_date', [$month,date('Y-m-t', strtotime($month))])
            ->where('production_feasibilities.material', $request['material'])
            ->where('production_feasibilities.changed_filament', 'LIKE', '%'.$request['filament'].'%')
            ->get();


        foreach ($productions as $pf) {
            dd($pf);
        }
    }

    public function getProductionHours(Request $request)
    {
        $request->validate([
        'material' => 'required',
        'machine' =>'required'
      ]);

        $material = ItemMaster::where('id', $request['material'])->get()->first();

        $ProdHr = 0;

        if ($request['machine'] == 1) {
            $ProdHr = ($material->cord_wt*$material->zell_a_spls_no*$material->zell_a_speed*60)/1000;
        } elseif ($request['machine'] == 2) {
            $ProdHr = ($material->cord_wt*$material->zell_b_spls_no*$material->zell_b_speed*60)/1000;
        } elseif ($request['machine'] == 3) {
            $ProdHr = ($material->cord_wt*$material->kidde_spls_no*$material->kidde_speed*60)/1000;
        }

        return response()->json(['code'=>1,'ProdHr'=>round($ProdHr, 2)]);
    }
    public function createBufferOrder(Request $request)
    {
        try {
            $businessPartner = BusinessPartner::where('bp_name', 'buffer')->first();

            $itemMaster = ItemMaster::where('id', $request['material'])->first();

            $createOrder = $businessPartner->saleOrder()->create([

          'material_id'=>$itemMaster->id,
          'material_name' => $itemMaster->material,
          'order_quantity'=>$request['weight'],
          'order_date'=>date('Y-m-d', strtotime($request['order-date'])),
          'customer_date'=>date('Y-m-d', strtotime($request['customer-date'])),
          'last_month_pending'=>0,
          'total_order'=>$request['weight'],
          'pending_quantity'=>$request['weight']

        ]);

            //Below is to create a empty Production feasibility when a sale order is created.

            $createFeasibility = $createOrder->productionFeasibility()->create([
            'material' => $itemMaster->id,
            'cord_weight'=>$itemMaster->cord_wt,
        ]);

            return response()->json(['status'=>true]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function deleteBuffer($id)
    {
        $saleOrder = SaleOrder::where('id', $id)->first();

        if ($saleOrder->businessPartner->bp_name == "buffer") {
            $saleOrder->delete();

            return redirect('/');
        }

        return redirect('/');
    }

    public function bpItem()
    {
        $businessPartner = BusinessPartner::where('bp_name', '!=', 'buffer')->with('bpMaterials')->get()->sortBy('bp_name');

        return view('production.bp_item_master')->with(compact('businessPartner'));
    }

    public function createBpItem(Request $request)
    {
        try {
            $bpItem = $request->all();
            if (is_null($request['id'])) {
                $itemMaster = ItemMaster::where('id', $request['material_id'])->first();
                unset($bpItem['id']);
                $bpItem['material'] = $itemMaster->material;
                BpMaterial::insert($bpItem);
                return response()->json(['status'=>true]);
            } else {
                $itemMaster = ItemMaster::where('id', $request['material_id'])->first();
                $id = $bpItem['id'];
                unset($bpItem['id']);
                unset($bpItem['bp_id']);
                $bpItem['material'] = $itemMaster->material;
                BpMaterial::where('id', $id)->update($bpItem);
                return response()->json(['status'=>true]);
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function getBpItem(Request $request)
    {
        $id = $request['id'];
        try {
            // dd($id);
            $businessPartner = BpMaterial::where('id', $id)->first();
            return response()->json(['status'=>true,'bp'=>$businessPartner]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function machine()
    {
        $machine = Machine::all();
        return view('production.machine')->with(compact('machine'));
    }

    public function machineUpdate(Request $request)
    {
        $data = $request->all();
        $id = $request['id'];
        unset($data['id']);

        try {
            Machine::where('id', $id)->update($data);
            return response()->json(['status'=>true]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function itemMaster()
    {
        $itemMaster = ItemMaster::all()->sortBy('material');
        return view('production.item_master')->with(compact('itemMaster'));
    }

    public function createItem(Request $request)
    {
        $request->validate([
          'material' => 'required|unique:item_masters',
          'descriptive_name' => 'required',
          'zell_a_spls_no' => 'required|numeric',
          'zell_a_speed' => 'required|numeric',
          'zell_b_spls_no' => 'required|numeric',
          'zell_b_speed' => 'required|numeric',
          'kidde_spls_no' => 'required|numeric',
          'kidde_speed' => 'required|numeric',
          'cord_wt' => 'required|numeric',
          'hsn_sac' => 'required',
          'sgst' => 'required|numeric',
          'cgst' => 'required|numeric',
          'cgst' => 'required|numeric',
      ]);
        $itemMaster = $request->all();
        try {
            ItemMaster::create($itemMaster);
            return response()->json(['status'=>true]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }

    public function updateProductionPlan(Request $request)
    {
        $ids = explode(',', $request['pp_id']);

        // $beforeUpdateArray
        ProductionPlanning::whereIn('id', $ids)
                        ->update(
                            [
                              'start_date'=>date('Y-m-d H:i', strtotime($request['start_date'])),
                              'end_date'=>date('Y-m-d H:i', strtotime($request['end_date']))
                            ]
                        );
        return response()->json(['code'=>1]);
    }
}
