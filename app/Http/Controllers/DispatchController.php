<?php

namespace App\Http\Controllers;

use App\BpMaterial;
use App\BusinessPartner;
use App\InvoiceMaster;
use App\ItemMaster;
use App\PackageMaster;
use App\SaleOrder;
use App\SalesReturn;
use App\SalesReturnDetails;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DispatchController extends Controller
{


    public function challan(){

        $doffs = \DB::select("
            select distinct(doff_no) from weight_logs where invoice_status = 0 and package_master_id is not null and package_master_id != 0
        ");


        return view('invoice.day-wise-pack-summary')->with(compact('doffs'));
    }

    public function getChallanMaterial(Request $request){

        $request->validate([
            'material' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);


        $packageMasters = \DB::select("
               
            select package_masters.id, package_masters.case_no, test.doff_no, test.material_name, 
                   round(test.net_weight + package_masters.moisture_weight, 2) as net_weight, 
                   round(test.gross_wt + package_masters.box_weight + package_masters.inset_weight ,2) as gross_weight, 
                   package_masters.box_type, test.spindle_count from package_masters
                inner join 
                (select distinct(package_master_id),count(*) as spindle_count, MAX(packing_name) as material_name, group_concat(distinct(doff_no)) as doff_no, round(sum(material_weight),2) as net_weight, sum(total_weight) as gross_wt from  weight_logs 
                where invoice_status = 0 
                and package_master_id is not null 

                group by package_master_id ) 
                as test
                on  package_masters.id = test.package_master_id
                where material_id = :material
                and packed_date between :start_date and  :end_date
                and challan_id is null
              
        ", ['material' => $request['material'], 'start_date' => date('Y-m-d', strtotime($request['start_date'])), 'end_date' => date('Y-m-d', strtotime($request['end_date']))]);

//        $material = \DB::select("
//            select material,descriptive_name from item_masters where id = :material
//        ",["material" => $request['material']])[0];


        if(count($packageMasters) == 0){
            return redirect()->back()->withErrors(['error' => 'No packages found']);
        }

        return view('invoice.challan')->with(compact('packageMasters'));

    }


    public function createChallan(Request $request){
        $request->validate([
            'challan_no' => 'required',
            'packing_details' => 'required',
            'reason' => 'required',
            'date' => 'required',
        ]);



        try{
            $createChallan = DB::insert("
            insert into challans (challan_no, reason, date) values (:no ,:reason, :date)
        ",['no' => $request['challan_no'], 'reason' => $request['reason'], 'date' => date('Y-m-d', strtotime($request['date']))]);

            $lastInsert = DB::select("select * from challans where id = last_insert_id()")[0];



            DB::update('
            update package_masters set challan_id = :challanId where id in(' . implode(',', $request['packing_details']) . ')', ['challanId' => $lastInsert->id]);


            DB::update('
            update weight_logs set challan_id = :challanId, active_status = 0 where package_master_id in (' . implode(',', $request['packing_details']) . ')
        ', ['challanId' => $lastInsert->id]);

        }catch(\Exception $e){

            dd($e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Cannot create challan Rightnow. Please contact the developers']);
        }


        return redirect('/challan-list');
    }


    public function challanList(){
        $challans = DB::select('
            select * from challans 
        ');

        return view('invoice.challan-list')->with(compact('challans'));
    }

    public function salesReturn(){
        return view('invoice.sales-return');
    }

    public function getBpInvoices(Request $request){
        $request->validate([
            'bp_id' => 'required'
        ]);

        $invoices = InvoiceMaster::where('bp_id', $request['bp_id'])->get();

        return response()->json(['invoices' => $invoices]);

    }


    public function getInvoiceMaterials(Request $request){
        $request->validate([
            //Invoice Id
            'id' => 'required'
        ]);

        $invoice = InvoiceMaster::where('id', $request['id'])->first();

        $materials = array_keys(PackageMaster::where('invoice_master_id', $request['id'])->get()->groupBy('material_id')->toArray());

        $bpMaterials = BpMaterial::where('bp_id',$invoice->bp_id)->whereIn('material_id',$materials)->get();

//        return 'hello';
        return response()->json(['materials' => $bpMaterials]);
    }

    public function createSalesReturn(Request $request){
        $request->validate([
            "intimation_by" => "required",
              "customer" => "required",
              "received_on" => "required",
              "docs_received" => "required",
              "grn" => "required",
              "shortage" => "required",
              "reason" => "required",
              "scpl_invoice" => "required",
              "condition" => "required",
              "frieght_details" => "required",
              "sales_folio" => "required",
              "attachment_details" => "required",
              "credit_note_details" => "required",
              "invoices" => "required",
              "material" => "required",
        ]);


//        dd($request->all());

        $invoices = $request['invoices'];
        $materials = $request['material'];
        unset($request['invoices']);
        unset($request['material']);

        $merged = array_combine($invoices, $materials);

        $create = SalesReturn::create($request->all());

        if($create){
            foreach ($merged as $invoice_id => $material_id){
                $srd = new SalesReturnDetails();
                $srd->sales_return_id = $create->id;
                $srd->invoice_id = $invoice_id;
                $srd->material_id = $material_id;
                $srd->spindles = null;

                $srd->save();
            }
        }
    }
}
