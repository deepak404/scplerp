<?php

namespace App\Http\Controllers;

use App\BpMaterial;
use App\BusinessPartner;
use App\IndentDetail;
use App\IndentMaster;
use App\SaleOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('sales');
//        $this->middleware(['admin','auth','sales']);
    }

    public function showPriceTrends(Request $request)
    {
        $request->validate([
            'client' => 'required',
            'from-date' => 'required',
            'to-date' => 'required'
        ]);

        $fromDate = date('Y-m-d', strtotime($request['from-date']));
        $toDate = date('Y-m-d', strtotime($request['to-date']));

        $dateRange = $this->getDateRange($fromDate, $toDate);

        $results = [];
        $clientName = BusinessPartner::where('id', $request['client'])->value('bp_name');


        $indentMaster = IndentMaster::whereBetween('dated_on', [$fromDate, $toDate])->where('bp_id', $request['client'])->get();



        foreach ($indentMaster as $master) {
            $materialGroup = $master->indentDetails()->get()->groupBy('material_id');

            foreach ($materialGroup as $materialId => $indent) {
                $materialName = BpMaterial::where('bp_id', $request['client'])
                        ->where('material_id', $materialId)
                        ->value('descriptive_name');


                $results[$materialName][$master->dated_on] = [$indent[0]->rate_per_kg, date('M-Y', strtotime($master->dated_on)), $indent[0]->quantity];
            }
        }


//            dd($results);


        foreach ($dateRange as $month) {
            $startDate = date('Y-m-01', strtotime($month->format('Y-m')));
            $endDate = date('Y-m-t', strtotime($month->format('Y-m')));
//
            foreach ($results as $materialName => $result) {
                $temp =0;
                foreach ($result as $d => $data) {
                    if ($startDate <= $d && $endDate >= $d) {
                        $temp = 1;
                    }
                }
                if ($temp == 0) {
                    $results[$materialName][$month->format('Y-m-01')] = [0,$month->format('M-Y'),0 ];
                }
                ksort($results[$materialName]);
            }
        }


//            dd($sDate);

//            dd($results, 'hey');


//
//
//                foreach ($results as $materialName => $result){
//                    foreach ($dateRange as $month){
//
//                        foreach($result as $indentDate => $r){
//
//                            if(date('Y-m', strtotime($month->format('Y-m'))) != date('Y-m', strtotime($r[1]))){
        ////                                dd(date('Y-m', strtotime($r[1])), $month->format('Y-m'), $r[1]);
//                                $result[$month->format('Y-m-01')] = [0,$month->format('M-Y') ];
//
//                            }else{
        ////                                $result[$month->format('Y-m-01')] = [$r[0],$month->format('Y-m-01') ];
//
//                            }
//
//                        }
//
//
//                    }
//
        ////                    dd($result);
//                    ksort($result);
//
//                    unset($results[$materialName]);
//                    $results[$materialName] = $result;
//
//
//                }
//
        ////                dd($results);


        return view('sales.price-trends-result')->with(compact('results', 'clientName'));
    }


    public function getDateRange($fromDate, $toDate)
    {
        $start    = (new \DateTime($fromDate))->modify('first day of this month');
        $end      = (new \DateTime($toDate))->modify('first day of next month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);

        return $period;
    }


    public function customerOrderReport($bpid = null)
    {




        //This entire function can be re written more efficiently. Just for the sake of time it is written like this. Sorry !.

        $sixMonths = Carbon::today()->subMonths(6)->toDateString('Y-m-d');
        $saleOrders = SaleOrder::whereBetween('order_date', [$sixMonths, date('Y-m-d')])->get()->groupBy('bp_id');

        if ($bpid != null) {
            $saleOrders = SaleOrder::where('bp_id', $bpid)->whereBetween('order_date', [$sixMonths, date('Y-m-d')])->get()->groupBy('bp_id');
        }

        $results = [];

        $dateRange = $this->getDateRange($sixMonths, date('Y-m-d'));
        $dRange = [];

        $previousSaleOrders = [];




        foreach ($dateRange as $m) {
            $dRange[$m->format('M-Y')] = [];


            if ($bpid != null) {
                $previousSaleOrders[$m->format('M-Y')] = SaleOrder::where('order_date', 'LIKE', $m->format('Y-m').'%')->where('bp_id', '!=', 114)->where('bp_id', $bpid)->get()->sum('order_quantity');
            } else {
                $previousSaleOrders[$m->format('M-Y')] = SaleOrder::where('order_date', 'LIKE', $m->format('Y-m').'%')->where('bp_id', '!=', 114)->get()->sum('order_quantity');
            }
        }

        foreach ($saleOrders as $bp => $sos) {
            $bpName = BusinessPartner::where('id', $bp)->value('bp_name');
//            $bpName = $bp;
            foreach ($sos as $material_id => $saleOrder) {

//                foreach()
                $results[$bpName][date('M-Y', strtotime($saleOrder->order_date))][] = [$saleOrder->material_name,$saleOrder->order_quantity];
            }
        }


        $finalResult = [];

        foreach ($results as $bpName => $rs) {
            foreach ($rs as $month => $rr) {
                $temp[$month] = [];
                foreach ($rr as $r) {
                    if (array_key_exists($r[0], $temp[$month])) {
                        $temp[$month][$r[0]] += $r[1];
                    } else {
                        $temp[$month][$r[0]] = $r[1];
                    }
                }
            }

            $finalResult[$bpName] = $temp;
            $temp = [];
        }



        foreach ($finalResult as $bpName => $res) {
            foreach ($dateRange as $month) {
                if (array_key_exists($month->format('M-Y'), $res)) {
                } else {
                    $finalResult[$bpName][$month->format('M-Y')] = [];
                }
            }

            $res = array_merge($dRange, $res);
            $finalResult[$bpName] = $res;
        }
        $firstPriority = [];

        $highPriority = ['J.K. Fenner India Ltd (MADURAI)','J.K Fenner (India) Ltd., (Patancheru)', 'Pix Transmissions Ltd.,(SOFT)', 'Pix Transmissions Ltd.,(STIFF)','Contitech India Pvt Ltd.,',
            'OMFA Rubbers Ltd.,','446','OMFA Rubbers Ltd., - KASNA', 'N.K. Enterprises', 'OM Enterprises', 'Volga Transmission Pvt Ltd.,', 'Dharamshila Belting Pvt Ltd.,', 'Gates (India) Pvt Ltd.,'];

        foreach ($highPriority as $bp) {
            if (array_key_exists($bp, $finalResult)) {
                $firstPriority[$bp] = $finalResult[$bp];
                unset($finalResult[$bp]);
            }
        }

        ksort($finalResult);
        unset($finalResult['buffer']);

        return view('sales.order-trends-result')->with(['results' => $finalResult, 'highPriority' => $firstPriority, 'saleOrders' => $previousSaleOrders]);
    }



    public function customerDispatchReport($bpid = null)
    {
        //This entire function can be re written more efficiently. Just for the sake of time it is written like this. Sorry !.

        $sixMonths = Carbon::today()->subMonths(6)->toDateString('Y-m-d');
        $saleOrders = SaleOrder::whereBetween('order_date', [$sixMonths, date('Y-m-d')])->get()->groupBy('bp_id');

        if ($bpid != null) {
            $saleOrders = SaleOrder::where('bp_id', $bpid)->whereBetween('order_date', [$sixMonths, date('Y-m-d')])->get()->groupBy('bp_id');
        }

        $results = [];

        $dateRange = $this->getDateRange($sixMonths, date('Y-m-d'));
        $dRange = [];

        $previousSaleOrders = [];




        foreach ($dateRange as $m) {
            $dRange[$m->format('M-Y')] = [];


            if ($bpid != null) {
                $previousSaleOrders[$m->format('M-Y')] = SaleOrder::where('order_date', 'LIKE', $m->format('Y-m').'%')->where('bp_id', '!=', 114)->where('bp_id', $bpid)->get()->sum('dispatch_quantity');
            } else {
                $previousSaleOrders[$m->format('M-Y')] = SaleOrder::where('order_date', 'LIKE', $m->format('Y-m').'%')->where('bp_id', '!=', 114)->get()->sum('dispatch_quantity');
            }
        }

        foreach ($saleOrders as $bp => $sos) {
            $bpName = BusinessPartner::where('id', $bp)->value('bp_name');
//            $bpName = $bp;
            foreach ($sos as $material_id => $saleOrder) {

//                foreach()
                $results[$bpName][date('M-Y', strtotime($saleOrder->order_date))][] = [$saleOrder->material_name,round($saleOrder->dispatch_quantity, 1)];
            }
        }


        $finalResult = [];

        foreach ($results as $bpName => $rs) {
            foreach ($rs as $month => $rr) {
                $temp[$month] = [];
                foreach ($rr as $r) {
                    if (array_key_exists($r[0], $temp[$month])) {
                        $temp[$month][$r[0]] += $r[1];
                    } else {
                        $temp[$month][$r[0]] = $r[1];
                    }
                }
            }



            $finalResult[$bpName] = $temp;
            $temp = [];
        }



        foreach ($finalResult as $bpName => $res) {
            foreach ($dateRange as $month) {
                if (array_key_exists($month->format('M-Y'), $res)) {
                } else {
                    $finalResult[$bpName][$month->format('M-Y')] = [];
                }
            }

            $res = array_merge($dRange, $res);
            $finalResult[$bpName] = $res;
        }
        $firstPriority = [];

        $highPriority = ['J.K. Fenner India Ltd (MADURAI)','J.K Fenner (India) Ltd., (Patancheru)', 'Pix Transmissions Ltd.,(SOFT)', 'Pix Transmissions Ltd.,(STIFF)','Contitech India Pvt Ltd.,',
            'OMFA Rubbers Ltd.,','446','OMFA Rubbers Ltd., - KASNA', 'N.K. Enterprises', 'OM Enterprises', 'Volga Transmission Pvt Ltd.,', 'Dharamshila Belting Pvt Ltd.,', 'Gates (India) Pvt Ltd.,'];

        foreach ($highPriority as $bp) {
            if (array_key_exists($bp, $finalResult)) {
                $firstPriority[$bp] = $finalResult[$bp];
                unset($finalResult[$bp]);
            }
        }

        ksort($finalResult);
        unset($finalResult['buffer']);


        return view('sales.order-trends-result')->with(['results' => $finalResult, 'highPriority' => $firstPriority, 'saleOrders' => $previousSaleOrders]);
    }
}
