<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessPartner;
use App\BankAccount;
use App\ItemMaster;
use Faker\Factory as Faker;
use App\ProductionFeasibility;
use App\ProductionPlanning;
use App\SaleOrder;
use Carbon\Carbon;
use App\Filament;
use PDF;
use App\BpMaterial;
use App\DipSolutionParameter;
use App\WeightLog;
use App\PackageMaster;
use App\DipCordSpecification;
use App\InvoiceMaster;

class TestController extends Controller
{
    public function update()
    {
        $itemMaster_one = ItemMaster::all();

        foreach ($itemMaster_one as $key => $value) {
            $prodHr = ($value->cord_wt*60*$value->no_of_spls*$value->speed_in_mpm)/1000;
            $value->update(['prod_hr'=>round($prodHr, 2)]);
        }

        $saleOrders = SaleOrder::all();
        foreach ($saleOrders as $key => $value) {
            $itemMaster = ItemMaster::where('id', $value->material_id)->first();
            if ($itemMaster->prod_hr == 0) {
                $runningHrs = 0;
            } else {
                $runningHrs = round(($value->total_order/$itemMaster->prod_hr), 1);
            }

            //Below is to create a empty Production feasibility when a sale order is created.

            $createFeasibility = $value->productionFeasibility()->update([
                'material' => $value->material_id,
                'cord_weight'=>$itemMaster->cord_wt,
                'speed_in_mpm'=>$itemMaster->speed_in_mpm,
                'production_per_hr'=>$itemMaster->prod_hr,
                'no_of_spls'=>$itemMaster->no_of_spls,
                'running_hrs'=>$runningHrs,
            ]);
        }

        return redirect('/');
    }

    // public function bpUploade()
    // {
    //     $file = fopen('itemMaster.csv',"r");
    //     $column=fgetcsv($file);
    //     while(!feof($file)){
    //      $rowData[]=fgetcsv($file);
    //     }
    //     foreach ($rowData as $key => $value) {
    //         ItemMaster::insert(['material'=>$value[0],'cord_wt'=>$value[1],'no_of_spls'=>$value[2],'speed_in_mpm'=>$value[3],'prod_hr'=>$value[4]]);

    //     }
    //     dd('work Done!!!!');

    // }

    // public function upload()
    // {
    //     $file = fopen('dip_cord_spec.csv', "r");
    //     while (!feof($file)) {
    //         $rowData[]=fgetcsv($file);
    //     }
    //     foreach ($rowData as $value) {
    //       $itemMaster = ItemMaster::where('material',$value[0])->first();
    //       if (!is_null($itemMaster)) {
    //         $id = $itemMaster->id;
    //         DipCordSpecification::create([
    //           'material_id'=>$id,
    //           'material'=>$value[0],
    //           'floor_code'=>$value[1],
    //           'fhs'=>$value[2],
    //           'adhesion'=>$value[3],
    //           'grade'=>$value[4]
    //         ]);
    //       }
    //       // $i`
    //       // dd($wl);
    //     }
    //     dd('updated.');
    //     // DipSolutionParameter::truncate();
    //     // foreach ($rowData as $key => $value) {
    //     //   if ($value[3] == '') {
    //     //     $lsl = null;
    //     //   }else{
    //     //     $lsl = trim($value[3]);
    //     //   }
    //     //   if ($value[4] == '') {
    //     //     $usl = null;
    //     //   }else{
    //     //     $usl = trim($value[4]);
    //     //   }
    //     //   DipSolutionParameter::insert(['solution_name'=>$value[0],
    //     //   'parameter'=>$value[1],
    //     //   'uom'=>$value[2],
    //     //   'lsl'=>$lsl,
    //     //   'usl'=>$usl]);
    //     // }
    //     dd('work done!!!');
    // }


    // public function updateMaterialName(){

    //     foreach(ProductionFeasibility::all() as $plan){
    //       // dd($plan->material);
    //         if (strpos($plan->material,'|') !== false) {
    //           $plan->update(['material_name' => explode('|',$plan->material)[0]]);
    //         }
    //         // $plan->update([
    //         //     'material_name' => ItemMaster::where('id', $sale->material_id)->value('descriptive_name')
    //         // ]);
    //     }
    // }

    // public function TestReportLayout()
    // {
    //   return PDF::loadView('reports.weight-log.v2.plan-vs-actual')
    //       ->setPaper('a4', 'landscape')->setWarnings(false)->stream('Packing Clearance.pdf');
    // }

    public function RunTest()
    {
//      $file = file('weightlog.txt');
        // dd($file);
        // dd(count($file));
        // $plans = ProductionPlanning::where('start_date', '>=',date('Y-m-01'))
        //     ->where('start_date','<=',date('Y-m-t'))
        //     ->join('production_feasibilities','production_plannings.feasibility_id','=','production_feasibilities.id')
        //     ->where('production_feasibilities.machine', 2)
        //     ->join('item_masters','production_plannings.material','=','item_masters.id')
        //     ->join('sale_orders','production_feasibilities.order_id','=','sale_orders.id')
        //     ->join('business_partners','sale_orders.bp_id','=','business_partners.id')
        //     ->get(
        //               [
        //                 'production_plannings.start_date',
        //                 'production_plannings.end_date',
        //                 'production_plannings.filament_type',
        //                 'production_plannings.batch',
        //                 'production_plannings.order_kg',
        //                 'item_masters.material',
        //                 'business_partners.bp_name'
        //               ]
        //       );
        //
        // $data = [];
        // // dd($plans);
        // foreach ($plans as $matrialId => $plan) {
        //   if (empty($data[$plan->start_date][$plan->material][$plan->filament_type][$plan->batch]['sum'])) {
        //     $data[$plan->start_date][$plan->material][$plan->filament_type][$plan->batch]['sum'] = $plan->order_kg;
        //     $data[$plan->start_date][$plan->material][$plan->filament_type][$plan->batch]['customer'] = $plan->bp_name;
        //     $data[$plan->start_date][$plan->material][$plan->filament_type][$plan->batch]['end_date'] = $plan->end_date;
        //   }else{
        //     $data[$plan->start_date][$plan->material][$plan->filament_type][$plan->batch]['sum'] += $plan->order_kg;
        //   }
        // }
        // $order = SaleOrder::all()->sum('total_order');
        // $productionFeasibility = ProductionFeasibility::where('consumption','!=',null)->sum('consumption');

        // dd($productionPlanning);

        foreach (file('weightlog.txt') as $weightLog) {
            $weightLogEntry = json_decode($weightLog);

//            dd($weightLogEntry);
            $weightLog = new WeightLog();

            $materialId = ItemMaster::where('material', $weightLogEntry->wl_part)->value('id');

            if ($materialId == null) {
                dd($weightLogEntry, $weightLogEntry->wl_part);
            }

            $wlDate = preg_replace("/[^0-9]/", "", $weightLogEntry->wl_date);
            $doffDate = preg_replace("/[^0-9]/", "", $weightLogEntry->wl_dt_doff);

            $weightLog->unique_id = $weightLogEntry->UniqID;
            $weightLog->material_id = $materialId;
            $weightLog->material = $weightLogEntry->wl_part;
            $weightLog->machine = $weightLogEntry->wl_eq_id;
            $weightLog->wl_time = date('Y-m-d H:i:s', (round($wlDate/1000) + 19800));
            $weightLog->op_name = $weightLogEntry->wl_op;
            $weightLog->doff_no = $weightLogEntry->wl_doff;
            $weightLog->spindle = $weightLogEntry->wl_spindle;
            $weightLog->tare_weight = $weightLogEntry->wl_tare;
            $weightLog->material_weight = $weightLogEntry->wl_weight;
            $weightLog->total_weight = $weightLogEntry->wl_scale_wt;
            $weightLog->weight_status = $weightLogEntry->wl_ok;
            $weightLog->rw_status = $weightLogEntry->wl_rw;
            $weightLog->doff_date = date('Y-m-d H:i:s', (round($doffDate/1000) + 19800));
            $weightLog->reason = $weightLogEntry->wl_reason;

            $weightLog->save();
        }
    }

    public function orderUpdate()
    {
        $package = PackageMaster::with('weightLogs')->get();
        $error = [];
        foreach ($package as $value) {
            $material = $value->material_id;
            foreach ($value->weightLogs as $data) {
                if ($data->material_id != $material) {
                    $error[] = $value;
                }
            }
        }
        dd($error);
    }

    public function onRun()
    {
        $weightLog = WeightLog::all();
        foreach ($weightLog as $key => $value) {
            $matrialId = ItemMaster::where('material', $value->material)->value('id');
            if (!is_null($matrialId)) {
                WeightLog::where('id', $value->id)->update(['material_id'=>$matrialId]);
            } else {
                WeightLog::where('id', $value->id)->delete();
            }
        }

        $package = PackageMaster::with('weightLogs')->get();
        foreach ($package as $value) {
            $material = $value->material_id;
            foreach ($value->weightLogs as $data) {
                if ($data->material_id != $material) {
                    PackageMaster::where('id', $value->id)->update(['material_id'=>$data->material_id]);
                }
            }
        }
        dd('work done');
    }

    // public function salesRev()
    // {
    //   $saleEntrys = SaleOrder::all();
    //   // dd($saleEntrys->toArray());
    //   foreach ($saleEntrys as $saleEntry) {
    //     // dd($saleEntry);
    //     if($saleEntry->pending_quantity <= 0){
    //       SaleOrder::where('id',$saleEntry->id)->update([
    //         'pending_quantity'=>0
    //       ]);
    //     }else{
    //       if ($saleEntry->last_month_pending > 0) {
    //         $value = round($saleEntry->pending_quantity - $saleEntry->last_month_pending,1);
    //         if($value < 0){
    //           $value = 0;
    //         }
    //         SaleOrder::where('id',$saleEntry->id)->update([
    //           'pending_quantity'=>$value
    //         ]);
    //       }
    //     }
    //   }
    //   dd('Done');
    // }

    public function upload()
    {
        $file = file('weightlog1.txt');
        $start = microtime(true);
        foreach ($file as $value) {
            $json = json_decode(str_replace('\n', "", $value));
            if (!empty($json->wl_reason) && $json->wl_ok == '0') {
                $weightLog = WeightLog::where('unique_id', $json->UniqID)
                                ->where('weight_status', 0)
                                ->orderBy('doff_date')
                                ->get();
                if ($weightLog->count() > 0) {
                    $first = $weightLog->first();
                    if (is_null($first->reason)) {
                        WeightLog::where('id', $first->id)->update(['reason'=>$json->wl_reason]);
                    }
                }
            }
        }
        dd((microtime(true) - $start)/60);
    }

    public function removeDup()
    {
        $file1 = fopen('file1.csv', "r");
        while (!feof($file1)) {
            $unique[]=fgetcsv($file1)[1];
        }

        $file2 = fopen('file2.csv', "r");

        while (!feof($file2)) {
            $test = fgetcsv($file2);
            if ($test[0] == 'id') {
                $toremove[] = $test;
            }
            if (!in_array($test[1], $unique) && $test[23] == 0 && $test[27] == 'NULL') {
                $toremove[] = $test[1];
            }
        }
        // $file = fopen("contacts.csv","w");

        // foreach ($toremove as $line)
        //   {
        //   fputcsv($file,$line);
        //   }

        //   fclose($file);

        dd($toremove);
    }

    public function testUpdate()
    {
        $file = fopen('leader.csv', "r");

        while (!feof($file)) {
            $rowData[]=fgetcsv($file);
        }
        unset($rowData[0]);
        foreach ($rowData as  $value) {
            // dd($value);
            $packageMaster = PackageMaster::create([
                'material_type' => 'LEADER',
                'package_type' => $value[16],
                'packed_date' => date('Y-m-d', strtotime($value[18])),
                'case_no' => $value[2],
                'reason'=> null,
                'material_id' => $value[0],
                'box_type' => $value[14],
                'packing_box' => $value[15],
                'box_weight' => $value[12],
                'moisture_weight' => 0,
                'bobbin_count' => $value[8],
                'dispatch_date' => 'no',
                'expiry_date' => null,
                'inset_weight' => 0,
            ]);

            $uniqueId = 'LR'.time();

            WeightLog::create([
                'unique_id' => $uniqueId,
                'material_id' => $value[0],
                'material' => $value[1],
                'machine' => $value[6],
                'wl_time' => date('Y-m-d 00:00:00', strtotime($value[8])),
                'op_name' => '-',
                'doff_no' => $value[7],
                'spindle' => 'ALL',
                'tare_weight' => $value[9],
                'material_weight' => $value[10],
                'total_weight' => round(($value[11] - $value[12]), 2),
                'weight_status' => 0,
                'rw_status' => 0,
                'doff_date' => date('Y-m-d 00:00:00', strtotime($value[8])),
                'inspection_status' => 0,
                'active_status' => 0,
                'reason' => null,
                'ncr_status' => 0,
                'floor_code' => $value[4],
                'filament' => $value[3],
                'package_master_id' => $packageMaster->id,
                'packing_name'=> $value[5],
                'wl_id' => null,
            ]);
        }
        dd($rowData);
    }


    public function uploadFloorStock()
    {
        $file = fopen('1.csv', "r");
        while (!feof($file)) {
            $rowData[]=fgetcsv($file);
        }

//             dd($rowData);

        unset($rowData[0]);

        $result = [];

        foreach ($rowData as $wlEntry) {
            $result[$wlEntry[2]][] = $wlEntry;
        }


        unset($result[""]);


        foreach ($result as $caseNo => $entries) {
            $packageMaster = PackageMaster::create([
                     'material_type' => 'normal',
                     'package_type' => $result[$caseNo][0][17],
                     'packed_date' => date('Y-m-d', strtotime($result[$caseNo][0][8])),
                     'case_no' => $caseNo,
                     'reason'=> $result[$caseNo][0][18],
                     'material_id' => $result[$caseNo][0][0],
                     'box_type' => $result[$caseNo][0][15],
                     'packing_box' => $result[$caseNo][0][16],
                     'box_weight' => $result[$caseNo][0][13],
                     'moisture_weight' => $result[$caseNo][0][14],
                     'bobbin_count' => count($result[$caseNo]),
                     'dispatch_date' => 'no',
                     'expiry_date' => null,
                     'inset_weight' => 0,


                 ]);

//                 dd($result, $caseNo)




            foreach ($entries as $entry) {
                if ($entry['6'] == 'Zell B') {
                    $uniqueId = 'zellbm'.time();
                } elseif ($entry['6'] == 'Zell A') {
                    $uniqueId = 'zellam'.time();
                } else {
                    $uniqueId = 'kiddem'.time();
                }

                WeightLog::create([
                         'unique_id' => $uniqueId,
                         'material_id' => $entry[0],
                         'material' => $entry[1],
                         'machine' => $entry[6],
                         'wl_time' => date('Y-m-d 00:00:00', strtotime($entry[8])),
                         'op_name' => '-',
                         'doff_no' => $entry[7],
                         'spindle' => $entry[9],
                         'tare_weight' => $entry[10],
                         'material_weight' => $entry[11],
                         'total_weight' => round($entry[12], 2),
                         'weight_status' => 1,
                         'rw_status' => 0,
                         'doff_date' => date('Y-m-d 00:00:00', strtotime($entry[8])),
                         'inspection_status' => 1,
                         'active_status' => 1,
                         'reason' => null,
                         'ncr_status' => 0,
                         'floor_code' => $entry[4],
                         'filament' => $entry[3],
                         'package_master_id' => $packageMaster->id,
                         'packing_name'=> $entry[5],
                         'wl_id' => null,
                     ]);
            }
        }

        return "Completed";
    }

    public function braidingUpdate(Request $request)
    {
        $file = $request->file('file');
        $status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());
        if ($status) {
            if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(), 'r')) !== false) {
                while (!feof($handle)) {
                    $rowData[]=fgetcsv($handle);
                }
                $count = 0;
                // dd($rowData[0]);
                unset($rowData[0]);
                foreach ($rowData as $value) {
                    $packageMaster = PackageMaster::create([
                            'material_type' => 'braiding',
                            'package_type' => $value[17],
                            'packed_date' => date('Y-m-d', strtotime($value[19])),
                            'case_no' => $value[2],
                            'reason'=> $value[18],
                            'material_id' => $value[0],
                            'box_type' => $value[15],
                            'packing_box' => $value[16],
                            'box_weight' => $value[13],
                            'moisture_weight' => $value[14],
                            'bobbin_count' => $value[8],
                            'dispatch_date' => 'no',
                            'expiry_date' => null,
                            'inset_weight' => $value[12],
                        ]);

                    $uniqueId = $value[20].$count.time();
                    $count += 1;

                    WeightLog::create([
                        'unique_id' => $uniqueId,
                        'material_id' => $value[0],
                        'material' => $value[1],
                        'machine' => $value[6],
                        'wl_time' => date('Y-m-d 00:00:00', strtotime($value[19])),
                        'op_name' => '-',
                        'doff_no' => $value[7],
                        'spindle' => 'ALL',
                        'tare_weight' => $value[9],
                        'material_weight' => trim($value[10]),
                        'total_weight' => round($value[11], 2),
                        'weight_status' => 1,
                        'rw_status' => 0,
                        'doff_date' => date('Y-m-d 00:00:00', strtotime($value[19])),
                        'inspection_status' => 1,
                        'packed_status' => 1,
                        'active_status' => 1,
                        'reason' => null,
                        'ncr_status' => 0,
                        'floor_code' => $value[4],
                        'filament' => $value[3],
                        'package_master_id' => $packageMaster->id,
                        'packing_name'=> $value[5],
                        'wl_id' => null,
                    ]);
                }
                return response()->json(['msg'=>$file->getClientOriginalName().' Updated']);
            } else {
                return response()->json(['Error'=>$file->getClientOriginalName().' Not Updated']);
            }
        }
    }



    public function runVBelt()
    {
        $file = fopen('v-belt.csv', "r");
        while (!feof($file)) {
//            dd(fgetcsv($file)[0]);
            $rowData[]=fgetcsv($file)[0];
        }

        $rowData = array_filter($rowData);

//        dd($rowData);


        $packageMasterCases = PackageMaster::where('invoice_status', '!=', 1)->where('material_type', 'normal')->pluck('case_no')->toArray();


        $extra1 = array_diff($packageMasterCases, $rowData);

        $extra2 = [];
        $matching = [];

//        dd(count($extra1));
        $file = fopen("not-in-erp.csv", "w");



        foreach ($packageMasterCases as $r) {
            if (!in_array($r, $rowData)) {
                $extra2[] = $r;
            } else {
                $matching[] = $r;
            }
        }


        $notInErp = [];
        foreach ($rowData as $s) {
            if (!in_array($s, $packageMasterCases)) {
                $notInErp[] = $s;
                fputcsv($file, [$s,0]);
            }
        }

        fclose($file);

        dd(['csv'=> $rowData], ['Extra 1' => $extra1], ['Extra 2' => $extra2], ['extra 1 count' =>  count($extra1)], ['extra 2 count' => count($extra2)], ['diff' => array_diff($extra1, $extra2)], ['match' => $matching], ['not In ERP' => $notInErp]);
    }


    public function updateInvoice()
    {
        $file = fopen('not-matching.csv', "r");
        while (!feof($file)) {
//            dd(fgetcsv($file)[0]);
            $rowData[]=fgetcsv($file)[0];
        }

        unset($rowData[0]);

//        dd($rowData);


        foreach ($rowData as $r) {
            $p = PackageMaster::where('case_no', $r)->where('invoice_status', 0)->first();
            $updateP = PackageMaster::where('case_no', $r)->where('invoice_status', 0)->update(['invoice_status'=> 1]);


            try {
                WeightLog::where('package_master_id', $p->id)->update(['invoice_status' => 1]);
            } catch (\Exception $e) {
//                dd($p);
            }


//            dd($p, $p->weightLogs);
        }


        return "Completed";
    }

    public function onTest()
    {
        $master = InvoiceMaster::whereBetween('date', ['2019-06-01','2019-06-20'])
                                ->with('Weightlogs', 'indent', 'businessPartner', 'packageMaster')
                                ->get();
        $finalArray = [];
        $totalBilled = 0;
        foreach ($master as $invoice) {
            $packageMasters = $invoice->packageMaster;
            $weightLogs = $invoice->Weightlogs;
            $indentMater = $invoice->indent;
            $businessPartner = $invoice->businessPartner;
            $indentDetails = $indentMater->indentDetails;
            foreach ($indentDetails as $indent) {
                $finalData = [];
                $saleOrder = $indent->saleOrder;
                $finalData[$saleOrder->material_id]['item_name'] = $saleOrder->material_name;
                $finalData[$saleOrder->material_id]['weight'] = 0;
                $finalData[$saleOrder->material_id]['count'] = 0;
                $finalData[$saleOrder->material_id]['no_box'] = 1;
                $finalData[$saleOrder->material_id]['hsn'] = $indent->hsn_sac;
                $finalData[$saleOrder->material_id]['gst_value'] = $indent->gst_rate;
                $finalData[$saleOrder->material_id]['rate'] = $indent->rate_per_kg;
                $finalData[$saleOrder->material_id]['invoice_no'] = $invoice->invoice_no;
                $finalData[$saleOrder->material_id]['invoice_date'] = date('d-m-Y', strtotime($invoice->date));
                $finalData[$saleOrder->material_id]['ref_no'] = $indentMater->order_ref_no;
                $finalData[$saleOrder->material_id]['handling_charges'] = $invoice->handling_charges;
                $finalData[$saleOrder->material_id]['transit_insurance'] = $indentMater->transit_insurance;
                foreach ($packageMasters as $package) {
                    $inBox = $weightLogs->where('package_master_id', $package->id)->where('material_id', $saleOrder->material_id);
                    if ($inBox->count() > 0) {
                        $finalData[$saleOrder->material_id]['weight'] += $inBox->sum('material_weight');
                        $finalData[$saleOrder->material_id]['count'] += $inBox->count();
                        $finalData[$saleOrder->material_id]['no_box']++;
                    }
                }
                $totalAmount = $finalData[$saleOrder->material_id]['weight'] * $finalData[$saleOrder->material_id]['rate'];
                $finalData[$saleOrder->material_id]['value'] = $totalAmount;
                if ($finalData[$saleOrder->material_id]['weight'] > 0) {
                    foreach ($finalData as $material => $od) {
                        $invoiceValue = array_sum(array_column($finalData, 'value'));
    
                        $tempHandlingCharges = ($finalData[$material]['handling_charges'] / $invoiceValue) * $od['value'];
                        $insuranceCost = ((($finalData[$material]['transit_insurance'] / 100) * $invoiceValue) / $invoiceValue) * $od['value'];
    
                        $gst = $finalData[$material]['gst_value'] / 100 * ($od['value'] + $tempHandlingCharges + $insuranceCost);
                        $grantTotal = $od['value'] + $tempHandlingCharges + $insuranceCost + $gst;
                        $totalBilled += $grantTotal;
    
                        $finalData[$material]['handling_charges'] = $tempHandlingCharges;
                        $finalData[$material]['insurance'] = $insuranceCost;
                        $finalData[$material]['gst'] = $gst;
                        $finalData[$material]['grant_total'] = $grantTotal;
                    }
                    if (!empty($finalData)) {
                        $finalResult[$businessPartner->bp_name][] = $finalData;
                    }
                }
            }
        }
        // dd($finalResult);
        ksort($finalResult);
        $masterArrays = $finalResult;
        $start = '2019-06-01';
        $end = '2019-06-04';


        return view('reports.test.test')->with(compact('masterArrays', 'start', 'end'));
    }

    public function simulateManualPackingandInvoicing(){

//        $packages = PackageMaster::where('packed_date','<','2019-06-05')->where('invoice_status',0)->get();
////
////        dd($packages);
//
//        foreach ($packages as $pack){
//
//            foreach ($pack->weightLogs as $wl){
//
//                $wl->update([
//                    'packed_status' => 1,
//                    'inspection_status' => 1,
//                    'invoice_status' => 1,
//                    'inspection_date' => $wl->wl_time,
//                    'packed_date' => $wl->wl_time,
//                ]);
//            }
//
//
////            $pack->update([
////                'invoice_status' => 1,
////            ]);
//
//            $pack->invoice_status = 1;
//            $pack->save();
//
////            dd($pack, $pack->weightLogs);
//
//
//        }



        $weightlogs = WeightLog::where('doff_date','<','2019-04-01 00:00:00')->where('invoice_status',0)->get();

//        dd($weightlogs);

        foreach ($weightlogs as $wl){


                    $wl->packed_status = 1;
                    $wl->inspection_status = 1;
                    $wl->invoice_status = 1;
                    $wl->inspection_date = $wl->wl_time;
                    $wl->packed_date = $wl->wl_time;

                    $wl->save();

//            dd($wl);
        }



    }
}
