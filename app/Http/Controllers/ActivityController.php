<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Loggers\DatabaseLogger;
use Illuminate\Http\Request;
use App\Contracts\Logger;
use DateTime;

class ActivityController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::where('log_time','LIKE','%'.date('Y-m').'%')->get();


        return view('activity.index')->with(compact('activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }



    public function showActivityChanges($id){

        $activity = Activity::where('id', $id)->first();

        $activity->changed_columns = explode(',', $activity->changed_columns);

        $logDetails = $activity->log_details;

        $changed = explode(',',str_replace(array("Changed","Deleted" ,"and"), ",", $logDetails));

        unset($changed[0]);


        $activity->sorted_columns = array_values($changed);


        return response()->json(['activity' => $activity]);


    }
}
