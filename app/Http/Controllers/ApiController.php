<?php

namespace App\Http\Controllers;

use App\ItemMaster;
use App\NcrMaster;
use App\PackageMaster;
use App\PackagingMaterial;
use App\RewindWeightLog;
use Illuminate\Http\Request;
use App\WeightLog;
use Illuminate\Support\Facades\Validator;
use App\Filament;
use function GuzzleHttp\json_decode;
use App\Http\Resources\WeightLogResource;

class ApiController extends Controller
{
    public function weightLog(Request $request)
    {
        $request->validate([
          'api-key' => 'required|regex:"UNICO2019wl"',
          'UniqID'=>'required',
          'wl_eq_id'=>'required',
          'wl_date'=>'required',
          'wl_op'=>'required',
          'wl_part'=>'required',
          'wl_doff'=>'required',
          'wl_spindle'=>'required',
          'wl_tare'=>'required|numeric',
          'wl_weight'=>'required|numeric',
          'wl_scale_wt'=>'required|numeric',
          'wl_ok'=>'required',
          'wl_rw'=>'required',
          'wl_min'=>'required|numeric',
          'wl_max'=>'required|numeric',
          'wl_dt_doff'=>'required'
      ]);

        try {
            $myfile = fopen("weightlog.txt", "a");
            $txt = json_encode($request->all())."\n";
            fwrite($myfile, $txt);
            fclose($myfile);

            $wlDate = $request['wl_date'];
            $doffDate = $request['wl_dt_doff'];

            if ($request['wl_rw'] == '1') {
                if ($request['update'] != true) {
                    Weightlog::where('unique_id', $request['UniqID'])
                                ->update(['active_status'=>0]);
                }
            }

            if ($request['update'] == true) {
                WeightLog::where('unique_id', $request['unique_id'])
                    ->update([
                      'material' => $request['wl_part'],
                      'machine' => $request['wl_eq_id'],
                      'wl_time' => date('Y-m-d H:i:s', strtotime($wlDate)),
                      'op_name' => $request['wl_op'],
                      'doff_no' => $request['wl_doff'],
                      'spindle' => $request['wl_spindle'],
                      'tare_weight' => $request['wl_tare'],
                      'material_weight' => $request['wl_weight'],
                      'total_weight' => $request['wl_scale_wt'],
                      'weight_status' => $request['wl_ok'],
                      'rw_status' => $request['wl_rw'],
                      'doff_date' => date('Y-m-d H:i:s', strtotime($doffDate)),
                      'reason' => $request['reason'],
                      'ncr_status' => $request['ncr_status'] ?? 0,
                      'filament' => $request['wl_filament'] ?? null,
                      'floor_code' => $request['wl_floor_code'] ?? null,
                      'packing_name' => $request['wl_packing_name'] ?? null,
                      'wl_id' => $request['wl_id'] ?? null,
                    ]);
            } else {
                $weightLog = new WeightLog();

                $materialId = ItemMaster::where('material', $request['wl_part'])->value('id');

                if (is_null($materialId)) {
                    return response()->json(['status'=>0,'msg'=>'Material Not available in ERP.']);
                }

                //        dd((($wlDate/1000)+19800), (($doffDate/1000)+19800));

                $weightLog->unique_id = $request['UniqID'];
                $weightLog->material_id = $materialId;
                $weightLog->material = $request['wl_part'];
                $weightLog->machine = $request['wl_eq_id'];
                $weightLog->wl_time = date('Y-m-d H:i:s', strtotime($wlDate));
                $weightLog->op_name = $request['wl_op'];
                $weightLog->doff_no = $request['wl_doff'];
                $weightLog->spindle = $request['wl_spindle'];
                $weightLog->tare_weight = $request['wl_tare'];
                $weightLog->material_weight = $request['wl_weight'];
                $weightLog->total_weight = $request['wl_scale_wt'];
                $weightLog->weight_status = $request['wl_ok'];
                $weightLog->rw_status = $request['wl_rw'];
                $weightLog->doff_date = date('Y-m-d H:i:s', strtotime($doffDate));
                $weightLog->reason = $request['reason'];
                $weightLog->ncr_status = $request['ncr_status'] ?? 0;
                $weightLog->filament = $request['wl_filament'] ?? null;
                $weightLog->floor_code = $request['wl_floor_code'] ?? null;
                $weightLog->packing_name = $request['wl_packing_name'] ?? null;
                $weightLog->wl_id = $request['wl_id'] ?? null;
                if (!empty($request['inspection_status'])) {
                    $weightLog->inspection_status = $request['inspection_status'];
                    $weightLog->inspection_date = ($request['inspection_status'] == 1 ? date('Y-m-d H:i:s') : null);
                }

                $weightLog->save();
            }
        } catch (\Exception $e) {
            return response()->json(['status'=>0,'msg'=>'Contact administrator for more information', 'error' => $e->getMessage()]);
        }

        return response()->json(['status'=>1,'msg'=>'Successfully Created.']);
    }


    public function showWeightLog()
    {
        try {
            $file = file('weightlog.txt');
            $response = [];
            $arrayData = array_slice($file, -20);
            foreach ($arrayData as $value) {
                $response[] = json_decode(str_replace('\n', "", $value));
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'msg' => 'Cannot fetch the Data, Contact admin for more info.'], 500);
        }

        return response()->json(['status'=>1,'msg'=>'Successfully Created.', 'count'=>count($file) , 'data' => $response]);
    }


    public function deleteWeightLog(Request $request)
    {
        $request->validate([
          'api-key' => 'required|regex:"UNICO2019wl"',
          'wl_id'=>'required',
          'machine'=>'required',
      ]);
        try {
            $weightLog = WeightLog::where('wl_id', $request['wl_id'])
            ->where('machine', $request['machine'])
            ->where('inspection_status', 0)
            ->where('active_status', 1)
            ->first();

            if (!is_null($weightLog)) {
                $delete = $weightLog->delete();

                if ($delete) {
                    return response()->json(['msg' => 'WeightLog Entry Deleted Successfully.', 'status' => true], 200);
                }

                return response()->json(['msg' => 'Cannot Delete Entry Right now.', 'status' => false], 200);
            } else {
                return response()->json(['msg' => 'Weightlog Entry Already processed. Cannot Delete Entry Right now.', 'status' => false], 200);
            }
        } catch (\Exception $th) {
            return response()->json(['msg' => $th->getMessage(), 'status' => false], 200);
        }
    }

    public function getBarcodeWeightLog(Request $request)
    {
        $request->validate([
          'unique_id' => 'required'
      ]);


        $weightlog  = WeightLog::where('unique_id', $request['unique_id'])->get();


        if ($weightlog) {
            return response()->json(['status' => true, 'data' => $weightlog]);
        }

        return response()->json(['status' => false, 'data' => null]);
    }

    public function getBarcodeRewinding(Request $request)
    {
        $request->validate([
          'unique_id' => 'required'
      ]);


        $weightlog  = WeightLog::where('unique_id', $request['unique_id'])->get();


        if ($weightlog) {
            return response()->json(['status' => true, 'data' => $weightlog]);
        }

        return response()->json(['status' => false, 'data' => null]);
    }

    public function updateInspectionWeightLog(Request $request)
    {
        $request->validate([
          'api_key' => 'required',
          'data' => 'required',
      ]);

        $data = $request['data'];

        $validator = Validator::make($data, [
          'id' => 'required',
          'reason' => 'required',
          'status' => 'required',
          'reweight' => 'required',
      ]);


        if ($validator->fails()) {
            return response()->json(['status' => false, 'response' => 'Invalid Data'], 500);
        }

        $save = 0;


        $weightLogCount = WeightLog::where('unique_id', WeightLog::where('id', $data['id'])->where('active_status', 1)->where('rw_status', '!=', 1)->value('unique_id'))->count();

        if ($weightLogCount >= 2) {
            return response()->json(['status' => false, 'response' => 'This unique ID already has 2 entries. Cannot update more'], 200);

//          //Rewinding Check
//          $rewindCheck = WeightLog::where('unique_id', WeightLog::where('id',$data['id'])->where('active_status',1)->where('rw_status',1))->where('inspection_status',0)->count();
//
//          if($rewindCheck >= 2){
//          }
        }

        if ($data['status'] == 'notok') {
            $ncrReason = NcrMaster::all()->pluck('ncr_check', 'reason');
            $ncrStatus = 0;

            if ($ncrReason[$data['reason']] == 'yes') {
                $ncrStatus = 1;
            }
            $weightLog = WeightLog::where('id', $data['id'])->first();


            $weightLog->reason = $data['reason'];
            $weightLog->weight_status = 0;
            $weightLog->inspection_status = 1;
            $weightLog->ncr_status = $ncrStatus;
            $weightLog->inspection_date = date('Y-m-d H:i:s');

            $save = $weightLog->save();

            if ($save) {
                return response()->json(['status' => true, 'response' => 'Inspection Weightlog Updated Successfully'], 200);
            }

            return response()->json(['status' => false, 'response' => 'Inspection Weightlog Cannot be updated'], 500);
        } else {
            if ($data['reweight'] == 0) {
                /*
                 * This says Inspection weight is OK and no reweight was taken in the inspection process.
                 * */

                $weightLog = WeightLog::where('id', $data['id'])->first();
                $weightLog->inspection_status = 1;
                $weightLog->inspection_date = date('Y-m-d H:i:s');

                $save = $weightLog->save();

                if ($save) {
                    return response()->json(['status' => true, 'response' => 'Inspection Weightlog Updated Successfully'], 200);
                }

                return response()->json(['status' => false, 'response' => 'Inspection Weightlog Cannot be updated'], 500);
            } else {
                /*
                 * This says Inspection weight is OK and reweight was taken in the inspection process.
                 * */

                $oldWeightLog = WeightLog::where('id', $data['id'])->first();
                $oldWeightLog->active_status = 0;
                $oldWeightLog->inspection_date = date('Y-m-d H:i:s');
                $oldWeightLog->save();

                $weightLog = new WeightLog();

                $weightLog->unique_id = $oldWeightLog->unique_id;
                $weightLog->material_id = $oldWeightLog->material_id;
                $weightLog->material = $oldWeightLog->material;
                $weightLog->machine = $oldWeightLog->machine;
                $weightLog->floor_code = $oldWeightLog->floor_code;
                $weightLog->packing_name = $oldWeightLog->packing_name;
                $weightLog->filament = $oldWeightLog->filament;
                $weightLog->wl_time = $oldWeightLog->wl_time;
                $weightLog->op_name = $oldWeightLog->op_name;
                $weightLog->doff_no = $oldWeightLog->doff_no;
                $weightLog->spindle = $oldWeightLog->spindle;
                $weightLog->tare_weight = $oldWeightLog->tare_weight;
                $weightLog->material_weight = round(($data['reweight'] - $oldWeightLog->tare_weight), 2);
                $weightLog->total_weight = $data['reweight'];
                $weightLog->weight_status = 1;
                $weightLog->rw_status = $oldWeightLog->rw_status;
                $weightLog->doff_date = $oldWeightLog->doff_date;
                $weightLog->reason = $oldWeightLog->reason;
                $weightLog->inspection_reason = $data['reason'];
                $weightLog->weight_status = 1;
                $weightLog->inspection_status = 1;
                $weightLog->inspection_date = date('Y-m-d H:i:s');

                $save = $weightLog->save();
            }

            if ($save) {
                return response()->json(['status' => true, 'response' => 'Inspection Weightlog Updated Successfully'], 200);
            }

            return response()->json(['status' => false, 'response' => 'Inspection Weightlog Cannot be updated'], 500);
        }
    }


    public function getPackingCaseNo()
    {
        $bCount = PackageMaster::where('box_type', 'B')
          ->orWhere('box_type', 'BI')
          ->orWhere('box_type', 'BIS')
          ->orWhere('box_type', 'BS')
          ->orderBy('id', 'desc')->first();

        $cCount = PackageMaster::where('box_type', 'C')
          ->orWhere('box_type', 'CI')
          ->orWhere('box_type', 'CIS')
          ->orWhere('box_type', 'CS')
          ->orWhere('box_type', 'CK')
          ->orWhere('box_type', 'CKS')
          ->orderBy('id', 'desc')->first();

        $byCount = PackageMaster::where('box_type', 'BYS')
          ->orWhere('box_type', 'BYIS')
          ->orderBy('id', 'desc')->first();

        $hCount = PackageMaster::where('box_type', 'HDPE')
        ->orderBy('id', 'desc')->first();

        if ($cCount == null) {
            $cCount = 70001;
        } else {
            $cCount = $cCount->case_no + 1;
        }

        if ($byCount == null) {
            $byCount = 90001;
        } else {
            $byCount = $byCount->case_no + 1;
        }

        if ($bCount == null) {
            $bCount = 10001;
        } else {
            $bCount = $bCount->case_no + 1;
        }

        if ($hCount == null) {
            $hCount = 10001;
        } else {
            $hCount = $hCount->case_no + 1;
        }


        $lastEntry = PackageMaster::orderBy('id', 'desc')->first();
        $packagingMaterial = PackagingMaterial::where('name', 'LIKE', '%CORRUGATED%')->get()->groupBy('name');
        $material = ItemMaster::orderBy('material')->get();
        $packedEntry = PackageMaster::whereIn('material_type', ['WASTE','SLB','leader'])
                                    ->where('invoice_status', '0')
                                    ->with('weightLogs', 'itemMaster')
                                    ->orderBy('id', 'desc')
                                    ->get();
        $filament = array_keys(Filament::all()->groupBy('name')->toArray());
        $manufacturedDate = date('d-m-Y', strtotime($lastEntry->weightLogs->first()->value('doff_date')));

        return response()->json(['bCount'=>$bCount,
          'cCount' => $cCount,
          'byCount' => $byCount,
          'hCount' => $hCount,
          'lastEntry' => $lastEntry,
          'packingMaster' => $packagingMaterial,
          'material'=>$material,
          'packedEntry'=>$packedEntry,
          'filament'=>$filament,
          'manufactured_date' => $manufacturedDate
      ]);
    }

    public function getRepacking()
    {
        $material = ItemMaster::orderBy('material')->get();
        $filament = array_keys(Filament::all()->groupBy('name')->toArray());
        $unique = WeightLog::where('challan_id', '!=', 0)->pluck('unique_id');
        $packedEntry = WeightLog::whereIn('unique_id', $unique)
                                    ->where('active_status', 1)
                                    ->where('invoice_status', 0)
                                    ->orderBy('id', 'desc')
                                    ->get();
        return response()->json([
          'material'=>$material,
          'packedEntry'=>$packedEntry,
          'filament'=>$filament,
      ]);
    }

    public function getQrNcr($uniqueId)
    {
        if (WeightLog::where('unique_id', $uniqueId)->where('rw_status', 1)->where('ncr_status', '1')->count() > 0) {
            return response()->json(['status' => false,'msg'=>'NCR already updated.']);
        }
        $weightLog = WeightLog::where('unique_id', $uniqueId)
                          ->where('weight_status', '0')
                          ->where('rw_status', '0')
                          ->get();

        $count = $weightLog->count();

        if ($count == 0) {
            return response()->json(['status' => false,'msg'=>'No Spindle to NCR']);
        } elseif ($count > 1) {
            return response()->json(['status' => false,'msg'=>'Getting more the one spindle. Please Contact administrator.']);
        }

        return response()->json(['status' => true,'data'=>$weightLog]);
    }

    public function createNcr(Request $request)
    {
        $processData = $request->all();
        $oldWeightLog = WeightLog::where('id', $request['id'])->first();
        $oldWeightLog->active_status = 0;
        $oldWeightLog->save();

        $weightLog = new WeightLog();

        $weightLog->unique_id = $oldWeightLog->unique_id;
        $weightLog->material_id = $oldWeightLog->material_id;
        $weightLog->material = $oldWeightLog->material;
        $weightLog->machine = $oldWeightLog->machine;
        $weightLog->floor_code = $oldWeightLog->floor_code;
        $weightLog->packing_name = $oldWeightLog->packing_name;
        $weightLog->filament = $oldWeightLog->filament;
        $weightLog->wl_time = $processData['wl_time'];
        $weightLog->op_name = $oldWeightLog->op_name;
        $weightLog->doff_no = $oldWeightLog->doff_no;
        $weightLog->spindle = $processData['spindle'];
        $weightLog->tare_weight = $processData['tare_weight'];
        $weightLog->material_weight = $processData['actual_weight'];
        $weightLog->total_weight = $processData['scale_weight'];
        $weightLog->weight_status = 0;
        $weightLog->rw_status = 1;
        $weightLog->ncr_status = 1;
        $weightLog->doff_date = $oldWeightLog->doff_date;
        $weightLog->reason = $processData['reason'];
        $weightLog->inspection_reason = null;
        $weightLog->inspection_status = 0;
        $weightLog->wl_id = $processData['wl_id'];

        $save = $weightLog->save();

        if ($save) {
            return response()->json(['status' => true]);
        }

        return response()->json(['status' => false, 'msg' => 'Unable to update NCR.']);
    }


    public function bulkUpload(Request $request)
    {
        $entrys = json_decode($request['entry']);
        try {
            $count = count($entrys);
            $update = 0;

            foreach ($entrys as $key => $value) {
                $countW = WeightLog::where('unique_id', $value->unique_id)->where('weight_status', $value->weight_status)->where('rw_status', $value->rw_status)->where('spindle', $value->spindle)->count();
                if ($countW == 0) {
                    $materialId = ItemMaster::where('material', $value->material)->value('id');

                    $weightLog = new WeightLog();

                    $weightLog->unique_id = $value->unique_id;
                    $weightLog->material_id = $materialId;
                    $weightLog->material = $value->material;
                    $weightLog->machine = $value->machine;
                    $weightLog->floor_code = $value->floor_code;
                    $weightLog->packing_name = $value->packing_name;
                    $weightLog->filament = $value->filament_type;
                    $weightLog->wl_time = $value->wl_time;
                    $weightLog->op_name = $value->op_name;
                    $weightLog->doff_no = $value->doff_no;
                    $weightLog->spindle = $value->spindle;
                    $weightLog->tare_weight = $value->tare_weight;
                    $weightLog->material_weight = $value->total_weight;
                    $weightLog->total_weight = $value->material_weight;
                    $weightLog->weight_status = $value->weight_status;
                    $weightLog->rw_status = $value->rw_status;
                    $weightLog->ncr_status = $value->ncr_status;
                    $weightLog->doff_date = $value->doff_date;
                    $weightLog->reason = $value->reason;
                    $weightLog->inspection_reason = null;
                    $weightLog->inspection_status = 0;
                    $weightLog->wl_id = $value->id;

                    $save = $weightLog->save();
                    if ($save) {
                        $update += 1;
                    }
                } else {
                    WeightLog::where('unique_id', $value->unique_id)
                                ->where('wl_id', $value->id)
                                ->update([
                                    'wl_time' => $value->wl_time,
                                    'op_name' => $value->op_name,
                                    'tare_weight' => $value->tare_weight,
                                    'material_weight' => $value->total_weight,
                                    'total_weight' => $value->material_weight,
                                    'weight_status' => $value->weight_status,
                                    'reason' => $value->reason,
                                    'filament' => $value->filament_type,
                                ]);
                    $update += 1;
                }
            }
            if ($count == $update) {
                return response()->json(['status'=>true]);
            } else {
                return response()->json(['status'=>false]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false,'msg'=>$e->getMessage()]);
        }
    }

    public function addPackagingDetails(Request $request)
    {
        $request->validate([
            'box_type' => 'required',
            'case_no' => 'required',
            'package_type' => 'required',
            'package_date' => 'required',
            'bobbin_count' => 'required',
            'weight_log' => 'required',
            'moisture_regain' => 'required',
            'box_weight' => 'required',
            'packing_box' => 'required',
            'material_type' => 'required',
            'inset_weight' => 'required',
        ]);

        // ------------- For testing --------------- //
        $myfile = fopen("packing.txt", "a");
        $txt = json_encode($request->all())."\n";
        fwrite($myfile, $txt);
        fclose($myfile);
        // ---------------------------------------- //

        $fiscalYear = (date('m') > 3) ? date('Y') + 1 : date('Y');

        $fiscalStart = date(''.($fiscalYear - 1).'-04-01');

        $fiscalEnd = date(''.($fiscalYear).'-03-31');
//        $fiscalEnd = date('Y-m-d', strtotime("+12 months $fiscalEnd"));

        $existingCaseNoCount = PackageMaster::whereBetween('packed_date', [$fiscalStart, $fiscalEnd])->where('case_no', $request['case_no'])->count();

        if ($existingCaseNoCount > 0) {
            return response()->json(['status' => false, 'response' => 'Case No already Exists. Cannot Add package.']);
        }



        $weightlog = explode('|', $request['weight_log']);

        $material_id = WeightLog::where('id', $weightlog[0])->value('material_id');


        $savePackage = PackageMaster::create([

            'package_type' => $request['package_type'],
            'packed_date' => date('Y-m-d', strtotime($request['package_date'])),
            'case_no' => $request['case_no'],
            'reason' => $request['reason'],
            'material_id' => $material_id,
            'box_type' => $request['box_type'],
            'packing_box' => $request['packing_box'],
            'box_weight' => $request['box_weight'],
            'moisture_weight' => $request['moisture_regain'],
            'bobbin_count' => $request['bobbin_count'],
            'material_type' => $request['material_type'],
            'dispatch_date' => $request['dispatch_date'],
            'expiry_date' => $request['expiry_date'],
            'inset_weight' => $request['inset_weight']

        ]);




        if ($savePackage) {

//            return response()->json(['status' => false, 'response' => 'Successfully Added the Packing Details'],200);


            array_pop($weightlog);

            $weightlogEntries = WeightLog::whereIn('id', $weightlog)->get();

            foreach ($weightlogEntries as $wl) {
                $wl->package_master_id = $savePackage->id;
                $wl->packed_status = 1;
                $wl->packed_date = date('Y-m-d H:i:s');
                $wl->save();
            }

            return response()->json(['status' => true, 'response' => 'Successfully Added the Packing Details', 'id'=> $savePackage->id], 200);
        } else {
            return response()->json(['status' => false, 'response' => 'Cannot Add the packaging details right now. Please try again Later']);
        }
    }


    public function leaderIndirectPacking(Request $request)
    {
        try {
            $savePackage = PackageMaster::create([

            'package_type' => $request['package_type'],
            'packed_date' => date('Y-m-d', strtotime($request['package_date'])),
            'case_no' => $request['case_no'],
            'reason' => $request['reason'],
            'material_id' => $request['material_id'],
            'box_type' => $request['box_type'],
            'packing_box' => $request['packing_box'],
            'box_weight' => $request['box_weight'],
            'moisture_weight' => $request['moisture_regain'],
            'bobbin_count' => $request['bobbin_count'],
            'material_type' => $request['material_type'],
            'dispatch_date' => 'no',
            'expiry_date' => null,
            'inset_weight' => 0
        ]);

            $uniqueId = $request['material_type'].time();
            $material = ItemMaster::where('id', $request['material_id'])->first();
            $machine = ($request['material_type'] == 'leader' ? 'leader':'kidde');
            $actualWeight = round(($request['scale_weight'] - ($request['bobbin_count'] * $request['tare_weight']) - $request['box_weight']) + $savePackage->moisture_weight, 1);

            $weightLog = WeightLog::create([
                              'unique_id'=>$uniqueId,
                              'package_master_id'=>$savePackage->id,
                              'material_id'=>$material->id,
                              'material'=>$material->material,
                              'filament'=>$request['filament'],
                              'floor_code'=>$material->descriptive_name,
                              'packing_name'=>$material->descriptive_name,
                              'machine'=>$machine,
                              'wl_time'=>date('Y-m-d H:i:s'),
                              'op_name'=>'Packing',
                              'doff_no'=>$request['doff_no'] ?? '0',
                              'spindle'=>'ALL',
                              'tare_weight'=>$request['tare_weight'],
                              'material_weight'=>$actualWeight,
                              'total_weight'=>round($request['scale_weight'], 1),
                              'weight_status'=>1,
                              'rw_status'=>0,
                              'doff_date'=>date('Y-m-d 00:00:00', strtotime($request['package_date'])),
                              'reason'=>null,
                              'ncr_status'=>0,
                              'packed_status'=>1
                            ]);

            return response()->json(['status'=>true]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg'=>$e->getMessage()]);
        }
    }



    public function getPackedCases()
    {
        $weightLogs = WeightLog::where('packed_status', 1)->where('invoice_status', 0)->get()->groupBy('package_master_id')->toArray();

        $packageMasterIds = array_keys($weightLogs);

        $weightLogs = WeightLog::whereIn('package_master_id', $packageMasterIds)->get()->groupBy('package_master_id')->toArray();

        $packageMasters = PackageMaster::whereIn('id', $packageMasterIds)->with('invoiceMaster')->orderBy('id', 'DESC')->get();

        return response()->json(['status' => true, 'weightLogs' => $weightLogs, 'packageMasters' => $packageMasters]);
    }

    /**
     * Get Packed Cases Optimization
     *
     * @return void
     */
    public function getPackedCaseTwo()
    {
        $packageMaster = PackageMaster::where('invoice_status', 0)->with('weightLogs')->orderBy('id', 'DESC')->paginate(30);

        return $packageMaster;
    }

    public function getQrPackage(Request $request)
    {
        $request->validate([
          'package_id' => 'required'
      ]);

        $weightLog = WeightLog::where('package_master_id', $request['package_id'])->get();
        $package = PackageMaster::where('id', $request['package_id'])->first();

        $descriptiveName = ItemMaster::where('id', $package->material_id)->value('descriptive_name');

        return response()->json(['package'=> $package,'weightLogs' => $weightLog, 'material_name' => $descriptiveName]);
    }


    public function getManualCases(Request $request)
    {
        $request->validate([
          'case_no' => 'required'
      ]);

        $packageMaster = PackageMaster::whereIn('case_no', $request['case_no'])->pluck('id');

        return $packageMaster;
    }


    public function editPackage(Request $request)
    {
        $request->validate([
          'reason' => 'required',
          'package_id' => 'required',
      ]);


        $update = PackageMaster::where('id', $request['package_id'])->update(['reason' => $request['reason']]);

//      return PackageMaster::where('id', $request['package_id'])->first();

        if ($update) {
            return response()->json(['status' => true, 'response' => 'Package Reason Updated Successfully']);
        }

        return response()->json(['status' => false, 'response' => 'Package Reason Updation Failed']);
    }



    public function updateRewindingInProcess(Request $request)
    {
        $request->validate([
            'weight_log_entries' => 'required',
        ]);


        $weightLogEntries = explode('|', $request['weight_log_entries']);

        foreach ($weightLogEntries as $wlId) {
            WeightLog::where('id', $wlId)->update([
                'rewind_process' => 1,
                'final_winding_date' => date('Y-m-d H:s:i')
            ]);
        }


        return response()->json(['status' => true, 'response' => 'Spindles Status updated to In Rewinding Process']);
    }

    public function createRewindWeightLog(Request $request)
    {
        $request->validate([
            'doff_no' => 'required',
            'doff_date' => 'required',
            'material_id' => 'required',
            'material' => 'required',
            'new_material' => 'required',
            'tare_weight' => 'required',
            'floor_code' => 'required',
            'material_weight' => 'required',
            'total_weight' => 'required',
            'weight_status' => 'required',
            'ncr_status' => 'required',
//            'reason' => 'required',
            'wl_time' => 'required',
            'spindle' => 'required',
            'wl_id' => 'required',
            'op_name' => 'required',
        ]);




        $create = RewindWeightLog::create($request->all());

        if ($create) {
            return response()->json(['status' => true, 'response' => 'Successfully created Rewind Weightlog Entries']);
        }

        return response()->json(['status' => false, 'response' => 'Rewind Weightlog Entries creation failed']);
    }


    public function deleteRewindWeightLog(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);


        $delete = RewindWeightLog::where('wl_id', $request['id'])->delete();

        if ($delete) {
            return response()->json(['status' => true, 'response' => 'Successfully deleted Rewind Weightlog Entry']);
        }

        return response()->json(['status' => false, 'response' => 'Rewind Weightlog Entry deletion failed']);
    }


    public function kiddeWeightLogBulkUpload(Request $request)
    {
        // $request->validate([
        //     'weight_logs' => 'required',
        //     'rewinding' => 'required',
        // ]);
        if (!is_null($request['rewinding'])) {
            $updatedRewinding = WeightLog::whereIn('unique_id', $request['rewinding'])
                                            ->where('active_status', 1)
                                            ->update(['rewind_process'=> 1,'final_winding_date'=>date('Y-m-d H:s:i')]);
        }

        $weightLogs = json_decode($request['weight_logs']);
        $total = count($weightLogs);
        $saved = 0;

        foreach ($weightLogs as $wl) {
            $id = $wl->id;
            if (RewindWeightLog::where('wl_id', $id)->get()->count() == 0) {
                try {
                    RewindWeightLog::create(
                        [
                            'doff_no' => $wl->doff_no,
                            'doff_date' => $wl->doff_date,
                            'material_id' => $wl->material_id,
                            'material' => $wl->material,
                            'new_material' => $wl->new_material,
                            'tare_weight' => $wl->tare_weight,
                            'floor_code' => $wl->floor_code,
                            'material_weight' => $wl->material_weight,
                            'total_weight' => $wl->total_weight,
                            'weight_status' => $wl->weight_status,
                            'ncr_status' => $wl->ncr_status,
                            'reason' => $wl->reason,
                            'wl_time' => $wl->wl_time,
                            'spindle' => $wl->spindle,
                            'wl_id' => $wl->id,
                            'op_name' => $wl->op_name
                        ]
                    );
                } catch (\Exception $th) {
                    return response()->json(['status' => false, 'response' => $th->getMessage() ?? 'Weightlogs created but Rewinding Updation Failed', 'data' => null]);
                }
            }
            $saved += 1;
        }

        if ($total == $saved) {
            if (!empty($updatedRewinding)) {
                return response()->json(['status' => true, 'response' => 'Successfully Updated Weightlogs and Rewinding Process']);
            }

            return response()->json(['status' => false, 'response' => 'Weightlogs created but Rewinding Updation Failed', 'data' => true]);
        } else {
            return response()->json(['status' => false, 'response' => 'Both Weightlog creation and rewinding updation failed', 'data' => false]);
        }
    }

    public function createRepack(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'doff_no' => 'required',
            'doff_date' => 'required|date',
            'material' => 'required',
            'floor_code' => 'required',
            'spindle' => 'required'
        ]);
        
        try {
            $weightLog = WeightLog::where('id', $request['id'])->where('active_status', 1)->where('challan_id', '!=', 0)->get();
            
            if ($weightLog->count() == 0) {
                return response()->json(['status' => false, 'response' => 'Challan Not created or Alrady repack for this spindel']);
            }

            $itemMaster = ItemMaster::where('id', $request['material'])->first();

            $first = $weightLog->first();

            $created = WeightLog::create([
                            'unique_id' => $first->unique_id,
                            'material_id' => $request['material'],
                            'material' => $itemMaster->material,
                            'machine' => $first->machine,
                            'wl_time' => $first->wl_time,
                            'op_name' => $first->op_name,
                            'doff_no' => $request['doff_no'],
                            'spindle' => $request['spindle'],
                            'tare_weight' => $first->tare_weight,
                            'material_weight' => $first->material_weight,
                            'total_weight' => $first->total_weight,
                            'weight_status' => $first->weight_status,
                            'rw_status' => $first->rw_status,
                            'doff_date' => $request['doff_date'],
                            'reason' => $first->reason,
                            'ncr_status' => $first->ncr_status,
                            'filament' => $first->filament,
                            'floor_code' => $request['floor_code'],
                            'packing_name' => $first->packing_name,
                            'wl_id' => null,
                            'inspection_status'=>1,
                            'active_status'=>1,
                            'rewind_process'=>$first->rewind_process,
                            'packing_name'=>$itemMaster->descriptive_name
                        ]);
            $update = WeightLog::where('id', $request['id'])->where('active_status', 1)->update(['active_status'=>0]);
    
            if ($update) {
                return response()->json(['status' => true, 'response' => 'Successfully Updated.','data'=>$created]);
            } else {
                return response()->json(['status' => false, 'response' => 'Not able to Create Re-Pack, Contact Admin.']);
            }
        } catch (\Exception $th) {
            return response()->json(['status' => false, 'response' => $th->getMessage(),'code'=>$th->getCode()]);
        }
    }

    /**
     * Undocumented function
     *
     * @param WeightLog $id
     * @return WeightLogResource
     */
    
    public function getWeightlog(WeightLog $id): WeightLogResource
    {
        return new WeightLogResource($id);
    }

    public function getBraidingPack()
    {
        $bCount = PackageMaster::where('box_type', 'B')
          ->orWhere('box_type', 'BI')
          ->orWhere('box_type', 'BIS')
          ->orWhere('box_type', 'BS')
          ->latest()->first();

        $cCount = PackageMaster::where('box_type', 'C')
          ->orWhere('box_type', 'CI')
          ->orWhere('box_type', 'CIS')
          ->orWhere('box_type', 'CS')
          ->orWhere('box_type', 'CK')
          ->orWhere('box_type', 'CKS')
          ->latest()->first();

        $byCount = PackageMaster::where('box_type', 'BYS')
          ->orWhere('box_type', 'BYIS')
          ->latest()->first();

        $hCount = PackageMaster::where('box_type', 'HDPE')
        ->latest()->first();

        if ($cCount == null) {
            $cCount = 70001;
        } else {
            $cCount = $cCount->case_no + 1;
        }

        if ($byCount == null) {
            $byCount = 90001;
        } else {
            $byCount = $byCount->case_no + 1;
        }

        if ($bCount == null) {
            $bCount = 10001;
        } else {
            $bCount = $bCount->case_no + 1;
        }

        if ($hCount == null) {
            $hCount = 10001;
        } else {
            $hCount = $hCount->case_no + 1;
        }


        $lastEntry = PackageMaster::whereIn('material_type', ['braiding'])->latest()->first();
        $packagingMaterial = PackagingMaterial::where('name', 'LIKE', '%CORRUGATED%')->get()->groupBy('name');
        $doffs = RewindWeightLog::where('packing_status', 0)->get(['material_id','doff_no','new_material','doff_date'])->groupBy(['doff_no','new_material']);
        $doff_array = [];
        foreach ($doffs as $doff => $materials) {
            foreach ($materials as $material => $value) {
                $filaments = WeightLog::where('doff_no', $value->first()->doff_no)->where('doff_date', $value->first()->doff_date)->pluck('filament')->toArray();
                $filaments = array_unique($filaments);
                $doff_array[$doff]['filaments'] = $filaments;
                $doff_array[$doff]['dom'] = date('d-m-Y', strtotime($value->first()->doff_date));
                $doff_array[$doff]['materials'][] = ['material'=>$material,'material_id'=>$value->first()->material_id,'count'=>$value->count()];
            }
        }
        ksort($doff_array);

        $manufacturedDate = date('d-m-Y', strtotime($lastEntry->weightLogs->first()->value('doff_date')));
        return response()->json(['bCount'=>$bCount,
          'cCount' => $cCount,
          'byCount' => $byCount,
          'hCount' => $hCount,
          'lastEntry' => $lastEntry,
          'packingMaster' => $packagingMaterial,
          'manufactured_date' => $manufacturedDate,
          'doffs'=> $doff_array
      ]);
    }

    public function createBraiding(Request $request)
    {
        $request->validate([
            'doff_no' => 'required',
            'material_id' => 'required',
            'filament' => 'required',
            'b_count' => 'required',
            'box-type' => 'required',
            'case-no' => 'required',
            'moisture-regain' => 'required',
            'box-weight' => 'required',
            'package-date' => 'required',
            'inset-weight' => 'required',
            'dispatch-date' => 'required',
            'package-type' => 'required',
            'g_wt' => 'required',
        ]);

        try {
            $fiscalYear = (date('m') > 3) ? date('Y') + 1 : date('Y');

            $fiscalStart = date(''.($fiscalYear - 1).'-04-01');
    
            $fiscalEnd = date(''.($fiscalYear).'-03-31');
    
            $existingCaseNoCount = PackageMaster::whereBetween('packed_date', [$fiscalStart, $fiscalEnd])->where('case_no', $request['case-no'])->count();
    
            if ($existingCaseNoCount > 0) {
                return response()->json(['status' => false, 'response' => 'Case No already Exists. Cannot Add package.']);
            }
    
            $weightLogs = [];
    
            foreach ($request['doff_no'] as $key => $doff) {
                $finalWinding = RewindWeightLog::where('doff_no', $doff)
                                                ->where('material_id', $request['material_id'][$key])
                                                ->where('new_material', $request['new_material'][$key])
                                                ->where('packing_status', 0)
                                                ->where('weight_status', 1)
                                                ->get();
    
                $bCount = $request['b_count'][$key];
                if ($finalWinding->count() < $bCount) {
                    return response()->json(['status' => false, 'response' => "Doff $doff doesn't have $bCount Spindles - Available Spindle ".$finalWinding->count()]);
                }
                $first = $finalWinding->first();
                $weightLogs[] = [
                    'material_id'=>$first->material_id,
                    'material'=>$first->material,
                    'new_material'=>$first->new_material,
                    'floor_code'=>$first->floor_code,
                    'tare'=>$first->tare_weight,
                    'count'=>$bCount,
                    'doff_date'=>$first->doff_date,
                    'doff_no'=>$first->doff_no,
                    'filament'=>$request['filament'][$key]
                ];
            }
            $totalSpindle = array_sum($request['b_count']);
            $grossWeight = $request['g_wt'];
            $totalWeight = $grossWeight - $request['box-weight'] - $request['inset-weight'];
            $spindleWeight = round($totalWeight / $totalSpindle, 3);
            $wlIds = [];
            foreach ($weightLogs as $weightLog) {
                $totalDoffWeight = $spindleWeight * $weightLog['count'];
                $totalDoffNet = $totalDoffWeight - ($weightLog['tare'] * $weightLog['count']);
                
                $updatedRewinding = RewindWeightLog::where('doff_no', $doff)
                                                    ->where('material_id', $weightLog['material_id'])
                                                    ->where('packing_status', 0)
                                                    ->where('new_material', $weightLog['new_material'])
                                                    ->where('weight_status', 1)
                                                    ->take($weightLog['count'])
                                                    ->update(['packing_status'=>1]);
    
                $uniqueId = 'kiddeP'.time();
    
                $createWeightLog = new WeightLog();
    
    
                $createWeightLog->unique_id = $uniqueId;
                $createWeightLog->material_id = $weightLog['material_id'];
                $createWeightLog->material = $weightLog['material'];
                $createWeightLog->machine = 'kiddeP';
                $createWeightLog->wl_time = date('Y-m-d H:i:s');
                $createWeightLog->op_name = 'packing';
                $createWeightLog->doff_no = $weightLog['doff_no'];
                $createWeightLog->spindle = 'ALL-'.$weightLog['count'];
                $createWeightLog->tare_weight = $weightLog['tare'];
                $createWeightLog->material_weight = round($totalDoffNet, 1);
                $createWeightLog->total_weight = round($totalDoffWeight, 3);
                $createWeightLog->weight_status = 1;
                $createWeightLog->rw_status = 0;
                $createWeightLog->doff_date = $weightLog['doff_date'];
                $createWeightLog->reason = null;
                $createWeightLog->ncr_status = 0;
                $createWeightLog->filament = $weightLog['filament'];
                $createWeightLog->floor_code = $weightLog['floor_code'];
                $createWeightLog->packing_name = $weightLog['new_material'];
                $createWeightLog->wl_id = null;
                $createWeightLog->inspection_status = 1;
                $createWeightLog->packed_status = 1;
     
                $createWeightLog->save();
                $wlIds[] = $createWeightLog->id;
            }
            $savePackage = PackageMaster::create([
    
                'package_type' => $request['package-type'],
                'packed_date' => date('Y-m-d', strtotime($request['package-date'])),
                'case_no' => $request['case-no'],
                'reason' => $request['reason'],
                'material_id' => $request['material_id'][0],
                'box_type' => $request['box-type'],
                'packing_box' => $request['packing_box'],
                'box_weight' => $request['box-weight'],
                'moisture_weight' => $request['moisture-regain'],
                'bobbin_count' => $totalSpindle,
                'material_type' => 'braiding',
                'dispatch_date' => $request['dispatch-date'],
                'expiry_date' => $request['expiry-date'],
                'inset_weight' => $request['inset-weight']
    
            ]);
            $updatedWeightLog = WeightLog::whereIn('id', $wlIds)->update(['package_master_id'=>$savePackage->id]);
            if ($updatedWeightLog) {
                return response()->json(['status'=>true,'response'=>'Package Created Successfully.']);
            } else {
                return response()->json(['status' => false, 'response' => 'Package Not Updated Please contact Admin.']);
            }
        } catch (\Exception $error) {
            return response()->json(['status' => false, 'response' => $error->getMessage()]);
        }
    }

    public function updateKiddeSpindle(Request $request)
    {
        $request->validate([
            'api_key' => 'required|regex:"scpl-finalwinding-001"',
            'id'=>'required',
            'scale'=>'required',
            'status'=>'required',
            'unique_id'=>'required',
        ]);
        try {
            $weightLog = WeightLog::where('id', $request['id'])
                                ->where('rewind_process', 1)
                                ->where('active_status', 1)
                                ->get();

            if ($weightLog->count() == 0) {
                return response()->json(['status' => false, 'response' => 'Selected Spindle Not Move to Final Winding Process.']);
            }
            $first = $weightLog->first();
            $scaleWeight = $request['scale'];
            $tare = $first->tare_weight;
            $materialWeight = $scaleWeight - $tare;
            
            if ($scaleWeight <= 0) {
                return response()->json(['status' => false, 'response' => 'Scale Weight should not less then tare Weihgt.']);
            }

            WeightLog::create([
                "unique_id" => $first->unique_id,
                "material_id" => $first->material_id,
                "material" => $first->material,
                "filament" => $first->filament,
                "floor_code" => $first->floor_code,
                "packing_name" => $first->packing_name,
                "machine" => $first->machine,
                "wl_time" => $first->wl_time,
                "op_name" => $first->op_name,
                "doff_no" => $first->doff_no,
                "spindle" => $first->spindle,
                "tare_weight" => $first->tare_weight,
                "material_weight" => $scaleWeight,
                "total_weight" => round($materialWeight, 2),
                "rewind_process" => $first->rewind_process,
                "final_winding_reason" => $request['status'],
                "weight_status" => $first->weight_status,
                "rw_status" => $first->rw_status,
                "doff_date" => $first->doff_date,
                "reason" => $first->reason,
                "ncr_status" => $first->ncr_status,
                "quality_check" => $first->quality_check,
                "packed_status" => $first->packed_status,
                "inspection_status" => $first->inspection_status,
                "inspection_reason" => $first->inspection_reason,
                "active_status" => 1,
                "invoice_status" => 0,
                "wl_id" => null
            ]);

            WeightLog::where('id', $request['id'])->update(['active_status'=>0]);

            return response()->json(['status'=>true,'response'=>'Kidde spindle Updated Successfully.']);
        } catch (\Exception $error) {
            return response()->json(['status' => false, 'response' => $error->getMessage()]);
        }
    }
}
