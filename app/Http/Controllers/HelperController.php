<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class HelperController extends Controller
{
    public function sendOtp(Request $request){


        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $otp = rand(1000, 9999);

        try{

            $user = User::where('email', $request['email'])->first();
            if (is_null($user)) {
              return response()->json(['msg'=>false,'error' => 'Invalid Credentails'], 401);
            }

            if(Hash::check($request['password'], $user->password)){
                $request->session()->put('otp', $otp);

                if (strpos($user->email, 'demo') !== false) {

                  $request->session()->put('otp', '2017');
                  return response()->json(['msg'=>true]);

                }elseif (is_null($user->mobile)) {

                  return response()->json(['msg'=>false,'error' => 'Mobile Number not available.'], 401);

                }
                // dd($user->mobile);
                // $request->session()->put('dept', $user->name);
                // $to_name = $user->name;
                // $to_modile = $user->modile;
                // $to_email = $user->otp_email;

                $ch = curl_init();
      	        $u_info="office@indtextile.com:Shakticords";
      	        $receipientno = $user->mobile;
      	        $senderID="SCPLOT";
      	        // $otp_value = rand(1000,9999);
              	$msgtxt="Your OTP is ".$otp;
      	        curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
      	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      	        curl_setopt($ch, CURLOPT_POST, 1);
      	        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$u_info&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
      	        $buffer = curl_exec($ch);
      	        curl_close($ch);

                // $data = array('name'=>"Naveen Kumar", "body" => "The Otp is :".$otp);
                //
                //
                // Mail::send('email.sales-otp', $data, function($message) use ($to_name, $to_email) {
                //     $message->to($to_email, $to_name)
                //         ->subject('Login OTP');
                //     $message->from('scplmdu@gmail.com','Login OTP');
                // });
                if(!empty ($buffer)) {
                  return response()->json(['msg'=>true]);
                }else{
                  return response()->json(['msg'=>false,'error' => 'Something went Worng.']);
                }
            }else{

                return response()->json(['msg'=>false,'error' => 'Invalid Credentails'], 401);

            }


        }catch (\Exception $e){

            return response()->json(['msg'=>false,'error' => $e->getMessage()]);

        }
    }


    public function handleOtp(Request $request){
        $request->validate([
            'otp' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $otp = implode($request['otp']);
        $credentials = $request->only('email', 'password');

        if($request->session()->get('otp') == $otp){
            if(Auth::attempt($credentials)){
                return redirect('/');
            }else{
                return redirect()->back()->withErrors(['error' => 'Invalid Credentials']);
            }


        }else{
            return redirect()->back()->withErrors(['error' => 'Invalid OTP']);
        }
    }
}
