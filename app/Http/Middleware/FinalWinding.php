<?php

namespace App\Http\Middleware;

use Closure;

class FinalWinding
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->dept_id == '8') {
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
