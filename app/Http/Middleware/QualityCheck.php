<?php

namespace App\Http\Middleware;

use Closure;

class QualityCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->dept_id == '4') {
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
