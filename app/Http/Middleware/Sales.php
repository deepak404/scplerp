<?php

namespace App\Http\Middleware;

use Closure;

class Sales
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->dept_id == '1' || \Auth::user()->dept_id == 0) {
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
