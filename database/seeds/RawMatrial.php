<?php

use Illuminate\Database\Seeder;
use App\ItemMaster;

class RawMatrial extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	ItemMaster::truncate();

		$data = [["2X3 150s/ 125z 1100DTEX U TYPE","2X3 150s/ 125z"],
				["2X3 190s/ 100z 1100DTEX U TYPE","2X3 190s/ 100z"],
				["2X3 190z/ 100s 1100DTEX U TYPE","2X3 190z/ 100s"],
				["2X3 220s/ 120z 1100DTEX U TYPE","2X3 220s/ 120z"],
				["2X5 190s/ 100z 1100DTEX U TYPE","2X5 190s/ 100z"],
				["5X3 100s/ 100z 1100DTEX U TYPE","5X3 100s/ 100z"],
				["5X3 120s/ 60z 1100DTEX U TYPE","5X3 120s/ 60z"],
				["5X3 130s/ 90z 1100DTEX U TYPE","5X3 130s/ 90z"],
				["5X3 160s/ 80z 1100DTEX U TYPE","5X3 160s/ 80z"],
				["5X4 120s/ 60z 1100DTEX U TYPE","5X4 120s/ 60z"],
				["9X3 118s/ 60z 1100DTEX U TYPE","9X3 118s/ 60z"],
				["9X6 80s/ 40z 1100DTEX U TYPE","9X6 80s/ 40z"],
				["2X3 190s/ 100z 1100DTEX Y TYPE","2X3 190s/ 100z"],
				["2X3 190z/ 100s 1100DTEX Y TYPE","2X3 190z/ 100s"],
				["2X5 190s/ 100z 1100DTEX Y TYPE","2X5 190s/ 100z"],
				["3X3 150s/ 90z 1100DTEX Y TYPE","3X3 150s/ 90z"],
				["3X3 166s/ 100z 1100DTEX Y TYPE","3X3 166s/ 100z"],
				["3X4 70s / 110s 1100DTEX Y TYPE","3X4 70s / 110s"],
				["5X3 130s/ 90z 1100DTEX Y TYPE","5X3 130s/ 90z"],
				["5X4 120s/ 60z 1100DTEX Y TYPE","5X4 120s/ 60z"],
				["6X3 100s/ 70z 1100DTEX Y TYPE","6X3 100s/ 70z"],
				["9X3 118s/ 60z 1100DTEX Y TYPE","9X3 118s/ 60z"],
				["9X6 80s/ 40z 1100DTEX Y TYPE","9X6 80s/ 40z"],
				["1X3 190s/ 100z 1100DTEX CH TYPE","1X3 190s/ 100z"],
				["2X3 190s/ 100z 1100DTEX CH TYPE","2X3 190s/ 100z"],
				["2X3 190z/ 100s 1100DTEX CH TYPE","2X3 190z/ 100s"],
				["2X5 190s/ 100z 1100DTEX CH TYPE","2X5 190s/ 100z"],
				["3X5 160s/ 80z 1100DTEX CH TYPE","3X5 160s/ 80z"],
				["5X3 100s/ 100z 1100DTEX CH TYPE","5X3 100s/ 100z"],
				["5X4 120s/ 60z 1100DTEX CH TYPE","5X4 120s/ 60z"],
				["5X3 130s/ 90z 1100DTEX CH TYPE","5X3 130s/ 90z"],
				["5X3 160s/ 80z 1100DTEX CH TYPE","5X3 160s/ 80z"],
				["5X4 120s/ 60z 1100DTEX CH TYPE","5X4 120s/ 60z"],
				["9X3 118s/ 60z 1100DTEX CH TYPE","9X3 118s/ 60z"],
				["2X3 ARAMID SOFT","2X3 ARAMID SOFT"],
				["2X3 NFHS","2X3 NFHS"],
				["2X3 SOFT","2X3 SOFT"],
				["2X3 STIFF PIX","2X3 STIFF PIX"],
				["2X3 STIFF","2X3 STIFF"],
				["2X3 STIFF R","2X3 STIFF R"],
				["2X3 STIFF EPDM","2X3 STIFF EPDM"],
				["2X3 SUPERIOR STIFF","2X3 SUPERIOR STIFF"],
				["2X5 LFHS","2X5 LFHS"],
				["2X5 LFHS CEMENTING","2X5 LFHS CEMENTING"],
				["2X5 NFHS","2X5 NFHS"],
				["2X5 NFHS LC","2X5 NFHS LC"],
				["2X5 SOFT","2X5 SOFT"],
				["2X5 SOFT HITEC","2X5 SOFT HITEC"],
				["2X5 SEMISTIFF","2X5 SEMISTIFF"],
				["2X5 STIFF PIX","2X5 STIFF PIX"],
				["2X5 STIFF","2X5 STIFF"],
				["2X5 SUPERIOR STIFF","2X5 SUPERIOR STIFF"]];
		foreach ($data as $key => $value) {
		      	ItemMaster::insert(['matrial'=>$value[1],'description'=>$value[0]]);
		      }      
    }
}
