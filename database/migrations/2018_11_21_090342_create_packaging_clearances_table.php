<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagingClearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packaging_material_clearances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item');
            $table->string('supplier_name');
            $table->string('packing_lot_no');
            $table->string('quantity_ordered');
            $table->string('quantity_received');
            $table->string('po');
            $table->string('grn');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packaging_clearances');
    }
}
