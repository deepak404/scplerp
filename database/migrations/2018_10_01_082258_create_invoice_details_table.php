<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('master_id');
            $table->unsignedInteger('bp_id');
            $table->unsignedInteger('material_id');
            $table->unsignedInteger('order_id');
            $table->string('hsn');
            $table->decimal('dispatch_qty');
            $table->decimal('price_kg');
            $table->decimal('price');
            $table->decimal('insurance');
            $table->decimal('handling_charges');
            $table->decimal('sgst');
            $table->decimal('cgst');
            $table->decimal('igst');
            $table->decimal('gst_rate');
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('invoice_masters')->onDelete('cascade');
            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');
            $table->foreign('bp_id')->references('id')->on('business_partners')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('sale_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
