<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChemicalClearanceResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemical_clearance_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clearance_id');
            $table->string('tested_by');
            $table->date('tested_date');
            $table->date('test_arrival_date');
            $table->string('result');
            $table->text('sample');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemical_clearance_results');
    }
}
