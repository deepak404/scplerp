<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indent_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('indent_id'); // Indent ID select from IndentMater
            $table->unsignedInteger('order_id'); // Order ID select from SaleOrder
            $table->unsignedInteger('material_id'); // Add Matrial From ItemMaster Table
            $table->string('hsn_sac'); // HSN/SAC Given by indent creater
            $table->float('gst_rate'); // GST Rate get from accounting (or) get form Indent Creater
            $table->date('due_on'); // Due Date given by Indent Creater
            $table->double('quantity'); // Quantity given by Indent Creater
            $table->double('rate_per_kg'); // Rate Per Kg Given by Indent Creater
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');
            $table->foreign('indent_id')->references('id')->on('indent_masters')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('sale_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indent_details');
    }
}
