<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndentMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indent_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bp_id'); // Add business partner from BP Table
            $table->string('quotation_number'); // Quotation number added by indent creater
            $table->double('transit_insurance'); // Intergrate Tax (IGST) has to follow accounting formulas
            $table->double('handling_charges');
            $table->string('delivery_type');
            $table->string('indent_type');
            $table->string('payment_mode');
            $table->date('dated_on'); // Indent creation date
            $table->string('order_ref_no'); // Order (or) Reference Numaber given by indent creater 
            $table->string('other_ref_no')->nullable(); // Other reference Number given by indent creater 
            $table->string('dispatcher'); // Dispatch Throug set by indent creater
            $table->string('destination'); // Dispatch Throug set by indent creater
            $table->text('terms_of_delivery')->nullable(); // Description of delivery
            $table->smallInteger('edit_state')->nullable(); // Edit can have multiple option
            $table->smallInteger('delete_status')->nullable(); // Delete act as binary option
            $table->timestamps();
            
            $table->foreign('bp_id')->references('id')->on('business_partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indent_masters');
    }
}
