<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestedPalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tested_pallets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fl_id');
            $table->string('ps_no');
            $table->string('code');
            $table->string('pallet_no');
            $table->double('lea_weight');
            $table->double('actual');
            $table->longText('result');
            $table->timestamps();

            $table->foreign('fl_id')->references('id')->on('filament_clearances')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tested_pallets');
    }
}
