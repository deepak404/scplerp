<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePalletEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pallet_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fl_id');
            $table->longText('pallets');
            $table->timestamps();

            $table->foreign('fl_id')->references('id')->on('filament_clearances')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pallet_entries');
    }
}
