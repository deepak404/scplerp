<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDipCordSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dip_cord_specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('material_id');
            $table->string('material');
            $table->string('floor_code');
            $table->string('fhs')->nullable();
            $table->string('adhesion')->nullable();
            $table->string('grade')->nullable();
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dip_cord_specifications');
    }
}
