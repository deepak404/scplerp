<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('change_hr');
            $table->double('ends_drawn_hrs');
            $table->double('creel_waste');
            $table->double('white_peel_waste');
            $table->double('brown_peel_waste');
            $table->double('leader_waste');
            $table->double('maintanance');
            $table->double('trials');
            $table->double('other');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
