<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilamentClearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filament_clearances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lot_batch_no')->unique();
            $table->unsignedInteger('supplier_id');
            $table->string('material_code');
            $table->string('twisting');
            $table->string('type');
            $table->string('denier');
            $table->string('net_wt');
            $table->string('tube_color');
            $table->string('tube_id');
            $table->string('inv_no');
            $table->date('inv_date');
            $table->string('arrival_date');
            $table->integer('pallet_count');
            $table->integer('gate_entry_no');
            $table->date('gate_entry_date');
            $table->longtext('note')->nullable();
            $table->string('product_code')->nullable();
            $table->longtext('description')->nullable();
            $table->date('sample_date')->nullable();
            $table->date('test_date')->nullable();
            $table->string('clearance_status')->nullable();
            $table->longtext('report_result')->nullable();
            $table->string('tested_by')->nullable();
            $table->string('edit_delete')->nullable();
            $table->timestamps();

            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filament_clearances');
    }
}
