<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionPlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_plannings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feasibility_id');
            $table->unsignedInteger('order_id');
            $table->dateTime('stert_date');
            $table->dateTime('end_date');
            $table->string('batch');
            $table->string('material');
            $table->double('order_kg');
            $table->string('filament_type');
            $table->smallInteger('delete_status');
            $table->timestamps();

            $table->foreign('feasibility_id')->references('id')->on('production_feasibilities')->onDelete('cascade');
            $table->foreign('order_id')->references('id')->on('sale_orders')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_plannings');
    }
}
