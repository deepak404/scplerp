<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewindWeightLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewind_weight_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('wl_id');
            $table->string('doff_no');
            $table->dateTime('doff_date');
            $table->string('op_name');
            $table->string('material_id');
            $table->string('material');
            $table->string('new_material');
            $table->double('tare_weight');
            $table->string('floor_code')->nullable();
            $table->double('material_weight');
            $table->double('total_weight');
            $table->tinyInteger('weight_status');
            $table->tinyInteger('ncr_status')->default(0);
            $table->string('reason')->nullable();
            $table->dateTime('wl_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewind_weight_logs');
    }
}
