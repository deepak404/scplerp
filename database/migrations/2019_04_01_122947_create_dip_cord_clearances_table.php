<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDipCordClearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dip_cord_clearances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('material_id');
            $table->string('customer');
            $table->string('filament_type');
            $table->string('lot_no');
            $table->string('tpm');
            $table->string('machine');
            $table->text('temp_c');
            $table->text('stretch_tension');
            $table->text('damper_opening');
            $table->text('dewebber_suction');
            $table->double('speed_mpm');
            $table->text('solution');
            $table->text('batch_no');
            $table->string('scrapper_squeeze')->nullable();
            $table->integer('no_cords');
            $table->string('nip_roll_pressure')->nullable();
            $table->text('no_of_pass');
            $table->string('fan_usage')->nullable();
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dip_cord_clearances');
    }
}
