<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bp_id'); // Add business partner name from BP Table
            $table->unsignedInteger('material_id'); // Add Matrial From ItemMaster Table
            $table->string('material_name'); // Add Matrial From ItemMaster Table
            $table->double('order_quantity'); // Order Quantity by Customer in Kgms
            $table->date('order_date'); // Order Placed Date
            $table->double('total_order'); // Total Sale Order
            $table->date('customer_date'); // Customer Required Date
            $table->double('last_month_pending')->nullable(); // Current holding and pending from pre month order in Kgms
            $table->date('committed_date')->nullable(); // Committed date will set by Production planing team
            $table->date('scpl_confirmation_date')->nullable(); // Sales team add buffer days to committed date for customer dispatch date
            $table->date('dispatch_date')->nullable(); // Last Dispatched date from Dispatch Unit
            $table->double('dispatch_quantity')->nullable(); // Total dispatched Quantity in Kgms
            $table->double('pending_quantity')->nullable(); // Pending Quantity in Kgms
            $table->smallInteger('indent_status')->default('0'); // Indent_status      0 => order has no indent, 1 => order has indent 
            $table->smallInteger('edit_state')->nullable(); // Edit can have multiple option
            $table->smallInteger('delete_state')->nullable(); // Delete act as binary option
            $table->timestamps();

            $table->foreign('bp_id')->references('id')->on('business_partners')->onDelete('cascade');
            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_orders');
    }
}
