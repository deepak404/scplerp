<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_type');
            $table->unsignedInteger('material_id');
            $table->date('packed_date');
            $table->string('box_type');
            $table->double('box_weight');
            $table->double('bobbin_weight');
            $table->double('moisture_weight');
            $table->integer('bobbin_count');
            $table->smallInteger('invoice_status');
            $table->timestamps();

            $table->foreign('material_id')->references('id')->on('item_masters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_masters');
    }
}
