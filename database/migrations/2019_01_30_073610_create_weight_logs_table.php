<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeightLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->integer('material_id');
            $table->string('material');
            $table->string('machine');
            $table->string('floor_code');
            $table->string('filament');
            $table->dateTime('wl_time');
            $table->string('op_name');
            $table->string('doff_no');
            $table->string('spindle');
            $table->string('tare_weight');
            $table->string('material_weight');
            $table->string('total_weight');
            $table->string('weight_status');
            $table->string('rw_status');
            $table->dateTime('doff_date');
            $table->text('reason')->nullable();
            $table->tinyInteger('quality_check');
            $table->tinyInteger('packed_status');
            $table->string('inspection_reason')->nullable();
            $table->tinyInteger('inspection_status')->default(0);
            $table->tinyInteger('active_status')->default(1);
            $table->tinyInteger('ncr_status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight_logs');
    }
}
