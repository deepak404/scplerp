<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material');
            $table->string('descriptive_name');
            $table->double('cord_wt');
            $table->double('zell_a_spls_no');
            $table->double('zell_a_speed');
            $table->double('zell_b_spls_no');
            $table->double('zell_b_speed');
            $table->double('kidde_spls_no');
            $table->double('kidde_speed');
            $table->string('hsn_sac');
            $table->double('sgst');
            $table->double('cgst');
            $table->double('igst');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_masters');
    }
}
