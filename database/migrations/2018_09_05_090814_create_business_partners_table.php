<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bp_name');
            $table->string('gst_code');
            $table->string('state');
            $table->string('district');
            $table->string('code');
            $table->string('ar_no');
            $table->string('contact_person');
            $table->string('contact_no');
            $table->string('dispatcher');
            $table->string('dispatch_destination');
            $table->text('address')->nullable();
            $table->string('pincode')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_partners');
    }
}
