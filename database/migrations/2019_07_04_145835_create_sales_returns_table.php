<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('intimation_by');
            $table->string('customer');
            $table->string('received_on');
            $table->string('docs_received');
            $table->string('grn');
            $table->string('shortage');
            $table->string('reason');
            $table->string('scpl_invoice');
            $table->string('condition');
            $table->string('frieght_details');
            $table->string('sales_folio');
            $table->string('attachment_details');
            $table->string('credit_note_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_returns');
    }
}
