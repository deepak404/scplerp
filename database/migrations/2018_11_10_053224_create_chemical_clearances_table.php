<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChemicalClearancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chemical_clearances', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('supplier_id');
            $table->tinyInteger('supplier_type');
            $table->date('received_on');
            $table->string('chemical');
            $table->string('chemical_code');
            $table->string('gate_in_no');
            $table->date('gate_date');
            $table->string('lot_no');
            $table->date('manufactured_date');
            $table->string('total_weight');
            $table->date('expiry_date');
            $table->string('no_of_packs');
            $table->string('grn_remarks');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chemical_clearances');
    }
}
