<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBpMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bp_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bp_id');
            $table->unsignedInteger('material_id');
            $table->string('descriptive_name');
            $table->string('material');
            $table->timestamps();

            $table->foreign('bp_id')->references('id')->on('business_partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bp_materials');
    }
}
