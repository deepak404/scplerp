<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionFeasibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_feasibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->string('material');
            $table->string('machine')->nullable();
            $table->string('existing_filament')->nullable();
            $table->string('changed_filament')->nullable();
            $table->string('consumption')->nullable();
            $table->integer('no_of_spls')->nullable();
            $table->double('cord_weight')->nullable();
            $table->string('speed_in_mpm')->nullable();
            $table->string('production_per_hr')->nullable();
            $table->string('running_hrs')->nullable();
            $table->integer('no_of_changes')->nullable();
            $table->integer('changes_per_hr')->nullable();
            $table->integer('update_state')->nullable();
            $table->integer('in_stock')->nullable();
            $table->timestamps();


            $table->foreign('order_id')->references('id')->on('sale_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_feasibilities');
    }
}
