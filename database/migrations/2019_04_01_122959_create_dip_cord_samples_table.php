<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDipCordSamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dip_cord_samples', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('master_id');
            $table->dateTime('sample_date');
            $table->string('process');
            $table->string('doff_no');
            $table->text('spindles');
            $table->text('result');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('dip_cord_clearances')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dip_cord_samples');
    }
}
