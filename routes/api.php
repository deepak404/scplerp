<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/weight-log', 'ApiController@weightLog');

Route::post('/delete-weight-log', 'ApiController@deleteWeightLog');

Route::get('/show-weight-log', 'ApiController@showweightLog');

Route::post('/get-barcode-weight-log', 'ApiController@getBarcodeWeightLog');

Route::post('/get-packing-barcode', 'ApiController@getPackingBarcode');

Route::post('/get-barcode-rewinding', 'ApiController@getBarcodeRewinding');

Route::post('/update-inspection-weight-log', 'ApiController@updateInspectionWeightLog');

Route::get('/get-qr-ncr/{uniqueId}', 'ApiController@getQrNcr');

Route::post('/create-ncr', 'ApiController@createNcr');

Route::post('/bulk-upload', 'ApiController@bulkUpload');

Route::get('/get-packing-case-no', 'ApiController@getPackingCaseNo');

Route::get('/get-repacking', 'ApiController@getRepacking');

Route::get('/get-weight-log/{id}', 'ApiController@getWeightlog');

Route::post('/update-inspection-weight-log', 'ApiController@updateInspectionWeightLog');

Route::post('/add-packaging-details', 'ApiController@addPackagingDetails');

Route::post('/add-leader-indirect-packing', 'ApiController@leaderIndirectPacking');

Route::get('/get-packed-cases', 'ApiController@getPackedCases');

Route::get('v2/get-packed-cases', 'ApiController@getPackedCaseTwo');

Route::post('/get-qr-package', 'ApiController@getQrPackage');

Route::post('/get-manual-cases', 'ApiController@getManualCases');

Route::post('/edit-package', 'ApiController@editPackage');


Route::post('/update-rewinding-inprocess', 'ApiController@updateRewindingInProcess');

Route::post('/create-rewind-weightlog', 'ApiController@createRewindWeightLog');

Route::post('/delete-rewind-weightlog', 'ApiController@deleteRewindWeightLog');

Route::post('/bulk-kidde-upload', 'ApiController@kiddeWeightLogBulkUpload');

Route::post('/create-repack', 'ApiController@createRepack');

Route::get('/get-braiding-pack', 'ApiController@getBraidingPack');

Route::post('/create-braiding', 'ApiController@createBraiding');

Route::post('/update-kidde-spindle', 'ApiController@updateKiddeSpindle');
