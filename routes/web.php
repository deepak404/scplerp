<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    $user = Auth::user();
    if (!$user) {
        return redirect('/login');
    } elseif ($user->dept_id == 1) {
        return redirect('/sale-order');
    } elseif ($user->dept_id == 2) {
        return redirect('/production-feasibility');
    } elseif ($user->dept_id == 3) {
        return redirect('/weight-log-list');
    } elseif ($user->dept_id == 4) {
        return redirect('/filament-clearance-qc');
    } elseif ($user->dept_id == 5) {
        return redirect('/packaging');
    } elseif ($user->dept_id == 6) {
        return redirect('/instock');
    } elseif ($user->dept_id == 7) {
        return redirect('/filament-clearance');
    } elseif ($user->dept_id == 8) {
        return redirect('/final-winding');
    } else {
        return redirect('customer-order-report');
    }
});


// Route::get('/csv-upload', 'MasterController@uploadFromCsv');

Route::post('/send-otp', 'HelperController@sendOtp');

Route::post('/handle-otp', 'HelperController@handleOtp');

// Route::get('/email', function (){
//     return view('email.sales-otp');
// });


Route::get('/price-trends', function () {
    $user = Auth::user();
    if (!$user) {
        return redirect('/login');
    } elseif ($user->dept_id == 1 || $user->dept_id == 0) {
        return view('sales.price-trends');
    } else {
        return redirect('/');
    }
});



Route::post('/show-price-trends', 'AdminController@showPriceTrends');

Route::get('/customer-order-report', 'AdminController@customerOrderReport');

Route::get('/customer-dispatch-report', 'AdminController@customerDispatchReport');


Route::get('/customer-order-trend', function () {
    return view('sales.customerwise-order-trends');
});

Route::get('/customer-order-trend-report/{bpid?}', 'AdminController@customerOrderReport');




// **********************  Sales  ***************************************

Route::get('/sale-order/{date?}', 'SalesController@index')->name('SaleOrders');

Route::post('/create-sale-order', 'SalesController@createSaleOrder');

Route::post('/update-sale-order', 'SalesController@updateSaleOrder');

Route::post('/delete-sales', 'SalesController@deleteSaleOrder');

Route::post('/get-sale-order', 'SalesController@getSaleOrder');

Route::get('/generate-indent', 'SalesController@generateIndent')->name('generateIndent');

Route::get('/indent-list/{date?}', 'SalesController@indentList')->name('indentList');

Route::post('/get-bp-order', 'SalesController@getBpOrder');

Route::post('/create-indent', 'SalesController@createIndent');

Route::get('/get-sales-report', 'SalesController@getSalesReport');

Route::post('/generate-sales-report', 'ReportController@dateCommitmentReport');

Route::get('/generate-indent-report/{id}', 'ReportController@indentReport');

Route::get('/delete-indent/{id}', 'SalesController@deleteIndent');

Route::get('/edit-indent/{id}', 'SalesController@editIndent');

Route::get('/delete-indent-detail/{id}', 'SalesController@deleteIndentDetail');

Route::post('/add-indent-detail', 'SalesController@addIndentDetail');

Route::post('/update-indent', 'SalesController@updateIndent');

Route::get('/item-master', 'SalesController@showItemMaster');

Route::post('/create-customer', 'MasterController@createCustomer');

Route::get('/pending-contracts/{group?}', 'ReportController@pendingContracts');

Route::get('/get-sales-csv', 'SalesController@getSalesCsv');

Route::get('/sales-summary', 'SalesController@salesSummary');

Route::post('/get-sales-summary', 'SalesController@getSalesSummary');

Route::get('/customer-summary', 'SalesController@customerSummary');

Route::post('/customer-summary-data', 'ReportController@customerSummary');

Route::post('/get-bp-materials', 'SalesController@getBusinessPartnerMaterials');

Route::post('/update-date-committed', 'SalesController@updateDateCommitted');

Route::get('/pre-close-order/{id?}', 'SalesController@preCloseOrder');

Route::post('/pre-close-data', 'SalesController@preCloseData');

Route::get('/business-partner', 'SalesController@businessPartner');

Route::post('/create-business-partner', 'SalesController@createBusinessPartner');

Route::post('/get-bp', 'SalesController@getBp');

Route::get('/pre-close-report', function () {
    return view('invoice.dispatch-by-date');
});

Route::post('/show-preclose-report', 'SalesController@showPrecloseReport');

Route::get('/sales-plan-vs-actual', function () {
    return view('sales.plan_vs_actual');
});

// ******************** Production Planning *******************************



Route::get('/production-feasibility/{date?}', 'ProductionPlanningController@index');

Route::post('/update-production-feasibility', 'ProductionPlanningController@updateProductionFeasibility');

Route::get('/machine-selection', 'ProductionPlanningController@machineSelection');

Route::get('/machine-selection-two', 'ProductionPlanningController@machineSelectionTwo');

Route::post('/production-planning', 'ProductionPlanningController@productionPlanning');

Route::post('/planned-production', 'ProductionPlanningController@plannedProduction');

Route::post('/create-production-batch', 'ProductionPlanningController@createProductionBatch');

Route::post('/delete-production-plan', 'ProductionPlanningController@deleteproductionPlanning');

Route::get('/production-feasibility-report', 'ProductionPlanningController@productionFeasibilityReport');

Route::get('/production-planning-report', 'ProductionPlanningController@productionPlanningReport');

Route::post('/tentative-production-plan-one', 'ReportController@tentativeProductionPlanOne');

Route::post('/tentative-production-plan-two', 'ReportController@tentativeProductionPlanTwo');

Route::post('/get-production-feasibility', 'ProductionPlanningController@getProductionFeasibility');

Route::post('/get-production-hours', 'ProductionPlanningController@getProductionHours');

Route::post('/add-buffer-order', 'ProductionPlanningController@createBufferOrder');

Route::get('/delete-buffer/{id}', 'ProductionPlanningController@deleteBuffer');

Route::get('/bp-item', 'ProductionPlanningController@bpItem');

Route::post('/create-bp-item', 'ProductionPlanningController@createBpItem');

Route::post('/get-bp-item', 'ProductionPlanningController@getBpItem');

Route::get('/machine', 'ProductionPlanningController@machine');

Route::post('/machine-update', 'ProductionPlanningController@machineUpdate');

Route::get('/item-master', 'ProductionPlanningController@itemMaster');

Route::post('/create-item', 'ProductionPlanningController@createItem');

Route::post('/update-production-plan', 'ProductionPlanningController@updateProductionPlan');



// ****************** Filament Production ************************

Route::get('/filament-clearance-production', 'ProductionController@filamentClearance');

Route::get('/pallet-entry/{id}', 'ProductionController@palletEntry');

Route::post('/update-pallet-entry', 'ProductionController@updatePalletEntry');

Route::get('/filament-clearance-report-one', 'ReportController@filamentClearanceReportOne');

Route::get('/filament-clearance-report-one/{id}', 'ReportController@filamentClearanceReportOne');

Route::get('/filament-clearance-report-two/{id}', 'ReportController@filamentClearanceReportTwo');




// ******************** Weight Log *******************************

Route::get('/weight-log', 'WeightLogController@index');

Route::get('/weight-log-list/{date?}', 'WeightLogController@showWeightLogList');

Route::post('/insert-weight-log', 'WeightLogController@insertWeightLogDetails');

Route::post('/update-weight-log', 'WeightLogController@updateWeightLogDetails');

Route::get('/edit-weight-log/{id}', 'WeightLogController@editWeightLogDetails');

Route::get('/delete-weight-log/{id}', 'WeightLogController@deleteWeightLogDetails');

Route::get('/delete-weight-log/{id}', 'WeightLogController@deleteWeightLogDetails');

Route::get('/show-weightlog-report/{id}/{machine}', 'ReportController@generateWeightLogReport');

Route::post('/plan-actual-report', 'ReportController@planActualReport');

Route::post('/production-stock-report', 'ReportController@productionStockReportTwo');


// ******************************* Production ************************************

Route::get('/plan-vs-actual', 'ProductionController@planVsActual');

Route::get('/dip-solution-clearance-production', 'ProductionController@dipSolution');

Route::post('/create-dip-solution', 'ProductionController@createDipSolutionClearance');

Route::get('/dip-cords-list/{date?}', 'ProductionController@dipCordsList');

Route::get('/dip-cord-clearance', 'ProductionController@dipCordsClearance');

Route::post('/dip-cord-clearance-create', 'ProductionController@dipCordsClearanceCreate');

Route::get('/all-defect-date-range', function () {
    return view('production.all-defect');
});


Route::get('/ncr-report', function () {
    return view('production.all-defect');
});

Route::get('/doff-wise-defect', function () {
    return view('production.all-defect');
});

Route::post('/generate-defect-report', 'ReportController@generateDefectReport');

Route::post('/generate-doffwise-defect', 'ReportController@generateDoffwiseDefect');

Route::post('/generate-ncr-report', 'ReportController@generateNcrReport');

Route::get('/packed-case', 'ProductionController@packedCase');

Route::post('/packed-case', 'ReportController@packedCase');

// Route::get('/production-report-ok', 'ReportController@productionReportOk');

// Route::get('/production-report-not-ok', 'ReportController@productionReportNotOk');

// Route::get('/production-report-ncr', 'ReportController@productionReportNcr');

Route::get('/production-stock', 'ProductionController@productionStock');

Route::get('/production-report', 'ProductionController@productionReport');

Route::post('/production-report', 'ReportController@productionReport');

Route::get('/sticker-change-report', 'ProductionController@stickerChange');

Route::post('/sticker-changes-report', 'ReportController@stickerChange');

Route::get('/production-sales-return', 'ProductionController@salesReturn');

Route::get('/update-sr-spindles/{id}', 'ProductionController@updateSalesReturnSpindles');

Route::post('/create-sr-spindles', 'ProductionController@createSalesReturnSpindles');


// ******************** Packaging *********************************

Route::get('/packaging', 'PackagingController@index');

Route::get('/packaging/{date?}', 'PackagingController@index');

Route::get('/packaging-detail/{id}', 'PackagingController@packagingDetail');

Route::get('/delete-packaging/{id}', 'PackagingController@deletePackagingDetail');

Route::post('/add-packaging-master', 'PackagingController@addPackagingMaster');

Route::post('/add-package-details', 'PackagingController@addPackageDetails');

Route::get('/packaging-detail-report/{id}', 'ReportController@packagingDetail');

Route::get('/get-package-details/{id}', 'PackagingController@getPackageDetails');

Route::post('/update-packaging-details', 'PackagingController@updatePackagingMaster');

Route::post('/get-doff-weightlog-details', 'PackagingController@getDoffWeightLogDetails');

Route::get('/get-package-details/{id}', 'PackagingController@getPackageDetails');


// ********************* Invoice **********************************

Route::get('/create-invoice/{indentId}', 'InvoiceController@createInvoiceTwo');

Route::post('/create-invoice-details', 'InvoiceController@createInvoiceDetails');

Route::get('/generate-invoice', 'InvoiceController@index');

Route::get('/invoice-list/{date?}', 'InvoiceController@invoiceList');

Route::post('/generate-invoice-report', 'ReportController@generateInvoice');

Route::get('/indent-list-invoice/{bp_id?}', 'InvoiceController@indentList');

Route::get('/get-package-slip/{id}', 'ReportController@getPackageSlip');

Route::get('/delete-invoice/{id}', 'InvoiceController@deleteInvoice');

Route::get('/instock/{exp?}', 'InvoiceController@instock');

Route::get('/instock-ncr/{exp?}', 'InvoiceController@instockNcr');

Route::get('/instock-leader/{exp?}', 'InvoiceController@instockLeader');

Route::get('/instock-waste/{exp?}', 'InvoiceController@instockWeast');

Route::get('/instock-slb/{exp?}', 'InvoiceController@instockSlb');

Route::get('/get-ref-no/{ref?}', 'InvoiceController@getRefNo')->where('ref', '(.*)');

Route::get('/finshed-goods-stock/{exp?}', 'InvoiceController@finshedGoods');

Route::get('/finshed-goods-stock-ncr/{exp?}', 'InvoiceController@finshedGoodsNcr');

Route::get('/finshed-goods-stock-leader/{exp?}', 'InvoiceController@finshedGoodsLeader');

Route::get('/finshed-goods-stock-waste/{exp?}', 'InvoiceController@finshedGoodsWaste');

Route::get('/finshed-goods-stock-slb/{exp?}', 'InvoiceController@finshedGoodsSlb');

Route::get('/day-wise-pack', 'InvoiceController@dayWisePack');

Route::get('/box-usage', 'InvoiceController@boxUsage');

Route::post('/get-day-wise-pack', 'ReportController@getDayWisePack');

Route::post('/get-box-usage', 'ReportController@getBoxUsage');

Route::get('/count-pack/{item?}', 'InvoiceController@countPacked');

Route::get('/day-wise-prod', 'InvoiceController@dayWiseProduction');

Route::post('/get-count-wise-pack', 'ReportController@getCountPack');

Route::post('/get-material-pack', 'ReportController@getMaterialPack');

Route::post('/get-day-prod-summary', 'ReportController@getDayProductionSummary');

// ********************* Dispatch ***************************************

Route::get('/dispatch-by-date', 'InvoiceController@dispatchByDate');

Route::post('/get-dispatch-date', 'ReportController@dispatchByDate');

Route::get('/dispatch-by-customer', 'InvoiceController@dispatchByDate');

Route::post('/get-dispatch-by-customer', 'ReportController@dispatchByCustomer');

Route::get('/dispatch-all-customer', 'InvoiceController@dispatchByDate');

Route::post('/get-cumulative-dispatch', 'ReportController@getCumulativeDispatch');

Route::get('/dispatch-by-material', 'InvoiceController@dispatchByMaterial');

Route::post('/get-dispatch-by-material', 'InvoiceController@dispatchByCount');

Route::get('/production-summary', 'InvoiceController@productionSummary');

Route::post('/get-production-summary', 'ReportController@productionSummary');


Route::get('as-on-packed', function () {
    return view('invoice.day-wise-pack-summary');
});


Route::get('test-view', function () {
    return view('reports.test.test');
});

Route::post('/get-as-on-packed-summary', 'ReportController@getAsOnPacked');


Route::get('test-view', function () {
    return view('reports.test.test');
});


Route::get('challan', 'DispatchController@challan');

Route::post('/get-challan-material', 'DispatchController@getChallanMaterial');

Route::post('/create-challan-details', 'DispatchController@createChallan');

Route::get('challan-list', 'DispatchController@challanList');

Route::get('generate-challan-list/{id}', 'ReportController@generateChallanList');

Route::get('sales-return', 'DispatchController@salesReturn');

Route::post('get-bp-invoices', 'DispatchController@getBpInvoices');

Route::post('get-invoice-materials', 'DispatchController@getInvoiceMaterials');

Route::post('create-sales-return', 'DispatchController@createSalesReturn');




// ********************* Quality Control ********************************

Route::get('/quality-chemical-clearance', 'QualityController@showChemicalLot')->name('quality-chemical-clearance');

Route::get('/chemical-quality-update/{id}', 'QualityController@updateChemicalLot')->name('chemical-quality-update');

Route::get('/chemical-clearance-report/{id}', 'ReportController@generateChemicalClearance')->name('chemical-clearance-report');

Route::post('/insert-chemical-clearance-result', 'QualityController@insertChemicalClearanceResult')->name('insert-chemical-clearance-result');

Route::get('/chemical-clearance-updated-list', 'QualityController@showUpdatedChemicalClearance');

Route::get('/edit-updated-chemical-details/{id}', 'QualityController@editUpdatedChemical');





Route::get('/filament-clearance-qc', 'QualityController@filamentClearanceQc');

Route::get('/filament-quality-update/{id}', 'QualityController@filamentQualityUpdate');

Route::post('/filament-quality-status', 'QualityController@filamentQualityStatus');

Route::post('/filament-report-update', 'QualityController@ReportUpdate');

Route::post('/filament-pallet-result', 'QualityController@filamentPalletResult');

Route::get('/filament-pallet-delete/{id}', 'QualityController@filamentPalletDelete');

Route::get('/filament-clearance-update/{id}', 'QualityController@filamentClearanceUpdate');




Route::get('/pm-clearance-list', 'QualityController@showPackingMaterialClearance');

Route::get('/update-packing-material-lot/{id}', 'QualityController@updatePackingMaterialLot');

Route::get('/packing-clearance-updated-list', 'QualityController@showUpdatedPackingClearance');

Route::get('/edit-updated-packing-details/{id}', 'QualityController@editUpdatedPacking');

Route::get('/packing-clearance-report/{id}', 'ReportController@generatePackingClearance');

Route::post('/insert-packing-clearance-result', 'QualityController@insertPackingClearanceResult');



Route::get('/ds-clearance-list', 'QualityController@showDipSolutionClearance');

Route::get('/dip-solution-update/{id}', 'QualityController@updateDipSolutionLot')->name('dip-solution-update');

Route::post('/insert-dip-solution-result', 'QualityController@insertdipSolutionClearanceResult');

Route::get('/dip-solution-report/{id}', 'ReportController@dipSolution');

Route::get('/delete-dip-solution-lot/{id}', 'ProductionController@deleteDipSolutionLot');

Route::get('/ds-cleared-list', 'QualityController@showClearedDsList');




// ********************* Purchase ***************************************

Route::get('/filament-clearance', 'PurchaseController@filamentClearance');

Route::post('/create-filament-memo', 'PurchaseController@createFilamentMemo');

Route::post('/get-filament-memo', 'PurchaseController@getFilamentMemo');

Route::get('/delete-filament-memo/{id}', 'PurchaseController@deleteFilamentMemo');

Route::post('/get-filament-code', 'PurchaseController@getFilamentCode');



Route::get('/chemical-clearance', 'PurchaseController@chemicalClearance');

Route::post('/get-chemical-clearance', 'PurchaseController@getChemicalClearance');

Route::post('/create-chemical-lot', 'PurchaseController@createChemicalLot');

Route::post('/edit-chemical-lot', 'PurchaseController@createChemicalLot');

Route::get('/delete-chemical-lot/{id}', 'PurchaseController@deleteChemicalLot');




Route::get('/packaging-material-clearance', 'PurchaseController@packageClearance');

Route::post('/find-packing-material-lot', 'PurchaseController@findPackingMaterialLot');

Route::get('/delete-packaging-material-lot/{id}', 'PurchaseController@deletePackageMaterialLot');

Route::post('/create-packaging-lot', 'PurchaseController@createPackagingLot');





// ****************************** QR Code ********************************

Route::get('generate-qr', 'QrController@generateQr');

Route::get('generate-fabric-qr', 'QrController@generateFabricQr');


Route::post('generate-qr-code', 'QrController@generateQrCode');

Route::post('generate-fabric-qr-code', 'QrController@generateFabricQrCode');

// ********************** Logout ****************************************

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');





// ********************** Test Run **************************************

//  Route::get('/salesRev', 'TestController@salesRev');
//
// Route::get('/test', 'TestController@onTest');

Route::get('/runTest', 'TestController@simulateManualPackingandInvoicing');

Route::post('/doffUpdate', 'TestController@braidingUpdate');

Route::get('/manual-braiding', function () {
    return view('manual-braiding');
});




Route::get('runvbelt', 'TestController@uploadFloorStock');


// ********************* Admin ********************************************

Route::prefix('admin')->group(function () {
    Route::get('dashboard', function () {
        $user = Auth::user();
        if (!$user) {
            return redirect('/login');
        }
        return view('admin.home');
    });
});


/*********************** Activity System **********************************************/


Route::get('/activity', 'ActivityController@index');

Route::get('/get-activity-changes/{id}', 'ActivityController@showActivityChanges');


/*********************** Final Winding **********************************************/

Route::get('/final-winding', 'FinalWindingController@index');

Route::post('/final-winding-report', 'ReportController@FinalWindingReport');
