<html style="width: 60%;">
<head>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"--}}
    {{--integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <style>

        body{
            width: 60% !important;
            margin-left: auto;
            margin-right: auto;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        .logo{
            height: 20px;
            margin-bottom: 10px;
            margin-top: 25px;
            display: block;

        }
    </style>

</head>
<body>
    <img src="https://i.ibb.co/xsYjQjB/logo.png" class="logo" alt=""/>
    <p>Dear {{\Illuminate\Support\Facades\Session::get('dept')}} Dept,</p>
    <p>   The Login OTP is :  <strong>{{\Illuminate\Support\Facades\Session::get('otp')}}</strong> </p>

    <p>Regards</p>

</body>
</html>
