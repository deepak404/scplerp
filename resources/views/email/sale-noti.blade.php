<html style="width: 60%;">
<head>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"--}}
          {{--integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <style>

        body{
            width: 60% !important;
            margin-left: auto;
            margin-right: auto;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
        .logo{
            height: 20px;
            float: right;
            /*margin-left: 40%;*/
            margin-bottom: 10px;
            margin-top: 25px;
            display: inline;

        }

        table{
            margin-top: 30px;
            border-collapse: collapse;
            width: 100%;
        }

        .table>thead{
            background-color: #f3f3f3;
            border-radius: 4px !important;
        }

        td,th{
            padding: 4px;
        }

        .table>thead>tr>th{
            padding-top: 10px;
            padding-bottom: 10px;
        }

        tbody>tr{
            border-bottom: 1px solid gainsboro;

        }

        table>tbody>tr>td{
            padding-top: 15px;
            padding-bottom: 15px;
        }

        table>tbody>tr>td:last-child{
            text-align: center;
        }
    </style>

</head>
<body>
    <img src="https://i.ibb.co/xsYjQjB/logo.png" class="logo" alt=""/>
    <h1 style="display: inline">Sale Orders</h1>

    <table class="table">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Customer Name</th>
                <th>Material</th>
                <th>Order Date</th>
                <th style="word-wrap: break-word;">Customer Expected Date</th>
            </tr>
        </thead>
        <tbody>
            <?php  $sno = 0; ?>
            @foreach($saleOrders as $sale)
<!--                --><?php // dd($sale); ?>

            <tr>
                <td>{{++$sno}}</td>
                <td>{{$sale->businessPartner->bp_name}}</td>
                <td>{{$sale->material_name}} <strong>({{$sale->itemMaster->material}})</strong></td>
                <td>{{date('d/m/Y', strtotime($sale->order_date))}}</td>
                <td style="text-align: center">{{date('d/m/Y', strtotime($sale->customer_date))}}</td>
            </tr>
            @endforeach
            {{--<tr>--}}
                {{--<td>1.</td>--}}
                {{--<td>JK Fenner</td>--}}
                {{--<td>1100/2X3 SOFT DIPPED POLY CORD</td>--}}
                {{--<td>10/12/2006</td>--}}
                {{--<td>10/12/2006</td>--}}
            {{--</tr>--}}
        </tbody>
    </table>
</body>
</html>