<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">


		<!-- <title>SCPL ERP</title> -->

  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/login.css" >
	</head>

<body class="index-body">
  <div class="container ">
    <div class="row v-align">
      <div class="row card card-width center-block">
      <img class="login-logo center-block" src="assets/logo.svg">
      <h3 class="text-center login-title">Login</h3>
       <form method="POST" id="login-form" action="{{ route('login') }}">
        @csrf
        <div class="inputfield-group">
          <h5 class="label">Username</h5>
          <input class="login-textbox center-block" id="email" type="email" name="email" value="{{ old('email') }}" required>
        </div>
        <div  class="inputfield-group">
          <h5 class="label">Password</h5>
          <input class="login-textbox center-block" id="password" type="password" name="password" required>
        </div>
           @if ($errors->any())
               @foreach ($errors->all() as $error)
                   {{--<div>{{$error}}</div>--}}
                   <span class="login-cases login-title failed">{{ $error}}</span>

               @endforeach
           @endif
        <button class="center-block login-button" type="submit">Login</button>
     </form>
    </div>
  </div>
  </div>


  <div id="otpModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-body otp-modal-body">

                  <h4 class="text-center">We have sent you an access code</h4>
                  <h4 class="text-center"> via Phone</h4>

                  <form name="otp-ver" action="/handle-otp" method="POST" id="otp-ver" class="form-inline">
                      {{csrf_field()}}
                      <input type="hidden" name="email" id="otp-mail">
                      <input type="hidden" name="password" id="otp-pass">

                      <div class="form-group otp-form-group">
                          <div class="row otp-row">
                              <div class="col-lg-12 col-md-12 col-sm-12" id="otp-container">
                                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                                      <input id="start" type="text" name="otp[]" class="otp-field input-field" minlength="1" maxlength="1" required="">
                                  </div>
                                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                                      <input type="text" name="otp[]"  class="otp-field input-field" minlength="1" maxlength="1" required="">
                                  </div>
                                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                                      <input type="text" name="otp[]"  class="otp-field input-field" minlength="1" maxlength="1" required="">
                                  </div>
                                  <div class="col-xs-3 col-md-3 col-lg-3 col-sm-3 padding-lr-zero">
                                      <input type="text" name="otp[]"  class="otp-field input-field" minlength="1" maxlength="1" required="">
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div id="otp_error_msg"><span><span></span></span></div>
                      <div id="otp_success_msg"><span><span></span></span></div>

                      <input type="submit" name="verify" value="VERIFY" id="verify-btn" class="btn btn-primary center-block">
                  </form>
              </div>
          </div>
      </div>


  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script>
      $(document).ready(function(){
					$('.otp-field').attr('autocomplete','off');
            $('#login-form').on('submit', function (e) {
                // if($('input[name="email"]').val() == "sales@indtextile.com"){
                    e.preventDefault();
										var password = encodeURIComponent($('#password').val())
                    var formData = 'email='+$('#email').val()+'&password='+password;

                    $.ajax({
                        type: "POST",
                        url: "/send-otp",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data : formData,
                        success: function(data, status, xhr) {
                            if (data.msg) {

                                $('#otp-mail').val($('#email').val());
                                $('#otp-pass').val($('#password').val());

                                $('#otpModal').modal('show');
                            } else{
                                alert('Kindly Refresh the page and try again');
                            }

                        },
                        error: function(data, status, error) {
                            $('<p class = "text-danger error-text center-block""><strong>'+data.responseJSON.error+'</strong></p>').insertAfter($('#password'));
                        },
                    })

                // }else{
                //     $(this).submit();
                // }
            })

						$('#otpModal').on('shown.bs.modal', function () {
						    $('#start').focus();
						})

          $('.otp-field').keyup(function(e) {
              if ($(this).val() == '') {
                  var inputs = $(this).closest('form').find('.otp-field');
                  inputs.eq(inputs.index(this) + 0).focus()
              } else {
                  if (e.keyCode == 8) {
                      $(this).focus()
                  } else {
                      var inputs = $(this).closest('form').find('.otp-field');
                      inputs.eq(inputs.index(this) + 1).focus()
                  }
              }
          });


            $('input').on('keydown', function(){
                $('.error-text').remove();
            })
      });
  </script>
</body>
</html>
