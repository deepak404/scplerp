@extends('layouts.admin')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/sales-index.css') }}">
    <link rel="stylesheet" href="{{url('responsive-css/sales-order.css')}}">
@endsection

@section('content')


@endsection

@section('script')
    <script src="{{ url('js/sales-index.js?v=0.1') }}"></script>

@endsection
