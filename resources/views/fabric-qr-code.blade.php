@extends('layouts.sales')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .inline-div{
            display: inline-block;
        }

        form{
            width: 30%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            box-shadow: 0px 0px 5px gainsboro;
            padding: 20px;
            padding-left: 40px;
        }

        .clearance-list{
            height: 100px;
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            padding-left: 5%;
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10%;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 90px;
        }

        #createFilamentModal .modal-dialog{
            width: 90% !important;
        }

        .filament-row{
            margin: 20px auto;
        }

        #note{
            width: 90%;
            height: 30px;
        }
        select{
            height: 27px;
            width: 100%;
        }
    </style>
    @endsection

    @section('content')
<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="/generate-fabric-qr-code" method="POST" target="_blank" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="fabric-csv">CSV File</label>
                        <input type="file" id="fabric-csv" name="fabric-csv" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Generate QR">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{url('/js/filament-clearance.js')}}"></script>
@endsection
