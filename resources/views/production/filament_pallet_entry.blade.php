@extends('layouts.production')

@section('css')
    <link rel="stylesheet" href="{{url('responsive-css/pallet-entry.css')}}">

<style>
    .inline-div{
        display: inline-block;
    }

    .clearance-list{
        height: 100px;
        padding: 20px 15px;
        box-shadow: 0px 0px 5px gainsboro;
        padding-left: 5%;
        margin: 30px auto 15px;
    }

    .lot-no{
        width: 10%;
    }

    .type{
        width: 10%;
    }

    .weight, .date, .invoice-date, .status{
        width: 10%;
    }

    .denier{
        width: 10%;
    }

    .download,.edit,.delete{
        width: 10%;
        margin: 0px 10px;
    }

    .action{
        vertical-align: middle;
        margin-top: -30px;
    }

    .list-header{
        font-weight: bold;
    }

    .download,.edit{
        margin: 0px 10px;
        padding: 10px;
        background-color: #DDE8FE;
        color: #5D5F61;
        font-weight: bold;
    }

    .pallet-entry{
        padding : 15px;
        border-bottom: 1px solid #d6d6d6;
    }
    #pallet-entry{
        margin-top: 80px;
    }

    .pallet-card{
        background-color: white;
        box-shadow: 0px 0px 5px gainsboro;
    }

    .card-header>span, .pallet-entry>span{
        display: inline-block;
    }

    .card-header>span:nth-child(1), .pallet-entry>span:nth-child(1){
        width: 5%;
    }

    .card-header>span:nth-child(2), .pallet-entry>span:nth-child(2){
        width: 50%;
        text-align: center;
    }

    .card-header>span:nth-child(3), .pallet-entry>span:nth-child(3){
        width: 40%;
        text-align: center;
    }

    .card-header{
        padding: 15px;
        border-bottom: 1px solid #d6d6d6;
        margin-bottom: 0px;
    }

    input[type="submit"]{
        margin: 20px auto;
    }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            background-color: #ffffff;
        }
    .btn-spacing {
    margin-bottom: 70px;
    padding: 0px 50px !important;
}

</style>
@endsection
@section('content')
<section id="pallet-entry">
    <div class="container-fluid">
        <div class="row">

            <?php

                $count = 1;
                if (is_null($pallets)) {
                    $palletCount = 0;
                }else{
                    $palletCount = count($pallets->pallet_no);
                }
            ?>

            <form action="/update-pallet-entry" method="POST" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                @csrf
                <input type="hidden" name="id" value="{{$id}}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                        <div class="pallet-card">
                            <p class="card-header">
                                <span>S.no</span><span>Pallet No</span><span>Gross Weight</span>
                            </p>
                            <?php for ($i=0; $i < 40; $i++) { ?>
                                <div class="pallet-entry">
                                    <span>{{$count}}.</span>
                                    <?php if ($palletCount >= $count): ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]" value="{{$pallets->pallet_no[($count - 1)]}}"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]" value="{{$pallets->weight[($count - 1)]}}"></span>
                                        <?php else: ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]"></span>
                                    <?php endif ?>
                                </div>

                            <?php $count += 1; } ?>

                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="pallet-card">
                            <p class="card-header">
                                <span>S.no</span><span>Pallet No</span><span>Gross Weight</span>
                            </p>
                            <?php for ($i=0; $i < 40; $i++) { ?>
                                <div class="pallet-entry">
                                    <span>{{$count}}.</span>
                                    <?php if ($palletCount >= $count): ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]" value="{{$pallets->pallet_no[($count - 1)]}}"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]" value="{{$pallets->weight[($count - 1)]}}"></span>
                                        <?php else: ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]"></span>
                                    <?php endif ?>
                                </div>

                            <?php $count += 1; } ?>

                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 ">
                        <div class="pallet-card">
                            <p class="card-header">
                                <span>S.no</span><span>Pallet No</span><span>Gross Weight</span>
                            </p>
                            <?php for ($i=0; $i < 40; $i++) { ?>
                                <div class="pallet-entry">
                                    <span>{{$count}}.</span>
                                    <?php if ($palletCount >= $count): ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]" value="{{$pallets->pallet_no[($count - 1)]}}"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]" value="{{$pallets->weight[($count - 1)]}}"></span>
                                        <?php else: ?>
                                        <span>
                                        <input type="text" class="pallet-no" name="pallet-no[]"></span>
                                        <span><input type="text" class="pallet-weight" name="pallet-weight[]"></span>
                                    <?php endif ?>
                                </div>

                            <?php $count += 1; } ?>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" class="btn btn-primary center-block btn-spacing " value="Save">

                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('script')
<!-- script -->
<script type="text/javascript">
  $(document).ready(function() {
    var pallet = '';
    $('.pallet-no').on('input',function() {
      pallet = $(this).val();
    });
    $('.pallet-weight').on('focus',function() {
      var selected = $(this).parent().parent().find('.pallet-no');
      if (pallet != '' && selected.val() == '') {
        selected.val(pallet);
      }
    })
  });
</script>
@endsection
