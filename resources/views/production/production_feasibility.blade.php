@extends('layouts.pp')

@section('css')

<link rel="stylesheet" href="{{url('css/pf-index.css?v=1.0')}}">

@endsection

@section('content')

<section id="pf-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 ">
                <span class="name-title">Production feasibility</span>
                <div class="pf-options">
                    <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date">
                    <input type="button" class="btn btn-primary" name="buffer" id="buffer" value="Add Buffer">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="pf-table">
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-12" style="max-width: 100vw; overflow-x: scroll;">


              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif

              @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
              @endif

              <div class="table table-bordered">
                  <div class="tr header-row">
                      <div class="td">S.No</div>
                      <div class="td">Customer <br> Name</div>
                      <div class="td">Customer <br> Date</div>
                      <div class="td">Quality</div>
                      <div class="td">Order <br>(Kgs)</div>
                      <div class="td">Pre Order <br>(Kgs)</div>
                      <div class="td">Total Order <br>(Kgs)</div>
                      <div class="td">In Stock</div>
                      <div class="td">Machine</div>
                      <div class="td">Existing Filament</div>
                      <div class="td">Change Over filament</div>
                      <div class="td">Consumption<br>(Kgs)</div>
                      <div class="td">Cord Weight</div>
                      <div class="td">No.of spls.</div>
                      <div class="td">Speed in MPM</div>
                      <div class="td">Production Hours</div>
                      <div class="td">Runnings Hours</div>
                      <div class="td">No of. <br>changes</div>
                      <div class="td">changes/hr</div>
                      <div class="td">Action</div>

                  </div>


                  <?php $count=1; ?>
                  @foreach($salesList as $sale)



                  <form  action = "/update-production-feasibility" method="POST" class="pf-form tr">
                          {{csrf_field()}}
                          <input type="hidden" value="{{$sale->productionFeasibility->id}}" name="id" class="id">
                          <div class="td">{{$count}}</div><?php $count; ?>
                          <div class="td">
                            <?php if ($sale->businessPartner->bp_name == 'buffer'): ?>
                              {{$sale->businessPartner->bp_name}}<i class="material-icons delete pull-right" data-id="{{$sale->id}}">delete</i>
                              <?php else: ?>
                                {{$sale->businessPartner->bp_name}}
                            <?php endif; ?>
                          </div>
                          <div class="td">{{date('d-m-Y', strtotime($sale->customer_date))}}</div>
                          <div class="td">{{$sale->itemMaster->material}}</div>
                          <div class="td">{{$sale->order_quantity}}</div>
                          <div class="td">{{$sale->last_month_pending}}</div>
                          <div class="td">{{$sale->total_order}}</div>
                          <div class="td">
                            <?php if (!is_null($sale->productionFeasibility->in_stock)): ?>
                              <input type="checkbox" name="in_stock" value="1" checked>
                              <?php else: ?>
                                <input type="checkbox" name="in_stock" value="1">
                            <?php endif; ?>
                          </div>
                          <div class="td">
                              <select name="machine" id="" value="{{$sale->productionFeasibility->machine}}" data-order="{{$sale->total_order}}" required>
                                <?php
                                    $zellA = 'disabled';
                                    $zellB = 'disabled';
                                    $kidde = 'disabled';
                                    $zellAData = '';
                                    $zellBData = '';
                                    $kiddeData = '';
                                    if ($sale->itemMaster->zell_a_spls_no > 0) {
                                      $zellA = 'enable';
                                      $zellAData = $sale->itemMaster->zell_a_spls_no.'|'.$sale->itemMaster->zell_a_speed;
                                    }
                                    if ($sale->itemMaster->zell_b_spls_no > 0) {
                                      $zellB = 'enable';
                                      $zellBData = $sale->itemMaster->zell_b_spls_no.'|'.$sale->itemMaster->zell_b_speed;
                                    }
                                    if ($sale->itemMaster->kidde_spls_no > 0) {
                                      $kidde = 'enable';
                                      $kiddeData = $sale->itemMaster->kidde_spls_no.'|'.$sale->itemMaster->kidde_speed;
                                    }
                                ?>
                                  @if($sale->productionFeasibility->machine == 1)
                                      <option value="1" data-item="{{$zellAData}}" selected>ZELL A</option>
                                      <option value="2" data-item="{{$zellBData}}" {{$zellB}}>ZELL B</option>
                                      <option value="3" data-item="{{$kiddeData}}" {{$kidde}}>KIDDIE</option>

                                  @elseif($sale->productionFeasibility->machine == 2)
                                      <option value="1" data-item="{{$zellAData}}" {{$zellA}}>ZELL A</option>
                                      <option value="2" data-item="{{$zellBData}}" selected>ZELL B</option>
                                      <option value="3" data-item="{{$kiddeData}}" {{$kidde}}>KIDDIE</option>
                                  @elseif($sale->productionFeasibility->machine == 3)

                                  <option value="1" data-item="{{$zellAData}}" {{$zellA}}>ZELL A</option>
                                  <option value="2" data-item="{{$zellBData}}" {{$zellB}}>ZELL B</option>
                                  <option value="3" data-item="{{$kiddeData}}" selected>KIDDIE</option>
                                  @else
                                      <option value="0">NONE</option>
                                      <option value="1" data-item="{{$zellAData}}" {{$zellA}}>ZELL A</option>
                                      <option value="2" data-item="{{$zellBData}}" {{$zellB}}>ZELL B</option>
                                      <option value="3" data-item="{{$kiddeData}}" {{$kidde}}>KIDDIE</option>
                                  @endif
                              </select>

                              <?php

                                  if($sale->productionFeasibility->machine != null){
                                      $change_hour = $sale->productionFeasibility->machine()->first()->change_hr;
                                  }else{
                                      $change_hour = '';
                                  }
                              ?>

                          </div>
                          <div class="td"><input type="text" class="existing_filament" name="existing_filament" value="{{$sale->productionFeasibility->existing_filament}}" style="text-transform:uppercase" disabled autocomplete="off"></div>
                          <!-- <div class="td"><input type="text" class="changed_filament" name="changed_filament" value="{{$sale->productionFeasibility->changed_filament}}" style="text-transform:uppercase" required autocomplete="off"></div> -->
                          <div class="td"><input type="text" class="changed_filament" name="changed_filament" value="{{$sale->productionFeasibility->changed_filament}}" style="text-transform:uppercase" required autocomplete="off">
                            <select class="changed_filament_select" name="temp">
                              <option value="" disabled selected>Filament</option>
                              <?php foreach ($filaments as $filament): ?>
                                <option value="{{$filament}}">{{$filament}}</option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                          <div class="td"><input type="text" class="consumption" name="consumption" value="{{$sale->productionFeasibility->consumption}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="cord_weight" name="cord_weight" value="{{$sale->productionFeasibility->cord_weight}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="no_of_spls" name="no_of_spls" value="{{$sale->productionFeasibility->no_of_spls}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="speed_in_mpm" name="speed_in_mpm" value="{{$sale->productionFeasibility->speed_in_mpm}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="production_per_hr" name="production_per_hr" value="{{$sale->productionFeasibility->production_per_hr}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="running_hrs" name="running_hrs" value="{{$sale->productionFeasibility->running_hrs}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="no_of_changes" name="no_of_changes" value="{{$sale->productionFeasibility->no_of_changes}}" required autocomplete="off"></div>
                          <div class="td"><input type="text" class="changes_per_hr" name="changes_per_hr" value="{{$sale->productionFeasibility->changes_per_hr}}" required autocomplete="off"></div>
                          <div class="td"><input type="submit" class="btn btn-primary" value="Update"></div>
                      <!--</div>-->
                  </form>
                  <?php $count++; ?>
                  @endforeach

              </div>
          </div>
      </div>
  </div>
</section>

<section id="pop-ups">
  <div id="sales-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Buffer Order</h4>
              </div>
              <div class="modal-body">
                  <form action="#" id="add-buffer-form" name="add-sales-form">

                      <div class="form-group">
                          <label for="material">Material</label>
                          <select name="material" id="material">
                            <?php foreach ($itemMaster as $key => $value): ?>
                              <option value="{{$value->id}}">{{$value->material}}</option>
                            <?php endforeach; ?>
                          </select>
                      </div>

                      <div class="form-group">
                          <label for="weight">Weight (Kgs)</label>
                          <input type="text" class="text-input" id="weight" name="weight">
                      </div>

                      <div class="form-group">
                          <label for="order-date">Order Placement Date</label>
                          <input type="text" class="text-input" id="order-date" name="order-date">
                      </div>

                      <div class="form-group">
                          <label for="customer-date">Customer Date</label>
                          <input type="text" class="text-input" id="customer-date" name="customer-date">
                      </div>


                      <div class="form-group">
                          <input type="submit" class="btn btn-primary" id="submit-sale-form" value="Create Order">
                      </div>

                  </form>
              </div>
          </div>

      </div>
  </div>
</section>

@endsection

@section('script')

<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
  	    headers:
  	    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
  	});
    $('input').attr('autocomplete','off');

    $(document).on('input','.changed_filament' ,function() {
      $(this).val($(this).val().replace(/[^]/gi, ''));
    });

    $('.changed_filament_select').on('change',function() {
      var val = $(this).val();
      $(this).parent().find('.changed_filament').val(function () {
        if (this.value == '') {
          return val;
        }
        return this.value +','+ val;
      });
      $(this).val('');
    });

  $('#filter-date').datepicker({
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: 'mm-yy',
      onClose: function(dateText, inst) {
          $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
      }
  });

  $(document).on('input','#weight' ,function() {
    $(this).val($(this).val().replace(/[^0-9]/gi, ''));
  });

  $('#order-date, #customer-date').datepicker({
      dateFormat : 'dd-mm-yy',
  });

  $('#filter-date').focus(function(){
      $(".ui-datepicker-calendar").hide();
  });

      $(document).on('click','.ui-datepicker-close',function(){
      window.location.href = '/production-feasibility/'+$('#filter-date').val();
  });

      const changeHours = ['',{{$zellAChangeHour}},{{$zellBChangeHour}},{{$kiddeChangeHour}}];


      $('.no_of_changes').on('keyup', function(){
         updateChangeHours($(this));
      });


      $('.pf-form').on('submit', function(){
          // console.log($(this).find('select').val());

          if($(this).find('select').val() == 0){
              $(this).find('select').parent().append('<span class="text-danger" style="display: inline-block; margin-top: 5px;">Select Machine</span>');
              $(this).find('select').focus();
              return false;
          }else{

              return true;
              // $(this).submit();
          }
      })

      $('select[name="machine"]').on('change', function(){
          if ($(this).find(':selected').data('item') !== undefined) {
            var inpuData = $(this).find(':selected').data('item').split('|');
            var cordWeight = $(this).closest('form').find("input[name='cord_weight']").val();
            var prodHr = ((cordWeight*inpuData[0]*inpuData[1]*60)/1000).toFixed(2);
            var runningHrs = ($(this).data('order')/prodHr).toFixed(2);
            $(this).closest('form').find("input[name='no_of_spls']").val(inpuData[0]);
            $(this).closest('form').find("input[name='speed_in_mpm']").val(inpuData[1]);
            $(this).closest('form').find("input[name='production_per_hr']").val(prodHr);
            $(this).closest('form').find("input[name='running_hrs']").val(runningHrs);
          }else{
            $(this).closest('form').find("input[name='no_of_spls']").val('');
            $(this).closest('form').find("input[name='speed_in_mpm']").val('');
            $(this).closest('form').find("input[name='production_per_hr']").val('');
            $(this).closest('form').find("input[name='running_hrs']").val('');
          }
          var root = $(this).closest('form').find('.no_of_changes');

          updateChangeHours(root)
      });


      function updateChangeHours(root){
          if(root.val() != ''){
              var form = root.closest('form');
              var currentMachine = form.find("select[name='machine']").val();
              form.find('.changes_per_hr').val(root.val() * changeHours[currentMachine]);
          }
      }

      $('#buffer').on('click',function(){
        $('#sales-modal').modal('show');
      });
      $('#add-buffer-form').on('submit',function(e){
          e.preventDefault();
          var formData = $(this).serialize();
          $.ajax({
              type: "POST",
              url: "/add-buffer-order",
              data: formData,
              success: function(data, status, xhr) {
                  if(data.status){
                      location.reload();
                  }else{
                    alert(data.msg);
                  }
              },
              error: function(xhr, status, error) {

        			},
          });
      });

      $('.delete').on('click',function(){
        window.location.href = "/delete-buffer/"+$(this).data('id');
      });
  });
</script>

@endsection
