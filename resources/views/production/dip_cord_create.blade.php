@extends('layouts.production')
@section('css')
<style media="screen">
  #dip-cord-clearance{
    margin-top: 75px;
  }
  /* #dip-cord-clearance-div{
    overflow: auto;
    max-height: 80vh;
  } */
  .text-input{
    width: 100% !important;
  }
  form{
    margin-top: 20px;
    padding-bottom: 40px;
  }
  h4{
    color: #003ebb;
  }
  #add-spl{
    margin: 0px 10px;
    padding: 10px;
    background-color: white;
    color: #003ebb;
    font-weight: bold;
    border-radius: 5px;
  }
  #add-spl:focus{
    outline: none;
  }
  .spl-div{
    margin-bottom: 10px;
  }
  h5{
    text-align: center;
  }
  .p-lr-0{
      padding-left: 0px !important;
      padding-right: 0px !important;
  }
</style>
@endsection
@section('content')
<?php
  // Group Solution for N number Tanks
  $sols = [];
  foreach ($solutions as $value) {
    $sols[$value['name']][] = $value;
  }

 ?>
<section id="dip-cord-clearance">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Create Production Clearance Memo</h3>
            </div>
            <div class="col-md-12" id="dip-cord-clearance-div">
              <form class="col-md-12 col-lg-12 col-sm-12" action="/dip-cord-clearance-create" method="post">
                @csrf
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4  class="col-md-12 col-lg-12 col-sm-12">Basic Inputs</h4>
                  <div class="col-md-12 col-lg-12 col-sm-12 p-lr-0">
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="machine">Machine</label>
                      <select class="text-input" name="machine" required>
                        <option value="" selected disabled>Machines</option>
                        <option value="1">ZELL A</option>
                        <option value="2">ZELL B</option>
                        <option value="3">KIDDE</option>
                      </select>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="material">Material</label>
                      <select class="text-input" name="material" required>
                        <option value="" selected disabled>Select material</option>
                        <?php foreach ($itemMaster as $value): ?>
                          <option value="{{$value->material_id}}">{{$value->material}}</option>
                        <?php endforeach; ?>
                      </select>
                      <span id="floor_code">Floor Code</span>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 from-group">
                      <label for="filament">Filament Type</label>
                      <select class="text-input" name="filament" required>
                        <option value="" selected disabled>Select filament</option>
                        <?php foreach ($filaments as $filament): ?>
                          <option value="{{$filament->name}}">{{$filament->name}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="lot_no">Lot No.</label>
                      <input type="text" class="text-input number" id="lot_no" name="lot_no" value="" required>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-12 col-sm-12 p-lr-0">
                    <div class="col-md-3 col-lg-3 col-sm-6 from-group">
                      <label for="customer">Customer</label>
                      <input type="text" class="text-input cap-lock" name="customer" value="" required>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="tpm">TPM</label>
                      <input type="text" class="text-input" name="tpm" value="" required>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="speed">Speed (in MPM)</label>
                      <input type="text" class="text-input number" name="speed" value="" required>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="no_cords">No of Cords</label>
                      <input type="text" class="text-input number" name="no_cords" value="" required>
                    </div>
                  </div>
                  <div class="col-md-12 col-lg-12 col-sm-12 p-lr-0">
                    <div class="col-md-3 col-lg-3 col-sm-6 form-group">
                      <label for="scrapper">Scrapper/Squeeze</label>
                      <input type="text" class="text-input" name="scrapper" value="">
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 from-group">
                      <label for="nip_roll">Nip roll pressure</label>
                      <input type="text" class="text-input" name="nip_roll" value="">
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6 from-group">
                      <label for="fan_usage">Fan Usage</label>
                      <input type="text" class="text-input" name="fan_usage" value="">
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6">
                      <label for="created_by">Created By</label>
                      <input type="text" class="text-input cap-lock" name="created_by" value="" required>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Temp˚C</h4>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="temp">zone 1</label>
                      <input type="text" class="text-input number" name="temp[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="temp">zone 2</label>
                      <input type="text" class="text-input number" name="temp[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="temp">zone 3</label>
                      <input type="text" class="text-input number" name="temp[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="temp">zone 4</label>
                      <input type="text" class="text-input number" name="temp[]" value="">
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Stretch/Tenction:</h4>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="s_t">L1</label>
                      <input type="text" class="text-input double" name="s_t[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="s_t">L2</label>
                      <input type="text" class="text-input double" name="s_t[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="s_t">L3</label>
                      <input type="text" class="text-input double" name="s_t[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="s_t">L4</label>
                      <input type="text" class="text-input double" name="s_t[]" value="">
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Damper Opening:</h4>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="damper">L1</label>
                      <input type="text" class="text-input number" name="damper[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="damper">L2</label>
                      <input type="text" class="text-input number" name="damper[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="damper">L3</label>
                      <input type="text" class="text-input number" name="damper[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="damper">L4</label>
                      <input type="text" class="text-input number" name="damper[]" value="">
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Dewebber suction:</h4>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="dewebber">L1</label>
                      <input type="text" class="text-input double" name="dewebber[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="dewebber">L2</label>
                      <input type="text" class="text-input double" name="dewebber[]" value="">
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                      <label for="dewebber">L3</label>
                      <input type="text" class="text-input double" name="dewebber[]" value="">
                    </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">No. of Pass:</h4>
                  <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                    <label for="pass">zone 1</label>
                    <input type="text" class="text-input number" name="pass[]" value="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                    <label for="pass">zone 2</label>
                    <input type="text" class="text-input number" name="pass[]" value="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                    <label for="pass">zone 3</label>
                    <input type="text" class="text-input number" name="pass[]" value="">
                  </div>
                  <div class="col-md-3 col-sm-6 col-lg-3 from-group">
                    <label for="pass">zone 4</label>
                    <input type="text" class="text-input number" name="pass[]" value="">
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Solution/Batch:</h4>
                  <div class="col-md-3 col-lg-3 col-sm-3">
                    <h5><strong>Tank 1</strong></h5>
                    <div class="form-group">
                      <label for="solution">Solution 1</label>
                      <select class="text-input" name="solution[]">
                        <option value="" selected disabled>solution</option>
                        <?php foreach ($sols as $key=>$solution): ?>
                          <option value="{{$key}}">{{$key}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="batch">Batch 1</label>
                      <select class="text-input" name="batch[]">

                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-3">
                    <h5><strong>Tank 2</strong></h5>
                    <div class="form-group">
                      <label for="solution">Solution 2</label>
                      <select class="text-input" name="solution[]">
                        <option value="" selected disabled>solution</option>
                        <?php foreach ($sols as $key=>$solution): ?>
                          <option value="{{$key}}">{{$key}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="batch">Batch 2</label>
                      <select class="text-input" name="batch[]">

                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-3">
                    <h5><strong>Tank 3</strong></h5>
                    <div class="form-group">
                      <label for="solution">Solution 3</label>
                      <select class="text-input" name="solution[]">
                        <option value="" selected disabled>solution</option>
                        <?php foreach ($sols as $key=>$solution): ?>
                          <option value="{{$key}}">{{$key}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="batch">Batch 3</label>
                      <select class="text-input" name="batch[]">

                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-3">
                    <h5><strong>Tank 4</strong></h5>
                    <div class="form-group">
                      <label for="solution">Solution 4</label>
                      <select class="text-input" name="solution[]">
                        <option value="" selected disabled>solution</option>
                        <?php foreach ($sols as $key=>$solution): ?>
                          <option value="{{$key}}">{{$key}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="batch">Batch 4</label>
                      <select class="text-input" name="batch[]">

                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Sample:</h4>
                  <div class="col-md-4 col-lg-4 col-sm-4 form-group">
                    <label for="date">Date/Time</label>
                    <input type="text" class="text-input date-time" name="date" value="" required>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-4 form-group">
                    <label for="process">Sample From</label>
                    <input type="text" class="text-input" name="process" value="IL" readonly>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-4 form-group">
                    <label for="doff_no">Doff No.</label>
                    <input type="text" class="text-input cap-lock no-space" name="doff_no" value="" required>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <h4 class="col-md-12 col-lg-12 col-sm-12">Spindel No.</h4>
                  <div class="col-lg-12 col-md-12 col-sm-12 p-lr-0 spl-div">

                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 p-lr-0">
                    <button type="button" name="button" id="add-spl">ADD SPL</button>
                  </div>
                </div>
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <input type="submit" class="btn btn-primary center-block" name="save" value="Save Lot">
                </div>
              </form>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('input','.cap-lock',function() {
      $(this).val($(this).val().toUpperCase());
    });

    $(document).on('input','.number' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });

    $(document).on('input','.double' ,function() {
      $(this).val($(this).val().replace(/[^0-9.]/gi, ''));
    });

    $(document).on('input','.no-space',function() {
      $(this).val($(this).val().replace(/\s+/g, ''));
    });

    $('.date-time').datetimepicker({
      format:'d-m-Y H:i',
    });

    $('input[type="text"]').attr('autocomplete', 'off');


    var itemMaster = {!! json_encode($itemMaster->toArray()) !!};
    var solutions = {!! json_encode($solutions) !!}
    var count = 1;

    $('#add-spl').on('click',function() {
      if (count <= 5) {
        $('.spl-div').append('<div class="col-md-3 col-sm-6 col-lg-3 from-group">'+
        '<label for="pass">sample '+count+'</label>'+
        '<input type="text" class="text-input cap-lock" name="spl[]" value="" required>'+
        '</div>');
      }
      count += 1;
    });

    $('select[name="material"]').on('change',function(){
      var materialId = $(this).val();
      $.each(itemMaster, function(index, val) {
          if (val.material_id == materialId) {
            $('#floor_code').text(val.floor_code);
          }
        });
    });

    $('select[name="solution[]"]').on('change',function() {
      var selectedTank = $(this).parent().parent().find('select[name="batch[]"]');
      var sol = $(this).val();
      $(selectedTank).empty();
      $.each(solutions, function(key, value) {
        if (sol == value.name) {
          $(selectedTank).append('<option value="'+value.batch_no+'">'+value.batch_no+'</option>');
        }
      });

    });

  });

</script>
@endsection
