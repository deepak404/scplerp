@extends('layouts.pp')

@section('css')
<style>
  thead>tr>th{
    text-align: center;
  }
  .table{
    margin: 20px 0px;
  }
  td{
    vertical-align: middle !important;
  }
  .page-spacing{
    overflow: auto;
    max-height: 80vh;
    max-width: 100vw;
  }
  .material-icons{
    cursor: pointer;
  }
  .material-icons:hover{
    color: #003fbb !important;
  }
  .center{
    text-align: center;
  }
  h3,.bp-options{
    display: inline-block;
  }
  .bp-options{
    float: right;
    margin-top: 10px;
  }
  .modal-dialog{
    width: 750px !important;
  }
  .feedback{
    color: #ff3737e0;
    font-weight: 600;
  }
  .p-lr-0{
    padding: 0px !important;
  }
</style>
@endsection

@section('content')

<section id="pp">
  <div class="container-fluid">
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
              <h3>Items Master</h3>
              <div class="bp-options">
                  <button type="button" class="btn btn-primary" id="create-item">Add Item</button>
              </div>
          </div>
      </div>
  </div>
    <div class="container-fluid">
        <div class="row">
          <!-- <h3>Business Partner Items</h3> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-spacing">
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>S No.</th>
                      <th>Material</th>
                      <th>Description</th>
                      <th>Cord Weight</th>
                      <th>Zell A <br>Spindel No.</th>
                      <th>Zell A <br>Speed</th>
                      <th>Zell B <br>Spindel No.</th>
                      <th>Zell B <br>Speed</th>
                      <th>Kidde<br>Spindel No.</th>
                      <th>Kidde<br>Speed</th>
                      <th>Package Type</th>
                      <th>HSN/SAC</th>
                      <th>SGST</th>
                      <th>CGST</th>
                      <th>IGST</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $count=1; foreach ($itemMaster as $key => $value): ?>
                      <tr>
                        <td>{{$count}}</td>
                        <td>{{$value->material}}</td>
                        <td>{{$value->descriptive_name}}</td>
                        <td>{{$value->cord_wt}}</td>
                        <td>{{$value->zell_a_spls_no}}</td>
                        <td>{{$value->zell_a_speed}}</td>
                        <td>{{$value->zell_b_spls_no}}</td>
                        <td>{{$value->zell_b_speed}}</td>
                        <td>{{$value->kidde_spls_no}}</td>
                        <td>{{$value->kidde_speed}}</td>
                        <td>{{$value->package}}</td>
                        <td>{{$value->hsn_sac}}</td>
                        <td>{{$value->sgst}}</td>
                        <td>{{$value->cgst}}</td>
                        <td>{{$value->igst}}</td>
                      </tr>
                    <?php $count++; endforeach; ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<section id="pop-ups">
  <div id="bpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Business Partner</h4>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12">
          <form class="col-md-12 col-lg-12 col-sm-12" id="bp-form" action="#" method="post">
            <input type="hidden" name="id" value="">

          </form>
        </div>
      </div>

    </div>
  </div>
</section>

<div id="itemModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content col-lg-12 col-md-12 col-sm-12">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Item</h4>
      </div>
      <div class="modal-body col-lg-12 col-md-12 col-sm-12">
        <!-- <p>Some text in the modal.</p> -->
        <form class="col-lg-12 col-md-12 col-sm-12" id="item-form" action="#" method="post">
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="material">Material:</label>
              <input class="form-control" type="text" name="material" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="descriptive_name">Description:</label>
              <input class="form-control" type="text" name="descriptive_name" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="zell_a_spls_no">Zell A Spindel:</label>
              <input class="form-control numeric" type="text" name="zell_a_spls_no" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="zell_a_speed">Zell A Speed:</label>
              <input class="form-control numeric" type="text" name="zell_a_speed" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="zell_b_spls_no">Zell B Spindel:</label>
              <input class="form-control numeric" type="text" name="zell_b_spls_no" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="zell_b_speed">Zell B Speed:</label>
              <input class="form-control numeric" type="text" name="zell_b_speed" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="kidde_spls_no">Kidde Spindel:</label>
              <input class="form-control numeric" type="text" name="kidde_spls_no" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="kidde_speed">Kidde Speed:</label>
              <input class="form-control numeric" type="text" name="kidde_speed" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="cord_wt">Cord Weight:</label>
              <input class="form-control numeric" type="text" name="cord_wt" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="package">Package:</label>
              <input class="form-control" type="text" name="package" value="">
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="hsn_sac">HSN/SAC:</label>
              <input class="form-control upper-case" type="text" name="hsn_sac" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="sgst">SGST:</label>
              <input class="form-control numeric" type="text" name="sgst" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="clo-md-12 col-lg-12 col-sm-12 p-lr-0">
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="cgst">CGST:</label>
              <input class="form-control numeric" type="text" name="cgst" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-6 col-lg-6 col-sm-12">
              <label for="igst">IGST:</label>
              <input class="form-control numeric" type="text" name="igst" value="" required>
              <span class="feedback"></span>
            </div>
          </div>
          <div class="form-group col-md-12 col-lg-12 col-sm-12">
              <input type="submit" class="btn btn-primary center-block" value="Save">
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('input','.numeric' ,function() {
      $(this).val($(this).val().replace(/[^0-9.]/gi, ''));
    });
    $(document).on('input','input[name="material"]' ,function() {
      $(this).val($(this).val().replace(/[^0-9A-Z. ]/gi, ''));
    });
    $('#create-item').on('click',function() {
      $('#itemModal').modal('show');
    });
    $(document).on('input','.upper-case',function(){
        $(this).val($(this).val().toUpperCase());
    });
    $('#item-form').on('submit',function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
  				type: "POST",
  				url: "/create-item",
  				data: formData,
  				success: function(data) {
  					if (data.status) {
  						location.reload();
  					}else{
  						alert(data.msg);
  					}
  				},
  				error: function(xhr) {
  						if (xhr.status == 422) {
  							errorHandler(xhr.responseJSON.errors);
  						}
  				},
  			});
    });
    function errorHandler(errors) {
      $.each(errors, function(key,value){
      var selected = 'input[name="'+key+'"]';
      $(selected).next(".feedback").text(value[0]);
      });
    }
    $('.form-control').on('keyup',function(){
      $(this).next(".feedback").text('');
    });
  });

</script>

@endsection
