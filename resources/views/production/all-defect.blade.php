@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.production' )

@section('css')
    <style>
        .machine-list-card{
            width: 350px;
            height: auto;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .card-holder{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 80vh;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 20px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }
    </style>
@endsection
@section('content')
    <section id="pf-head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 card-holder">
                    <div class="machine-list-card">

                       @if(Route::current()->uri == 'all-defect-date-range')
                            <h3 class="text-center">Defect Report</h3>
                            <form action="/generate-defect-report" method="POST" target="_blank">
                            {{csrf_field()}}
                            <div class="form-group">
                                <span for="start-date">Start Date : </span>
                                <input type="text" id="start-date" class="text-input" name="start-date" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                                <span for="end-date">End Date : </span>
                                <input type="text" id="end-date" class="text-input" name="end-date" autocomplete="off" required>
                            </div>
                            
                            <div class="form-group">
                                <span for="defect">Defect</span>
                                <select name="defect" id="defect">
                                    <option value="all">All Defect</option>
                                    @foreach(\App\Defects::all() as $defect)
                                        <option value="{{$defect->reason}}">{{$defect->reason}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <span for="machine">Machine</span>
                                <select name="machine" id="machine">
                                    <option value="Zell A">ZELL A</option>
                                    <option value="Zell B">ZELL B</option>
                                    <option value="Kidde">KIDDE</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary center-block" value="Show Details">
                            </div>
                        </form>

                           @elseif(Route::current()->uri == "doff-wise-defect")
                            <h3 class="text-center">Defect Report</h3>
                            <form action="/generate-doffwise-defect" method="POST" target="_blank">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <span for="start-date">Start Date : </span>
                                    <input type="text" id="start-date" class="text-input" name="start-date" autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                    <span for="end-date">End Date : </span>
                                    <input type="text" id="end-date" class="text-input" name="end-date" autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                    <span for="machine">Machine</span>
                                    <select name="machine" id="machine">
                                        <option value="Zell A">ZELL A</option>
                                        <option value="Zell B">ZELL B</option>
                                        <option value="Kidde">KIDDE</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <span for="defect">Material</span>
                                    <select name="material" id="material">
                                        @foreach(\App\ItemMaster::all() as $item)
                                            <option value="{{$item->id}}">{{$item->descriptive_name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary center-block" value="Show Details">
                                </div>
                            </form>
                           @else
                            <h3 class="text-center">NCR Report</h3>
                            <form action="/generate-ncr-report" method="POST" target="_blank">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <span for="start-date">Start Date : </span>
                                    <input type="text" id="start-date" class="text-input" name="start-date" autocomplete="off" required>
                                </div>

                                <div class="form-group">
                                    <span for="end-date">End Date : </span>
                                    <input type="text" id="end-date" class="text-input" name="end-date" autocomplete="off" required>
                                </div>
                                <div class="form-group">
                                    <span for="machine">Machine</span>
                                    <select name="machine" id="machine">
                                        <option value="Zell A">ZELL A</option>
                                        <option value="Zell B">ZELL B</option>
                                        <option value="Kidde">KIDDE</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary center-block" value="Show Details">
                                </div>
                            </form>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            // $('#start-date, #end-date').datepicker({
            //     changeMonth: true,
            //     changeYear: true,
            //     showButtonPanel: true,
            //     dateFormat: 'dd-mm-yy',
            // });
            $('#start-date, #end-date').datetimepicker({
                format:'d-m-Y H:i',
            });
        });
    </script>
@endsection
