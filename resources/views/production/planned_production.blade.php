@extends('layouts.pp')

@section('css')

    <link rel="stylesheet" href="css/production-planning.css">
    <link rel="stylesheet" href="{{url('responsive-css/production-planning.css')}}">
    <style>
        .table{
            display: table;
            /*padding: 20px;*/
            margin-bottom: 10px;
        }

        .table-holder{
            box-shadow: 0px 0px 10px #dedede;
        }

        .bg-white{
            background-color: white;
        }

        .tr{
            display: table-row;
        }

        .td{
            display: table-cell;
            vertical-align: middle;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 20px;
            padding-right: 20px;
        }


        .bottom-shadow{
            box-shadow: 0 8px 5px -5px #dedede;
        }

        .border-bottom-table .tr .td{
            border-bottom: 1px solid gainsboro;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .border-bottom-table .tr:last-child .td{
            border-bottom: none;
        }

        #batch-count{
            width: 300px;
        }

        .batch-group .form-group{
            display: inline-block;
            margin-right: 2px;
        }

        .page-spacing{
                    padding-bottom: 15%;
        }


        .material-icons:hover{
            cursor: pointer;
        }

    </style>
@endsection

@section('content')

<section id="pp">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-spacing">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php $machineArray = ['','ZELL A', 'ZELL B', 'KIDDE']; ?>
                    <h3 class="title">{{$machineArray[$machine]}} - Planned Production</h3><span style="margin-left: 15px; font-size: 18px; display: inline-block;">({{$month}})</span>
                </div>


                <div class="table table-header">
                    <div class="tr">
                        <div class="td" style="width: 5%;">Sl.No</div>
                        <div class="td" style="width: 30%;">Quality</div>
                        <div class="td" style="width: 25%;">Order (Kgs)</div>
                        <div class="td" style="width: 15%;">Filament</div>
                    </div>
                </div>



                <?php $c = 0; ?>
                @foreach($finalArray as $material => $value)
                  @foreach($value as $filament => $batchs)
                      <div class="table-holder bg-white">

                        <div class="table">
                          <div class="tr bottom-shadow">
                            <div class="td" style="width: 5%;">{{++$c}}</div>
                            <div class="td" style="width: 30%;">{{$material}}</div>
                            <div class="td" style="width: 25%;">{{$batchs['sum']}}</div>
                            <div class="td" style="width: 15%;">{{$filament}}</div>
                          </div>
                        </div>

                        <div class="table border-bottom-table">
                          <div class="tr">

                            <div class="td">Start Date</div>
                            <div class="td">End Date</div>
                            <div class="td">Batch</div>
                            <div class="td">Order (kgs)</div>
                            <div class="td">Edit</div>
                            <div class="td">Delete</div>
                          </div>
                          @foreach($batchs as $batch => $plan)

                          <?php if (!empty($plan['start_date'])): ?>
                            <div class="tr">

                              <div class="td">{{date('d-m-Y H:i',strtotime($plan['start_date']))}}</div>
                              <div class="td">{{date('d-m-Y H:i',strtotime($plan['end_date']))}}</div>
                              <div class="td">{{$batch}}</div>
                              <div class="td">{{$plan['consumption']}}</div>
                              <div class="td">
                                <i class="material-icons edit-batch" data-id="{{ implode(",",$plan['id']) }}" data-prodhr="{{$plan['prod_hr']}}">edit</i>
                              </div>
                              <div class="td">
                                <i class="material-icons delete-batch" id="{{implode(",",$batchs['feasibility_id'])}}" data-filament="{{$filament}}">delete</i>
                              </div>
                            </div>
                          <?php endif; ?>
                          @endforeach
                        </div>

                      </div>
                    @endforeach

                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<section id="pop-ups">

  <div id="batch-order-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Production Batch</h4>
              </div>
              <div class="modal-body">
                  <p>Batch Qty: <span id="b-qty"></span> </p>
                  <form action="#" id="add-batch-form" name="add-batch-form">
                      @csrf
                      <input type="hidden" value="" name="pp_id" id="pp_ids">
                      <div class="batch-master">
                        <div class="batch-group">

                          <h5 class="blue-text" id="batch-title">Batch 1</h5>

                          <div class="form-group">
                            <label for="batch_date">Start Date</label>
                            <input type="text" class="text-input batch_date" name="start_date" autocomplete="off" required>
                          </div>

                          <div class="form-group">
                            <label for="end_date">End Date</label>
                            <input type="text" class="text-input end_date" name="end_date" value="" autocomplete="off" required>
                          </div>
                        </div>
                      </div>


                      <div class="form-group btn-gr">
                          <input type="submit" class="btn btn-primary" id="submit-batch-form" value="Add Batch">
                      </div>

                  </form>
              </div>
          </div>

      </div>
  </div>
</section>

@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function(){
    var prdHours = 0;
    var qty = 0.0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.batch_date').datetimepicker({
        format:'d-m-Y H:i',
    });
    $(document).on('click','.edit-batch',function(){
      var batch_ids = $(this).data('id');
      var start_date = $(this).parent().parent().children()[0].lastChild.textContent;
      var end_date = $(this).parent().parent().children()[1].lastChild.textContent;
      var batch = $(this).parent().parent().children()[2].lastChild.textContent;
      qty = parseFloat($(this).parent().parent().children()[3].lastChild.textContent);
      prdHours = $(this).data('prodhr');
      $('#pp_ids').val(batch_ids);
      $('#batch-title').text(batch);
      $('#b-qty').text(qty);
      $('.batch_date').val(start_date);
      $('.end_date').val(end_date);
      $('#batch-order-modal').modal('show');
    });
    $(document).on('change','.batch_date',function(){
        var productionHr = (qty/prdHours).toFixed(2);
        // console.log(productionHr);
        
        productionHr = productionHr.split(".");
        var dateTime = $(this).val().split(" ");
        var myDate = dateTime[0].split("-");
        var myTime = dateTime[1].split(":");
        var formatedDate = new Date(myDate[2], (myDate[1]-1), myDate[0], myTime[0], myTime[1], 0);
        formatedDate.setHours(formatedDate.getHours() + parseInt(productionHr[0]));
        formatedDate.setMinutes(formatedDate.getMinutes() + parseInt(productionHr[1]));
        var endDate = formatDate(formatedDate);
        $(this).parent().parent().find('.end_date').val(endDate);
        // console.log(formatDate(formatedDate));
        // console.log(startDate);
      });

      function formatDate(date) {
        // console.log(date);
        
          var d = new Date(date),
              month = '' + (d.getMonth() + 1),
              day = '' + d.getDate(),
              year = d.getFullYear(),
              hours = d.getHours().toString(),
              min = d.getMinutes().toString();

          if (month.length < 2) month = '0' + month;
          if (day.length < 2) day = '0' + day;
          if (hours.length < 2) hours = '0' + hours;
          if (min.length < 2) length = '0' + min;


          var finalDate = [day, month, year].join('-');
          var finalTime = [hours, min].join(':');
          return [finalDate, finalTime].join(" ");
      }
      $('#add-batch-form').on('submit',function(e){
        e.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
          type:'POST',
          url:'/update-production-plan',
          data: formData,
          success: function(data, status, xhr) {
              if(data.code == 1){
                  location.reload();
              }else {
                alert(data.error);
              }
          },
          error: function(xhr, status, error) {

          }
        });
      });
    $(document).on('click','.delete-batch',function(){
      var formData = 'id='+$(this).attr('id');
      $.ajax({
        type:'POST',
        url:'/delete-production-plan',
        data: formData,
        success: function(data, status, xhr) {
            if(data.code == 1){
                location.reload();
            }else {
              alert(data.error);
            }
        },
        error: function(xhr, status, error) {

        },
      })
    });
  })
</script>

@endsection
