@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.production' )

@section('css')
<style>
    .machine-list-card{
        width: 350px;
        box-shadow: 0px 0px 5px #cecece;
        /*text-align: center;*/
        background-color: white;
    }

    .card-holder{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 80vh;
    }

    .form-group span{
        width: 80px;
        display: inline-block;
        margin-left: 30px;
    }

    form{
        margin-top: 50px;
    }

    input[type="submit"]{
        width: 150px;
        margin-top: 30px;
    }

    h3{
        margin-top: 40px;
    }

    body{
        background-color: #f5f5f5;
    }

    form input[type="text"], form select{
        width: 170px !important;
        height: 35px !important;
        background-color: #f7f7f7 !important;
    }
</style>
@endsection
@section('content')
<section id="pf-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 card-holder">
                <div class="machine-list-card">
                    <h3 class="text-center">Production Stock</h3>
                    <form action="/production-stock-report" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <span for="machine">Machine : </span>
                            <select name="machine" id="machine">
                                <option value="1">ZELL A</option>
                                <option value="2">ZELL B</option>
                                <option value="3">KIDDE</option>
                                <option value="4">UNDIPPED</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <span for="filter-date">Date : </span>
                            <input type="text" id="filter-date" class="text-input" name="filter-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="rpt_type">Type : </span>
                            <select name="rpt_type" id="rpt_type">
                                <option value="1">AWAIT INSPECTION</option>
                                <option value="2">AWAIT PACKING</option>
                                <option value="3">AWAIT REWINDING (REWORK 1)</option>
                                <option value="4">NCR(D) (REWORK 2)</option>
                                <option value="5">NCR</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <span for="group_by">Group By:</span>
                            <select name="group_by" id="group_by">
                                <option value="doff_no">Doff No</option>
                                <option value="material_id">Quality</option>
                                <option value="spindle">Spindle</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Show Details">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        // $('#filter-date').datepicker({
        //     changeMonth: true,
        //     changeYear: true,
        //     showButtonPanel: true,
        //     dateFormat: 'dd-mm-yy',
        //     // onClose: function(dateText, inst) {
        //     //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        //     // }
        // });
        // $('#filter-date').focus(function(){
        //     $(".ui-datepicker-calendar").hide();
        // });
        $('#filter-date').datetimepicker({
          format:'d-m-Y H:i',
      });
    });
</script>
@endsection
