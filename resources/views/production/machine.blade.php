@extends('layouts.pp')

@section('css')
    <style>
      .center{
        text-align: center;
      }
      .material-icons{
        cursor: pointer;
      }
      .material-icons:hover{
        color: #003fbb !important;
      }
      #machine{
        text-align: center;
      }
    </style>
@endsection

@section('content')

<section id="pp">
  <div class="container-fluid">
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
              <h3>Machines</h3>
              <!-- <div class="bp-options">
                  <button type="button" class="btn btn-primary" id="create-bp">Add Business Partner</button>
              </div> -->
          </div>
      </div>
  </div>
    <div class="container-fluid">
        <div class="row">
          <!-- <h3>Business Partner Items</h3> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-spacing">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Machine</th>
                    <th>Changes (hr)</th>
                    <th>Maintanance (hr)</th>
                    <th>Trials (hr)</th>
                    <th>Other (hr)</th>
                    <th class="center">Edit</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($machine as $key => $value): ?>
                    <tr>
                      <td>{{$value->name}}</td>
                      <td>{{$value->change_hr}}</td>
                      <td>{{$value->maintanance}}</td>
                      <td>{{$value->trials}}</td>
                      <td>{{$value->other}}</td>
                      <td class="center"><i class="material-icons edit" data-id="{{$value->id}}">edit</i></td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</section>


<section id="pop-ups">
  <div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Machine Updated</h4>
        </div>
        <div class="modal-body">
          <!-- <p>Some text in the modal.</p> -->
          <h2 id="machine">Zell A</h2>
          <form class="editForm" action="#" method="post">
            <input type="hidden" name="id" value="">
            <div class="form-group">
              <label for="change_hr">Changes:</label>
              <input type="text" class="form-control" id="change_hr" name="change_hr">
            </div>
            <div class="form-group">
              <label for="maintanance">Maintanance:</label>
              <input type="text" class="form-control" id="maintanance" name="maintanance">
            </div>
            <div class="form-group">
              <label for="trials">Trials:</label>
              <input type="text" class="form-control" id="trials" name="trials">
            </div>
            <div class="form-group">
              <label for="other">Other:</label>
              <input type="text" class="form-control" id="other" name="other">
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-primary center-block" value="Update">
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>

@endsection

@section('script')

<script type="text/javascript">
  $(document).ready(function() {
    $('.edit').on('click',function() {
      var id = $(this).data('id');
      var tr = $(this).parent().parent();
      var machine = tr.find("td").eq(0).html();
      var change = tr.find("td").eq(1).html();
      var maintanance = tr.find("td").eq(2).html();
      var trials = tr.find("td").eq(3).html();
      var other = tr.find("td").eq(4).html();
      $('input[name="id"]').val(id);
      $('#machine').text(machine);
      $('#change_hr').val(change);
      $('#trials').val(trials);
      $('#maintanance').val(maintanance);
      $('#other').val(other);
      $('#editModal').modal('show');
    });
    $(document).on('input','input' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });
    $('.editForm').on('submit', function(e) {
      e.preventDefault();
			var formData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "/machine-update",
				data: formData,
				success: function(data) {
					if (data.status) {
            location.reload();
					}else{
						alert(data.msg);
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
			});
		});
  });

</script>

@endsection
