@extends('layouts.pp')

@section('css')

    <link rel="stylesheet" href="css/production-planning.css">
    <link rel="stylesheet" href="{{url('responsive-css/production-planning.css')}}">
    <style>
        .table{
            display: table;
            /*padding: 20px;*/
            margin-bottom: 10px;
        }

        .table-holder{
            box-shadow: 0px 0px 10px #dedede;
        }

        .bg-white{
            background-color: white;
        }

        .tr{
            display: table-row;
        }

        .td{
            display: table-cell;
            vertical-align: middle;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 20px;
            padding-right: 20px;
        }

        .batch-btn{
            /*height: 30px !important;*/
            height: 36px !important;
            width: 90px;
            /* margin-left: auto; */
            /* margin-right: auto; */
            /* display: block; */
            float: right;
            vertical-align: middle;
        }

        .bottom-shadow{
            box-shadow: 0 8px 5px -5px #dedede;
        }

        .border-bottom-table .tr .td{
            border-bottom: 1px solid gainsboro;
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .border-bottom-table .tr:last-child .td{
            border-bottom: none;
        }

        .text-input{
            height: 30px;
        }

        #batch-count{
            width: 300px;
        }

        .batch-group .form-group{
            display: inline-block;
            margin-right: 2px;
        }

        .blue-text{
            color: #003ebb;
            background-color: #f4f8ff;
            padding: 8px;
            border-radius: 2px;
            font-weight: 600;
        }

        #submit-batch-form{
            margin-top: 20px;
        }

        .no-plan{
            margin: 20px;
            font-size: 16px;

        }
        .page-spacing{
                    padding-bottom: 15%;
        }
        .modal-dialog{
          width: 62%;
        }
    </style>
@endsection

@section('content')

<section id="pp">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-spacing">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php $machineArray = ['','ZELL A', 'ZELL B', 'KIDDE']; ?>

                    <h3 class="title">{{$machineArray[$machine]}} - Production Planning</h3><span style="margin-left: 15px; font-size: 18px; display: inline-block;">({{$month}})</span>
                </div>


                <div class="table table-header">
                    <div class="tr">
                        <div class="td" style="width: 5%;">Sl.No</div>
                        <div class="td" style="width: 30%;">Quality</div>
                        <div class="td" style="width: 25%;">Order (Kgs)</div>
                        <div class="td" style="width: 15%;">Filament</div>
                        <div class="td" style="width: 25%;">batch</div>

                    </div>
                </div>



                <?php $c=0; ?>
                @foreach($feasibility as $material => $pfs)
                    @foreach($pfs as $pf)
                    <?php $materialName = \App\ItemMaster::where('id', $pf['material'])->value('material');
                                        ?>
                    <div class="table-holder bg-white">

                        <div class="table">
                            <div class="tr bottom-shadow">
                                <div class="td" style="width: 5%;">{{++$c}}</div>
                                <div class="td" style="width: 30%;">{{$materialName}}</div>
                                <div class="td" style="width: 25%;">{{$pf['consumption']}}</div>
                                <div class="td" style="width: 15%;">{{$pf['filament']}}</div>
                                <div class="td" style="width: 25%;">
                                    <button href="#" data-material="{{$pf['material']}}" data-orderqty="{{$pf['consumption']}}" data-filament="{{$pf['filament']}}" data-month="{{$month}}" class="btn-primary btn batch-btn">batch</button>
                                </div>
                            </div>
                        </div>


                    </div>

                    @endforeach
                @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


<section id="pop-ups">

    <div id="batch-order-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Production Batch</h4>
                </div>
                <div class="modal-body">
                    <p>Total Order: <span id="total-p"></span> </p>
                    <form action="#" id="add-batch-form" name="add-batch-form">
                        @csrf
                        <input type="hidden" value="" id="total-qty">
                        <div class="form-group batch-count-fg">
                            <label for="batch_count">No Of Batches</label>
                            <input type="text" class="text-input" id="batch_count" name="batch_count" value="1">
                        </div>
                        <div class="batch-master">
                          <div class="batch-group">

                            <h5 class="blue-text">Batch 1</h5>
                            <div class="form-group">
                              <label for="batch_weight">Weight (Kgs)</label>
                              <input type="text" class="text-input batch_weight" name="batch_weight[]" required>
                            </div>

                            <div class="form-group">
                              <label for="batch_date">Start Date</label>
                              <input type="text" class="text-input batch_date" name="start_date[]" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                              <label for="end_date">End Date</label>
                              <input type="text" class="text-input end_date" name="end_date[]" value="" autocomplete="off" required>
                            </div>
                          </div>
                        </div>


                        <div class="form-group btn-gr">
                            <input type="submit" class="btn btn-primary" id="submit-batch-form" value="Add Batch">
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function(){

      $('.batch_date').datetimepicker({
          format:'d-m-Y H:i',
      });

      var data = {!! json_encode($filaments) !!};
      var obj = JSON.parse(data);
      var machine = {{$machine}};
      var prdHours = 0;
      var material = '';
      var filament = '';
      var month = '';


        $('.batch-btn').on('click', function(){

            material = $(this).data('material');
            filament = $(this).data('filament');
            month = $(this).data('month');
            var orderQty = $(this).attr('data-orderqty');

            var datastring='material='+material+'&machine='+machine;
            $.ajax({
                type: "POST",
                url: "/get-production-hours",
                data: datastring,
                success: function(data, status, xhr) {
                    if(data.code == 1){
                      prdHours = data.ProdHr;
                      $('#batch-order-modal').find('form #total-qty').val(orderQty);
                      $('#total-p').text(orderQty);
                      $('#batch-order-modal').modal('show');
                    }
                },
                error: function(xhr, status, error) {},
            })
            // $('#batch-order-modal').find('form #pf-id').val($(this).attr('data-pfid'));
            //
        });

        $(document).on('change','.batch_date',function(){
          var batchWeight = $(this).parent().parent().find('.batch_weight').val();
          var productionHr = (batchWeight/prdHours).toFixed(2);
          productionHr = productionHr.split(".");
          var dateTime = $(this).val().split(" ");
          var myDate = dateTime[0].split("-");
          var myTime = dateTime[1].split(":");
          var formatedDate = new Date(myDate[2], (myDate[1]-1), myDate[0], myTime[0], myTime[1], 0);
          formatedDate.setHours(formatedDate.getHours() + parseInt(productionHr[0]));
          formatedDate.setMinutes(formatedDate.getMinutes() + parseInt(productionHr[1]));
          var endDate = formatDate(formatedDate);
          $(this).parent().parent().find('.end_date').val(endDate);
          // console.log(formatDate(formatedDate));
          // console.log(startDate);
        });

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear(),
                hours = d.getHours(),
                min = d.getMinutes();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            if (hours.length < 2) hours = '0' + hours;
            if (min.length < 2) length = '0' + min;


            var finalDate = [day, month, year].join('-');
            var finalTime = [hours, min].join(':');
            return [finalDate, finalTime].join(" ");
        }

        $('#batch_count').on('blur', function(){

            $('.batch-master').empty();
            var count = parseInt($(this).val());
            var i;

            for(i=1;i<=count; i++){
                $('.batch-master').append('<div class="batch-group">'+
                      '<h5 class="blue-text">Batch '+i+'</h5>'+
                      '<div class="form-group">'+
                          '<label>Weight (Kgs)</label>'+
                          '<input type="text" class="text-input batch_weight" name="batch_weight[]" required>'+
                      '</div>'+
                      '<div class="form-group">'+
                          '<label>Start Date</label>'+
                          '<input type="text" class="text-input batch_date" name="start_date[]" autocomplete="off" required>'+
                      '</div>'+
                      '<div class="form-group">'+
                          '<label>End Date</label>'+
                          '<input type="text" class="text-input end_date" name="end_date[]" value="" autocomplete="off" required>'+
                      '</div>'+
                  '</div>');
            }
            $('.batch_date').datetimepicker({
                format:'d-m-Y H:i',
            });
        });



        $('#add-batch-form').on('submit', function(e){
            var batchDetails = [];
            e.preventDefault();
            var totalOrderQty = parseInt($(this).find('#total-qty').val());
            var totalBatchQty = 0;

            $('.batch_date').each(function(){
                // console.log($(this).val());
                // console.log($(this).closest('.batch-group').find('.batch_weight').val());


                batchDetails.push({
                    'start_date': $(this).val(),
                    'end_date': $(this).closest('.batch-group').find('.end_date').val(),
                    'weight': $(this).closest('.batch-group').find('.batch_weight').val(),
                })

                totalBatchQty += parseInt($(this).closest('.batch-group').find('.batch_weight').val());
            });

            console.log(totalBatchQty, totalOrderQty);

            if(totalBatchQty > totalOrderQty){

                $('#add-batch-form').append('<p class = "text-danger">Total Batch Quantity is higher than the Total Order Quantity by :' + (totalBatchQty - totalOrderQty)+'kgs.</p>');

            }else if(totalBatchQty < totalOrderQty){

                $('#add-batch-form').append('<p class = "text-danger">Total Order Quantity is higher than the Total Batch Quantity by :' + (totalOrderQty - totalBatchQty)+'kgs.</p>');

            }else{

                batchDetails = JSON.stringify(batchDetails);

                var formData = new FormData();
                formData.append('batch_details', batchDetails);
                formData.append('filament', filament);
                formData.append('material', material);
                formData.append('month', month);
                formData.append('machine',machine);

                $.ajax({
                    type: "POST",
                    url: "/create-production-batch",
                    data: formData,
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    beforeSend: function() {},
                    success: function(data, status, xhr) {
                        if(xhr.status == 200){
                            location.reload();
                        }
                    },
                    error: function(xhr, status, error) {},
                })
            }

        });


        $(document).on('keydown','.batch_weight', function(e){
           $(this).closest('form').find('.text-danger').remove();
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.delete-batch').on('click',function() {
            var id = $(this).data('id');
            $.ajax({
              type:'GET',
              url:'/delete-production-plan/'+id,
              success: function(data, status, xhr) {
                  if(data.code == 1){
                      location.reload();
                  }else {
                    alert(data.error);
                  }
              },
              error: function(xhr, status, error) {

              },
            })
        });

    })
</script>

@endsection
