@extends('layouts.production')
@section('css')
<style media="screen">
  #dip-cord-clearance{
    margin-top: 75px;
  }
  h3{
    display: inline-block;
  }
  #create-btn{
    margin: 15px 0px;
  }
  .active{
    cursor: pointer;
  }

  .not-active{
    cursor: not-allowed;
  }

  .active:hover{
    color: #003ebb !important;
  }
  .red{
    color: #e80404 !important;
  }
  .green{
    color: #2d6100 !important;
  }
  .process{
    color: #003ebb !important;
  }
  #dip-cord-clearance-div{
    overflow: auto;
    max-height: 80vh;
  }
  .filter{
    margin: 15px 10px 0px 0px;
    height: 38px;
    padding: 5px;
    border-radius: 5px;
    border-style: ridge;
    text-align: center;
  }
  td{
    vertical-align: middle !important;
  }
</style>
@endsection
@section('content')
<section id="dip-cord-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Dip Cords</h3>
                <a class="btn btn-primary pull-right" href="/dip-cord-clearance" id="create-btn">Create Lot</a>
                <input type="text" class="filter pull-right" placeholder="Filter Date">
            </div>
            <div class="col-md-12" id="dip-cord-clearance-div">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Lot No.</th>
                    <th>Material</th>
                    <th>Customer</th>
                    <th>Filament</th>
                    <th>Sample</th>
                    <th>Doff</th>
                    <th>Status</th>
                    <th>Add</th>
                    <th>Report</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for ($i=0; $i < 10; $i++) { ?>
                  <tr>
                    <td>727</td>
                    <td>2x5 SS 001</td>
                    <td>PIX</td>
                    <td>TYPE CH</td>
                    <td>IL<br>FDL<br>Sample1</td>
                    <td>BPX1/1<br>BPX1/1<br>BPX1/2 </td>
                    <td>
                      <strong class="green">Cleared</strong><br>
                      <strong class="red">Rejected</strong><br>
                      <strong class="process">on Process</strong>
                    </td>
                    <td><i class="material-icons active">add</i></td>
                    <td><i class="material-icons active">save_alt</i></td>
                    <td><i class="material-icons active">delete</i></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $('.filter').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd-mm-yy',
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    });
    $('.filter').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });
    $(document).on('click','.ui-datepicker-close',function(){
        window.location.href = '/dip-cords-list/'+$('.filter').val();
    });
  });

</script>
@endsection
