@extends('layouts.production')

@section('css')
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="css/global.css">
        <link rel="stylesheet" href="responsive-css/purchase-material-clearance.css">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
        <style>

            #filament-clearance{
                margin-top: 100px;
            }

            .inline-div{
                display: inline-block;
            }

            .clearance-list{
                /*height: 100px;*/
                padding: 20px 15px;
                box-shadow: 0px 0px 5px gainsboro;
                /*padding-left: 5%;*/
                margin: 30px auto 15px;
            }


            .download,.edit,.delete{
                /*width: 10%;*/
                margin: 0px 10px;
            }

            .action{
                vertical-align: middle;
                margin-top: -30px;
            }

            .list-header{
                font-weight: bold;
            }

            .download,.edit{
                margin: 0px 10px;
                padding: 10px;
                background-color: #DDE8FE;
                color: #5D5F61;
                font-weight: bold;
            }

            h3{
                display: inline-block;
                margin-top: 0px;
            }


            .chemical-row{
                margin: 10px auto;
                padding: 0px;
            }

            #filament-clearance{
                margin-top: 100px;
            }

            .chemical-row input, .chemical-row select {
                margin: 0px auto 20px;
            }
            .form-spacing{
                margin: 0px;
                padding: 0px;
            }

            .modal-body {
                padding: 5px;
            }


            input, select {
                height: auto !important;
                border: 1px solid #d7d7d7 !important;
                padding: 5px !important;
                border-radius: 0px !important;
                background-color: #ffffff;
            }

            #create-chemical{
                margin: 0px;
            }

            .modal-content {
                padding-bottom: 25px;
            }

            select {
                height: 32.5px !important;
            }
        </style>
@endsection

@section('content')
    <section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Dip Solution Clearance</h3>
                <button class="btn btn-primary pull-right" data-target="#createPackingModal" data-toggle="modal">Create Lot</button>
            </div>

            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (Session::has('message'))
                    <div class="alert alert-success" style="margin-top: 30px;">{{Session::get('message')}}</div>
                @endif
            </div>

            <div class="col-md-12" id="packing-clearance-div">

            @foreach($dipSolutionResult as $clearance)


                    <div class="clearance-list">
                        <div class="lot-no inline-div">
                            <p class="list-header">Solution Name</p>
                            <p class="list-details">{{$clearance->name}}</p>
                        </div>

                        <div class="type inline-div">
                            <p class="list-header">Batch No</p>
                            <p class="list-details">{{$clearance->batch_no}}</p>
                        </div>

                        <div class="weight inline-div">
                            <p class="list-header">Start Date</p>
                            <p class="list-details">{{$clearance->start_date}}</p>
                        </div>

                        <div class="date inline-div">
                            <p class="list-header">End Date</p>
                            <p class="list-details">{{$clearance->end_date}}</p>
                        </div>

                        <div class="invoice-date inline-div">
                            <p class="list-header">Prepared By</p>
                            <p class="list-details">{{$clearance->prepared_by}}</p>
                        </div>

                        <div class="denier inline-div">
                            <p class="list-header">Collected By</p>
                            <p class="list-details">{{$clearance->collected_by}}</p>
                        </div>

                        <div class="status inline-div">
                            <p class="list-header">Status</p>
                            @if($clearance->status == 0)
                                <p class="list-details blue-text"><b>on Process</b></p>
                            @elseif($clearance->status == 1)
                                <p class="list-details green"><b>Cleared</b></p>
                            @else
                                <p class="list-details red"><b>Rejected</b></p>
                            @endif
                        </div>


                        <div class="action inline-div">
                            @if($clearance->status == 0)
{{--                                <a href="#" class="edit edit-packaging-lot" data-id="{{$clearance->id}}">Edit</a>--}}
                                <a href="/delete-dip-solution-lot/{{$clearance->id}}" class="delete btn btn-primary" >Delete</a>
                            @else
                                <a href="/dip-solution-report/{{$clearance->id}}" target="_blank" class="download">Download</a>
                                <a href="#" class="edit" disabled>Edit</a>
                                <a href="#" class="delete btn btn-primary" disabled>Delete</a>
                            @endif

                            {{--<a href="#" class="download">Download</a>--}}
                            {{--<a href="#" class="edit">Edit</a>--}}
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
</section>

<section id="pop-ups">
    <div id="createPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Solution Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-dip-solution" method="POST" class="col-md-12" name="dip-solution-form">
                        {{csrf_field()}}
                        <input type="hidden" name="form-type" value="create">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="solution-name">Solution Name</label>
                                <select class="input-field" name="solution-name" id="solution-name">
                                    @foreach($solutions as $solution)
                                        <option value="{{$solution}}">{{$solution}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="batch-no">Batch No</label>
                                <input type="text" id="batch-no" name="batch-no" required autocomplete="off">
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="start-date">Start Date and Time</label>
                                <input type="text" id="start-date" name="start-date"  required autocomplete="off">
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="completion-date">Completion Date and Time</label>
                                <input type="text" id="completion-date" name="completion-date" required autocomplete="off">
                            </div>


                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="prepared-by">Prepared By</label>
                                <input type="text" id="prepared-by" name="prepared-by" required placeholder="Operator 1, operator 2" autocomplete="off">
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="sample-collected">Sample Collected By</label>
                                <input type="text" id="sample-collected" name="sample-collected"  required autocomplete="off">
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="master-date">Master Solution Date</label>
                                <input type="text" id="master-date" name="master-date"  required autocomplete="off">
                            </div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row ">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Package Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-packaging-lot" method="POST" class="col-md-12" id="create-packing">
                        {{csrf_field()}}

                        <input type="hidden" name="form-type" value="update">
                        <input type="hidden" name="packing-id" id="packing-id">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-name">Item Name</label>
                                <select class="input-field" name="item" id="item-name">
                                    {{--@foreach($packagingMaterial as $material)--}}
                                        {{--<option value="{{$material}}">{{$material}}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier-name">Supplier Name</label>
                                <input type="text" id="supplier-name" name="supplier_name"  required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="packing-lot-no">Lot No</label>
                                <input type="text" id="packing-lot-no" name="packing_lot_no" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-ordered">Quantity Order</label>
                                <input type="text" id="quantity-ordered" name="quantity_ordered" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="po">PO</label>
                                <input type="text" id="po" name="po"  required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="grn">GRN</label>
                                <input type="text" id="grn" name="grn" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-received">Quantity Received</label>
                                <input type="text" id="quantity-received" name="quantity_received" required>
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</section>


@endsection

@section('script')

{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
        {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>--}}

<script type="text/javascript">
    $(document).ready(function(){

        $('#start-date, #completion-date, #master-date').datetimepicker({
            format:'d-m-Y H:i',
        });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    });
</script>
@endsection
