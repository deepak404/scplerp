@extends('layouts.pp')

@section('css')
    <style>
      thead>tr>th{
        text-align: center;
      }
      .table{
        margin: 20px 0px;
      }
      td{
        vertical-align: middle !important;
      }
      .page-spacing{
        overflow: auto;
        max-height: 80vh;
        max-width: 100vw;
      }
      .material-icons{
        cursor: pointer;
      }
      .material-icons:hover{
        color: #003fbb !important;
      }
      .center{
        text-align: center;
      }
      h3,.bp-options{
        display: inline-block;
      }
      .bp-options{
        float: right;
        margin-top: 10px;
      }
    </style>
@endsection

@section('content')

<section id="pp">
  <div class="container-fluid">
      <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
              <h3>Business Partner Items</h3>
              <div class="bp-options">
                  <button type="button" class="btn btn-primary" id="create-bp">Add Business Partner</button>
              </div>
          </div>
      </div>
  </div>
    <div class="container-fluid">
        <div class="row">
          <!-- <h3>Business Partner Items</h3> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-spacing">
              <?php foreach ($businessPartner as $value): ?>
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th colspan="3" style="background-color: aliceblue;">{{$value->bp_name}}</th>
                    </tr>
                    <?php if ($value->bpMaterials->count() > 0): ?>
                      <tr>
                        <th style="width:20%;">Code</th>
                        <th style="width:60%;">Description</th>
                        <th style="width:20%;">Edit</th>
                      </tr>
                      <?php else: ?>
                        <tr>
                          <td colspan="">No Items for this Business Partner</td>
                        </tr>
                    <?php endif; ?>
                  </thead>
                  <tbody>
                    <?php foreach ($value->bpMaterials->sortBy('material') as $material): ?>
                      <tr>
                        <td>{{$material->material}}</td>
                        <td>{{$material->descriptive_name}}</td>
                        <td class="center"><i class="material-icons edit-bp" data-id="{{$material->id}}">edit</i></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>


<section id="pop-ups">
  <div id="bpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content col-md-12 col-lg-12 col-sm-12">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Business Partner</h4>
        </div>
        <div class="modal-body col-md-12 col-lg-12 col-sm-12">
          <form class="col-md-12 col-lg-12 col-sm-12" id="bp-form" action="#" method="post">
            <input type="hidden" name="id" value="">
            <div class="form-group col-md-12 col-lg-12 col-sm-12">
              <label for="name">BP Name:</label>
              <select class="form-control" name="bp_id" id="name" required>
                <?php foreach ($businessPartner as $value): ?>
                  <option value="{{$value->id}}">{{$value->bp_name}}</option>
                <?php endforeach; ?>
              </select>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-12 col-lg-12 col-sm-12">
              <label for="material">Code:</label>
              <select class="form-control" name="material_id" id="material" required>
                <?php foreach (\App\ItemMaster::all()->sortBy('material') as $value): ?>
                  <option value="{{$value->id}}">{{$value->material}}</option>
                <?php endforeach; ?>
              </select>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-12 col-lg-12 col-sm-12">
              <label for="descriptive_name">Description:</label>
              <input class="form-control" type="text" name="descriptive_name" value="" required>
              <span class="feedback"></span>
            </div>
            <div class="form-group col-md-12 col-lg-12 col-sm-12">
                <input type="submit" class="btn btn-primary center-block" value="Save">
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>

@endsection

@section('script')

<script type="text/javascript">
  $(document).ready(function() {
    $('#create-bp').on('click',function() {
      $('#bp-form').trigger("reset");
      $('input[name="id"]').val('');
      $('select[name="bp_name"]').removeAttr('disabled');
      $('#bpModal').modal('show');
    });
    $(document).on('submit', '#bp-form', function(event) {
			event.preventDefault();
			console.log($(this).serialize());
			var formData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "/create-bp-item",
				data: formData,
				success: function(data) {
					if (data.status) {
						location.reload();
					}else{
						alert(data.msg);
					}
				},
				error: function(xhr) {
						if (xhr.status == 422) {
							errorHandler(xhr.responseJSON.errors);
						}
				},
			});
    });
    $(document).on('click', '.edit-bp', function() {
			var formData = 'id='+$(this).data('id');
			$.ajax({
				type: "POST",
				url: "/get-bp-item",
				data: formData,
				success: function(data) {
					if (data.status) {
						var bp = data.bp;
						$.each(bp, function(key,val) {
              if (key == "descriptive_name" || key == "id") {
                var select = 'input[name="'+key+'"]';
                $(select).val(val);
              }else{
                var select = 'select[name="'+key+'"]';
                $(select).val(val);
              }
						});
            $('select[name="bp_name"]').attr('disabled','disabled');
            $('#bpModal').modal('show');
					}else{
						alert(data.msg);
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
			});
		});
  });

</script>

@endsection
