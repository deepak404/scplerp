@extends('layouts.production')
@section('css')

    <link rel="stylesheet" href="responsive-css/qc-filament-clearance.css">
<style>
   
    .inline-div{
            display: inline-block;
        }

        .clearance-list{
            /*height: 100px;*/
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            /*padding-left: 5%;*/
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10% ;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }
/*
        .action{
            vertical-align: middle;
            margin-top: -30px;
        }*/

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }


        .filament-row input, .filament-row select {
            margin: 0px auto 20px;
        }
        .form-spacing{
                    margin: 0px;
            padding: 0px;
        }

        .modal-body {
        padding: 5px; 
        }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }

        #filament-clearance{
            margin-top: 120px;
        }
        
.download-btn{
    display: inline-block;
}

</style>
@endsection
@section('content')
<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Filament Clearance</h3>
            </div>
            <div class="col-md-12" id="filament-clearance-div">
                @foreach($lots as $lot)
                <div class="clearance-list">
                    <div class="lot-no inline-div">
                        <p class="list-header">LOT No</p>
                        <p class="list-details">{{$lot['lot_batch_no']}}</p>
                    </div>

                    <div class="type inline-div">
                        <p class="list-header">Type</p>
                        <p class="list-details">{{$lot['type']}}</p>
                    </div>

                    <div class="weight inline-div">
                        <p class="list-header">Net Weight</p>
                        <p class="list-details">{{$lot['net_wt']}} Kgs.</p>
                    </div>

                    <div class="date inline-div">
                        <p class="list-header">Date</p>
                        <p class="list-details">{{$lot['arrival_date']}}</p>
                    </div>

                    <div class="invoice-date inline-div">
                        <p class="list-header">Invoice No/Date</p>
                        <p class="list-details">{{$lot['inv_no_date']}}</p>
                    </div>

                    <div class="denier inline-div">
                        <p class="list-header">Denier</p>
                        <p class="list-details">{{$lot['denier']}}</p>
                    </div>

                    <div class="status inline-div">
                        <p class="list-header">Status</p>
                        @if(is_null($lot['clearance_status']))
                        <p class="list-details blue-text"><b>on Process</b></p>
                        @elseif($lot['clearance_status'] == 1)
                        <p class="list-details green"><b>Cleared</b></p>
                        @else
                        <p class="list-details red"><b>Rejected</b></p>
                        @endif
                    </div>

                    <div class="action inline-div">
                        <?php if (!is_null($lot['clearance_status'])): ?>
                            <a href="/filament-clearance-report-one/{{$lot['id']}}" class="download">Download</a>
                        <?php endif ?>
                        <?php if ($lot['edit_delete'] != 2): ?>
                            <a class="edit" href="/pallet-entry/{{$lot['id']}}">Update</a>
                        <?php endif ?>
                    </div>

                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
@endsection
