@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.production' )

@section('css')
<style>
    .machine-list-card{
        width: 350px;
        box-shadow: 0px 0px 5px #cecece;
        /*text-align: center;*/
        background-color: white;
    }

    .card-holder{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 80vh;
    }

    .form-group span{
        width: 80px;
        display: inline-block;
        margin-left: 30px;
    }

    form{
        margin-top: 50px;
    }

    input[type="submit"]{
        width: 150px;
        margin-top: 30px;
    }

    h3{
        margin-top: 40px;
    }

    body{
        background-color: #f5f5f5;
    }

    form input[type="text"], form select{
        width: 170px !important;
        height: 35px !important;
        background-color: #f7f7f7 !important;
    }
</style>
@endsection
@section('content')
<section id="pf-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 card-holder">
                <div class="machine-list-card">
                    <h3 class="text-center">Production Summary</h3>
                    <form action="/production-report" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <span for="machine">Machine : </span>
                            <select name="machine" id="machine">
                                <option value="Zell A">ZELL A</option>
                                <option value="Zell B">ZELL B</option>
                                <option value="kidde">KIDDE</option>
                                <option value="UDFW">UDFW</option>
                                <option value="ALL">ALL</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <span for="filter-date">From : </span>
                            <input type="text" class="text-input filter-date" name="from-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="filter-date">To : </span>
                            <input type="text" class="text-input filter-date" name="to-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="rpt_type">Type : </span>
                            <select name="rpt_type" id="rpt_type">
                                <option value="doff">DOFF WISE</option>
                                <option value="material">QUALITY WISE</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <span for="status">Status : </span>
                            <select name="status" id="status">
                                <option value="all">ALL</option>
                                <option value="ok">OK</option>
                                <option value="not-ok">NOT OK</option>
                                <option value="ncr">NCR</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Show Details">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        // $('.filter-date').datepicker({
        //     changeMonth: true,
        //     changeYear: true,
        //     showButtonPanel: true,
        //     dateFormat: 'dd-mm-yy',
        //     // onClose: function(dateText, inst) {
        //     //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        //     // }
        // });
        $('.filter-date').datetimepicker({
          format:'d-m-Y H:i',
      });
    });
</script>
@endsection
