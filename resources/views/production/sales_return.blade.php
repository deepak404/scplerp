@extends('layouts.production')
@section('css')

    <link rel="stylesheet" href="responsive-css/qc-filament-clearance.css">
    <style>

        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            /*height: 100px;*/
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            /*padding-left: 5%;*/
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10% ;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }
        /*
                .action{
                    vertical-align: middle;
                    margin-top: -30px;
                }*/

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }


        .filament-row input, .filament-row select {
            margin: 0px auto 20px;
        }
        .form-spacing{
            margin: 0px;
            padding: 0px;
        }

        .modal-body {
            padding: 5px;
        }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }

        #filament-clearance{
            margin-top: 120px;
        }

        .download-btn{
            display: inline-block;
        }

    </style>
@endsection
@section('content')
    <section id="filament-clearance">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3>Sales Returns</h3>
                </div>
                <div class="col-md-12" id="filament-clearance-div">

                    @if(count($salesReturns) > 0)
                        @foreach($salesReturns as $sr)
                        <div class="clearance-list">
                            <div class="lot-no inline-div">
                                <p class="list-header">Customer</p>
                                <p class="list-details">{{\App\BusinessPartner::where('id', $sr->customer)->value('bp_name')}}</p>
                            </div>

                            <div class="type inline-div">
                                <p class="list-header">Received On</p>
                                <p class="list-details">{{$sr->received_on}}</p>
                            </div>

                            <div class="weight inline-div">
                                <p class="list-header">Reason</p>
                                <p class="list-details">{{$sr->reason}}</p>
                            </div>

                            <div class="date inline-div">
                                <p class="list-header">Sales Folio</p>
                                <p class="list-details">{{$sr->sales_folio}}</p>
                            </div>

                            <div class="invoice-date inline-div">
                                <p class="list-header">Condition</p>
                                <p class="list-details">{{$sr->condition}}</p>
                            </div>

                            <div class="denier inline-div">
                                <p class="list-header">Shortage</p>
                                <p class="list-details">{{$sr->shortage}}</p>
                            </div>

                            <div class="status inline-div">
                                <p class="list-header">Status</p>
                                <p class="list-details blue-text"><b>on Process</b></p>
                            </div>

                            <div class="action inline-div">
                                <a href="/update-sr-spindles/{{$sr->id}}" class="download">Update Spindles</a>
                            </div>

                        </div>
                    @endforeach
                        @else
                        <h4 class="text-center" style="display: block; margin-top: 30px;">No Sales Return is available.</h4>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
