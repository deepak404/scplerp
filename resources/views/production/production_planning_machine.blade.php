<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/production-planning.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="gulpfile.js"></script>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class=" "><a class=" " href="/">Production Feasability</a></li>
                        <li class="active"><a class="active-menu" href="production-planning.html">Production Planning</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>




<section id="pp">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">

                <div class="col-lg-12 col-md-12">
                    <h3>Production Planning</h3>
                </div>
                <div class=" col-lg-12 col-md-12 tableheader_padding">  
                   <div class="slno">Sl.No</div> 
                   <div class="cn">Customer name</div> 
                   <div class="quality">Quality</div> 
                   <div class="order">Order (Kgs)</div> 
                   <div class="ft">Filament Type </div> 
                   <div class="split">Split </div> 
                </div>
  

                    <div class=" col-lg-12 col-md-12 table_padding table-cards">  
                        <div class=" col-lg-12 col-md-12 table-border">
                           <div class="slno-data">1</div> 
                           <div class="cn-data">Sathizspade</div> 
                           <div class="quality-data">9X3 Soft</div> 
                           <div class="order-data">1000</div> 
                           <div class="ft-data">A </div>  
                           <div class="split-data">  <a href=" " class="btn-primary splitbutton">Split</a></div> 
                       </div>
                        
                        <div class=" col-lg-12 col-md-12 table-border table1_padding">
                           <h5 class="sub-head"> Split Details </h5>
                                <span class="pull-right"><a href=" "><i class="material-icons deleteall-icon">delete_sweep</i></a></span>
                                <table class="table tablemod">
                                     <thead>
                                         <tr>
                                            <th>Date</th>
                                            <th>Customer name</th>
                                            <th>Order (kgs)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>26/10/2018</td>
                                            <td>Sathish kumar</td>
                                            <td>233</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        
                    </div>

                    <div class=" col-lg-12 col-md-12 table_padding table-cards">  
                        <div class=" col-lg-12 col-md-12 table-border">
                           <div class="slno-data">1</div> 
                           <div class="cn-data">Sathizspade</div> 
                           <div class="quality-data">9X3 Soft</div> 
                           <div class="order-data">1000</div> 
                           <div class="ft-data">A </div> 
                           <div class="split-data">  <a href="" class="btn-primary splitbutton">Split</a></div> 
                        </div>
                        
                        <div class=" col-lg-12 col-md-12 table-border table1_padding">
                           <h5 class="sub-head"> Split Details </h5>
                                <span class="pull-right"><a href=""><i class="material-icons deleteall-icon">delete_sweep</i></a></span>
                                <table class="table tablemod">
                                     <thead>
                                         <tr>
                                            <th>Date</th>
                                            <th>Customer name</th>
                                            <th>Order (kgs)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>26/10/2018</td>
                                            <td>Sathish kumar</td>
                                            <td>233</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/sales-index.js"></script>
<script type="text/javascript">

</script>
</body>
</html>