<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
          @yield('css')
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}} " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li><a class="active-menu" href="/production-feasibility">Production Feasibility</a></li>
                        <!-- <li><a class="active-menu" href="/machine-selection">Production Planning</a></li> -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Production Planning<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                            <ul class="dropdown-menu">
                              <li><a href="/machine-selection">Unplanned</a></li>
                              <li><a href="/machine-selection-two">Planned</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Reports <i class="material-icons">
                                arrow_drop_down
                            </i></a>
                            <ul class="dropdown-menu">
                              <li><a href="/production-feasibility-report">Filament Confirmation</a></li>
                              <li><a href="/production-planning-report">Tentative Production Plan</a></li>
                            </ul>
                        </li>
                        <li><a class="active-menu" href="/generate-qr">QR Code</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Masters <i class="material-icons">
                                arrow_drop_down
                            </i></a>
                            <ul class="dropdown-menu">
                              <li><a class="active-menu" href="/bp-item">BP Master</a></li>
                              <li><a class="active-menu" href="/machine">Machine</a></li>
                              <li><a class="active-menu" href="/item-master">Item Master</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>

@yield('content')

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
  });
</script>
@yield('script')
</body>
</html>
