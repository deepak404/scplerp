<!DOCTYPE html>
<html>
<head>
    <title>Sales</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    @yield('css')
</head>
<body>
<section id="header">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="\"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <?php if (\Auth::user()->dept_id == 6): ?>
                <ul class="nav navbar-nav">
                    <li><a class="active-menu" href="/">Invoice List</a></li>
                    <li class="active"><a class="active-menu" href="/indent-list">Indent List</a></li>
                </ul>
                <?php else: ?>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Pending Contracts<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/pending-contracts/group" target="_blank">Customer Wise</a></li>
                            <li><a href="/pending-contracts/count" target="_blank">Material Wise</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Production<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                        <ul class="dropdown-menu">
                          <li><a href="/production-planning-report">Tentative Production Plan</a></li>
                          <li><a href="/plan-vs-actual">Production Plan vs Actual</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Trends<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                        <ul class="dropdown-menu">
                          <li><a class="active-menu" href="/price-trends">Price Trends</a></li>
                          <li><a class="active-menu" href="/customer-order-report">Order Trends</a></li>
                          <li><a class="active-menu" href="/customer-order-trend">Cust. wise Order Trends</a></li>

                        </ul>
                    </li>
                    <li><a class="active-menu" href="/get-sales-report">Dispatch Summary</a></li>
                </ul>
                <?php endif; ?>

                <ul class="nav navbar-nav pull-right">
                    <li><a class="active-menu" href="/logout">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
</section>


@yield('content')

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function(){

    });
</script>
@yield('script')
</body>
</html>
