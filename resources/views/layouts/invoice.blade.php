<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @yield('css')

</head>
<body>
<section id="header">
    <header>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}} " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                      <!-- <li><a class="active-menu" href="/">Instock</a></li> -->
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Doff-Wise Stock<i class="material-icons">
                          arrow_drop_down
                        </i></a>
                        <ul class="dropdown-menu">
                          <li><a href="/instock">Sound</a></li>
                          <li><a href="/instock-ncr">NCR</a></li>
                          <li><a href="/instock-leader">LEADER</a></li>
                          <li><a href="/instock-waste">WASTE</a></li>
                          <li><a href="/instock-slb">SLB</a></li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Finished Goods<i class="material-icons">
                          arrow_drop_down
                        </i></a>
                        <ul class="dropdown-menu">
                          <li><a href="/finshed-goods-stock">Sound</a></li>
                          <li><a href="/finshed-goods-stock-ncr">NCR</a></li>
                          <li><a href="/finshed-goods-stock-leader">LEADER</a></li>
                          <li><a href="/finshed-goods-stock-waste">WASTE</a></li>
                          <li><a href="/finshed-goods-stock-slb">SLB</a></li>
                        </ul>
                      </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Repack<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/challan">Repack Cases</a></li>
                                <li><a href="/challan-list">Repack List</a></li>

                            </ul>
                        </li>
                        <li><a class="active-menu" href="/invoice-list">Invoice List</a></li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Indent List<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                            <ul class="dropdown-menu">
                              <li><a href="/indent-list-invoice">By Customer</a></li>
                              <li><a href="/get-ref-no">Reference No</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Reports<i class="material-icons">
                                arrow_drop_down
                            </i></a>
                            <ul class="dropdown-menu">
                              <li><a href="/day-wise-pack">Day Wise Packed Entry</a></li>
                              <li><a href="/count-pack">Packed group By Quality</a></li>
                              <li><a href="/count-pack/item">Packed Entry for Quality</a></li>
                              <li><a href="/box-usage">Box Usage Summary</a></li>
                              <li><a href="/pending-contracts" target="_blank">Pending Contracts</a></li>
                              <li><a href="/pending-contracts/group" target="_blank">Pending Contracts Group Customer</a></li>
                              <li><a href="/pending-contracts/count" target="_blank">Pending Contracts Group Quality</a></li>
                              <li><a href="/dispatch-by-date">Dispatch By Date</a></li>
                              <li><a href="/dispatch-by-customer">Dispatch By Customer</a></li>
                              <li><a href="/dispatch-all-customer">Cum. Customer wise</a></li>
                              <li><a href="/dispatch-by-material">Dispatch By Quality</a></li>
                              <li><a href="/production-summary">Production Summary</a></li>
                              <li><a href="/day-wise-prod">Day wise Prod.</a></li>
                              <li><a href="/as-on-packed">As on Packed Material</a></li>
                            </ul>
                        </li>
                        <li><a class="active-menu" href="/sales-return">Sales Return</a></li>

                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

@yield('content')
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $.ajaxSetup({
        headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
  });
</script>
@yield('script')
</body>
</html>
