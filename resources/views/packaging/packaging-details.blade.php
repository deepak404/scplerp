<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/packaging-details.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .dropdown-toggle{
            width: 165px;
        }

        table{
            margin-top: 40px;
        }
    </style>
</head>
<body>
<section id="header">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}} " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Packaging Details</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
</section>


<section id="packaging-details">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Package Details List</h3>

                <button class="btn btn-primary" id="add-details">Add Details</button>

                <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                        @endforeach
                        <!-- <li>Please re-create indent with required.</li> -->
                        </ul>
                    </div>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>St.No</th>
                            <th>Type of<br>package</th>
                            <th>Quality</th>
                            <th>Date</th>
                            <th>Add</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Export</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $count = 1; ?>

                        @foreach($packageMaster as $package)
                        <tr>
                            <td>{{$count}}</td>
                            <td>{{$package['package_type']}}</td>
                            <td>{{$package->itemMaster['material']}}</td>
                            <td>{{date('d-m-Y',strtotime($package['packed_date']))}}</td>
                            <td>
                                <a href="/packaging-detail/{{$package['id']}}" target="_blank" class="add-package">
                                    <i class="material-icons">add_circle</i>
                                </a>
                            </td>
                            <td>
                                <a href="#" data-id="{{$package->id}}" class="edit-package">
                                    <i class="material-icons">create</i>
                                </a>
                            </td>
                            <td>
                                <a href="/delete-packaging/{{$package->id}}" data-id="{{$package->id}}" class="delete-package">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                            <td>
                                <a href="/packaging-detail-report/{{$package['id']}}" target="_blank">
                                    <i class="material-icons">info</i>
                                </a>
                            </td>
                        </tr>
                        <?php $count++; ?>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<div id="package-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-md-12 col-sm-12">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Package Detail</h4>
            </div>
            <div class="modal-body col-lg-12 col-md-12 col-sm-12 p-0">
                <form action="#" id="packaging-detail-form" class="col-lg-12 col-md-12 col-sm-12 ">
                    <input type="hidden" name="id" id="package_id" value="0">
                    <div class="col-md-12 p-0">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="package-type">Type of package</label>
                            <input type="text" class="text-input" id="package-type" name="package_type" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="quality">Quality</label>
                            <select name="quality" id="quality">
                                @foreach($unpackedMaterialMaster as $item)
                                <option value="{{$item['id']}}">{{$item['material']}}</option>
                                @endforeach
                            </select><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="date">Date</label>
                            <input type="text" class="text-input" id="date" name="date" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="box_type">Box Type</label>
                            <input type="text" class="text-input" id="box_type" name="box_type" autocomplete="off" required><span class="feedback"></span>
                        </div>
                    </div>
                    <div class="col-md-12 p-0">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="bobbin_weight">Empty Bobbin Weight (kg)</label>
                            <input type="text" class="text-input" id="bobbin_weight" name="bobbin_weight" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="box_weight">Box Weight (kg)</label>
                            <input type="text" class="text-input" id="box_weight" name="box_weight" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="moisture">Moisture Regain</label>
                            <input type="text" class="text-input" id="moisture" name="moisture" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="bobbin_count">No of Bobbins</label>
                            <input type="text" class="text-input" id="bobbin_count" name="bobbin_count" autocomplete="off" required><span class="feedback"></span>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Add Details">
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <p class="text-danger">
                            <strong>NOTE : Only Material which are not packed yet and available in the weightlog will be displayed here.</strong></p>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>



<div id="update-package-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content col-lg-12 col-md-12 col-sm-12">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Package Detail</h4>
            </div>
            <div class="modal-body col-lg-12 col-md-12 col-sm-12 p-0">
                <form action="/update-packaging-details" method="POST" id="update-packaging-detail-form" class="col-lg-12 col-md-12 col-sm-12 ">
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="package_id">
                    <div class="col-md-12 p-0">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="package-type">Type of package</label>
                            <input type="text" class="text-input" id="package-type" name="package_type" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="quality">Quality</label>
                            <select name="quality" id="quality">
                                @foreach($itemMaster as $item)
                                    <option value="{{$item['id']}}">{{$item['material']}}</option>
                                @endforeach
                            </select><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="date">Date</label>
                            <input type="text" class="text-input" id="date" name="date" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="box_type">Box Type</label>
                            <input type="text" class="text-input" id="box_type" name="box_type" autocomplete="off" required><span class="feedback"></span>
                        </div>
                    </div>
                    <div class="col-md-12 p-0">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="bobbin_weight">Empty Bobbin Weight (kg)</label>
                            <input type="text" class="text-input" id="bobbin_weight" name="bobbin_weight" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="box_weight">Box Weight (kg)</label>
                            <input type="text" class="text-input" id="box_weight" name="box_weight" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="moisture">Moisture Regain</label>
                            <input type="text" class="text-input" id="moisture" name="moisture" autocomplete="off" required><span class="feedback"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="bobbin_count">No of Bobbins</label>
                            <input type="text" class="text-input" id="bobbin_count" name="bobbin_count" autocomplete="off" required><span class="feedback"></span>
                        </div>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Update Details">
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>


<div id="confirmationModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 400px !important;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <h4 class="confirmation-info">Are you surely want to delete this Packaging Details ? </h4>
                <p style="color: red;">NOTE : Deleting this will also delete the bobbin details added under this packaging. </p>
                <div class="confirm-div">
                    <input type="button" class="btn btn-primary delete-pack-btn" value="Yes">
                    <input type="button" class="btn btn-primary delete-pack-btn" value="No">
                </div>
            </div>
        </div>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{url('js/packaging-detail.js')}}"></script>
</body>
</html>
