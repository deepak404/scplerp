<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/packaging-details-one.css')}}">
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Packaging Details</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="packaging-details">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 p-0">
                <form action="#" id="package-form" class="col-lg-12 col-md-12 col-sm-12 p-0">
                    <div class="col-md-12 p-0" id="first-div">
                        <p class="col-md-3" id="title-p">
                            Add Package
                        </p>
                        <input type="hidden" name="id" value="{{$id}}">
                        <div class="col-md-3 form-group">
                            <label for="case_no" class="label-left">Case No</label>
                            <input type="number" class="text-input" id="case_no" min="1" name="case_no" required>
                        </div>
                        <div class="col-md-3 form-group">
                            <label for="doff_no" class="label-left">Doff No</label>
                            <select name="doff_no" id="doff_no" class="text-input" required>
                                <option value="" selected disabled>Select Doff</option>
                                @foreach($availableDoff as $doff)
                                    <option value="{{$doff->id}}">{{$doff->doff_no}}</option>
                                @endforeach

                            </select>
                            {{--<input type="text" class="text-input" id="doff_no" name="doff_no" required>--}}
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                    <div class="col-md-12 p-0" id="second-div">
                        <?php for ($i=0; $i < $bobbin_count; $i++) { ?>
                        <div class="col-md-3 p-0">

                            <select name="b_no[]" class="text-input bobbin-selection" id="b_no{{$i}}">
                                <option value="" selected disabled>Select Bobbin {{$i + 1}}</option>
                                {{--<option value="empty">Empty</option>--}}

                            </select>

                            {{--<div class="col-md-6 p-0 form-group">--}}
                                {{--<label for="b_wt{{$i}}">Bobbin {{$i+1}}<br> Weight</label>--}}
                                {{--<input type="number" step="0.01" class="text-input input-two" id="b_wt{{$i}}" name="b_wt[]" required>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 p-0 form-group">--}}
                                {{--<label for="b_no{{$i}}">Bobbin {{$i+1}}<br> No</label>--}}
                                {{--<input type="number" class="text-input input-two" id="b_no{{$i}}" name="b_no[]" required>--}}
                            {{--</div>--}}
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-12 p-0 form-group">
                        <input type="submit" class="btn btn-primary" value="Add Details">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="table-section">
    <div class="container-fluid">
        <h3>Package Detail list</h3>
        <div class="table-div">
            <table class="table">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Case No</th>
                        <th>Doff No</th>
                        <th>No of<br>Bobbin</th>
                        <?php for($i=0; $i < $bobbin_count; $i++) { ?>
                            <th>B{{$i+1}}<br>Weight/No</th>
                        <?php } ?>
                        <th>Empty Weight</th>
                        <th>Total Weight</th>
                        <th>Gross Weight</th>
                        <th>Net Weight</th>
                        <th>Commer-net<br>Weight</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    @foreach($data as $value)
                    <tr>
                        <td>{{$count}}</td>
                        <td>{{$value['case_no']}}</td>
                        <td>{{$value['doff_no']}}</td>
                        <td>{{$value['bobbin_count']}}</td>

                        <?php //var_dump($bobbin_count, count($value['packed_bobbin'])) ?>
                        @if($bobbin_count == count($value['packed_bobbin']))

                            @foreach($value['packed_bobbin'] as $bobbin)
                                <td>{{$bobbin['b_wt']}}/{{$bobbin['b_no']}}</td>
                            @endforeach

                        @else
                            @foreach($value['packed_bobbin'] as $bobbin)
                                <td>{{$bobbin['b_wt']}}/{{$bobbin['b_no']}}</td>
                            @endforeach
                            <?php for($j=0; $j < ($bobbin_count - count($value['packed_bobbin'])); $j++) { ?>
                                <td></td>
                            <?php } ?>

                        @endif

                        <td>{{$value['bobbin_weight']}}</td>
                        <td>{{$value['total_weight']}}</td>
                        <td>{{$value['gross_weight']}}</td>
                        <td>{{$value['net_weight']}}</td>
                        <td>{{$value['commercial_weight']}}</td>
                    </tr>
                    <?php $count += 1; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section>



<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{url('js/packaging-detail-one.js')}}"></script>
</body>
</html>
