@extends('layouts.pp')

@section('css')
    <style>
        .inline-div{
            display: inline-block;
        }

        form{
            width: 30%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            box-shadow: 0px 0px 5px gainsboro;
            padding: 20px;
            padding-left: 40px;
        }

        .clearance-list{
            height: 100px;
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            padding-left: 5%;
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10%;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }

        #createFilamentModal .modal-dialog{
            width: 90% !important;
        }

        .filament-row{
            margin: 20px auto;
        }

        #note{
            width: 90%;
            height: 30px;
        }
        select{
            height: 27px;
            width: 100%;
        }
    </style>
    @endsection

    @section('content')
<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="/generate-qr-code" method="POST" target="_blank">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="count">Count</label>
                        {{--<input type="text" id="count" name="count" class="input-field" required>--}}
                        <select name="count" id="count">
                            @foreach($materials as $material)
                                <option value="{{$material}}">{{$material}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="doff">DOFF No:</label>
                        <input type="text" id="doff" name="doff" class="input-field" required>
                    </div>

                    <div class="form-group">
                        <label for="dop">DOP</label>
                        <input type="text" id="dop" name="dop" class="input-field"  required>
                    </div>

                    <div class="form-group">
                        <label for="direction">Direction</label>
                        <input type="text" id="direction" name="direction" class="input-field">
                    </div>

                    <div class="form-group">
                        <p><b>Spindle No</b></p>
                        <label for="from">From</label>
                        <input type="text" id="from" name="from" class="input-field" required>
                    </div>

                    <div class="form-group">
                        <label for="to">To</label>
                        <input type="text" id="to" name="to" class="input-field" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Generate QR">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
<script src="{{url('/js/filament-clearance.js')}}"></script>
@endsection
