@extends('layouts.production')
@section('css')
    <link rel="stylesheet" href="{{url('css/weight-log-report.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />


    <style>
        #machine{
            width: 30%;
        }

        #machie-info{
            margin-top: 10px;
        }
    </style>

@endsection
@section('content')
<section id="weight-log">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Weight Log Form</h3>
            </div>

            <form action="#" name="weight-log-form" id="weight-log-form" class="col-md-12 col-sm-12">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="machine" id="machine" required>


                                @if($weightLogMaster->machine_id == 1)
                                    <option value="1" selected>ZELL A</option>
                                    {{--<option value="2">ZELL B</option>--}}
                                    {{--<option value="3">KIDDIE</option>--}}

                                @elseif($weightLogMaster->machine_id == 2)
                                    {{--<option value="1">ZELL A</option>--}}
                                    <option value="2" selected>ZELL B</option>
                                    {{--<option value="3">KIDDIE</option>--}}
                                @elseif($weightLogMaster->machine_id == 3)

                                    {{--<option value="1">ZELL A</option>--}}s--}}
                                    <option value="3" selected>KIDDIE</option>
                                @endif
                            </select>
                            <p class="text-danger" id="machie-info">Machine cannot be changed. To change the machine, Please delete this entry from Weight log list and create new one.</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4 form-group">
                        <label for="from-date">From date</label>
                        <input type="text" class="text-input" name="from-date" value="{{date('d-m-Y H:i', strtotime($weightLogMaster->from_date))}}" id="from-date" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="to-date">To date</label>
                        <input type="text" class="text-input" name="to-date" id="to-date" value="{{date('d-m-Y H:i', strtotime($weightLogMaster->to_date))}}" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="tare">Tare</label>
                        <input type="text" class="text-input" name="tare" id="tare" value="{{$weightLogMaster->tare_weight}}" autocomplete="off" required>
                        </label>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="quality">Quality</label>
                        <select  class="text-input" name="quality" id="quality" autocomplete="off" required>
                            @foreach($items as $item)
                            <?php if ($weightLogMaster->material_id == $item->id): ?>
                              <option value="{{$item->id}}" selected>{{$item->material}}</option>
                              <?php else: ?>
                                <option value="{{$item->id}}">{{$item->material}}</option>
                            <?php endif; ?>
                            @endforeach
                        </select>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="doff-no">DOFF No.</label>
                        <input type="text" class="text-input" name="doff-no" id="doff-no" value="{{$weightLogMaster->doff_no}}" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="tare">Done By</label>
                        <input type="text" class="text-input" name="tare" id="done-by" value="{{$weightLogMaster->done_by}}" autocomplete="off" required>
                        </label>
                    </div>

                </div>


                <h3>Weight Entering Form</h3>

                <div id="weight-form-div" class="col-md-12">

                    @if($weightLogMaster->machine_id == 1)
                    <div id="zell-a-div" class="machine-div">
                        <table class="table" id="zell-a-table">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>

                            @foreach($weightLogMaster->weightLogDetails as $weightLogDetail)
                                <tr class="spindle-row">
                                    <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input" value="{{$weightLogDetail->spindle}}"></td>
                                    <td><input type="text" name="weight" class="weight text-input hr-text-input" value="{{$weightLogDetail->weight}}"></td>
                                    <td>
                                    <select name="quality" class="text-input hr-text-input quality">

                                        @if($weightLogDetail->quality_check == 0)
                                            <option value="0" selected>Ok</option>
                                            <option value="1">Stitches</option>
                                            @else
                                            <option value="0">Ok</option>
                                            <option value="1" selected>Stitches</option>
                                        @endif
                                    </select>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>

                    @elseif($weightLogMaster->machine_id == 2)

                        <div id="zell-b-div-one" class="col-md-6 machine-div">
                            <table class="table" id="zell-b-table-1">
                                <thead>
                                <th>Spindle</th>
                                <th>Weight(kgs)</th>
                                <th>Quality</th>
                                </thead>
                                <tbody>

                                @foreach($weightLogMaster->weightLogDetails as $weightLogDetail)

                                    @if($weightLogDetail->spindle[0] == 'E')
                                    <tr class="spindle-row">
                                        <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input" value="{{$weightLogDetail->spindle}}"></td>
                                        <td><input type="text" name="weight" class="weight text-input hr-text-input" value="{{$weightLogDetail->weight}}"></td>
                                        <td>
                                            <select name="quality" class="text-input hr-text-input quality">

                                                @if($weightLogDetail->quality_check == 0)
                                                    <option value="0" selected>Ok</option>
                                                    <option value="1">Stitches</option>
                                                @else
                                                    <option value="0">Ok</option>
                                                    <option value="1" selected>Stitches</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                        <div id="zell-b-div-two" class="col-md-6 machine-div">
                            <table class="table" id="zell-b-table-2">
                                <thead>
                                <th>Spindle</th>
                                <th>Weight(kgs)</th>
                                <th>Quality</th>
                                </thead>
                                <tbody>
                                @foreach($weightLogMaster->weightLogDetails as $weightLogDetail)

                                    @if($weightLogDetail->spindle[0] == 'W')
                                    <tr class="spindle-row">
                                        <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input" value="{{$weightLogDetail->spindle}}"></td>
                                        <td><input type="text" name="weight" class="weight text-input hr-text-input" value="{{$weightLogDetail->weight}}"></td>
                                        <td>
                                            <select name="quality" class="text-input hr-text-input quality">

                                                @if($weightLogDetail->quality_check == 0)
                                                    <option value="0" selected>Ok</option>
                                                    <option value="1">Stitches</option>
                                                @else
                                                    <option value="0">Ok</option>
                                                    <option value="1" selected>Stitches</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div id="kidde-div" class="machine-div">
                            <table class="table" id="kidde-table">
                                <thead>
                                <th>Spindle</th>
                                <th>Weight(kgs)</th>
                                <th>Quality</th>
                                </thead>
                                <tbody>
                                @foreach($weightLogMaster->weightLogDetails as $weightLogDetail)
                                    <tr class="spindle-row">
                                        <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input" value="{{$weightLogDetail->spindle}}"></td>
                                        <td><input type="text" name="weight" class="weight text-input hr-text-input" value="{{$weightLogDetail->weight}}"></td>
                                        <td>
                                            <select name="quality" class="text-input hr-text-input quality">

                                                @if($weightLogDetail->quality_check == 0)
                                                    <option value="0" selected>Ok</option>
                                                    <option value="1">Stitches</option>
                                                @else
                                                    <option value="0">Ok</option>
                                                    <option value="1" selected>Stitches</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    @endif







                </div>

                <input type="hidden" id="master_id" value="{{$weightLogMaster->id}}">
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Generate">
                </div>
            </form>
        </div>
    </div>
</section>


@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#from-date, #to-date').datetimepicker({
            format:'d-m-Y H:i',
        });


        $(document).on('change','select[name="quality"]' ,function(){
            if($(this).val() == 1){

                var spindleCount = $(this).parent().parent().find('td:first-child input').val();
                var row =
                    $('<tr class="spindle-row">'+
                        '<td><input type="text" value="'+spindleCount+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                        '<td><input type="text" name="weight" value="10" class="weight text-input hr-text-input" required></td>'+
                        '<td>'+
                        '<select name="quality" class="text-input hr-text-input quality" required>'+
                        '<option value="0">Ok</option>'+
                        '<option value="1">Stitches</option>'+
                        '</select><a class = "remove-row" href="#">Remove</a>'+
                        '</td>'+
                        '</tr>');

                row.insertAfter($(this).parent().parent());
            }else{
                console.log('she');

            }
        });


        $(document).on('click', '.remove-row', function(e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        })


        $('#weight-log-form').on('submit', function(e){

            e.preventDefault();

            var spindleDetails = [];

            $('.spindle-row').each(function(){
                spindleDetails.push({
                    'no': $(this).find('.spindle-no').val(),
                    'weight': $(this).find('.weight').val(),
                    'quality': $(this).find('.quality').val(),
                })

            });



            spindleDetails = JSON.stringify(spindleDetails);
            console.log(spindleDetails);


            var formData = new FormData();
            formData.append('spindle_details', spindleDetails);

            formData.append('machine', $('#machine').val());
            formData.append('from_date', $('#from-date').val());
            formData.append('to_date', $('#to-date').val());
            formData.append('tare', $('#tare').val());
            formData.append('material', $('#quality').val());
            formData.append('doff_no', $('#doff-no').val());
            formData.append('done_by', $('#done-by').val());
            formData.append('master_id', $('#master_id').val());


            console.log($('#done-by').val());


            console.log(formData);

            $.ajax({
                type: "POST",
                url: "/update-weight-log",
                data: formData,
                cache: !1,
                contentType: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data, status, xhr) {
                    if(xhr.status == 200){
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 500){
                        alert('Cannot Create Weight Log Details Right now. Please try Again in sometime.');
                    }
                },
            })

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
@endsection
