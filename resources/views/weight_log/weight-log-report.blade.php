<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="css/weight-log-report.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="gulpfile.js"></script>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li><a class="active-menu" href="/">Pre Weight Log</a></li>
                        <li class="active"><a class="active-menu" href="/">Weight Log Report</a></li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="weight-log">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Weight Log Report</h3>
                <div class="options">
                    <input type="text" name="filter-date" class="text-input hasDatepicker" id="filter-date" placeholder="Filter Date">
                </div>

                <table class="table">
                    <thead>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>DOFF. No</th>
                        <th>Quality</th>
                        <th>Quantity (kgs)</th>
                        <th>Done By</th>
                        <th>View</th>
                        <th>Assign Order</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>26/10/2018</td>
                        <td>26/10/2018</td>
                        <td>001234</td>
                        <td>2x3 SS</td>
                        <td>50000</td>
                        <td>Sathish kumar</td>
                        <td><i class="material-icons">
                            open_in_new
                        </i></td>
                        <td><a href="#" class="assign" data-toggle="modal" data-target="#myModal">Assign</a></td>
                    </tr>

                    <tr>
                        <td>26/10/2018</td>
                        <td>26/10/2018</td>
                        <td>001234</td>
                        <td>2x3 SS</td>
                        <td>50000</td>
                        <td>Sathish kumar</td>
                        <td><i class="material-icons">
                            open_in_new
                        </i></td>
                        <td><a href="#" class="assign" data-toggle="modal" data-target="#myModal">Assign</a></td>
                    </tr>

                    <tr>
                        <td>26/10/2018</td>
                        <td>26/10/2018</td>
                        <td>001234</td>
                        <td>2x3 SS</td>
                        <td>50000</td>
                        <td>Sathish kumar</td>
                        <td><i class="material-icons">
                            open_in_new
                        </i></td>
                        <td><a href="#" class="assign" data-toggle="modal" data-target="#myModal">Assign</a></td>
                    </tr>
                    <tr>
                        <td>26/10/2018</td>
                        <td>26/10/2018</td>
                        <td>001234</td>
                        <td>2x3 SS</td>
                        <td>50000</td>
                        <td>Sathish kumar</td>
                        <td><i class="material-icons">
                            open_in_new
                        </i></td>
                        <td><a href="#" class="assign" data-toggle="modal" data-target="#myModal">Assign</a></td>
                    </tr>
                    <tr>
                        <td>26/10/2018</td>
                        <td>26/10/2018</td>
                        <td>001234</td>
                        <td>2x3 SS</td>
                        <td>50000</td>
                        <td>Sathish kumar</td>
                        <td><i class="material-icons">
                            open_in_new
                        </i></td>
                        <td><a href="#" class="assign" data-toggle="modal" data-target="#myModal">Assign</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Production Order Assign</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <td></td>
                            <td>From Date</td>
                            <td>Customer Name</td>
                            <td>Order(Kgs)</td>
                            <td>Quality</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                    </tbody>
                </table>
                <input type="button" class="btn btn-primary" value="Add Actual Plan">
            </div>
        </div>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

</script>
</body>
</html>