<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="css/weight-log-report.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="gulpfile.js"></script>

</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li><a class="active-menu" href="/">Pre Weight Log</a></li>
                        <li class="active"><a class="active-menu" href="/">Weight Log Report</a></li>

                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>


<section id="weight-log">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Weight Log Form</h3>
            </div>

            <form action="#" name="weight-log-form" id="weight-log-form" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 form-group">
                        <label for="from-date">From date</label>
                        <input type="text" class="text-input" name="from-date" id="from-date" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 form-group">
                        <label for="to-date">To date</label>
                        <input type="text" class="text-input" name="to-date" id="to-date" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="tare">Tare</label>
                        <input type="text" class="text-input" name="tare" id="tare" autocomplete="off" required>
                        </label>
                    </div>

                    <div class="col-md-4 form-group">
                        <label for="quality">Quality</label>
                        <select  class="text-input" name="quality" id="quality" autocomplete="off" required>
                            <option value="0">1</option>
                            <option value="1">2</option>
                        </select>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="doff-no">DOFF No.</label>
                        <input type="text" class="text-input" name="doff-no" id="doff-no" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="tare">Done By</label>
                        <input type="text" class="text-input" name="tare" id="done-by" autocomplete="off" required>
                        </label>
                    </div>

                </div>


                <h3>Weight Entering Form</h3>

                <div id="weight-form-div" class="col-md-12">
                    <!--<div id="zell-a-div">-->
                        <!--<table class="table" id="zell-a-table">-->
                            <!--<thead>-->
                            <!--<th>Spindle</th>-->
                            <!--<th>Weight(kgs)</th>-->
                            <!--<th>Quality</th>-->
                            <!--</thead>-->
                            <!--<tbody>-->
                            <!--<tr>-->
                                <!--<td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>-->
                                <!--<td><input type="text" name="weight" class="weight text-input hr-text-input"></td>-->
                                <!--<td>-->
                                    <!--<select name="quality" class="text-input hr-text-input">-->
                                        <!--<option value="0">Ok</option>-->
                                        <!--<option value="1">Stitches</option>-->
                                        <!--option-->
                                    <!--</select>-->
                                <!--</td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                                <!--<td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>-->
                                <!--<td><input type="text" name="weight" class="weight text-input hr-text-input"></td>-->
                                <!--<td>-->
                                    <!--<select name="quality" class="text-input hr-text-input">-->
                                        <!--<option value="0">Ok</option>-->
                                        <!--<option value="1">Stitches</option>-->
                                        <!--option-->
                                    <!--</select>-->
                                <!--</td>-->
                            <!--</tr>-->

                            <!--</tbody>-->
                        <!--</table>-->
                    <!--</div>-->


                    <div id="zell-b-div-one" class="col-md-6">
                        <table class="table" id="zell-b-table-1">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="zell-b-div-two" class="col-md-6">
                        <table class="table" id="zell-b-table-2">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Generate">
                </div>
            </form>
        </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

</script>
</body>
</html>