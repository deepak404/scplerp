@extends('layouts.production')
@section('css')
<style>
    #weight-log-list{
        /*background-color: white;*/
        /*min-height: 80vh;*/
        /*width: 60%;*/
        /*margin-left: auto;*/
        /*margin-right: auto;*/
        margin-top: 50px;
        /*box-shadow: 0px 0px 5px #cecece;*/
        padding-top: 50px;
    }

    h3{
        display: inline-block;
        margin-top: 0px;
    }

    body{
        /*background-color: #f3f3f3;*/
    }

    .list-options{
        float: right;
    }

    table{
        margin-top: 30px;
    }
</style>
@endsection
@section('content')
<section id="weight-log-list">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h3>Weight Log List</h3>

                <div class="list-options">
                    <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date">

                </div>
            </div>

            <?php //dd($weightLog); ?>

            @if(count($weightLog) > 0)
            <div class="col-md-12 table-wrapper">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Machine</th>
                            <th>Material</th>
                            <th>Doff Date</th>
                            <th>Spindle</th>
                            <th>DOFF. No</th>
                            <th>Done By</th>
                            <th>Weight Log Status</th>
                            <th>Inspection Status</th>
                            <th>Packed Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 1; ?>
                        @foreach($weightLog as $wl)

                        <?php //dd($weightLog->material); ?>
                        <tr>
                            <td>{{$count}}</td>
                            <td>{{$wl->machine}}</td>
                            <td>{{$wl->material}}</td>
                            <td>{{$wl->doff_date}}</td>
                            <td>{{$wl->spindle}}</td>
                            <td>{{$wl->doff_no}}</td>
                            <td>{{$wl->op_name}}</td>
                            <td style="text-align:center;">
                              <?php if ($wl->weight_status == "1"): ?>
                                <i class="material-icons">done</i>
                                <?php else: ?>
                                <i class="material-icons">clear</i>
                              <?php endif; ?>
                            </td>
                            <td style="text-align:center;">
                              <?php if ($wl->inspection_status == "1"): ?>
                                <i class="material-icons">done</i>
                                <?php else: ?>
                                <i class="material-icons">clear</i>
                              <?php endif; ?>
                            </td>
                            <td style="text-align:center;">
                              <?php if ($wl->packed_status == "1"): ?>
                                <i class="material-icons">done</i>
                                <?php else: ?>
                                <i class="material-icons">clear</i>
                              <?php endif; ?>
                            </td>
                            {{--<td><a href="edit-weight-log/{{$weightLog->id}}" target="_blank" class="edit-indent"><i class="material-icons">edit</i></a></td>--}}
{{--                            <td><a href="#" data-id="{{$weightLog->id}}" class="delete-indent"><i class="material-icons">delete</i></a></td>--}}
                            {{--<td><a href="show-weightlog-report/{{$weightLog->id}}/{{$weightLog->machine_id}}" class="info-indent" target="_blank"><i class="material-icons">info</i></a></td>--}}
                        </tr>
                        <?php $count++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <h4 class="text-center">No Weight Log Entries Found.</h4>
            @endif
        </div>
    </div>
</section>
<div id="confirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 400px !important;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <h4 class="confirmation-info">Are you surely want to delete this Weight Log ? </h4>
        <!-- <p style="color: red;">NOTE : The weightlog will be removed from the indent if the indent is already generated. </p> -->
          <div class="confirm-div">
            <input type="button" class="btn btn-primary delete-sale-btn" value="Yes">
            <input type="button" class="btn btn-primary delete-sale-btn" value="No">
          </div>
      </div>
    </div>

  </div>
</div>
@endsection
@section('script')
<!-- <script src="{{ url('js/indent-list.js') }}"></script> -->
<script type="text/javascript">
$(document).ready(function(){
  $('#filter-date').datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    dateFormat: 'mm-yy',
    onClose: function(dateText, inst) {
      $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
    }
  });

  $('#filter-date').focus(function(){
    $(".ui-datepicker-calendar").hide();
  });

  $(document).on('click','.ui-datepicker-close',function(){
      window.location.href = '/weight-log-list/'+$('#filter-date').val();
  });

  $('.delete-indent').on('click',function(e){
      e.preventDefault();
      var data_id = $(this).data('id');

      $('#confirmationModal').modal('show');

      $('.delete-sale-btn').click(function(e){

          if ($(this).val() == 'Yes'){
              window.location.href = "/delete-weight-log/"+data_id;
          } else{
              $('#confirmationModal').modal('hide');
          }

      });

  });
});
</script>
@endsection
