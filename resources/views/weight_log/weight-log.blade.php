@extends('layouts.production')
@section('css')
<link rel="stylesheet" href="css/weight-log-report.css">

@endsection
@section('content')
<section id="weight-log">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Weight Log Form</h3>
            </div>

            <form action="#" name="weight-log-form" id="weight-log-form" class="col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">

                      <div class="form-group txt-box">
                        <label for="from-date">Machine</label>
                          <select name="machine" id="machine" required>
                              <option value="" disabled selected>Machine</option>
                              <option value="1">ZELL A</option>
                              <option value="2">ZELL B</option>
                              <option value="3">KIDDIE</option>
                          </select>
                      </div>
                  </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  form-group txt-box">
                        <label for="from-date">From date</label>
                        <input type="text" class="text-input" name="from-date" value="" id="from-date" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 form-group txt-box">
                        <label for="to-date">To date</label>
                        <input type="text" class="text-input" name="to-date" id="to-date" value="" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  form-group txt-box">
                        <label for="tare">Tare</label>
                        <input type="text" class="text-input" name="tare" id="tare" value="" autocomplete="off" required>
                        </label>
                    </div>

                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  form-group txt-box">
                        <label for="quality">Quality</label>
                        <select style="width: 85%;" class="text-input" name="quality" id="quality" autocomplete="off" required>
                            @foreach($items as $item)
                                <option value="{{$item->id}}">{{$item->material}}</option>
                            @endforeach
                            <option value="1">2</option>
                        </select>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  form-group txt-box">
                        <label for="doff-no">DOFF No.</label>
                        <input type="text" class="text-input" name="doff-no" id="doff-no" value="" autocomplete="off" required>
                        </label>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  form-group txt-box">
                        <label for="tare">Done By</label>
                        <input type="text" class="text-input" name="tare" id="done-by" value="" autocomplete="off" required>
                        </label>
                    </div>

                </div>


                <h3>Weight Entering Form</h3>

                <div id="weight-form-div" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="zell-a-div" class="machine-div">
                        <table class="table" id="zell-a-table">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>
                            {{--<tr>--}}
                                {{--<td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>--}}
                                {{--<td><input type="text" name="weight" class="weight text-input hr-text-input"></td>--}}
                                {{--<td>--}}
                                    {{--<select name="quality" class="text-input hr-text-input">--}}
                                        {{--<option value="0">Ok</option>--}}
                                        {{--<option value="1">Stitches</option>--}}
                                        {{--option--}}
                                    {{--</select>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>--}}
                                {{--<td><input type="text" name="weight" class="weight text-input hr-text-input"></td>--}}
                                {{--<td>--}}
                                    {{--<select name="quality" class="text-input hr-text-input">--}}
                                        {{--<option value="0">Ok</option>--}}
                                        {{--<option value="1">Stitches</option>--}}
                                        {{--option--}}
                                    {{--</select>--}}
                                {{--</td>--}}
                            {{--</tr>--}}

                            </tbody>
                        </table>
                    </div>


                    <div id="zell-b-div-one" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 machine-div">
                        <table class="table" id="zell-b-table-1">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="zell-b-div-two" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 machine-div">
                        <table class="table" id="zell-b-table-2">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><input type="text" name="spindle-no" class="spindle-no text-input hr-text-input"></td>
                                <td><input type="text" name="weight" class="weight text-input hr-text-input"></td>
                                <td>
                                    <select name="quality" class="text-input hr-text-input">
                                        <option value="0">Ok</option>
                                        <option value="1">Stitches</option>
                                        option
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="kidde-div" class="machine-div">
                        <table class="table" id="kidde-table">
                            <thead>
                            <th>Spindle</th>
                            <th>Weight(kgs)</th>
                            <th>Quality</th>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-spacing center-block" value="Generate">
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('input','.weight,#tare' ,function() {
          $(this).val($(this).val().replace(/[^0-9.]/gi, ''));
        });
        $('#from-date, #to-date').datetimepicker({
            format:'d-m-Y H:i',
        });

        $('#machine').on('change', function(){
          if($(this).val() == 1){
            $('.machine-div').hide();
            $('#zell-a-div').show();

            $('#zell-a-table, #zell-b-table-1, #zell-b-table-2, #kidde-table').find('tbody').empty();

            for(var i =1; i <= 30; i++){
                $('#zell-a-table').find('tbody').append(
                    '<tr class="spindle-row">'+
                        '<td><input type="text" value="'+i+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                        '<td><input type="text" name="weight" value="" class="weight text-input hr-text-input" required></td>'+
                        '<td>'+
                        '<select name="quality" class="text-input hr-text-input quality" required>'+
                        '<option value="0">Ok</option>'+
                        '<option value="1">Stitches</option>'+
                        '</select>'+
                        '</td>'+
                    '</tr>'
                );
            }
          }else if($(this).val() == 2){

              $('.machine-div').hide();
              $('#zell-b-div-one, #zell-b-div-two').show();

              $('#zell-a-table, #zell-b-table-1, #zell-b-table-2, #kidde-table').find('tbody').empty();

              for(var i =1; i <= 27; i++){
                  $('#zell-b-table-1').find('tbody').append(
                      '<tr class="spindle-row">'+
                      '<td><input type="text" value="E'+i+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                      '<td><input type="text" name="weight" value="" class="weight text-input hr-text-input" required></td>'+
                      '<td>'+
                      '<select name="quality" class="text-input hr-text-input quality" required>'+
                      '<option value="0">Ok</option>'+
                      '<option value="1">Stitches</option>'+
                      '</select>'+
                      '</td>'+
                      '</tr>'
                  );
              }

              for(var j=1; j <= 27; j++){
                  $('#zell-b-table-2').find('tbody').append(
                      '<tr class="spindle-row">'+
                      '<td><input type="text" value="W'+j+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                      '<td><input type="text" name="weight" value="" class="weight text-input hr-text-input" required></td>'+
                      '<td>'+
                      '<select name="quality" class="text-input hr-text-input quality" required>'+
                      '<option value="0">Ok</option>'+
                      '<option value="1">Stitches</option>'+
                      '</select>'+
                      '</td>'+
                      '</tr>'
                  );
              }


          }else if($(this).val() == 3){
              $('.machine-div').hide();
              $('#kidde-div').show();

              $('#zell-a-table, #zell-b-table-1, #zell-b-table-2, #kidde-table').find('tbody').empty();

              for(var i =1; i <= 75; i++){
                  $('#kidde-table').find('tbody').append(
                      '<tr class="spindle-row">'+
                      '<td><input type="text" value="'+i+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                      '<td><input type="text" name="weight" value="" class="weight text-input hr-text-input" required></td>'+
                      '<td>'+
                      '<select name="quality" class="text-input hr-text-input quality" required>'+
                      '<option value="0">Ok</option>'+
                      '<option value="1">Stitches</option>'+
                      '</select>'+
                      '</td>'+
                      '</tr>'
                  );
              }
          }
       });


        $(document).on('change','select[name="quality"]' ,function(){
          if($(this).val() == 1){

              var spindleCount = $(this).parent().parent().find('td:first-child input').val();
              var row =
                  $('<tr class="spindle-row">'+
                      '<td><input type="text" value="'+spindleCount+'" name="spindle-no" class="spindle-no text-input hr-text-input" required></td>'+
                      '<td><input type="text" name="weight" value="" class="weight text-input hr-text-input" required></td>'+
                      '<td>'+
                      '<select name="quality" class="text-input hr-text-input quality" required>'+
                      '<option value="0">Ok</option>'+
                      '<option value="1">Stitches</option>'+
                      '</select><a class = "remove-row" href="#">Remove</a>'+
                      '</td>'+
                      '</tr>');

              row.insertAfter($(this).parent().parent());
          }
        });


        $(document).on('click', '.remove-row', function(e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        })

        $('#weight-log-form').on('keyup keypress', function(e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) {
            e.preventDefault();
            return false;
          }
        });


        $('#weight-log-form').on('submit', function(e){

            e.preventDefault();

            var spindleDetails = [];

            $('.spindle-row').each(function(){
                spindleDetails.push({
                    'no': $(this).find('.spindle-no').val(),
                    'weight': $(this).find('.weight').val(),
                    'quality': $(this).find('.quality').val(),
                })

            });



                spindleDetails = JSON.stringify(spindleDetails);
                console.log(spindleDetails);
                var formData = new FormData();
                formData.append('spindle_details', spindleDetails);

                formData.append('machine', $('#machine').val());
                formData.append('from_date', $('#from-date').val());
                formData.append('to_date', $('#to-date').val());
                formData.append('tare', $('#tare').val());
                formData.append('material', $('#quality').val());
                formData.append('doff_no', $('#doff-no').val());
                formData.append('done_by', $('#done-by').val());


                console.log($('#done-by').val());


                console.log(formData);

            $.ajax({
                    type: "POST",
                    url: "/insert-weight-log",
                    data: formData,
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    beforeSend: function() {},
                    success: function(data, status, xhr) {
                        if(xhr.status == 200){
                          window.location.href = "/weight-log-list";
                        }
                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 500){
                            alert('Cannot Create Weight Log Details Right now. Please try Again in sometime.');
                        }
                    },
                })

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
@endsection
