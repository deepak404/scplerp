<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		html{
			margin:5px;
		}
		table,td,th{
			border: 1px solid #333;
			border-collapse: collapse;
		}
		.result>tr>td,.result>tr>th{
			font-size: 12px;
			text-align: center;
			padding: 5px 0px;
		}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<thead>
			<tr>
				<th colspan="2" rowspan="2" style="text-align: center;">SCPL ERP</th>
				<th colspan="21" rowspan="2" style="text-align: center;">FILAMENT CLEARANCE REPORT</th>
				<td colspan="6">Document No.    : 6958</td>
			</tr>
			<tr>
					<td colspan="6">Inspection Date : 08-11-2018</td>
			</tr>
			<tr>
				<td colspan="18" style="border: none; padding: 5px;">Description  : {{\App\Filament::where('material_code',$filamentLot['material_code'])->value('description')}}</td>
				<td colspan="11" style="border: none; padding: 5px;">Sample Receiving date :{{$filamentLot['sample_date']}}</td>
			</tr>
			<tr>
				<td colspan="18" style="border: none; padding: 5px;">Material     :  {{$filamentLot['type']}}</td>
				<td colspan="11" style="border: none; padding: 5px;">Qty Receiving.kgs/Pallets :  {{$filamentLot['net_wt']}}</td>
			</tr>
			<tr>
				<td colspan="18" style="border: none; padding: 5px;">Product Code :  {{$filamentLot['material_code']}}</td>
				<td colspan="11" style="border: none; padding: 5px;">Sample Testing date :  {{$filamentLot['test_date']}}</td>
			</tr>
			<tr>
				<td colspan="18" style="border: none; padding: 5px;">Supplier  :  {{\App\Supplier::where('id',$filamentLot['supplier_id'])->value('name')}}</td>
				<td colspan="11" style="border: none; padding: 5px;">Lot/Batch No. :  {{$filamentLot['lot_batch_no']}}</td>
			</tr>
		</thead>
		<tbody class="result">
			<tr>
				<th rowspan="2">SI.No</th>
				<th rowspan="2">Testing Characters</th>
				<th rowspan="2">Condition</th>
				<th rowspan="2">UOM</th>
				<th colspan="2">Specification</th>
				<th colspan="22">Observation /Results</th>
				<th>Status</th>
			</tr>
			<tr>
				<th>Min</th>
				<th>Max</th>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
				<th>11</th>
				<th>12</th>
				<th>13</th>
				<th>14</th>
				<th>15</th>
				<th>16</th>
				<th>17</th>
				<th>18</th>
				<th>19</th>
				<th>20</th>
				<th>Avg(x)</th>
				<th>Rang(R)</th>
				<th>OK/ Not OK</th>
			</tr>
			<?php $count=1; foreach ($result as $key => $value): ?>
			<tr>
				<td>{{$count}}</td>
				<?php if (strpos($key,'@')): ?>
					<?php $con = explode('@',$key); ?>
					<td style="text-align: left;">{{$con[0]}} @</td>
					<td>{{$con[1]}}</td>
					<?php else: ?>
						<td style="text-align: left;">{{$key}}</td>
						<td></td>
				<?php endif; ?>
				<td>{{$value['uom']}}</td>
				<td>{{$value['min']}}</td>
				<td>{{$value['max']}}</td>
				<?php for ($i=0; $i < 20; $i++) { ?>
					<?php if (array_key_exists($i, $value['actual'])): ?>
						<?php if ($key == 'Variety'): ?>
							<td></td>
							<?php else: ?>
								<td>{{$value['actual'][$i]}}</td>
						<?php endif; ?>
					<?php else: ?>
					<td></td>
					<?php endif ?>
				<?php } ?>
				<td>{{$value['avg']}}</td>
				<td>{{$value['range']}}</td>
				<td>{{$value['status']}}</td>
			</tr>
			<?php $count += 1; endforeach ?>
		</tbody>
	</table>
	<div>
		<p>Remarks :- Based on Purchase Order 101.</p>
		<p>FINAL CONCLUSION:</p>
		<p style="padding-left: 10px; width: 50%; display: inline-block;">Inspected / Prepared by :- {{$filamentLot['tested_by']}}</p>
		<p style="padding-left: 10px; width: 50%; display: inline-block;">Approved By:</p>
	</div>
</body>
</html>
