<!DOCTYPE html>
<html>
<head>
    <title>Date Commitment Report</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">



    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"--}}
    {{--integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">--}}
    {{--<link href="https://fonts.googleapis.com/icon?family=Material+Icons"--}}
    {{--rel="stylesheet">--}}


    <style>



        .table{
            margin: 10px auto;
            /*font-size: 11px !important;*/
        }

        .table .tr .td{
            height: 10px;
        }

        .table>tbody>tr>td{
            padding: 2px;
            font-size: 9px  !important;
            /*font-weight: 600;*/
        }

        .party-name{
            padding-top: 10px;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #333;
        }

        .table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 10px;
            padding: 0px;
        }
        table{
            width: 100% !important;
        }

        .text-left{
            text-align: left;
        }

        h3{
            font-size: 16px !important;
        }

        .report-header{
            font-size: 16px !important;
        }

        td p{
            font-size: 14px !important;
            margin: 0px !important;
        }

        .first-row>td:last-child p{
            margin-left: 20px !important;
            margin-bottom: 5px !important;
        }

        .second-row>td>p, .third-row>td>p{
            text-align: left !important;
            margin-left: 10px !important;
            margin-top: 7px !important;
            margin-bottom: 7px !important;
        }

        .second-row>td>p span:first-child, .report-header, .first-row>td>p span:first-child, .third-row>td>p span:first-child{
            font-weight: 600;
        }

        .fourth-row td{
            padding: 10px 0px !important;
        }

        /*.fifth-row td:first-child{*/
            /*width: 30px !important;*/
        /*}*/
        html{
            margin: 10px;
            font-family: "Helvetica";
        }


        .fifth-row>td{
            padding: 10px 0px !important;
        }

        .fifth-row>td p{
            font-weight: 600 !important;
        }
        .test-data td{
            padding: 8px 0px !important;
            font-size: 10px !important;
        }

        .test-table{
            margin-top: -12px !important;

        }

        .test-table>tbody>tr:first-child>td{
            /*margin-top: -12px !important;*/
            border-top: none !important;
        }

        .third-row>td{
            height: 150px;
        }

        .second-row>td{
            height: 80px;
        }
    </style>
</head>
<body>


<table class="table table-bordered">
    <tbody>
        <tr class="first-row">
            <td style="width: 10px;" colspan="2" rowspan="1"><h3>SCPL ERP</h3></td>
            <td  colspan="7" style="width: 500px;" rowspan="1"><p class="text-center report-header">CHEMICAL CLEARANCE</p></td>
            <td  colspan="3" style="width: 300px" rowspan="1">
                <p class="text-left"><span>GRN Ref : </span><span>151</span></p>
                <p class="text-left"><span>Received Date : </span><span>{{date('d-m-Y', strtotime($chemicalClearanceResult->test_arrival_date))}}</span></p>
            </td>
        </tr>

        <tr class="second-row">
            <td  colspan="12">
                <p><span>Chemical Name : </span><span>{{$chemicalClearance->chemical}}</span></p>
                <p><span>Chemical Code : </span><span>{{$chemicalClearance->chemical_code}}</span></p>
                <p><span>Supplier : </span><span>{{$chemicalClearance->supplier->name}}</span></p>
            </td>

        </tr>


        <tr class="third-row">
            <td  colspan="6" style="border-right: none;">
                <p class="text-left"><b>Lot Details</b></p>
                <p class="text-left"><span>Lot Number : </span><span>{{$chemicalClearance->lot_no}}</span></p>
                <p class="text-left"><span>Total Weight : </span><span>{{$chemicalClearance->total_weight}}</span></p>
                <p class="text-left"><span>No of Packs/Barrel : </span><span>{{$chemicalClearance->no_of_packs}}</span></p>
                <p class="text-left"><span>GRN Remarks : </span><span>{{$chemicalClearance->grn_remarks}}</span></p>
            </td>

            <td colspan="6" style="border-left: none;">
                <p class="text-left"><span>Manufacturing Date : </span><span>{{date('d-m-Y', strtotime($chemicalClearance->manufactured_date))}}</span></p>
                <p class="text-left"><span>Expiry Date : </span><span>{{date('d-m-Y', strtotime($chemicalClearance->expiry_date))}}</span></p>
                <p class="text-left"><span>No of Samples : </span><span>{{$chemicalClearance->no_of_samples}}</span></p>
            </td>

        </tr>

        {{--<tr></tr>--}}
        <tr class="fourth-row">
            <td style="width: 200px; border-right: none" colspan="2" rowspan="1"><p><b>Test Report</b></p></td>
            <td  colspan="4" rowspan="1" style="border: none"><p><span><b>Inspection Date</b> : </span><span>{{date('d-m-Y', strtotime($chemicalClearanceResult->tested_date))}}</span></p></td>
            <td  style="width: 200px; border-left: none" colspan="6" rowspan="1">
                <p class="text-left"><span><b>Inp. Receipt No</b> : </span><span>1134</span></p>
            </td>
        </tr>
    </tbody>
</table>

<table class="table table-bordered test-table">
    <tbody>
    <tr class="fifth-row">
        <td rowspan="2" style="width: 20px;"><p>S.No</p></td>
        <td style="width: 160px;" rowspan="2"><p>Parameter</p></td>
        <td style="width: 50px;" rowspan="2"><p>UOM</p></td>
        <td colspan="2"><p>Specification</p></td>
        <td colspan="7"><p>Actual Observation</p></td>
    </tr>
    <tr class="fifth-row">
        <td style="width: 50px;"><p>LSL</p></td>
        <td style="width: 50px;"><p>USL</p></td>
        <td style="width: 60px;"><p>Sample 1</p></td>
        <td style="width: 60px;"><p>Sample 2</p></td>
        <td style="width: 60px;"><p>Sample 3</p></td>
        <td style="width: 60px;"><p>Sample 4</p></td>
        <td style="width: 60px;"><p>Sample 5</p></td>
        <td style="width: 60px;"><p>Sample 6</p></td>
        <td style="width: 60px;"><p>Result</p></td>
    </tr>

    <?php $count= 1; $samples = json_decode($chemicalClearanceResult->sample); //dd($samples); ?>

    @foreach($samples as $parameter => $sample)
    <tr class="test-data">
        <td><p>{{$count}}.</p></td>
        <td><p>{{$parameter}}</p></td>
        <td><p>{{$sample->uom}}</p></td>
        <td><p>{{$sample->lsl}}</p></td>
        <td><p>{{$sample->usl}}</p></td>
        <td><p>{{$sample->sample1}}</p></td>
        <td><p>{{$sample->sample2}}</p></td>
        <td><p>{{$sample->sample3}}</p></td>
        <td><p>{{$sample->sample4}}</p></td>
        <td><p>{{$sample->sample5}}</p></td>
        <td><p>{{$sample->sample6}}</p></td>
        <td><p>{{$sample->result}}</p></td>
    </tr>
    <?php $count++; ?>
    @endforeach

    {{--<tr class="test-data">--}}
        {{--<td><p>2.</p></td>--}}
        {{--<td><p>Aldehyde Content</p></td>--}}
        {{--<td><p>%</p></td>--}}
        {{--<td><p>36.5</p></td>--}}
        {{--<td><p>37.5</p></td>--}}
        {{--<td><p>37.33</p></td>--}}
        {{--<td><p>37.18</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>Ok</p></td>--}}
    {{--</tr>--}}

    {{--<tr class="test-data">--}}
        {{--<td><p>3.</p></td>--}}
        {{--<td><p>Appearance</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td style="width: 50px;"><p>CLEAR</p></td>--}}
        {{--<td style="width: 50px;"><p>COLOURLESS</p></td>--}}
        {{--<td style="width: 60px;"><p>CLEAR</p></td>--}}
        {{--<td style="width: 60px;"><p>CLEAR</p></td>--}}
        {{--<td style="width: 60px;"><p></p></td>--}}
        {{--<td style="width: 60px;"><p></p></td>--}}
        {{--<td style="width: 60px;"><p></p></td>--}}
        {{--<td style="width: 60px;"><p></p></td>--}}
        {{--<td style="width: 60px;"><p>Ok</p></td>--}}
    {{--</tr>--}}

    {{--<tr class="test-data">--}}
        {{--<td><p>4.</p></td>--}}
        {{--<td><p>pH Value</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>2.5</p></td>--}}
        {{--<td><p>4.5</p></td>--}}
        {{--<td><p>3.0</p></td>--}}
        {{--<td><p>3.0</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>Ok</p></td>--}}
    {{--</tr>--}}

    {{--<tr class="test-data">--}}
        {{--<td><p>5.</p></td>--}}
        {{--<td><p>Appearance</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>1.1</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>1.1</p></td>--}}
        {{--<td><p>1.1</p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p></p></td>--}}
        {{--<td><p>Ok</p></td>--}}
    {{--</tr>--}}

    <tr>
        <td colspan="4" style="height: 80px; vertical-align: top; text-align: left; padding: 10px;">
            <p><b>Testing Remarks : </b></p>
        </td>
        <td colspan="4" style="vertical-align: top; text-align: left; padding: 10px;"><p><b>Space for Stamp:</b></p></td>
        <td colspan="4" style="vertical-align: bottom; text-align: center; padding: 10px;"><p><b>Q.C Incharge</b></p></td>
    </tr>
    </tbody>
</table>

</body>
</html>






