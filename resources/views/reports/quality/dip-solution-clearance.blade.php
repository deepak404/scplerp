<!DOCTYPE html>
<html>
<head>
	<title>Dip Solution Clearance</title>
</head>

<style>
html{
	font-family: "Helvetica Neue", Arial, sans-serif;
}
table{
	width: 100%;
	
}

table, th, td{
	border:1px solid #000000; 
	border-collapse: collapse;
}

.no-border{
	border-style: none;
}

.br-border{
	border-bottom-style: none;
	border-right-style: none;
}
.bl-border{
	border-bottom-style: none;
	border-left-style: none;
}

.scpl-style, .title-style{
	font-size: 20px;
	padding: 20px 5px; 
	text-align: center;
}
.title-style{
	width: 550px;
	/*border-right: 1px solid; */
}

.solution1{
	padding: 10px;

	font-size: 14px;
}

.solution2 {
	padding-top: 10px;
	padding-bottom: 40px;
	padding-left: 10px;
	font-size: 14px;
}
.solution3{

	padding-top: 5px;
	padding-bottom: 5px;
	padding-left: 10px;
	font-size: 11px;
}
.sol-input{
	font-weight: 300;
	padding-left: 5px; 
}
.stamp, .sign{
	padding-top: 70px;
	text-align: center;
	font-size: 14px;
}
.remarks{
	padding-top: 5px;
	padding-left: 5px;
padding-bottom: 80px;

	font-size: 14px;
}
.bold{
	font-weight: 600;
}
.rt1, .rt1-uom{
	text-align: center;
	font-weight: 600;
	font-size: 12px;
}
.rt-parameter{
	width: 150px;
	font-weight: 600;
	font-size: 14px;
}
.center-aligh{
	text-align: center;
}

.rt1-uom{
width: 40px;
}

.parameter-table-head{
	text-align: center;
	font-size: 12px;
	padding: 8px;
}
.parameter-table-slno, .parameter-table-data1{
	text-align: center;
	font-size: 14px;
	padding: 7px;
}

.parameter-table-data{
	font-size: 14px;
	padding: 7px;
}
.test-spacing{
	padding-top:30px;
}

.sub-table-head{
	padding-top: 5px;
	padding-bottom: 5px;
	font-size:12px;
}
</style>

<body>
	<table>
		<thead>
			<tr>
				<th  class="scpl-style" colspan="2">SCPL ERP</th>
				<th  class="title-style" colspan="9">DIP SOLUTION CLEARANCE</th>
			</tr>
		<tbody>

			<?php $solution = \App\DipSolution::where('name', $clearance->name)->first(); ?>
			<tr>
				<td colspan="6" class="solution1 no-border"><strong>Solution Name: </strong><span class="sol-input">{{$clearance->name}}</span></td>
				{{--<td colspan="5"></td>--}}
				<td colspan="5" class="solution1 no-border"></td>
			</tr>
			<tr>
				<td colspan="6"  class="solution1 no-border"><strong>Solution code: </strong><span class="sol-input">{{$solution->code}}</span></td>
				{{--<td colspan="5"></td>--}}
				<td colspan="5" class="solution1 no-border"></td>
			</tr>
			<tr>
				<td colspan="6" class="solution1 no-border"><strong>Solution Type: </strong><span class="sol-input">{{$solution->type}}</span></td>
				<td colspan="5" class="solution1 no-border"></td>
			</tr>
			<tr>
				<td colspan="6" class="solution1 bold br-border">Process Details:</td>
				<td colspan="5" class="left-border bl-border"></td>
			</tr>
			<tr>
				<td colspan="6"  class="solution1 no-border"><strong>Start Date :</strong><span class="sol-input">{{date('d-m-Y', strtotime($clearance->start_date))}}</span></td>
				<td colspan="5" class="solution1 no-border"><strong>Start Time :</strong><span class="sol-input">{{date('H:i', strtotime($clearance->start_date))}}</span></td>
			</tr>
			<tr>
				<td colspan="6"  class="solution1 no-border"><strong>Completion Date :</strong><span class="sol-input">{{date('d-m-Y', strtotime($clearance->end_date))}}</span></td>
				<td colspan="5" class="solution1 no-border"><strong>Completion Time :</strong><span class="sol-input">{{date('H:i', strtotime($clearance->end_date))}}</span></td>
			</tr>
			<tr>
				<td colspan="6"  class="solution2 no-border"><strong>Process Remarks:</strong></td>
				<td colspan="5" class="solution2 no-border"><strong>Expiry Date:</strong> <span class="sol-input">{{date('d-m-Y', strtotime($clearance->expiry_date))}}</span></td>
			</tr>
			<tr class="bold ">
				<td colspan="1" class="parameter-table-head">Sl.no </td>
				<td colspan="6" class="parameter-table-head">Parameter</td>
				<td colspan="4" class="parameter-table-head">Result</td>
			</tr>
			<tr>
				<td colspan="1" class="parameter-table-slno">1 </td>
				<td colspan="6" class="parameter-table-data">Diluted on</td>
				<td colspan="4" class="parameter-table-data1">{{date('d-m-Y', strtotime($clearance->start_date))}}</td>
			</tr>

			<?php $count = 1; ?>
			@foreach(explode(',', $clearance->prepared_by) as $op)
				<tr>
					<td colspan="1" class="parameter-table-slno">{{++$count}} </td>
					<td colspan="6" class="parameter-table-data">Sample collected by</td>
					<td colspan="4" class="parameter-table-data1">{{$op}}</td>
				</tr>
			@endforeach

			<tr>
				<td colspan="3" class="solution1 no-border test-spacing">iv) Test Report </td>
				<td colspan="4" class="solution1 no-border bold test-spacing">Inspection Doc:<span class="sol-input">-</span></td>
				<td colspan="4" class="solution1 no-border bold test-spacing">Inspection Date: <span class="sol-input">{{date('d-m-Y', strtotime($clearanceResult->tested_date))}}</span></td>
			</tr>
			<tr >
				<td colspan="1" rowspan="2" class="rt1-sl rt1 center-aligh bold">S.No </td>
				<td colspan="2" rowspan="2" class="rt-parameter center-aligh">Parameter</td>
				<td colspan="1" rowspan="2" class="rt1-uom center-aligh">UOM</td>
				<td colspan="1" rowspan="2" class="rt1 center-aligh">Batch No</td>
				<td colspan="2" rowspan="1" class="rt1 center-aligh">Specification</td>
				<td colspan="3" rowspan="1" class="rt1 center-aligh">Actual Observation</td>
				<td colspan="1" rowspan="2" class="rt1 center-aligh">Status</td>
			</tr>
			<tr>
				
				<td colspan="1" rowspan="1" class="rt1 sub-table-head">LSL</td>
				<td colspan="1" rowspan="1" class="rt1 sub-table-head">USL</td>
				<td colspan="1" rowspan="1" class="rt1 sub-table-head">Value 1</td>
				<td colspan="1" rowspan="1" class="rt1 sub-table-head">Value 2</td>
				<td colspan="1" rowspan="1" class="rt1 sub-table-head">Value 3</td>
				
			</tr>

            <?php $count= 0; $samples = json_decode($clearanceResult->sample); //dd($samples); ?>
			@foreach($samples as $parameter => $sample)
			<tr>
				<td colspan="1" class="solution3 center-aligh">{{++$count}}</td>
				<td colspan="2" class="solution3">{{$parameter}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->uom}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$clearance->batch_no}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->lsl}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->usl}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->sample1}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->sample2}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->sample3}}</td>
				<td colspan="1" class="solution3 center-aligh">{{$sample->result}}</td>
			</tr>
			@endforeach

			<tr>
				<td colspan="5"  class="remarks">Test Remarks </td>
				<td colspan="4"  class="stamp">Space for stamps</td>
				<td colspan="2"  class="sign">QC Incharge</td>
			</tr>
		</tbody>
	</table>
</body>
</html>