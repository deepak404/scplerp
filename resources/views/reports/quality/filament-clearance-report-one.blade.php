<html>
<head>
	<meta charset="UTF-8">
	<title>Filament clearance report</title>
	<style>
		table,td,th{
			border: 1px solid #333;
			border-collapse: collapse;
		}
		.no-border>tr>td{
			border: none;
			padding: 4px 5px;
		}
		.center>tr>td{
			text-align: center;
        	padding: 2px;
		}
		html{
			margin: 10px;
		}
		body
        {
            counter-reset: Serial;           /* Set the Serial counter to 0 */
        }
        .tested-pallet>tr td:first-child:before
        {
          counter-increment: Serial;      /* Increment the Serial counter */
          content: counter(Serial); /* Display the counter */
        }
        .tested-pallet>tr>td{
        	text-align: center;
        	padding: 2px;
        }
        th {
        	text-align: center;
        }
        .approved-tr>tr>td{
			padding: 4px;
        }
        .page-break {
		    page-break-after: always;
		}
		.pallet-entry>td{
			padding: 2px;
		}
	</style>
</head>
<body>
	<div class="master">
		<table style="width: 100%; margin-bottom:-1px;">
			<thead>
				<tr>
					<th colspan="3" rowspan="3" style="width: 20%; text-align: center;">SCPL ERP</th>
					<th colspan="3" rowspan="3" style="width: 50%; text-align: center;">FILLAMENT LOT CLEARANCE</th>
					<td>FOR/PRO/TWG/11</td>
				</tr>
				<tr>
					<td>Rev.no & Date: 01/{{date("d.m.Y")}}</td>
				</tr>
				<tr>
					<td>Page.No:1 of 2</td>
				</tr>
			</thead>
		</table>
		<table style="width: 100%; margin-top:-1px;">
			<tbody>
				<tr>
					<td style="width: 40%;"></td>
					<td style="width: 10%;">LOT.NO</td>
					<td style="width: 10%;">{{$filamentLot['lot_batch_no']}}</td>
					<td style="width: 40%;"></td>
				</tr>
			</tbody>
		</table>
		<table style="width: 100%; margin-top:-1px;">
			<tbody class="no-border">
				<tr>
					<td style="width: 50%; vertical-align: text-top;">FROM:
						<table style="width: 100%;margin-top: 2%; margin-bottom: 2%;">
							<tbody  class="no-border">
								<tr>
									<td style="width: 30%;">Supplier Type :</td>
									<td>{{$filamentLot['supplier_type']}}</td>
								</tr>
								<tr>
									<td style="width: 30%;">Type :</td>
									<td>{{$filamentLot['type']}}</td>
								</tr>
								<tr>
									<td style="width: 30%;">Denier :</td>
									<td>{{$filamentLot['denier']}}</td>
								</tr>
								<tr>
									<td style="width: 30%;">N.Wt / Ch :</td>
									<td>{{$filamentLot['net_wt']}} Kgs</td>
								</tr>
								<tr>
									<td style="width: 30%;">Tube Colour :</td>
									<td>{{$filamentLot['tube_color']}}</td>
								</tr>
								<tr>
									<td style="width: 30%;">Tube I.D :</td>
									<td>{{$filamentLot['tube_id']}}</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="width: 50%;">TO:
						<table style="width: 100%;margin-top: 2%; margin-bottom: 2%;">
							<tbody class="no-border">
								<tr>
									<td style="width: 50%;">Inv.No :</td>
									<td>{{$filamentLot['inv_no']}}</td>
								</tr>
								<tr>
									<td style="width: 50%;">Inv.Date :</td>
									<td>{{$filamentLot['inv_date']}}</td>
								</tr>
								<tr>
									<td style="width: 50%;">Date of Arraival :</td>
									<td>{{$filamentLot['arrival_date']}}</td>
								</tr>
								<tr>
									<td style="width: 50%;">Total No.of Patllets :</td>
									<td>{{$filamentLot['pallet_count']}}</td>
								</tr>
								<tr>
									<td style="width: 50%;">Total Net Wt,in kgs :</td>
									<td>{{$filamentLot['net_wt']}} kgs</td>
								</tr>
								<tr>
									<td style="width: 50%;">material Code :</td>
									<td>{{$filamentLot['material_code']}}</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<?php if (!empty($reportResult->parameter)): ?>
		<table style="width: 100%; margin-top:-1px;" class="tested-parameter">
			<thead>
				<tr>
					<th>S.No</th>
					<th>Parameters</th>
					<th>Actual Results</th>
					<th>SCPL Specification</th>
					<th>status</th>
				</tr>
			</thead>
			<tbody class="center">
				<?php $count=1; foreach ($reportResult->parameter as $key => $value): ?>
					<tr>
						<td>{{$count}}</td>
						<td>{{$value}}</td>
						<td>{{$reportResult->actual[$key]}}</td>
						<td>{{$reportResult->scpl[$key]}}</td>
						<td>{{$reportResult->status[$key]}}</td>
					</tr>
				<?php $count+=1; endforeach ?>
			</tbody>
		</table>
		<?php endif; ?>
		<table style="width: 100%; margin-top:-1px;">
			<tbody class="approved-tr">
				<tr>
					<td>Tested by:{{$filamentLot['tested_by']}}</td>
					<?php if ($filamentLot['clearance_status'] == 1): ?>
						<td rowspan="2" style="width: 20%; text-align: center;">CLEARED</td>
						<?php else: ?>
						<td rowspan="2" style="width: 20%; text-align: center;">REJECTED</td>
					<?php endif ?>
				</tr>
				<tr>
					<td>Date     :{{$filamentLot['sample_date']}}</td>
				</tr>
			</tbody>
		</table>
		<table style="width: 100%;  margin-top:-1px;">
			<thead>
				<tr>
					<th>S.No</th>
					<th>P.S.No</th>
					<th>Code</th>
					<th>Pallet.No</th>
					<th>Lea Weight</th>
					<th>Actual Denier</th>
				</tr>
			</thead>
			<tbody class="tested-pallet">
				@foreach($testedPallets as $key => $value)
				<tr>
					<td></td>
					<td>{{$value->ps_no}}</td>
					<td>{{$value->code}}</td>
					<td>{{$value->pallet_no}}</td>
					<td>{{$value->lea_weight}}</td>
					<td>{{$value->actual}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="page-break"></div>
	<div class="pallets">
		<table style="width: 100%; border: none; margin-bottom:10px;">
			<?php $count=1; $palletData=[]; ?>
			<tbody>
				<tr style="border: none;">
					<td style="border: none;">
						<table style="width: 90%;">
							<thead>
								<tr>
									<th>SI.No</th>
									<th>Pallet.NO</th>
									<th>Gross Weight</th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i=1; $i <= 40; $i++) { ?>
								<tr class="pallet-entry">
									<td style="text-align: center;">{{$count}}</td>
									<?php if (!is_null($palletEntrys)): ?>
									<?php $val=$count-1; if (array_key_exists($val, $palletEntrys->pallet_no)): ?>
										<?php $palletData[] = $palletEntrys->weight[$val]; ?>
										<td>{{$palletEntrys->pallet_no[$val]}}</td>
										<td>{{$palletEntrys->weight[$val]}}</td>
										<?php else: ?>
											<td></td>
											<td></td>
									<?php endif ?>
										<?php else: ?>
										<td></td>
										<td></td>
									<?php endif ?>
								</tr>
								<?php $count += 1; } ?>
							</tbody>
						</table>
					</td>
					<td style="border: none;">
						<table style="width: 90%;">
							<thead>
								<tr>
									<th>SI.No</th>
									<th>Pallet.NO</th>
									<th>Gross Weight</th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i=1; $i <= 40; $i++) { ?>
								<tr class="pallet-entry">
									<td style="text-align: center;">{{$count}}</td>
									<?php if (!is_null($palletEntrys)): ?>
									<?php $val=$count-1; if (array_key_exists($val, $palletEntrys->pallet_no)): ?>
										<?php $palletData[] = $palletEntrys->weight[$val]; ?>
										<td>{{$palletEntrys->pallet_no[$val]}}</td>
										<td>{{$palletEntrys->weight[$val]}}</td>
										<?php else: ?>
											<td></td>
											<td></td>
									<?php endif ?>
										<?php else: ?>
										<td></td>
										<td></td>
									<?php endif ?>
								</tr>
								<?php $count += 1; } ?>
							</tbody>
						</table>
					</td>
					<td style="border: none;">
						<table style="width: 90%;">
							<thead>
								<tr>
									<th>SI.No</th>
									<th>Pallet.NO</th>
									<th>Gross Weight</th>
								</tr>
							</thead>
							<tbody>
								<?php for ($i=1; $i <= 40; $i++) { ?>
								<tr class="pallet-entry">
									<td style="text-align: center;">{{$count}}</td>
									<?php if (!is_null($palletEntrys)): ?>
									<?php $val=$count-1; if (array_key_exists($val, $palletEntrys->pallet_no)): ?>
										<?php $palletData[] = $palletEntrys->weight[$val]; ?>
										<td>{{$palletEntrys->pallet_no[$val]}}</td>
										<td>{{$palletEntrys->weight[$val]}}</td>
										<?php else: ?>
											<td></td>
											<td></td>
									<?php endif ?>
										<?php else: ?>
										<td></td>
										<td></td>
									<?php endif ?>
								</tr>
								<?php $count += 1; } ?>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php if (count($palletData) > 0): ?>
		<span>MAX Wt: {{max($palletData)}}</span>
		<span style="margin-left:20px;">MIN Wt: {{min($palletData)}}</span>
	<?php endif; ?>
</body>
</html>
