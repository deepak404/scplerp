<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<style>
		html{
			margin: 20px;
			font-family: "Helvetica";
		}
		table{
			width: 100%;
		}
		table, th, td {
		    border: 1px solid black;
		    border-collapse: collapse;
		    text-align: center;
		    font-size: 13px;
		    padding: 0px;
		}
		td{
			padding: 2px;
		}
		.border-remove{
			border: none !important;
			vertical-align: bottom;
			text-align: left;
			padding: 5px;
		}
	</style>
</head>
<body>
	<table>
		<thead>
			<tr>
				<th rowspan="3">SCPL ERP</th>
				<th colspan="7" rowspan="3">FINISHED GOODS PACKING DETAILS</th>
				<th colspan="3" style="text-align: left; padding-left: 5px;">FOR / MKT / PD</th>
			</tr>
			<tr>
				<th colspan="3" style="text-align: left; padding-left: 5px;">Rev.No./Date:00/</th>
			</tr>
			<tr>
				<th colspan="3" style="text-align: left; padding-left: 5px;">Approved By: GM</th>
			</tr>
			<tr>
				<th colspan="3" rowspan="2" class="border-remove">Type of<br>Package: <span style="margin-left: 10px;">{{$master['package_type']}}</span></th>
				<th colspan="5" rowspan="2" class="border-remove">Count : <span style="margin-left: 5px;">{{$master->itemMaster['material']}}</span></th>
				<th colspan="3" class="border-remove">Date : {{date('d.m.Y',strtotime($master['packed_date']))}}</th>
			</tr>
			<tr>
				<th colspan="3" class="border-remove">Box Type : {{$master['box_type']}}</th>
			</tr>
			<tr>
				<th>Case No.</th>
				<th>D.No.</th>
				<th style="font-size: 9px;">No.of<br>Bobbins<br>or<br>cheeses</th>
				<th colspan="2">N.Wt. With<br>Empty</th>
				<th>Total<br>Wt.</th>
				<th>Empty<br>Wt.</th>
				<th>N.Wt.<br>in Kgs.</th>
				<th>Com.<br>N.Wt.</th>
				<th>G.Wt.</th>
				<th>Spindle<br>Nos.</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $key => $value) { ?>
			<tr>
				<td>{{$value['case_no']}}</td>
				<td>{{$value['doff_no']}}</td>
				<td>{{$value['bobbin_count']}}</td>
				@foreach($value['packed_bobbin'] as $bobbin)
				<td>{{$bobbin['b_wt']}}</td>
				@endforeach
				<td>{{$value['total_weight']}}</td>
				<td>{{$value['bobbin_weight']}}</td>
				<td>{{$value['net_weight']}}</td>
				<td>{{$value['commercial_weight']}}</td>
				<td>{{$value['gross_weight']}}</td>
				<td>
					<?php for ($i=0; $i < $value['bobbin_count']; $i++) { ?>
						<span>{{$value['packed_bobbin'][$i]['b_no'] ?? 0}} |</span>
					<?php } ?>
					<!-- <span style="float: right; padding-right: 10px;">{{$value['packed_bobbin'][1]['b_no'] ?? 0}}</span> -->
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</body>
</html>
