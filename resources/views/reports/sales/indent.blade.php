<html xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:x="urn:schemas-microsoft-com:office:excel"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=ProgId content=Excel.Sheet>
    <meta name=Generator content="Microsoft Excel 15">
    <link rel=File-List href="demo.fld/filelist.xml">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->


    <style>
        html{
          font-family: "Helvetica Neue", Arial, sans-serif;
        }
        #content-tr>td{
            text-align: center !important;
        }

        table tr>td{
            border: 1px solid black;
        }
        table,tr,th,td{
          border-collapse: collapse !important;
        }

        table.table-bordered{
            border:1px solid black;
            margin-top:20px;
        }
        table.table-bordered > thead > tr > th{
            border:1px solid black;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black;
            padding: 3px 8px;
        }

        table.table-bordered > tbody > tr > td > p{
            margin: 0px;
            font-size: 10px;
        }


        #indent-details-table > tbody > tr > td{
            text-align: center;
        }

        small{
            font-size: 9px;
        }
    </style>
</head>

<body link="#0563C1" vlink="#954F72">


<?php $fmt = new NumberFormatter($locale = 'en_IN', NumberFormatter::DECIMAL);?>
<?php $integrateTax = 0; ?>
<div id="demo_31861" align=center x:publishsource="Excel">

    <p style="text-align: center; font-size: 12px;"><strong>{{$indentMaster->indent_type}}</strong></p>
    <table class="table table-bordered" border=0 cellpadding=0 cellspacing=0 width=1543 style='border-collapse:
 collapse;table-layout:fixed;width:100%;'>
        <col width=51 style='mso-width-source:userset;mso-width-alt:1621;width:38pt'>
        <col width=123 style='mso-width-source:userset;mso-width-alt:3925;width:92pt'>
        <col width=437 style='mso-width-source:userset;mso-width-alt:13994;width:328pt'>
        <col width=87 span=3 style='width:65pt'>
        <col width=89 style='mso-width-source:userset;mso-width-alt:2858;width:67pt'>
        <col width=67 style='mso-width-source:userset;mso-width-alt:2133;width:50pt'>
        <col width=85 style='mso-width-source:userset;mso-width-alt:2730;width:64pt'>
        <col width=147 style='mso-width-source:userset;mso-width-alt:4693;width:110pt'>
        <col width=0 style='display:none;mso-width-source:userset;mso-width-alt:42'>
        <col width=76 style='mso-width-source:userset;mso-width-alt:2432;width:57pt'>
        <col width=87 style='width:65pt'>
        <col width=120 style='mso-width-source:userset;mso-width-alt:3840;width:90pt'>
        <col width=0 style='display:none;mso-width-source:userset;mso-width-alt:597'>



        <tr height = 1000>
            <td colspan=9 rowspan=6 style="height: 40pt; padding-bottom: 0;">
                <p><strong>Shakti Cords Pvt. Ltd</strong></p>
                <p>CS 17&18, SIDCO INDUSTRIAL ESTATE</p>
                <p>KAPPALUR</p>
                <p>MADURAI - 625008</p>
                <p>GSTIN/UIN : 33AAHCS3989E1Z4</p>
                <p>State Name : Tamil Nadu, Code : 33</p>
                <p>CIN : U17297TN2003PTC051243</p>
                <p>Email : scplstores@indtextile.com, purchase@indtextile.com</p>
            </td>
            {{--Quotation Number--}}
            <td colspan=4 rowspan=2 style="height: 10pt;">
                <p>Quotation No.</p>
                <p><strong>{{$indentMaster->quotation_number}}</strong>
                </p>
            </td>
            {{--Dated--}}
            <td colspan=4 rowspan=2>
                <p>Dated</p>
                <p><strong>{{date('d-M-y', strtotime($indentMaster->dated_on))}}</strong>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td colspan=4 rowspan=2></td>
            <td colspan=4 rowspan=2>
                <p>Modes/Terms of Payment</p>
                {{--<p><strong>{{$indentMaster->modes_of_payment}}</strong><p>--}}
                <p><strong>{{$indentMaster->payment_mode}}</strong><p>
                </p>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td colspan=4 rowspan=2 height="40px">
                <p>Buyer's Ref/Order No</p>
                <p><strong>{{$indentMaster->order_ref_no}}</strong></p>
            </td>
            <td colspan=4 rowspan=2>
                <p>Other Ref No</p>
                <p><strong>{{$indentMaster->other_ref_no}}</strong></p>
            </td>
        </tr>
        <tr>
        </tr>


        <tr height=10>
            {{--client address--}}
            <td colspan=9 rowspan=4 height=100 class=xl65 width=1113 style='
  width:834pt;height:10.0pt;'>
                <small>Invoice To</small>
                <p><strong>{{$businessPartner['name']}}</strong></p>
                <p>{{$businessPartner['address']}}</p>
                <p>District : {{$businessPartner['district']}}</p>
                <p>{{$businessPartner['pincode']}}</p>
                <p>GST/UIN : {{$businessPartner['gst']}}</p>
                <p>StateName : {{$businessPartner['state']}}, Code : 033</p>
            </td>

            {{--Dispatch Through below--}}
            <td colspan=4 rowspan=2 class=xl65 width=223 style='width:167pt'>
                <p>Dispatch through</p>
                <p><strong>{{$indentMaster->dispatcher}}</strong></p>
            </td>
            {{--destination below--}}
            <td colspan=4 rowspan=2 class=xl65 width=207 style='width:155pt;'>
                <p>Destination</p>
                <p><strong>{{$indentMaster->destination}}</strong></p>
            </td>
        </tr>
        <tr height=10>
        </tr>

        {{--Terms of delivery below--}}
        <tr height=32>
            <td colspan=8 rowspan=2 height=42 class=xl65 style="height: 60pt;" >
                <p>Terms of Delivery</p>
                <p><strong>{{$indentMaster->terms_of_delivery}}</strong></p>
            </td>
            {{--<td colspan=4 rowspan=2 class=xl65></td>--}}
        </tr>
        <tr height=32 >
        </tr>

    </table>

    <table class="table table-bordered" id="indent-details-table" style="margin-top: -2px; width: 100%;">



        <tr height=35 style='mso-height-source:userset;height:26.0pt' id="content-tr">
            <td height=35 style='height:26.0pt;font-size: 10px; width: 10px;'>S.No</td>
            <td colspan=2 style="font-size: 10px; text-align: center">No &amp; Kind of <br> Pkgs.</td>
            <td colspan=6 class=xl65 style="font-size: 10px; text-align: center;">Description of Goods</td>
            <td style="font-size: 10px; text-align: center;">HSN/SAC</td>
            <td style="font-size: 10px; text-align: center;">GST RATE</td>
            <td style="font-size: 10px; text-align: center;" colspan=2>DUE ON</td>
            <td style="font-size: 10px; text-align: center;" colspan="2">Quantity <br>(Kgs.)</td>
            <td style="font-size: 10px; text-align: center;" >Rate</td>
            <td style="font-size: 10px; text-align: center;">Per</td>
            <td style="font-size: 10px; text-align: center;" colspan=4 class=xl65>Amount</td>
            {{--<td></td>--}}
        </tr>

        <?php $indentCount = 1;
            $totalAmount = 0;
            $totalQuantity = 0;

            $sgst = 0;
            $cgst = 0;
            $totalGst = 0;

        ?>
        @foreach($indentDetails as $indentDetail)

        <tr height=21 style='height:16.0pt'>
            <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'><p>{{$indentCount}}</p></td>
            <td colspan=2 class=xl65><p>{{$indentDetail->package_kind}}</p></td>
            <td colspan=6><p style="text-align: left;"><strong>{{$indentDetail->saleOrder->material_name}}</strong></p></td>
            <td colspan=1><p>{{$indentDetail->hsn_sac}}</p></td>
            <td colspan=1><p>{{$indentDetail->gst_rate}}%</p></td>
            <td colspan=2 style="padding: 3px;"><p>{{date('d-M-y', strtotime($indentDetail->due_on))}}</p></td>
            <td colspan=2><p><strong>{{$fmt->format($indentDetail->quantity)}}</strong></p></td>
            <td colspan=1><p>{{$indentDetail->rate_per_kg}}</p></td>
            <td colspan=1><p>Kg.</p></td>
            <td colspan=4><p><strong>{{$fmt->format(round($indentDetail->quantity * $indentDetail->rate_per_kg, 2))}}</strong></p></td>
        </tr>
            <?php $indentCount++;
            $totalAmount += $indentDetail->quantity * $indentDetail->rate_per_kg;
            $totalQuantity += $indentDetail->quantity;
            $integrateTax += $indentDetail->gst_rate;
            ?>

        @endforeach

        {{--insurance below--}}

        <?php
//            $integrateTax = (($integrateTax/$indentCount)/100) * $totalAmount;
//            $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
//            $totalBillable = $totalAmount + $insuranceCost + $integrateTax;


        // {{--insurance below--}}




            foreach ($indentDetails as $indentDetail){
                $itemMaster = $indentDetail->itemMaster;

                $handlingCharges = ($indentMaster->handling_charges/$totalAmount) * ($indentDetail->quantity * $indentDetail->rate_per_kg);
                $insuranceCost = ((($indentMaster->transit_insurance/100) * $totalAmount)/$totalAmount) * ($indentDetail->quantity * $indentDetail->rate_per_kg);
//                dd(($indentDetail->quantity * $indentDetail->rate_per_kg));
                if($indentMaster->delivery_type == 1){
                    //Within Tamil Nadu
//                    $gstSum = $itemMaster->cgst + $itemMaster->igst;
//                    $totalGst += $gstSum/100 * (($indentDetail->quantity * $indentDetail->rate_per_kg) + $handlingCharges + $insuranceCost);

                    $cgst += $itemMaster->cgst/100 * (($indentDetail->quantity * $indentDetail->rate_per_kg) + $handlingCharges + $insuranceCost);
                    $sgst += $itemMaster->sgst/100 * (($indentDetail->quantity * $indentDetail->rate_per_kg) + $handlingCharges + $insuranceCost);
//
//                    $totalGst += ($cgst + $sgst);

                }else{
                    //Outside Tamil Nadu


                    $totalGst += $itemMaster->igst/100 * (($indentDetail->quantity * $indentDetail->rate_per_kg) + $handlingCharges + $insuranceCost);

                }
            }

        if($indentMaster->delivery_type == 1){

            $handlingCharges = $indentMaster->handling_charges;
            $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
            $totalBillable = $totalAmount + $insuranceCost + $cgst +$sgst;

        }else{

            $handlingCharges = $indentMaster->handling_charges;
            $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;

            $totalBillable = $totalAmount + $insuranceCost + $totalGst;
        }



            $currencyToWords = new App\CurrencyToWords();
            $convertedCurrency = $currencyToWords->convert($totalBillable);




        if(is_numeric( $totalBillable ) && floor( $totalBillable ) != $totalBillable){
            $decimalValue = $totalBillable - floor($totalBillable);

            $decimalWords = $currencyToWords->convert(round($decimalValue, 2) * 100);

            $convertedCurrency = str_replace(' and ','',$convertedCurrency).' rupees and'. $decimalWords. ' paise only.';
        }






        ?>

        <tr height=21 style='height:16.0pt'>
            <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'></td>
            <td colspan=2 class=xl65></td>
            @if($indentMaster->delivery_type == 1)
            <td colspan=6 style="text-align: right;"><p><strong>Transit Insurance - Intrastate</strong></p></td>
            @else
                <td colspan=6 style="text-align: right;"><p><strong>Transit Insurance - Interstate</strong></p></td>
            @endif
            <td colspan=1></td>
            <td colspan=1 class=xl65></td>
            <td colspan=2></td>
            <td colspan=2></td>
            <td colspan=1><p>{{$indentMaster->transit_insurance}}</p></td>
            <td colspan=1><p>%</p></td>
            <td colspan=4><p><strong>{{$fmt->format($insuranceCost)}}</strong></p></td>
        </tr>

        @if($indentMaster->handling_charges != 0)

        <tr height=21 style='height:16.0pt'>
            <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'></td>
            <td colspan=2 class=xl65></td>
            <td colspan=6 style="text-align: right;"><p><strong>Handling Charges</strong></p></td>
            <td colspan=1></td>
            <td colspan=1 class=xl65></td>
            <td colspan=2></td>
            <td colspan=2></td>
            <td colspan=1><p></p></td>
            <td colspan=1><p>Rs.</p></td>
            <td colspan=4><p><strong>{{$fmt->format($indentMaster->handling_charges)}}</strong></p></td>
        </tr>

        @endif



        {{--Integrated Tax below--}}

        @if($indentMaster->delivery_type == 2)

        <tr height=21 style='height:16.0pt'>
            <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'></td>
            <td colspan=2></td>
            <td colspan=6><p style="text-align: right;"><strong>Integrated Tax</strong></p></td>
            <td colspan=1></td>
            <td colspan=1></td>
            <td colspan=2></td>
            <td colspan=2></td>
            <td colspan=1></td>
            <td colspan=1></td>
            <td colspan=4><p><strong>{{$fmt->format($totalGst)}}</strong></p></td>
        </tr>

        @else

            <tr height=21 style='height:16.0pt'>
                <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'></td>
                <td colspan=2></td>
                <td colspan=6><p style="text-align: right;"><strong>SGST</strong></p></td>
                <td colspan=1></td>
                <td colspan=1></td>
                <td colspan=2></td>
                <td colspan=2></td>
                <td colspan=1></td>
                <td colspan=1></td>
                <td colspan=4><p><strong>{{$fmt->format($sgst)}}</strong></p></td>
            </tr>


            <tr height=21 style='height:16.0pt'>
                <td height=21 colspan=1 style='height:16.0pt;mso-ignore:colspan'></td>
                <td colspan=2></td>
                <td colspan=6><p style="text-align: right;"><strong>CGST</strong></p></td>
                <td colspan=1></td>
                <td colspan=1></td>
                <td colspan=2></td>
                <td colspan=2></td>
                <td colspan=1></td>
                <td colspan=1></td>
                <td colspan=4><p><strong>{{$fmt->format($cgst)}}</strong></p></td>
            </tr>


            @endif

        {{--total below--}}

        <tr height=21 style='height:16.0pt'>
            <td colspan=1></td>
            <td colspan=2></td>
            <td colspan=6 style="font-size: 10px; text-align: right;"><strong>Total</strong></td>
            <td colspan=1></td>
            <td colspan=1></td>
            <td colspan=2></td>
            <td colspan=2><p><strong>{{$fmt->format($totalQuantity)}}</strong></p></td>
            <td colspan=1></td>
            <td colspan=1></td>
            <td colspan=4><p><strong>{{$fmt->format($totalBillable)}}</strong></p></td>
        </tr>

    </table>
    <table class="table table-bordered" style="margin-top: -2px; margin-bottom: 0px; width:100%;">


        <tr>
            <td colspan="17" rowspan="6">
                <small>Amount Chargeable (in words.)</small>
                <p><strong>{{$convertedCurrency}}</strong></p>

            </td>
        </tr>

        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>
        <tr></tr>



        <tr>
            <td rowspan="4" colspan="9">
                <small>Remarks</small>
                <p></p>
            </td>
            <td rowspan="4" colspan="8">
                <small>Company's Bank Details</small>
                <p>Bank Name : <strong>IDBI Bank</strong></p>
                <p>Acc Name : <strong>0388655000000028</strong></p>
                <p>Branch & IFSC Code : <strong>Madurai & IBKL0000388</strong></p>
            </td>

        </tr>

        <tr></tr>
        <tr></tr>
        <tr></tr>


        <tr>
            <td rowspan="4" colspan="9">
                <small>Declaration</small>
                <p>We the sellers and buyers confirm the above. Buyers to return duplicate copy duly signed.</p>
                <p>Any Statuatory variation in Govt. Levies on buyers account</p>
            </td>
            <td rowspan="4" colspan="8">
                <p style="text-align: right;"><strong>For shakti Cords Pvt. Ltd.</strong></p>

                <p style="text-align: right; vertical-align: bottom; margin-bottom: -30px;"><strong>Authorised Signatory</strong></p>

            </td>
        </tr>

        <tr></tr>
        <tr></tr>
        <tr></tr>

    </table>

    <small style="text-align: center; font-size: 7px;">SUBJECT TO MADURAI JURISDICTION</small>


</div>

</body>

</html>
