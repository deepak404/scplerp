<html>
<head>
	<meta charset="UTF-8">
	<title>Pending Contracts</title>
	<style>
		html{
			margin: 15px;
			font-size: 10px;
		}
		table,td,th{
			border: 1px solid #333;
			border-collapse: collapse;
		}
		thead>tr>th{
			text-align: center;
		}
		td,th{
			padding: 10px;
		}
		.title{
			font-size: 16px;
			font-weight: 600;
		}
	</style>
</head>
<body>
	<table style="width: 95%;">
		<thead>
			<tr>
				<th class="title" colspan="2">SCPL ERP</th>
				<?php if ($type == 'group'): ?>
				<th class="title" colspan="5">QUALITY WISE PENDING CONTRACTS</th>
				<?php else: ?>
				<th class="title" colspan="5">CONTRACTS BY CUSTOMERS</th>
				<?php endif ?>
				<th colspan="2" style="text-align: left;">Upto : {{date('d-m-Y',strtotime($date))}}</th>
			</tr>
			<?php if ($type == 'group'): ?>
			<tr>
				<th colspan="9">CODE</th>
			</tr>
			<?php else: ?>
			<tr>
				<?php foreach ($pendingOrders as $key => $value): ?>
				<th colspan="9">{{strtoupper($key)}}</th>
				<?php endforeach ?>
			</tr>
			<?php endif ?>
			<tr>
				<th colspan="2" style="width: 100px;">NAME</th>
				<th style="width: 10px;">CONTRACT</th>
				<th style="width: 10px;">CONT-DATE</th>
				<th style="width: 10px;">RATE</th>
				<th style="width: 10px;">QTY</th>
				<th style="width: 10px;">DELIVERED</th>
				<th style="width: 10px;">BALANCE</th>
				<th style="width: 10px;">DELI-ST</th>
			</tr>
		</thead>
		<tbody>
			<?php if ($type == 'group'): ?>
			<?php foreach ($pendingOrders as $key => $pending): ?>
				<?php if ($key != 'buffer'): ?>
					<tr>
						<th colspan="9" style="text-align: center;">{{$key}}</th>
					</tr>
					<?php 
					usort($pending, function ($item1, $item2) {
						return $item1['name'] <=> $item2['name'];
					}); 
					$qty = 0;
					$bal = 0;
					$dis = 0;
					?>
					<?php foreach ($pending as $value): ?>
					<?php 
						$qty += $value['qty'];
						$bal += $value['balance'];
						$dis += $value['delivered'];
					?>

						<tr>
							<td colspan="2">{{$value['name']}}</td>
							<td>{{$value['contract']}}</td>
							<td>{{date('d-m-Y',strtotime($value['date']))}}</td>
							<td>{{$value['rate']}}</td>
							<td>{{$value['qty']}}</td>
							<td>{{$value['delivered']}}</td>
							<td>{{$value['balance']}}</td>
							<td>{{$value['state']}}</td>
						</tr>
					<?php endforeach ?>
						<tr>
							<th colspan="2">Total</th>
							<th></th>
							<th></th>
							<th></th>
							<th>{{$qty}}</th>
							<th>{{$dis}}</th>
							<th>{{$bal}}</th>
							<th></th>
						</tr>
				<?php endif; ?>
			<?php endforeach ?>
			<?php else: ?>
			<?php foreach ($pendingOrders as $key => $pending): ?>
				<?php if (array_key_exists('pending', $pending)): ?>
				<tr>
					<th colspan="9" style="text-align: center;">PENDING</th>
				</tr>
				<?php foreach ($pending['pending'] as $value): ?>
				<tr>
					<td colspan="2">{{$value['name']}}</td>
					<td>{{$value['contract']}}</td>
					<td>{{date('d-m-Y',strtotime($value['date']))}}</td>
					<td>{{$value['rate']}}</td>
					<td>{{$value['qty']}}</td>
					<td>{{$value['delivered']}}</td>
					<td>{{$value['balance']}}</td>
					<td>{{$value['state']}}</td>
				</tr>
				<?php endforeach ?>
				<?php else: ?>
				<tr>
					<th colspan="9" style="text-align: center;">DISPATCHED</th>
				</tr>
				<?php foreach ($pending['dispatch'] as $value): ?>
				<tr>
					<td colspan="2">{{$value['name']}}</td>
					<td>{{$value['contract']}}</td>
					<td>{{date('d-m-Y',strtotime($value['date']))}}</td>
					<td>{{$value['rate']}}</td>
					<td>{{$value['qty']}}</td>
					<td>{{$value['delivered']}}</td>
					<td>{{$value['balance']}}</td>
					<td>{{$value['state']}}</td>
				</tr>
				<?php endforeach ?>
				<?php endif ?>
			<?php endforeach ?>

			<?php endif ?>
		</tbody>
	</table>
</body>
</html>
