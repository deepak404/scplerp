<!DOCTYPE html>
<html>
<head>
    <title>Date Commitment Report</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    {{--<link rel="stylesheet" href="css/bootstrap.css">--}}



    <style>

        .table{
            margin: 10px auto;
            /*font-size: 11px !important;*/
            border-collapse: collapse;
        }

        .table .tr .td{
            height: 10px;
        }

        thead>tr>td{
            padding: 5px;
            font-size: 10px;
            font-weight: 600;
        }

        .table>tbody>tr>td{
            padding: 6px;
            font-size: 9px  !important;
            font-weight: 600;
        }

        .party-name{
            padding-top: 10px;
        }

        .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid #333;
        }

        thead>tr>td{
            /*font-size: 9px;*/
        }
        th{
            font-size: 16px;
            font-weight: 600;
        }
    </style>
</head>
<body>


<table class="table table-bordered" style="width: 100%">
    <thead>
    <tr>
        <th rowspan="3" style="text-align: center; vertical-align: middle;">SCPL ERP</th>
        <th rowspan="2" colspan="6" style="text-align: center;">CORD SCHEDULE - DATE COMMITMENT</th>
        <td colspan="3">FORMAT NO</td>
    </tr>
    <tr>
        {{-- <th colspan="7" style="text-align: center">DIPPED CORDS</th> --}}
        <td colspan="3">REV. NO:</td>
    </tr>
    <tr>
        <td colspan="6" style="text-align: center; font-size:13px;">FOR THE MONTH OF {{$filter}}</td>
        <td colspan="3">APPR BY:</td>
    </tr>
    <tr>
        <td colspan="10" style="text-align: center">PARTY</td>
        {{--<th>&nbsp;</th>--}}
        {{--<th>&nbsp;</th>--}}
        {{--<th>&nbsp;</th>--}}
    </tr>
    <tr>
        {{--<th style="text-align: center;">&nbsp;Party</th>--}}
        <td style="text-align: center;">Quality</td>
        <td style="text-align: center;">Order Quantity</td>
        <td style="text-align: center;">Contract No.</td>
        <td style="text-align: center;">Cont Date</td>
        <td style="text-align: center;">Rate</td>
        <td style="text-align: center;">Customer Date</td>
        <td style="text-align: center;">SCPL Conf. Date</td>
        {{-- <td style="text-align: center;">Qty Schd/ Lot @scpl</td> --}}
        <td style="text-align: center;">Dispatched Qty.</td>
        <td style="text-align: center;">Pending</td>
        <td style="text-align: center;">Dispatch Date</td>
    </tr>
    </thead>
    <tbody>
    @foreach($salesList as $bpName => $orders)
          <tr><th colspan="10" style="text-align: center; font-size: 12px; padding-top: 8px; padding-bottom: 8px;">{{$bpName}}</th>
          </tr>

          @foreach ($orders as $type => $sales)
            <tr><th colspan="10" style="text-align: center; font-size: 10px; padding-top: 8px; padding-bottom: 8px;">{{str_replace('_', ' ', $type)}}</th></tr>
            @foreach($sales as $sale)
            <tr>
                <td>{{$sale['material_name']}}</td>
                <td>{{$sale['order_quantity']}}</td>                
                <td>{{$sale['order_ref_no']}}</td>
                <td>{{$sale['dated_on']}}</td>
                <td>{{$sale['rate_per_kg']}}</td>
                <td>{{$sale['customer_date']}}</td>
                <?php if (!is_null($sale['scpl_confirmation_date'])): ?>
                <td>
                    <?php foreach (explode( ",",$sale['scpl_confirmation_date']) as $entryDate): ?>
                    {{$entryDate}}<br>
                    <?php endforeach; ?>
                </td>
                <?php else: ?>
                <td></td>
                <?php endif; ?>

                {{-- <td>&nbsp;</td> --}}
                <td>{{$sale['dispatch_quantity']}}</td>
                <td>{{$sale['pending_quantity']}}</td>
                <td>{{$sale['dispatch_date']}}</td>

            </tr>
            @endforeach
          @endforeach



    @endforeach


    </tbody>
</table>

</body>
</html>
