 <html>
<head>
	<meta charset="UTF-8">
	<title>Pending Contracts</title>
	<style>
		html{
			margin: 15px;
			font-size: 10px;
		}
		table,td,th{
			border: 1px solid #333;
			border-collapse: collapse;
		}
		td,th{
			padding: 10px;
		}
		thead>tr>th{
			text-align: center;
		}
	</style>
</head>
<body>
	<table style="width: 95%">
		<thead>
			<tr>
				<th colspan="1">SCPL ERP</th>
				<th colspan="7">PENDING CONTRACTS</th>
				<th colspan="2" style="text-align: left;">Upto : <br>{{date('d-m-Y',strtotime($date))}}</th>
			</tr>
			<tr>
				<th colspan="2" style="width: 140px;">NAME</th>
				<th>CODE</th>
				<th style="width: 10px;">CONTRACT</th>
				<th style="width: 10px;">RATE</th>
				<th style="width: 10px;">QTY</th>
				<th style="width: 10px;">DELIVERED</th>
				<th style="width: 10px;">BALANCE</th>
				<th style="width: 10px;">CONT-DATE</th>
				<th style="width: 10px;">DELI-ST</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($pendingOrders as $key => $value): ?>
        <?php if ($value['name'] != 'buffer'): ?>
          <tr>
            <td colspan="2">{{$value['name']}}</td>
            <td>{{$value['code']}}</td>
            <td>{{$value['contract']}}</td>
            <td>{{$value['rate']}}</td>
            <td>{{$value['qty']}}</td>
            <td>{{$value['delivered']}}</td>
            <td>{{$value['balance']}}</td>
            <td>{{date('d-m-Y',strtotime($value['date']))}}</td>
            <td>{{$value['state']}}</td>
          </tr>
        <?php endif; ?>
			<?php endforeach ?>
		</tbody>
	</table>
</body>
</html>
