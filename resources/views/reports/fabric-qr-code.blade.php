<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style>

        .bar-info{
            display: inline-block;
            /*vertical-align: middle;*/
        }

        img{
            margin-bottom: 0px;
            margin-top: 30px;
        }

        table>tbody>tr>td:first-child{
            margin-right: 300px;
        }

        p{
            margin-top: 3px !important;
            margin-bottom: 3px !important;
        }

        table{
            width: 100%;
        }

        .page-break {
            page-break-after: always;
        }


    </style>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"--}}
    {{--integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
</head>
<body>


<table class="table table-bordered">
    <thead>
    <tr>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php $scount = 1; ?>
    @foreach($files as $file)
        <tr>
            @foreach($file as $bar)
                <td>

                    <img src="fabricqrcode/{{$bar}}.png" alt="">
                    <?php $bar = explode('|', $bar); ?>

                    <div class="bar-info">
                        <p><b>Quality</b> : {{str_replace('-','/',$bar[0])}}</p>
                        <p><b>Quantity</b> : {{$bar[1]}}</p>
                        <p><b>Roll No</b> : {{$bar[2]}}</p>
                    </div>
                </td>
            @endforeach


        </tr>
        <?php $scount++; ?>

    @endforeach

    </tbody>
</table>


</body>
</html>