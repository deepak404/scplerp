<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Dispatch Summary</title>
    <style media="screen">
        html{
            margin: 20px;
            font-family: "Helvetica";
        }
        table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            /* text-align: center; */
            font-size: 12px;
            padding: 3px;
        }
        td{
            padding: 5px;
        }
        .title{
            /* padding: 8px !important; */
            font-size: 15px;
        }
        tfoot>tr>th{
            padding: 5px !important;
        }
        .center{
            text-align: center;
        }
        .space{
            height: 10px;
        }
    </style>
</head>
<body>
<?php $fmt = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::DECIMAL);?>
<table>
    <thead>
    <tr>
        <th class="title" rowspan="2" style="text-align:center;">SCPL ERP</th>
        <th class="title" rowspan="2" colspan="8" style="text-align:center;">CUSTOMER WISE - DISPATCH SUMMARY</th>
        <th colspan="2">From: {{date('d-m-Y', strtotime($start))}}</th>
    </tr>
    <tr>
        <th colspan="2">To: {{date('d-m-Y', strtotime($end))}}</th>
    </tr>
    <tr>
        <th class="space" colspan="11">
            <p style="text-align: center;" >{{$businessPartner->bp_name}}</p>
        </th>
    </tr>
    <tr>
        <th class="center">Invoice No</th>
        <th class="center" style="width: 70px;">Date</th>
        <th class="center" style="width: 180px;">Quality</th>
        <th>Qty</th>
        <th>Rate</th>
        <th>Total Value</th>
        <th>Insurance</th>
        <th>Handling Ch.</th>
        <th>GST</th>
        <th>Total Amount</th>
        <th>Contract</th>
    </tr>
    </thead>
    <tbody>


    <?php //dd($masterArray); ?>

    @foreach($masterArray as $material => $result)
        @foreach($result as $res)
            <tr>
                <td>{{$res['invoice_no']}}</td>
                <td>{{$res['invoice_date']}}</td>
                <td>{{$res['item_name']}}</td>
                <td>{{$res['weight']}}</td>
                <td>{{$res['rate']}}</td>
                <td>{{$fmt->format($res['value'])}}</td>
                <td>{{round($res['insurance'], 2)}}</td>
                <td>{{$fmt->format(round($res['handling_charges'], 2))}}</td>
                <td>{{$fmt->format(round($res['gst'], 2))}}</td>
                <td>{{$fmt->format(round($res['grant_total'], 2))}}</td>
                <td>{{$res['ref_no']}}</td>
            </tr>
        @endforeach
            <tr>
                <td><strong>Subtotal</strong></td>
                <td></td>
                <td></td>
                <td><strong>{{$fmt->format(round($subDetails[$material]['total_weight'], 2))}}</strong></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>{{$fmt->format(round($subDetails[$material]['sub_total'], 2))}}</strong></td>
                <td></td>
            </tr>
    @endforeach

   <tr>
       <td><strong>Total</strong></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td><strong>{{$fmt->format(round($totalBilled, 2))}} </strong></td>
       <td></td>
   </tr>
    
    </tbody>
    
</table>
</body>
</html>
