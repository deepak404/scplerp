<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DEFECT REPORT</title>
    <style media="screen">
        html{
          margin: 20px;
          font-family: "Helvetica";
        }
        table{
          width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
            padding: 3px;
        }
        /* td{
          padding: 3px;
        } */
        .title{
          padding: 10px !important;
          font-size: 15px;
        }
        tfoot>tr>th{
          padding: 5px !important;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th rowspan="2" class="title">SCPL ERP</th>
                <th rowspan="2" colspan="5" class="title">Day Wise Quality Production Summary</th>
                <th colspan="2">From: {{date('d/m/Y',strtotime($start))}}</th>
            </tr>
            <tr>
                <th colspan="2">To: {{date('d/m/Y',strtotime($end))}}</th>
            </tr>
            <tr>
                <th>Quality</th>
                <th>Packed Date</th>
                <th>Doff</th>
                <th>Filament</th>
                <th>Net Wt</th>
                <th>Gross Wt</th>
                <th>No.of Box</th>
                <th>No.of SPL</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $totalNetWeight = 0;
                $totalGrossWeight = 0;
                $noBox = 0;
                $noSpl = 0;
                ?>
            @foreach ($packSummary as $matrial=>$doffs)
                @foreach ($doffs as $doff=>$filaments)
                    @foreach ($filaments as $filament=>$value)
                        <?php 
                            $totalGrossWeight += $value['total_weight'];
                            $totalNetWeight += $value['material_weight'];
                            $noBox += $value['box'];
                            $noSpl += $value['spindel'];
                        ?>
                        <tr>
                            <td>{{$matrial}}</td>
                            <td>{{date('d-m-Y',strtotime($value['packed_date']))}}</td>
                            <td>{{$doff}}</td>
                            <td>{{$filament}}</td>
                            <td>{{$value['material_weight']}}</td>
                            <td>{{$value['total_weight']}}</td>
                            <td>{{$value['box']}}</td>
                            <td>{{$value['spindel']}}</td>
                        </tr>
                    @endforeach
                @endforeach
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th>{{$totalNetWeight}}</th>
                <th>{{$totalGrossWeight}}</th>
                <th>{{$noBox}}</th>
                <th>{{$noSpl}}</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>