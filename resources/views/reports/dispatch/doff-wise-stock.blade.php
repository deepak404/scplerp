<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>DOFF WISE STOCK</title>
    <style media="screen">
        html{
          margin: 20px;
          font-family: "Helvetica";
        }
        table{
          width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
            padding: 3px;
        }
        /* td{
          padding: 3px;
        } */
        .title{
          padding: 10px !important;
          font-size: 15px;
        }
        tfoot>tr>th{
          padding: 5px !important;
        }
    </style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th class="title">SCPL ERP</th>
          <th class="title" colspan="4">DOFF WISE STOCK ({{$type}})</th>
          <th class="title">ON: {{date('d/m/Y')}}</th>
        </tr>
        <tr>
          <th>CODE</th>
          <th>Code Description</th>
          <th>Info</th>
          <th>Quantity</th>
          <th>No. Packs</th>
          <th>No. SPL</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($instock as $unique_id => $info): ?>
          <?php foreach ($info as $key => $value): ?>
            <tr>
              <td>{{$unique_id}}</td>
              <td>{{$value['material']}}</td>
              <td>{{$key}}</td>
              <td>{{$value['weight']}}</td>
              <td>{{$value['box']}}</td>
              <td>{{$value['spindle']}}</td>
            </tr>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr>
          <th colspan="3">Total</th>
          <th>{{$total['weight']}}</th>
          <th>{{$total['box']}}</th>
          <th>{{$total['spindle']}}</th>
        </tr>
      </tfoot>
    </table>
  </body>
</html>
