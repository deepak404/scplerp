<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Dispatch Summary</title>
    <style media="screen">
        html{
          margin: 20px;
          font-family: "Helvetica";
        }
        table{
          width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            /* text-align: center; */
            font-size: 12px;
            padding: 3px;
        }
        td{
          padding: 5px;
        }
        .title{
          /* padding: 8px !important; */
          font-size: 15px;
        }
        tfoot>tr>th{
          padding: 5px !important;
        }
        .center{
          text-align: center;
        }
        .space{
          height: 10px;
        }
    </style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th class="title" rowspan="2" style="text-align:center;">SCPL ERP</th>
          <th class="title" rowspan="2" style="text-align:center;">DISPATCH SUMMARY</th>
          <th>From: {{date('d/m/Y',strtotime($start))}}</th>
        </tr>
        <tr>
          <th>To: {{date('d/m/Y',strtotime($end))}}</th>
        </tr>
        <tr>
          <th class="space" colspan="3"></th>
        </tr>
        <tr>
          <th class="center">Code</th>
          <th class="center">Quality</th>
          <th class="center">Quantity</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($dispatch as $key => $value): ?>
          <tr>
            <td style="width:20%;">{{$key}}</td>
            <td>{{$value['material']}}</td>
            <td style="width:20%;">{{$value['value']}}</td>
          </tr>
        <?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr>
          <th>Total</th>
          <th></th>
          <th>{{$total}}</th>
        </tr>
      </tfoot>
    </table>
  </body>
</html>
