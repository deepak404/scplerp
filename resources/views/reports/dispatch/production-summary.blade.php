<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Production Summary</title>
    <style media="screen">
      html{
        margin: 20px;
        font-family: "Helvetica";
      }
      table{
        width: 100%;
      }
      table, th, td {
          border: 1px solid black;
          border-collapse: collapse;
          text-align: center;
          font-size: 12px;
          padding: 3px;
      }
      h2{
        text-align: center;

      }
      .title{
        padding: 10px !important;
        font-size: 15px;
      }
      .page-break {
          page-break-after: always;
      }
    </style>
  </head>
  <body>
    <?php $count=0; foreach ($productionSummary as $machine => $materials): ?>
      <?php if ($count != 0): ?>
        <div class="page-break"></div>
      <?php endif; ?>
      <table>
        <thead>
          <tr>
            <th class="title" rowspan="2">SCPL ERP</th>
            <th class="title" rowspan="2">PRODUCTION SUMMARY</th>
            <th>From: {{$start}}</th>
          </tr>
          <tr>
            <th>To: {{$end}}</th>
          </tr>
        </thead>
      </table>
      <h2>{{$machine}}</h2>
      <table>
        <thead>
          <tr>
            <th>Code</th>
            <th>Quality</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($materials as $code => $floorCode): ?>
            <?php foreach ($floorCode as $key=>$value): ?>
              <tr>
                <td>{{$code}}</td>
                <td>{{$key}}</td>
                <td>{{$value}}</td>
              </tr>
            <?php endforeach; ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="2">Total</th>
            <th>{{$total[$machine]}}</th>
          </tr>
        </tfoot>
      </table>
    <?php $count++; endforeach; ?>
  </body>
</html>
