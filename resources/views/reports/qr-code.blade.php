<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style>

        .bar-info{
            display: inline-block;
            /*vertical-align: middle;*/
        }

        img{
            margin-bottom: 0px;
            margin-top: 30px;
        }

        table>tbody>tr>td:first-child{
            margin-right: 300px;
        }

        p{
            margin-top: 3px !important;
            margin-bottom: 3px !important;
        }

        table{
            width: 100%;
        }

        .page-break {
            page-break-after: always;
        }


    </style>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"--}}
          {{--integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
</head>
<body>


<table class="table table-bordered">
    <thead>
        <tr>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <?php $scount = 1; ?>
        @foreach($files as $file)
            <?php //dd($file); ?>
            <tr>
                @foreach($file as $bar)
                <td>
                    <img src="qrcode/{{explode('|',$bar)[0]}}.png" alt="">
                    <div class="bar-info">
                        <p><b>Count</b> : {{$count}}</p>
                        <p><b>DOFF No</b> : {{$doff}}</p>
                        <p><b>DOP</b> : {{$dop}}</p>
                        <p><b>Spindle No</b> : {{explode('|',$bar)[1]}}</p>
                    </div>
                </td>
                @endforeach


            </tr>
            @if($scount/6 == 0)

                <?php //dd('hello'); ?>
                <div class="page-break"></div>
            @endif
            <?php $scount++; ?>

        @endforeach

    </tbody>
</table>


</body>
</html>