<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Partywise Cumulative - Dispatch Summary</title>
    <link rel="stylesheet" href="{{ url('css/sales-index.css') }}">
    <link rel="stylesheet" href="{{url('responsive-css/sales-order.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <style>
        html{
            margin: 20px;
        }

        table, td, th, tr{
            border-color: black !important;
        }

        td,th{
            font-size: 12px;
        }

        .table>thead>tr>th{
            padding: 2px;
        }

        td{
            font-weight: 600;
        }
        .table>tbody>tr>td{
            padding: 5px;
        }
    </style>

</head>
<body>
<?php $fmt = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::DECIMAL);?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th class="title" style = "vertical-align: middle; text-align: center; font-size: 16px;" rowspan="2" style="text-align:center;">SCPL ERP</th>
        <th class="title" style = "vertical-align: middle; text-align: center;font-size: 16px;" rowspan="2" colspan="8" >CUSTOMER WISE CUMULATIVE - DISPATCH SUMMARY</th>
        <th colspan="2">From: {{date('d-m-Y', strtotime($start))}}</th>
    </tr>
    <tr>
        <th colspan="2">To: {{date('d-m-Y', strtotime($end))}}</th>
    </tr>

    </thead>
    <tbody>
    <?php $superGrantTotal=0; ?>

    @foreach($masterArrays as $bp => $masterArray)
        <tr>
            <th class="space" colspan="11">
                <p style="text-align: center;" >{{$bp}}</p>
            </th>
        </tr>
        <tr>
            <th class="center">Invoice No</th>
            <th class="center" style="width: 95px;">Date</th>
            <th class="center" style="width: 180px;">Quality</th>
            <th>Qty</th>
            <th>Rate</th>
            <th>Total Value</th>
            <th>Insurance</th>
            <th>Handling Ch.</th>
            <th>GST</th>
            <th>Total Amount</th>
            <th>Contract</th>
        </tr>

        <?php $subtotal=0; $subQuantity = 0;?>

        @foreach($masterArray as $material => $result)

            @foreach($result as $res)
                <tr>
                    <td>{{$res['invoice_no']}}</td>
                    <td>{{$res['invoice_date']}}</td>
                    <td>{{$res['item_name']}}</td>
                    <td>{{$res['weight']}}</td>
                    <td>{{$res['rate']}}</td>
                    <td>{{$fmt->format($res['value'])}}</td>
                    <td>{{round($res['insurance'], 2)}}</td>
                    <td>{{$fmt->format(round($res['handling_charges'], 2))}}</td>
                    <td>{{$fmt->format(round($res['gst'], 2))}}</td>
                    <td>{{$fmt->format(round($res['grant_total'], 2))}}</td>
                    <td>{{$res['ref_no']}}</td>
                </tr>
                <?php $subtotal += $res['grant_total']; $subQuantity+= $res['weight']; ?>
            @endforeach
        @endforeach
        <?php $superGrantTotal += $subtotal; ?>
        <tr>
            <td><strong>Subtotal</strong></td>
            <td></td>
            <td></td>
            <td><strong>{{$fmt->format(round($subQuantity, 2))}}</strong></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><strong>{{$fmt->format(round($subtotal, 2))}}</strong></td>
            <td></td>
        </tr>

    @endforeach
    <tr>
        <td><strong>Grand total</strong></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><strong>{{$fmt->format(round($superGrantTotal, 2))}}</strong></td>
        <td></td>
    </tr>
    </tbody>

</table>
</body>
</html>
