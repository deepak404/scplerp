<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Repack Slip - {{$challanNo}}</title>

    <style>
        html{
            margin: 30px;
            font-family: "Helvetica";
        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }


        #spindle-table{
            margin-top: 20px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            /*border-top: none !important;*/
        }

        #spindle-table>tbody>tr>td{
            padding: 2px 5px;
        }
        #spindle-table{
            border: none !important;
        }


        #detail-table>tbody>tr>td{
            padding: 2px;
            height: 1px;
        }


        #total-table>tbody>tr>td{
            padding: 4px;
            border: none;
        }

        #total-table{
            border:none;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>



    <table style="width: 100%;" id="header-table">
        <tbody>
        <tr>
            <td style="width: 20%; text-align: center;" ><h3>SCPL ERP</h3></td>
            <td style="width: 90%; text-align: center;" ><h2>REPACK SLIP</h2></td>
            <td style="width: 35%; text-align: left;"><p style="margin-left: 0px;"><span>CHALLAN No : </span><span>{{$challanNo}}</span></td>
        </tr>

        </tbody>
    </table>

    <table id="spindle-table" style="width: 100%;">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Pack No</th>
            <th>Doff No</th>
            <th>Net. Wt</th>
            <th>G. Wt</th>
            <th>Spindle</th>
            <th>Pack type</th>
        </tr>
        </thead>
        <tbody>
        <?php $sno = 0; ?>
        @foreach($packages as $pack)

            <tr>
                <td>{{++$sno}}</td>
                <td>{{$pack->case_no}}</td>
                <td>{{$pack->doff_no}}</td>
                <td>{{$pack->net_weight}}</td>
                <td>{{$pack->gross_wt}}</td>
                <td>{{$pack->spindle_count}}</td>
                <td>{{$pack->box_type}}</td>
            </tr>

        @endforeach

        </tbody>

    </table>


</body>
</html>
