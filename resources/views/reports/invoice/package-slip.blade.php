<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Package Slip for Invoice - {{$invoice->invoice_no}}</title>

    <style>
        html{
            margin: 30px;
            font-family: "Helvetica";
        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }


        #spindle-table{
            margin-top: 20px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            /*border-top: none !important;*/
        }

        #spindle-table>tbody>tr>td{
            padding: 2px 5px;
        }
        #spindle-table{
            border: none !important;
        }


        #detail-table>tbody>tr>td{
            padding: 2px;
            height: 1px;
        }


        #total-table>tbody>tr>td{
            padding: 4px;
            border: none;
        }

        #total-table{
            border:none;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>


<?php $count_matrial=0; $businessPartner = \App\BusinessPartner::where('id', $invoice->bp_id)->value('bp_name'); ?>

@foreach($packages as $material_id => $packs)
<?php if ($count_matrial > 0): ?>
<div class="page-break"></div>
<?php endif; ?>
<?php $count_matrial++; ?>
<table style="width: 100%;" id="header-table">
  <tbody>
    <tr>
      <td style="width: 20%; text-align: center;" ><h3>SCPL ERP</h3></td>
      <td style="width: 90%; text-align: center;" ><h2>DISPATCH SLIP</h2></td>
      <td style="width: 35%; text-align: left;"><p style="margin-left: 0px;"><span>Invoice No :</span><span>{{$invoice->invoice_no}}</span><p><span>Date :  </span><span>{{$invoice->date}}</span></p></td>
    </tr>

  </tbody>
</table>

<p style="text-align: center; font-size: 15px; margin-top: 10px; margin-bottom: 7px;"><strong>{{$businessPartner}}</strong></p>

<p style="text-align: center; font-size: 15px; margin-top: 10px; margin-bottom: 7px;"><strong>{{$invoice->indent->indentDetails()->where('material_id',$material_id)->first()->saleOrder()->value('material_name')}}</strong></p>

<table id="spindle-table" style="width: 100%;">
    <thead>
      <tr>
        <th>S.No</th>
        <th>Pack No</th>
        <th>Doff No</th>
        <th>Net. Wt</th>
        <th>G. Wt</th>
        <th>Spindle</th>
        <th>Pack type</th>
      </tr>
    </thead>
    <tbody>

        <?php $sno = 0; $totalNetWeight = 0; $totalGrossWeight = 0;?>
        @foreach($packs as $pack)
            <?php $doff_no = implode(', ', $pack->weightLogs->pluck('doff_no')->toArray());

            $totalNetWeight += $pack->weightLogs->sum('material_weight') + $pack->moisture_weight;
            $totalGrossWeight += $pack->weightLogs->sum('total_weight') + $pack->box_weight + $pack->inset_weight;

            ?>
        <tr>
            <td>{{++$sno}}</td>
            <td>{{$pack->case_no}}</td>
            <td>{{$doff_no}}</td>
            <td>{{round($pack->weightLogs->sum('material_weight') + $pack->moisture_weight , 1)}}</td>
            <td>{{round($pack->weightLogs->sum('total_weight') + $pack->box_weight + $pack->inset_weight, 1)}}</td>
            <td>{{$pack->bobbin_count}}</td>
            <td>{{$pack->box_type}}</td>
        </tr>

        @endforeach

        <tr>
            <td></td>
            <td><strong>Total</strong></td>
            <td></td>
            <td><strong>{{$totalNetWeight}}</strong></td>
            <td><strong>{{round($totalGrossWeight,1)}}</strong></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>

</table>

    @endforeach

</body>
</html>
