<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
        <title>As on Packed</title>
    <style media="screen">
        html{
            margin: 10px !important;
            font-family: "Helvetica";
        }
        .table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 4px;
        }
        .space{
            height: 10px;
        }
        .title{
            font-size: 14px !important;
        }
        .center{
            text-align:center;
        }
        td{
            padding-left: 4px !important;
        }
        .date{
            width: 65px;
        }
        .px-ft{
            width: 40px;
        }
    </style>
</head>
<body>
<?php
set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);
?>
<!-- <table>
      <thead>
      </thead>
    </table> -->
<table class="table">
    <thead>
    <tr>
        <th class="title center" colspan="2" rowspan="2">SCPL ERP</th>
            <th class="title center" colspan=7 rowspan="2">AS ON PACKED ENTRY</th>
        <th class="title" colspan="3">Date: {{$start}}</th>
    </tr>
    <tr>
        <th class="title" colspan="3">No.Box: {{$boxCount}}</th>
    </tr>

    <tr>
        <th>Case No.</th>
        <th colspan="2">Quality</th>
        <th>Packed Date</th>
        <th>Doff No</th>
        <th>Net Wt.</th>
        <th>Gross Wt.</th>
        <th>No.of SPL</th>
        <th>Spindles</th>
        <th>Machine</th>
        <th>Box Type</th>
        <th>Bobbin Type</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($packageMaster as $key=>$pack): ?>
    <?php foreach ($pack as $value): ?>
    <tr>
        <td class="px-ft">{{$value['case_no']}}</td>
        <td colspan="2">{{$key}}</td>
        <td>{{$value['packed_date']}}</td>
        <td>{{$value['doff_no']}}</td>
        <td class="px-ft">{{$value['net_weight']}}</td>
        <td class="px-ft">{{$value['gross_weight']}}</td>
        <td class="px-ft center">{{$value['bobbin_count']}}</td>
        <td class="px-ft">{{implode(",",$value['spindle'])}}</td>
        <td class="px-ft">{{$value['machine']}}</td>
        <td class="px-ft">{{$value['box_type']}}</td>
        <td class="date">{{$value['package_type']}}</td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <th class="space" colspan="12"></th>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<table>
    <tbody>
    <tr>
        <th class="center" colspan="2">Total</th>
    </tr>
    <tr>
        <th>Date</th>
        <td>{{$start}}</td>
    </tr>
    <tr>
        <th>No.Box</th>
        <td>{{$boxCount}}</td>
    </tr>
    <tr>
        <th>Net Weight</th>
        <td>{{$netWeight}}</td>
    </tr>
    <tr>
        <th>Gross Weight</th>
        <td>{{$grossWeight}}</td>
    </tr>
    </tbody>
</table>
</body>
</html>
