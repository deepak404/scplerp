<html>

<head>
    {{--<meta http-equiv=Content-Type content="text/html; charset=utf-8">--}}
    {{--<meta name=ProgId content=Excel.Sheet>--}}
    {{--<meta name=Generator content="Microsoft Excel 15">--}}
    {{--<link rel=File-List href="demo.fld/filelist.xml">--}}
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->


    <style>
        html{
          font-family: "Helvetica Neue", Arial, sans-serif;
            margin: 15px;

        }
        #content-tr>td{
            text-align: center !important;
        }



        table tr>td{
            border: 1px solid black;
        }


        table,tr,td,th {
          border-collapse: collapse;
        }

        table.table-bordered{
            border:1px solid black;
            margin-top: 10px;
        }
        table.table-bordered > thead > tr > th{
            border:1px solid black;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black;
            padding: 3px 8px;
        }

        table.table-bordered > tbody > tr > td > p{
            margin: 0px;
            font-size: 10px;
        }


        #indent-details-table > tbody > tr > td{
            text-align: center;
        }

        small{
            font-size: 9px;
        }

        .no-border > td{
            border : none !important;
            /*border-bottom : none !important;*/
            border-right : 1px solid black !important;
        }
        table.table-bordered{
            margin-top: 0px !important;
        }
    </style>
</head>

<body link="#0563C1" vlink="#954F72">


<?php $fmt = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::DECIMAL);?>

<div id="demo_31861" align=center x:publishsource="Excel">

    <div class="row black center" style="position: relative;">
        <span class="first">TAX INVOICE</span>
    </div>
    <p style="font-size:10px; text-align: right ;margin-bottom: 5px !important; ">({{$invoiceName}})</p>

    <table class="table table-bordered" border=0 cellpadding=0 cellspacing=0 width=1543 style='border-collapse:
 collapse;table-layout:fixed;width:100%; margin-bottom: -46px'>
        <col width=51 style='mso-width-source:userset;mso-width-alt:1621;width:38pt'>
        <col width=123 style='mso-width-source:userset;mso-width-alt:3925;width:92pt'>
        <col width=437 style='mso-width-source:userset;mso-width-alt:13994;width:328pt'>
        <col width=87 span=3 style='width:65pt'>
        <col width=89 style='mso-width-source:userset;mso-width-alt:2858;width:67pt'>
        <col width=67 style='mso-width-source:userset;mso-width-alt:2133;width:50pt'>
        <col width=85 style='mso-width-source:userset;mso-width-alt:2730;width:64pt'>
        <col width=147 style='mso-width-source:userset;mso-width-alt:4693;width:110pt'>
        <col width=0 style='display:none;mso-width-source:userset;mso-width-alt:42'>
        <col width=76 style='mso-width-source:userset;mso-width-alt:2432;width:57pt'>
        <col width=87 style='width:65pt'>
        <col width=120 style='mso-width-source:userset;mso-width-alt:3840;width:90pt'>
        <col width=0 style='display:none;mso-width-source:userset;mso-width-alt:597'>



        <tr height = 1000>
            <td colspan=9 rowspan=6 style="height: 100px; padding-bottom: 0;">
                <p style="font-size: 14px;"><strong>Shakti Cords Pvt. Ltd</strong></p>
                <p>CS 17&18, SIDCO INDUSTRIAL ESTATE</p>
                <p>KAPPALUR</p>
                <p>MADURAI - 625008</p>
                <p>GSTIN/UIN : 33AAHCS3989E1Z4</p>
                <p>PAN : AAHCS3989E</p>
                <p>State Name : Tamil Nadu, Code : 33</p>
                <p>CIN : U17297TN2003PTC051243</p>
                <p>Email : sales@indtextile.com, scpl@indtextile.com</p>
            </td>
            {{--Quotation Number--}}
            <td colspan=4 rowspan=2 style="height: 10pt;">
                <p>Invoice No.</p>
                <p><strong>{{$master->invoice_no}}</strong>
                </p>
            </td>
            {{--Dated--}}
            <td colspan=4 rowspan=2>
                <p>Dated</p>
                <p><strong>{{date('d-M-y',strtotime($master->date))}}</strong>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td colspan=4 rowspan=2>
                <p>Dispatch Through</p>
                <p></p>
            </td>
            <td colspan=4 rowspan=2>
                <p>Modes/Terms of Payment</p>
                <p><strong>{{$indentMaster->payment_mode}}</strong><p>
                </p>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td colspan=4 rowspan=2 style="height:30px;">
                <p>Buyer's Ref/Order No</p>
                <p><strong>{{$master->indent->order_ref_no}} </strong></p>
            </td>
            <td colspan=4 rowspan=2>
                <p>Supplier Ref. No</p>
                <p><strong>{{$master->indent->other_ref_no}}</strong></p>
            </td>
        </tr>
        <tr>
        </tr>


        <tr>
            {{--client address--}}

            @if($master->bp_id == $master->consignee_id)
            <td colspan=9 rowspan=5 width=1113 style='width:834pt;height: 150px; vertical-align: top; margin-bottom: -41px; !important;'>
                <small><strong>Invoice To</strong></small>
                <p style="font-size: 14px;"><strong>{{$businessPartner['name']}}</strong></p>
                <p>{{$businessPartner['address']}}</p>
                <p>District : {{$businessPartner['district']}}</p>
                <p>{{$businessPartner['pincode']}}</p>
                <p>GST/UIN : {{$businessPartner['gst']}}</p>
                <p>StateName : {{$businessPartner['state']}}, Code : 033</p>
            @else
            <td colspan=9 rowspan=5 width=1113 style='width:834pt;height: 190px; vertical-align: top; margin-bottom: -41px; !important;'>
                <?php $consignee = \App\BusinessPartner::where('id', $master->consignee_id)->get()->toArray()[0]; //dd($consignee); ?>
                <small><strong>Invoice To</strong></small>
                <p style="font-size: 14px;"><strong>{{$businessPartner['name']}}</strong></p>
                <p>{{$businessPartner['address']}}</p>
                <p>District : {{$businessPartner['district']}}</p>
                <p>{{$businessPartner['pincode']}}</p>
                <p>GST/UIN : {{$businessPartner['gst']}}</p>
                <p>StateName : {{$businessPartner['state']}}, Code : 033</p>


                <small><strong>Consignee</strong></small>
                <p style="font-size: 14px;"><strong>{{$consignee['bp_name']}}</strong></p>
                <p>{{$consignee['address']}}</p>
                <p>District : {{$consignee['district']}}</p>
                <p>{{$consignee['pincode']}}</p>
                <p>GST/UIN : {{$consignee['gst_code']}}</p>
                <p>StateName : {{$consignee['state']}}, Code : 033</p>
            </td>
            @endif

            {{--Dispatch Through below--}}
            <td colspan=4 rowspan=2 height="10px" width=223 style='width:167pt; vertical-align: top;'>
                <p style="margin:0px;">Transport Mode</p>
                <p style="margin:0px;"><strong>{{$indentMaster->dispatcher}}</strong></p>
            </td>
            {{--destination below--}}
            <td colspan=4 rowspan=2  width=207 style='width:155pt; vertical-align: top;'>
                <p>Destination</p>
                <p><strong>{{$indentMaster->destination}}</strong></p>
            </td>
        </tr>

        <tr>
        </tr>

        <tr>
            <td colspan="8" rowspan="2" style="vertical-align: top;">
                <p style="vertical-align: top;">Terms of Delivery</p>
            </td>
        </tr>
        <tr></tr>



    </table>

    {{--<table class="table table-bordered" style='border-collapse: collapse;table-layout:fixed;width:100%;margin-top: -35px; margin-bottom: 33px;'>--}}
      {{--<tr>--}}
          {{--<td colspan="9" style="height: 100px;">--}}
              {{--<small>Consignee</small>--}}
              {{--<p style="font-size: 14px;"><strong>{{$businessPartner['name']}}</strong></p>--}}
              {{--<p>{{$businessPartner['address']}}</p>--}}
              {{--<p>District : {{$businessPartner['district']}}</p>--}}
              {{--<p>{{$businessPartner['pincode']}}</p>--}}
              {{--<p>GST/UIN : {{$businessPartner['gst']}}</p>--}}
              {{--<p>StateName : {{$businessPartner['state']}}, Code : 033</p>--}}
          {{--</td>--}}
          {{--<td colspan="8"></td>--}}
      {{--</tr>--}}
    {{--</table>--}}


    <table class="table table-bordered" id="indent-details-table" style="width:100%; margin-top: -30px;">

        <tr height=35 style='mso-height-source:userset;height:26.0pt'>
            <td height=35 style='height:26.0pt;font-size: 10px; width: 10px;'>S.No</td>
            <td colspan=2 style="font-size: 10px; text-align: center">No &amp; Kind of <br> Pkgs.</td>
            <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Description of Goods</td>
            <td style="font-size: 10px; text-align: center;">HSN/SAC</td>
            <td style="font-size: 10px; text-align: center;" colspan="2">Quantity <br>(Kgs.)</td>
            <td style="font-size: 10px; text-align: center;" >Rate <br>(Rs.)</td>
            <td style="font-size: 10px; text-align: center;">Per</td>
            <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65>Amount</td>
        </tr>

        <?php $orderCount = 1;
        $totalAmount = 0;
        $totalQuantity = 0;
        $taxAmount= [];

        ?>

        <?php

        foreach($orderDetails as $orderDetail){

            if(array_key_exists($orderDetail['hsn'], $taxAmount)){

                $taxAmount[$orderDetail['hsn']]['taxableAmount'] += $orderDetail['rate'] * $orderDetail['weight'];
                $taxAmount[$orderDetail['hsn']]['rate'] = $orderDetail['gst'];

            }else{

                $taxAmount[$orderDetail['hsn']]['taxableAmount'] = $orderDetail['rate'] * $orderDetail['weight'];
                $taxAmount[$orderDetail['hsn']]['rate'] = $orderDetail['gst'];

            }


        }

        foreach ($taxAmount as $hsn => $tax){
            $taxAmount[$hsn]['tax'] = ($tax['rate']/100 * $tax['taxableAmount']);
        }



        $integratedTax = array_sum(array_column($taxAmount, 'tax'));

        ?>

        <?php
            $taxHsn = [];
            $hsnInsurance = [];
            $hsnHandlingCharges = [];
            $hsnCgst = [];
            $hsnSgst = [];
            $hsnIgst = [];
            $hsnCgstTaxRate = [];
            $hsnSgstTaxRate = [];
            $hsnIgstTaxRate = [];

            $totalSubAmount = 0;
        ?>

        @foreach($orderDetails as $orderDetail)
            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'>{{$orderCount}}</td>
                <td colspan=2 style="font-size: 10px; text-align: center">{{$orderDetail['no_box']}}</td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;border-bottom: none !important;"><strong>{{$orderDetail['name']}}</strong></td>
                <td style="font-size: 10px; text-align: center;">{{$orderDetail['hsn']}}</td>
                <td style="font-size: 10px; text-align: center;" colspan="2">{{$fmt->format(round($orderDetail['weight'], 2))}}</td>
                <td style="font-size: 10px; text-align: center;" >{{$orderDetail['rate']}}</td>
                <td style="font-size: 10px; text-align: center;">kgs</td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($orderDetail['rate'] * $orderDetail['weight'], 2))}}</strong></td>
                {{--<td></td>--}}
            </tr>
            <?php $orderCount++;
                $taxHsn[$orderDetail['hsn']] = round($orderDetail['rate'] * $orderDetail['weight'], 2);
                $totalQuantity += $orderDetail['weight'];
                $totalAmount += $orderDetail['rate'] * $orderDetail['weight'];
            ?>
        @endforeach

        <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
            <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
            <td colspan=2 style="font-size: 10px; text-align: center"></td>
            <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;border-bottom: none !important;"><strong>Sub Total</strong></td>
            <td style="font-size: 10px; text-align: center;"></td>
            <td style="font-size: 10px; text-align: center;" colspan="2"></td>
            <td style="font-size: 10px; text-align: center;" ></td>
            <td style="font-size: 10px; text-align: center;"></td>
            <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($totalAmount))}}</strong></td>
            {{--<td></td>--}}
        </tr>
        <?php

        $cgst = 0;
        $sgst = 0;
        $igst = 0;
        $totalGst = 0;
        $handlingCharges = 0;

        $indentDetails = $indentMaster->indentDetails()->whereIn('material_id',array_keys($master->packageMaster->groupBy('material_id')->toArray()))->get();

        foreach ($indentDetails as $indentDetail){

            if(!array_key_exists($indentDetail->hsn_sac, $taxHsn)){
                $taxHsn[$indentDetail->hsn_sac] = 0;
            }

            if(!array_key_exists($indentDetail->hsn_sac, $hsnInsurance)){
                $hsnInsurance[$indentDetail->hsn_sac] = 0;
            }

            if(!array_key_exists($indentDetail->hsn_sac, $hsnHandlingCharges)){
                $hsnHandlingCharges[$indentDetail->hsn_sac] = 0;
            }

            if(!array_key_exists($indentDetail->hsn_sac, $hsnCgst)){
                $hsnCgst[$indentDetail->hsn_sac] = 0;
            }

            if(!array_key_exists($indentDetail->hsn_sac, $hsnSgst)){
                $hsnSgst[$indentDetail->hsn_sac] = 0;
            }

            if(!array_key_exists($indentDetail->hsn_sac, $hsnIgst)){
                $hsnIgst[$indentDetail->hsn_sac] = 0;
            }

            $packedQuantity = \App\WeightLog::whereIn('package_master_id', $master->packageMaster()->where('material_id', $indentDetail->material_id)->pluck('id')->toArray())->sum('material_weight');
            
            $moisture = $master->packageMaster()->where('material_id', $indentDetail->material_id)->sum('moisture_weight');
            $packedQuantity += $moisture;

            $itemMaster = $indentDetail->itemMaster;

            $tempHandlingCharges = ($master->handling_charges/$totalAmount) * ($packedQuantity * $indentDetail->rate_per_kg);
            $hsnHandlingCharges[$indentDetail->hsn_sac] += $tempHandlingCharges;
            $handlingCharges += $tempHandlingCharges;
            $insuranceCost = ((($indentMaster->transit_insurance/100) * $totalAmount)/$totalAmount) * ($packedQuantity * $indentDetail->rate_per_kg);
            $hsnInsurance[$indentDetail->hsn_sac] += $insuranceCost;


            if($indentMaster->delivery_type == 1){
                //Within Tamil Nadu

                $cgst += $itemMaster->cgst/100 * (($packedQuantity * $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);
                $sgst += $itemMaster->sgst/100 * (($packedQuantity * $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);

                $hsnCgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->cgst;
                $hsnSgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->sgst;
                $taxHsn[$indentDetail->hsn_sac] += ($cgst + $sgst);
                $hsnCgst[$indentDetail->hsn_sac] = $cgst;
                $hsnSgst[$indentDetail->hsn_sac] = $sgst;
            }else{
                //Outside Tamil Nadu

                $totalGst += $itemMaster->igst/100 * (($packedQuantity* $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);
                $taxHsn[$indentDetail->hsn_sac] += $totalGst;

                $hsnIgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->igst;
                $hsnIgst[$indentDetail->hsn_sac] = $igst;

            }



        } //foreach ends

        if($indentMaster->delivery_type == 1){
            $totalGst = $sgst + $cgst;

        }

        if($indentMaster->delivery_type == 1){

            $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
            $totalBillable = $totalAmount + $insuranceCost + $cgst +$sgst + $handlingCharges;

        }else{
            $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
            $totalBillable = $totalAmount + $insuranceCost + $totalGst + $handlingCharges;
        }

        ?>

    @if($indentMaster->delivery_type == 2 || $indentMaster->delivery_type == 3)

            @if($handlingCharges != 0)
                <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                    <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                    <td colspan=2 style="font-size: 10px; text-align: center"></td>
                    <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Handling Charges</td>
                    <td style="font-size: 10px; text-align: center;"></td>
                    <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                    <td style="font-size: 10px; text-align: center;" ></td>
                    <td style="font-size: 10px; text-align: center;"></td>
                    <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($handlingCharges, 2))}}</strong></td>
                </tr>
            @endif

            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                <td colspan=2 style="font-size: 10px; text-align: center"></td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Insurance</td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                <td style="font-size: 10px; text-align: center;" ></td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($insuranceCost, 2))}}</strong></td>
            </tr>

            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                <td colspan=2 style="font-size: 10px; text-align: center"></td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Integrated Tax</td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                <td style="font-size: 10px; text-align: center;" ></td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($totalGst, 2))}}</strong></td>
            </tr>
        @else

            @if($handlingCharges != 0)
                <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                    <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                    <td colspan=2 style="font-size: 10px; text-align: center"></td>
                    <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Handling Charges</td>
                    <td style="font-size: 10px; text-align: center;"></td>
                    <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                    <td style="font-size: 10px; text-align: center;" ></td>
                    <td style="font-size: 10px; text-align: center;"></td>
                    <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($handlingCharges, 2))}}</strong></td>
                </tr>
            @endif

            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                <td colspan=2 style="font-size: 10px; text-align: center"></td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Insurance</td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                <td style="font-size: 10px; text-align: center;" ></td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($insuranceCost, 2))}}</strong></td>
            </tr>

            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                <td colspan=2 style="font-size: 10px; text-align: center"></td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">SGST</td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan="2" class="no-border"></td>
                <td style="font-size: 10px; text-align: center;" ></td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($sgst, 2))}}</strong></td>
            </tr>


            <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
                <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
                <td colspan=2 style="font-size: 10px; text-align: center"></td>
                <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">CGST</td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan="2"></td>
                <td style="font-size: 10px; text-align: center;" ></td>
                <td style="font-size: 10px; text-align: center;"></td>
                <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format(round($cgst, 2))}}</strong></td>
            </tr>

        @endif
        {{--total below--}}

        <?php $finalTotal = ceil($totalAmount + $totalGst + $insuranceCost + $handlingCharges); $roundOffValue = round($finalTotal - ($totalAmount + $totalGst + $insuranceCost + $handlingCharges), 2);?>

        <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
            <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
            <td colspan=2 style="font-size: 10px; text-align: center"></td>
            <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;">Round off to Sales</td>
            <td style="font-size: 10px; text-align: center;"></td>

            <td style="font-size: 10px; text-align: center;" colspan="2"></td>
            <td style="font-size: 10px; text-align: center;" ></td>
            <td style="font-size: 10px; text-align: center;"></td>


            <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format((round(($roundOffValue), 2)))}}</strong></td>
        </tr>

        <tr height=15 style='mso-height-source:userset;height:15.0pt' id="content-tr" class="no-border">
            <td height=15 style='height:15.0pt;font-size: 10px; width: 10px;'></td>
            <td colspan=2 style="font-size: 10px; text-align: center"></td>
            <td colspan=8 class=xl65 style="font-size: 10px; text-align: center;"><strong>Total</strong></td>
            <td style="font-size: 10px; text-align: center;"></td>

            <td style="font-size: 10px; text-align: center;" colspan="2"><strong>{{$fmt->format($totalInvoiceWeight)}}</strong></td>
            <td style="font-size: 10px; text-align: center;" ></td>
            <td style="font-size: 10px; text-align: center;"></td>

            <td style="font-size: 10px; text-align: center;" colspan=5 class=xl65><strong>{{$fmt->format((round(($finalTotal), 2)))}}</strong></td>

        </tr>

    </table>



    <?php

    $totalBillable = $finalTotal;
    $currencyToWords = new App\CurrencyToWords();

    $totalTaxableValue = 0;
    $totalTaxAmount = 0;

    $cgstTotalTaxValue = 0;
    $sgstTotalTaxValue = 0;

    $igstTotalTaxValue = 0;

    $convertedCurrency = $currencyToWords->convertIndianCurrency(($totalBillable - ($totalBillable - floor($totalBillable))));


    if(is_numeric( $totalBillable ) && floor( $totalBillable ) != $totalBillable){
        $decimalValue = $totalBillable - floor($totalBillable);

        $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

        $convertedCurrency = str_replace(' and ','',$convertedCurrency).' rupees and '. $decimalWords. ' paise only.';
    }


    ?>

    {{--CGST Below--}}

@if($indentMaster->delivery_type == 1)
        <table class="table table-bordered" style="margin-top: -2px !important; margin-bottom: 0px;">
            <tr style="height: 10px !important;">
                <td colspan="17" rowspan="3" style="height: 10px;">
                    <small>Amount Chargeable (in words.)</small>
                    <p style="margin: 0px;"><strong>{{$convertedCurrency}}</strong></p>

                </td>
            </tr>

            <tr></tr>
            <tr></tr>

            <tr>
                <td rowspan="2" colspan="5"><p>HSN/SAC</p></td>
                <td rowspan="2" colspan="7"><p>Taxable Value</p></td>
                <td colspan="2"><p>Central Tax</p></td>
                <td colspan="2"><p>State Tax</p></td>
                <td><p>Total Tax Amount</p></td>
            </tr>
            <tr>
                <td><p>Rate (%)</p></td>
                <td><p>Amount</p></td>
                <td><p>Rate (%)</p></td>
                <td><p>Amount</p></td>
                <td></td>
            </tr>

            {{--below comes the HSN Details --}}
        <!--        --><?php //dd($tax); ?>
            @foreach($taxAmount as $hsn => $tax)
                <?php
                $amount = $tax['taxableAmount'] + $hsnInsurance[$hsn] + $hsnHandlingCharges[$hsn];
                $totalTaxableValue +=  $amount;
                $totalTaxAmount +=  $taxHsn[$hsn];
                $cgstTaxValue = ($hsnCgstTaxRate[$hsn]/100) * $amount;
                $sgstTaxValue = ($hsnSgstTaxRate[$hsn]/100) * $amount;

                $cgstTotalTaxValue += $cgstTaxValue;
                $sgstTotalTaxValue += $sgstTaxValue;

                ?>
                <tr>
                    <td rowspan="1" colspan="5"><p>{{$hsn}}</p></td>
                    <td rowspan="1" colspan="7"><p>{{$fmt->format(round($amount, 2))}}</p></td>
                    <td colspan="1"><p>{{$hsnCgstTaxRate[$hsn]}}</p></td>
                    <td colspan="1"><p>{{$fmt->format(round($cgstTaxValue, 2))}}</p></td>
                    <td colspan="1"><p>{{$hsnSgstTaxRate[$hsn]}}</p></td>
                    <td colspan="1"><p>{{$fmt->format(round($sgstTaxValue, 2))}}</p></td>
                    <td><p>{{$fmt->format(round($cgstTaxValue + $sgstTaxValue, 2))}}</p></td>
                </tr>



            @endforeach


            <?php

            $convertedCurrency = $currencyToWords->convertIndianCurrency(($totalTaxAmount - ($totalTaxAmount - floor($totalTaxAmount))));

            if(is_numeric( $totalTaxAmount ) && floor( $totalTaxAmount ) != $totalTaxAmount){
                $decimalValue = $totalTaxAmount - floor($totalTaxAmount);

                $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

                $convertedCurrency = str_replace(' and ','',$convertedCurrency).'rupees and '. $decimalWords. 'paise only.';
            }

            ?>
            <tr>
                <td rowspan="1" colspan="5"><p style="text-align: right;"><strong>Total</strong></p></td>
                <td rowspan="1" colspan="7"><p>{{$fmt->format(round($totalTaxableValue, 2))}}</p></td>
                <td colspan="1"><p></p></td>
                <td colspan="1"><p style="margin-top: 0px !important;">{{$fmt->format(round($cgstTotalTaxValue, 2))}}</p></td>
                <td colspan="1"><p></p></td>
                <td colspan="1"><p>{{$fmt->format(round($sgstTotalTaxValue, 2))}}</p></td>
                <td><p>{{$fmt->format(round($sgstTotalTaxValue + $cgstTotalTaxValue, 2))}}</p></td>
            </tr>




            <tr>
                <td rowspan="3" colspan="9" style="vertical-align: top;">
                    <p>Tax Amount in Words : </p>
                    <p><strong>{{$convertedCurrency}}</strong></p>
                </td>
                <td rowspan="3" colspan="8">
                    <small>Company's Bank Details</small>
                    <?php //$bankDetails = $indentMaster->businessPartner->bankAccount->first() ?>
                    <p>Bank Name : <strong>IDBI Bank</strong></p>
                    <p>Acc Name : <strong>0388655000000028</strong></p>
                    <p>Branch & IFSC Code : <strong>Madurai & IBKL0000388</strong></p>
                </td>

            </tr>

            <tr></tr>
            <tr></tr>


            <tr>
                <td rowspan="6" colspan="9">
                    <small>Declaration</small>
                    <p>Any Statutory variation in any govt levies will be to the account of buyer. Subject to Madurai Jurisdiction only.
                        Certified that the particulars given above are correct and the amount indicated represents the price actually charged and there is no
                        additional consideration flowing directly or indirectly from the buyer.
                    </p>
                </td>
                <td rowspan="6" colspan="8">
                    <p style="text-align: right; margin-top: 50px;"><strong>For shakti Cords Pvt. Ltd.</strong></p>
                    <p style="text-align: right; vertical-align: bottom; margin-bottom: -50px;"><strong>Authorised Signatory</strong></p>

                </td>
            </tr>

            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
        </table>
    @else

        {{--IGST and Export Below--}}
        <table class="table table-bordered" style="margin-top: -2px; margin-bottom: 0px;">




            <tr style="height: 10px !important;">
                <td colspan="17" rowspan="3" style="height: 10px;">
                    <small>Amount Chargeable (in words.)</small>
                    <p style="margin: 0px;"><strong>{{$convertedCurrency}}</strong></p>

                </td>
            </tr>

            <tr></tr>
            <tr></tr>

            <tr>
                <td rowspan="2" colspan="7"><p>HSN/SAC</p></td>
                <td rowspan="2" colspan="7"><p>Taxable Value</p></td>
                <td colspan="2"><p>Integrated Tax</p></td>
                <td><p>Total Tax Amount</p></td>
            </tr>
            <tr>
                <td><p>Rate (%)</p></td>
                <td><p>Amount</p></td>
                <td></td>
            </tr>

            {{--below comes the HSN Details --}}

            @foreach($taxAmount as $hsn => $tax)
                <?php
                $amount = $tax['taxableAmount'] + $hsnInsurance[$hsn] + $hsnHandlingCharges[$hsn];
                $totalTaxableValue +=  $amount;
                $totalTaxAmount +=  $taxHsn[$hsn];

                $igstTaxValue = ($hsnIgstTaxRate[$hsn]/100) * $amount;

                $igstTotalTaxValue += $igstTaxValue;
//
                ?>
                <tr>
                    <td rowspan="1" colspan="7"><p>{{$hsn}}</p></td>
                    <td rowspan="1" colspan="7"><p>{{$fmt->format(round($amount, 2))}}</p></td>
                    <td colspan="1"><p>{{$hsnIgstTaxRate[$hsn]}}</p></td>
                    <td colspan="1"><p>{{$fmt->format(round($igstTaxValue, 2))}}</p></td>
                    <td><p>{{$fmt->format(round($igstTaxValue, 2))}}</p></td>
                </tr>



            @endforeach


            <?php

            $convertedCurrency = $currencyToWords->convertIndianCurrency(($totalTaxAmount - ($totalTaxAmount - floor($totalTaxAmount))));

            if(is_numeric( $totalTaxAmount ) && floor( $totalTaxAmount ) != $totalTaxAmount){
                $decimalValue = $totalTaxAmount - floor($totalTaxAmount);

                $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

                $convertedCurrency = str_replace(' and ','',$convertedCurrency).'rupees and '. $decimalWords. 'paise only.';
            }


            ?>
            <tr>
                <td rowspan="1" colspan="7"><p style="text-align: right"><strong>Total</strong></p></td>
                <td rowspan="1" colspan="7"><p>{{$fmt->format(round($totalTaxableValue, 2))}}</p></td>
                <td colspan="1"><p></p></td>
                <td colspan="1"><p>{{$fmt->format(round($igstTotalTaxValue, 2))}}</p></td>
                <td><p>{{$fmt->format(round($igstTotalTaxValue, 2))}}</p></td>
            </tr>




            <tr>
                <td rowspan="4" colspan="9" style="vertical-align: top;">
                    <p>Tax Amount in Words : </p>
                    <p><strong>{{$convertedCurrency}}</strong></p>

                </td>
                <td rowspan="4" colspan="8">
                    <small>Company's Bank Details</small>
                    <p>Bank Name : <strong>IDBI Bank</strong></p>
                    <p>Acc Name : <strong>0388655000000028</strong></p>
                    <p>Branch & IFSC Code : <strong>Madurai & IBKL0000388</strong></p>
                </td>

            </tr>

            <tr></tr>
            <tr></tr>
            <tr></tr>


            <tr>
                <td rowspan="6" colspan="9">
                    <small>Declaration</small>
                    <p>Any Statutory variation in any govt levies will be to the account of buyer. Subject to Madurai Jurisdiction only.
                        Certified that the particulars given above are correct and the amount indicated represents the price actually charged and there is no
                        additional consideration flowing directly or indirectly from the buyer.
                    </p>
                </td>
                <td rowspan="6" colspan="8">
                    <p style="text-align: right; margin-top: 50px;"><strong>For shakti Cords Pvt. Ltd.</strong></p>
                    <p style="text-align: right; vertical-align: bottom; margin-bottom: -50px;"><strong>Authorised Signatory</strong></p>

                </td>
            </tr>

            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
            <tr></tr>
        </table>
@endif

    <small style="text-align: center; font-size: 7px;">This is a Computer Generated Invoice</small>


</div>

</body>

</html>
