<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Box Usage Summary</title>
    <style media="screen">
    html{
        margin: 30px !important;
        font-family: "Helvetica";
    }
    table{
      width: 100%;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 15px;
        padding: 7px;
    }
    .title{
      font-size: 17px !important;
    }
    .center{
      text-align:center;
    }
    .space{
      height: 10px;
    }
    </style>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th class="center title" colspan="2">SCPL ERP BOX USAGE SUMMARY</th>
        </tr>
        <tr>
          <th>From: {{$start}}</th>
          <th>To: {{$end}}</th>
        </tr>
        <tr>
          <th colspan="2" class="space"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Box Type</th>
          <th>No.of Box</th>
        </tr>
        <?php foreach ($boxUsage as $key => $value): ?>
          <tr>
            <td>{{$key}}</td>
            <td>{{$value}}</td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </body>
</html>
