<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    @if(\Route::current()->uri == 'generate-defect-report')
        <title>Defect Report</title>
    @elseif(\Route::current()->uri == 'generate-ncr-report')
        <title>NCR Spindles</title>
    @else
        <title>Countwise Packed</title>
    @endif
    <style media="screen">
        html{
            margin: 10px !important;
            font-family: "Helvetica";
        }
        .table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 4px;
        }
        .space{
            height: 10px;
        }
        .title{
            font-size: 14px !important;
        }
        .center{
            text-align:center;
        }
        td{
            padding-left: 4px !important;
        }
        .date{
            width: 65px;
        }
        .px-ft{
            width: 40px;
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<!-- <table>
      <thead>
      </thead>
    </table> -->

<?php
set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);
?>
<?php $temp = array_keys($defects); ?>


    @foreach($defects as $defectName => $defect)
        <table class="table" style="width: 100%; margin: 10px; margin-left: 0px;">
            <thead>

            <tr>
                <th class="title center" colspan="1" rowspan="2">SCPL ERP</th>

                @if(\Route::current()->uri == 'generate-defect-report')
                    <th class="title center" colspan=3 rowspan="2">{{$machine}} - DEFECT SPINDLES</th>
                @elseif(\Route::current()->uri == 'generate-ncr-report')
                    <th class="title center" colspan=3 rowspan="2">{{$machine}} - NCR SPINDLES</th>
                @else
                    <title>Countwise Packed</title>
                @endif

                <th class="title" colspan="3">From : {{date('d-m-Y H:i', strtotime($startDate))}}</th>
            </tr>
            <tr>
                <th class="title" colspan="3">To : {{date('d-m-Y H:i', strtotime($endDate))}}</th>
            </tr>
            <tr>
                <td colspan="7"><h3 style="text-align: center">{{$defectName}}</h3></td>
            </tr>
            <tr>
                {{--<th>Machine</th>--}}
                <th>Material</th>
                <th>Doff No</th>
                <th>Doff Date</th>
                <th>Filament</th>
                <th>Op. Name</th>
                <th style="width: 60px;">Spindle No</th>
                {{--<th>Tare Wt.</th>--}}
                <th style="width: 60px;">Material. Wt (kgs)</th>
                {{--<th>Total. Wt</th>--}}
            </tr>
            </thead>
        <tbody>


        @foreach($defect as $materialName => $entries)
            <?php //$totalWeight = array_sum(array_column($entries,'total_weight'));
             $materialWeight = array_sum(array_column($entries,'material_weight'));?>
            @foreach($entries as $entry)
                <?php $totalSpindles = count($entries); ?>
                <tr>
{{--                    <td class="px-ft">{{$entry->machine}}</td>--}}
                    <td>{{$materialName}}</td>
                    <td >{{$entry->doff_no}}</td>
                    <td>{{date('d-m-Y', strtotime($entry->doff_date))}}</td>
                    <td>{{$entry->filament}}</td>
                    <td class="px-ft">{{$entry->op_name}}</td>
                    <td class="px-ft">{{$entry->spindle}}</td>
{{--                    <td class="px-ft center">{{$entry->tare_weight}}</td>--}}
                    <td class="px-ft">{{$entry->material_weight}}</td>
                    {{--<td class="px-ft">{{$entry->total_weight}}</td>--}}
                </tr>
            @endforeach
                <tr>
                    {{--<td class="px-ft"></td>--}}
                    <td><strong>Total</strong></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="px-ft"></td>
                    <td class="px-ft"><strong>{{$totalSpindles}}</strong></td>
                    {{--<td class="px-ft center"></td>--}}
                    <td class="px-ft"><strong>{{$materialWeight}}</strong></td>
{{--                    <td class="px-ft"><strong>{{$totalWeight}}</strong></td>--}}
                </tr>

        @endforeach

        </tbody>
    </table>
        @if(end($temp) != $defectName)
            <div class="page-break"></div>
        @endif
    @endforeach


    {{--<tr>--}}
        {{--<td class="px-ft">1234</td>--}}
        {{--<td >456</td>--}}
        {{--<td>BP123</td>--}}
        {{--<td>TYPE - NH</td>--}}
        {{--<td class="px-ft">123.65</td>--}}
        {{--<td class="px-ft">150.5</td>--}}
        {{--<td class="px-ft center">1</td>--}}
        {{--<td class="px-ft">1</td>--}}
        {{--<td class="px-ft">ZELL A</td>--}}

    {{--</tr>--}}
{{--</table>--}}
</body>
</html>
