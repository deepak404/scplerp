<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Doff Wise Production Summary</title>
    <style>
        html{
            margin: 12px !important;
            font-family: "Helvetica";
        }
        table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 10px;
            padding: 4px;
        }
        th{
            text-align: center;
        }
        .t-left{
            text-align: left !important;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th rowspan="3">SCPL ERP</th>
                <th colspan="2" rowspan="3">PRODUCTION SUMMARY - QUALITY WISE {{$status}} - {{$title}} MACHINE</th>
                <th class="t-left">FOR NO:.</th>
            </tr>
            <tr>
                <th class="t-left">Form : {{$from}}</th>
            </tr>
            <tr>
                <th class="t-left">To : {{$to}}</th>
            </tr>
            <tr>
                <th colspan="4" class="space"></th>
            </tr>
            <tr>
                <th>Material</th>
                <th>Floor Code</th>
                <th>Spindle</th>
                <th>Qty</th>
            </tr>
        </thead>
        <?php $total = 0; ?>
        <tbody>
            @foreach ($master as $material => $floorCodes)
                @foreach ($floorCodes as $floorCode => $value)
                    <tr>
                        <td>{{$material}}</td>
                        <td>{{$floorCode}}</td>
                        <td>{{$value['spl']}}</td>
                        <td>{{round($value['net_wt'],2)}}</td>
                    </tr>
                    <?php $total += $value['net_wt']; ?>
                @endforeach
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Total</th>
                <th>{{round($total,2)}}</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>