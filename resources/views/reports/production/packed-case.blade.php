<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style> 
        html{
            margin: 10px !important;
            font-family: "Helvetica";
        }
        table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 9px;
            padding: 4px;
        }
        th{
            text-align: center;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th rowspan="2">SCPL ERP</th>
                <th rowspan="2" colspan="6">Packed Case : {{$type}}</th>
                <th colspan="2">From: {{date('d-m-Y',strtotime($startDate))}}</th>
            </tr>
            <tr>
                <th colspan="2">To: {{date('d-m-Y',strtotime($endDate))}}</th>
            </tr>
            <tr>
                <th>Case</th>
                <th>Code</th>
                <th>Date</th>
                <th>Doff</th>
                <th>Box Type</th>
                <th>Spindle</th>
                <th>Net.Wt</th>
                <th>Gw.Wt</th>
                <th>Info</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($processed as $code=>$cases)
                <?php 
                    $nw = 0;
                    $gw = 0;
                    $box = 0;
                ?>
                @foreach ($cases as $case => $dates)

                    @foreach ($dates as $date=>$data)
<!--                        --><?php //if($case == "35367"){
//                            dd($data);
                        //} ?>
                        <tr>


                            <td>{{$case}}</td>
                            <td>{{$code}}</td>
                            <td>{{date('d-m-Y',strtotime($date))}}</td>
                            <td>{{$data['doff_no']}}</td>
                            <td>{{$data['box_type']}}</td>
                            <td>{{$data['spindle']}}</td>
                            <td>{{round(($data['net_wt']), 1)}}</td>
                            <td>{{round(($data['gw_wt']), 1)}}</td>
                            <td>{{$data['info']}}</td>
                        </tr>
                        <?php 
                            $nw += round(($data['net_wt']), 1);
                            $gw += round(($data['gw_wt']), 1);
                            $box += 1;
                        ?>
                    @endforeach
                @endforeach
                <tr>
                    <th>TOTAL</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Box Count: {{$box}}</th>
                    <th></th>
                    <th>{{$nw}}</th>
                    <th>{{$gw}}</th>
                    <th></th>
                </tr>
                <tr>
                    <td colspan="9"></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>