<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
            padding: 0px;
        }
        .table{
            width: 100%;
        }
        html{
            margin: 15px;
            font-family: "Helvetica Neue", Arial, sans-serif;
        }
        .top-left{
            text-align: left !important;
            padding: 2px 5px;
        }
        .space{
            height: 10px;
            text-align: right !important;
            font-weight: 100;
            padding: 5px !important;
        }
        .title{
            padding: 5px;
        }
        #prep-by{
            text-align: left;
            padding: 5px 20px !important;
            border-right: none !important;
        }
        #verifed{
            text-align: right;
            padding: 5px 20px !important;
            border-left: none !important;
        }
        tfoot>tr>th{
            padding: 5px;
        }
        tbody>tr>td{
            padding: 4px;
        }
    </style>
</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th rowspan="4">SCPL ERP</th>
                <th rowspan="4" colspan="3">MONTHLY PHYSICAL STOCK</th>
                <th class="top-left">FOR/PRO/NI  DA 22</th>
            </tr>
            <tr>
                <th class="top-left">Rev.No:/Date : 00/24.6.2010</th>
            </tr>
            <tr>
                <th class="top-left">Page No:</th>
            </tr>
            <tr>
                <th class="top-left">Approved By: G.M.</th>
            </tr>
            <tr>
                <th class="title" colspan="5">{{$machine}} -DIPPED STOCK FOR {{$title}} AS ON :- {{date('d-m-Y H:i',strtotime($date))}}</th>
            </tr>
            <tr>
                <th class="space" colspan="5">STATEMENT NO  :- 2 A</th>
            </tr>
            <tr>
                <th style="">S.No.</th>
                <th style="">UNIQUE CODE</th>
                <th>FLOOR CODE</th>
                <th style="">NO.OF<br>SPLS.</th>
                <th style="">NET WT<br>IN KG</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; $total = 0; ?>

            @foreach ($weightLogs as $weightLog)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$weightLog->first()->material}}</td>
                    <td>{{$weightLog->first()->floor_code}}</td>
                    <td>{{$weightLog->count()}}</td>
                    <td>{{$weightLog->sum('material_weight')}}</td>
                </tr>
                <?php $count++; $total += $weightLog->sum('material_weight'); ?>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th></th>
                <th colspan=3>TOTAL</th>
                <th>{{$total}}</th>
            </tr>
        </tfoot>
        <tr>
            <th id="prep-by" colspan="2">PREP .BY</th>
            <th id="verifed" colspan="3">VERIFIED BY</th>
        </tr>
    </table>
</body>
</html>