<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Production Report</title>
    <style> 
            html{
                margin: 12px !important;
                font-family: "Helvetica";
            }
            table{
                width: 100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                font-size: 10px;
                padding: 4px;
            }
            th{
                text-align: center;
            }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="3">Production Report</th>
            </tr>
            <tr>
                <th colspan="3">Material - Doff</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productionStatus as $material=>$doffs)
                @foreach ($doffs as $doff=>$weightlogs)
                    <tr>
                        <th colspan="3">{{$material}} - {{$doff}} - {{date('d/m/Y',strtotime($weightlogs[0]->doff_date))}}</th>
                    </tr>
                    @foreach ($weightlogs as $item)
                        <tr>
                            <td>{{$item->spindle}}</td>
                            <td>{{$item->material_weight}}</td>
                            <td>{{$item->reason}}</td>
                        </tr>
                    @endforeach
                @endforeach
            @endforeach
            <tr>
                <td colspan="3">Total Count: {{$count}}</td>
            </tr>
        </tbody>
    </table>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script>
        $(document).bind('keydown', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 80) {
                window.print()
            }            
        });
    </script>
</body>
</html>