<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Production Report</title>
    <style> 
            html{
                margin: 12px !important;
                font-family: "Helvetica";
            }
            table{
                width: 100%;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                font-size: 10px;
                padding: 4px;
            }
            th{
                text-align: center;
            }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="2">Production Report</th>
            </tr>
            <tr>
                <th colspan="2">Material - Doff</th>
            </tr>
            <tr>
                <th>OK AVI</th>
                <th>OK AVP</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productionStatus as $material=>$doffs)
                @foreach ($doffs as $doff=>$weightogs)
                    <tr>
                        <th colspan="2">{{$material}} - {{$doff}} - {{date('d/m/Y',strtotime($weightogs['avi'][0]['doff_date'] ?? $weightogs['avp'][0]['doff_date']))}}</th>
                    </tr>
                    <tr>
                        <td>
                            @if (!empty($weightogs['avi']))
                                <?php $sum = 0; ?>
                                @foreach ($weightogs['avi'] as $item)
                                    <?php $sum += $item['material_weight']; ?>
                                @endforeach
                                    <span>COUNT:  {{count($weightogs['avi'])}}</span>
                                    <span>Material:  {{$sum}}</span>
                            @endif
                        </td>
                        <td>
                            @if (!empty($weightogs['avp']))
                                <?php $sum = 0; ?>
                                @foreach ($weightogs['avp'] as $item)
                                    <?php $sum += $item['material_weight']; ?>
                                @endforeach
                                    <span>COUNT:  {{count($weightogs['avp'])}}</span>
                                    <span>Material:  {{$sum}}</span>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endforeach
            <tr>
                <td colspan="2">Total Count: {{$count}}</td>
            </tr>
        </tbody>
    </table>
    <script src="//code.jquery.com/jquery.min.js"></script>
    <script>
        $(document).bind('keydown', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 80) {
                window.print()
            }            
        });
    </script>
</body>
</html>