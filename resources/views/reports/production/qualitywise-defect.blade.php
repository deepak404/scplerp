<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    @if(\Route::current()->uri == 'generate-defect-report')
        <title>Defect Report</title>
    @elseif(\Route::current()->uri == 'generate-ncr-report')
        <title>NCR Spindles</title>
    @else
        <title>Countwise Packed</title>
    @endif
    <style media="screen">
        html{
            margin: 10px !important;
            font-family: "Helvetica";
        }
        .table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 4px;
        }
        .space{
            height: 10px;
        }
        .title{
            font-size: 14px !important;
        }
        .center{
            text-align:center;
        }
        td{
            padding-left: 4px !important;
        }
        .date{
            width: 65px;
        }
        .px-ft{
            width: 40px;
        }

        .page-break {
            page-break-after: always;
        }

        .spindle-entries td{
            width: 16.66% !important;
        }

    </style>
</head>
<body>
<!-- <table>
      <thead>
      </thead>
    </table> -->

<?php
set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);
?>
<?php $temp = array_keys($result); $errorSummary = []; $doffsRan = [];

for($i=0; $i<=29; $i++){
    $j = $i+1;
    $doffsRan['E'.$j] = 0;
    $doffsRan['W'.$j] = 0;

}

?>


@foreach($result as $doff => $spindles)

<?php //dd($spindles); ?>
    <table class="table" style="width: 100%; margin: 10px; margin-left: 0px;">
        <thead>

        <tr>
            <th class="title center" colspan="1" rowspan="2">SCPL ERP</th>

            @if(\Route::current()->uri == 'generate-defect-report')
                <th class="title center" colspan=4 rowspan="2">{{$machine}} - DEFECT SPINDLES</th>
            @elseif(\Route::current()->uri == 'generate-ncr-report')
                <th class="title center" colspan=4 rowspan="2">{{$machine}} - NCR SPINDLES</th>
            @elseif(\Route::current()->uri == "generate-doffwise-defect")
                <th class="title center" colspan=4 rowspan="2">{{$machine}} - DOFF WISE QUALITY</th>
            @endif

            <th class="title" colspan="1">From : {{date('d-m-Y', strtotime($startDate))}}</th>
        </tr>
        <tr>
            <th class="title" colspan="1">To : {{date('d-m-Y', strtotime($endDate))}}</th>
        </tr>
        <tr>
            <td colspan="6"><h3 style="text-align: center">{{$material}}</h3></td>
        </tr>
        <tr>
            <td colspan="6"><h3 style="text-align: center">{{$doff}}</h3></td>
        </tr>
        <tr>
            {{--<th>Machine</th>--}}
            <th>Spindle</th>
            <th>Reason</th>
            <th>Ins. Reason</th>

            <th>Spindle</th>
            <th>Reason</th>
            <th>Ins. Reason</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $spindleCount = [];
        list($eSpindles, $wSpindles) = array_chunk($spindles, ceil(count($spindles) / 2));
        for($i=0; $i<=29; $i++){
            $spindleCount[] = $i;

        }





        ?>

        @foreach ($spindleCount as $spindleNo)
            <tr class="spindle-entries">
                <td style="text-align: center;">{{$eSpindles[$spindleNo]->spindle}}</td>

                @if($eSpindles[$spindleNo]->material_weight == 0)
                    <td>-</td>
                @else
                    @if($eSpindles[$spindleNo]->reason != '')
                            <td>{{$eSpindles[$spindleNo]->reason}}</td>

                        <?php

                            $doffsRan['E'.($spindleNo + 1)] += 1;


                            if(array_key_exists($eSpindles[$spindleNo]->reason, $errorSummary)){

                            }else{
                                $errorSummary[$eSpindles[$spindleNo]->reason] =[];
                            }


                            if(array_key_exists($eSpindles[$spindleNo]->spindle, $errorSummary[$eSpindles[$spindleNo]->reason])){
                                $errorSummary[$eSpindles[$spindleNo]->reason][$eSpindles[$spindleNo]->spindle] += 1;

                            }else{
                                $errorSummary[$eSpindles[$spindleNo]->reason][$eSpindles[$spindleNo]->spindle] = 1;

                            }

                        ?>
                    @else

                        <?php $doffsRan['E'.($spindleNo + 1)] += 1;?>
                        <td>OK</td>

                    @endif
                @endif
                <td>{{$eSpindles[$spindleNo]->inspection_reason}}</td>
                <td style="text-align: center;">{{$wSpindles[$spindleNo]->spindle}}</td>
                @if($wSpindles[$spindleNo]->material_weight == 0)
                    <td>-</td>
                @else
                    @if($wSpindles[$spindleNo]->reason != '')
                        <td>{{$wSpindles[$spindleNo]->reason}}</td>

                        <?php

                        $doffsRan['W'.($spindleNo + 1)] += 1;


                        if(array_key_exists($wSpindles[$spindleNo]->reason, $errorSummary)){

                        }else{
                            $errorSummary[$wSpindles[$spindleNo]->reason] =[];
                        }


                        if(array_key_exists($wSpindles[$spindleNo]->spindle, $errorSummary[$wSpindles[$spindleNo]->reason])){
                            $errorSummary[$wSpindles[$spindleNo]->reason][$wSpindles[$spindleNo]->spindle] += 1;

                        }else{
                            $errorSummary[$wSpindles[$spindleNo]->reason][$wSpindles[$spindleNo]->spindle] = 1;

                        }

                        ?>
                    @else
                        <?php $doffsRan['W'.($spindleNo + 1)] += 1;?>
                        <td>OK</td>

                    @endif
                @endif
                <td>{{$wSpindles[$spindleNo]->inspection_reason}}</td>
            </tr>

        @endforeach







        </tbody>
    </table>
@if(end($temp) != $doff)
    <div class="page-break"></div>
@endif

@endforeach

<div class="page-break"></div>

<?php //dd($doffsRan); ?>

<?php $spindleNos = array_keys($spindles); natsort($spindleNos);  $errorTemp = array_keys($errorSummary);?>

@foreach($errorSummary as $reason => $spindles)


    <table class="table" style="width: 100%; margin: 10px; margin-left: 0px;">
        <thead>

        <tr>
            <th class="title center" colspan="1" rowspan="3">SCPL ERP</th>


                <th class="title center" colspan=4 rowspan="3">ERROR SUMMARY</th>

            <th class="title" colspan="1">From : {{date('d-m-Y', strtotime($startDate))}}</th>
        </tr>
        <tr>
            <th class="title" colspan="1">To : {{date('d-m-Y', strtotime($endDate))}}</th>
        </tr>
        <tr>
            <th class="title" colspan="1">Total Doffs : {{$totalDoff}}</th>

        </tr>
        <tr>
            <td colspan="6"><h3 style="text-align: center">{{$material}}</h3></td>
        </tr>

        <tr>
            <td colspan="6"><h3 style="text-align: center">{{$reason}}</h3></td>
        </tr>

        <tr>
            <th colspan="2">Spindle</th>
            <th colspan="2">Occurences</th>
            <th colspan="2">Doffs </th>
        </tr>
        </thead>
        <tbody>

        <?php
//                dd($spindles);
        $spindleKeys = array_keys($spindles);
        natsort($spindleKeys);
        $sortedSpindles = [];

        foreach ($spindleKeys as $k){
            $sortedSpindles[$k] = $spindles[$k];
        }


//        dd($sortedSpindles);

        ?>
            @foreach($sortedSpindles as $spindle => $occ)
                <tr>
                    <td colspan="2">{{$spindle}}</td>
                    <td colspan="2">{{$occ}}</td>
                    <td colspan="2">{{$doffsRan[$spindle]}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @if(end($errorTemp) != $reason)
        <div class="page-break"></div>
    @endif

@endforeach


<div class="page-break"></div>






</body>
</html>
