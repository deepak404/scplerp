<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
            padding: 0px;
        }
        .table{
            width: 100%;
        }
        html{
            margin: 15px;
            font-family: "Helvetica Neue", Arial, sans-serif;
        }
        .top-left{
            text-align: left !important;
            padding: 2px 5px;
        }
        .space{
            height: 10px;
            text-align: right !important;
            font-weight: 100;
            padding: 5px !important;
        }
        .title{
            padding: 5px;
        }
        #prep-by{
            text-align: left;
            padding: 5px 20px !important;
            border-right: none !important;
        }
        #verifed{
            text-align: right;
            padding: 5px 20px !important;
            border-left: none !important;
        }
        tfoot>tr>th{
            padding: 5px;
        }
        tbody>tr>td{
            padding: 4px;
        }
    </style>
</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th rowspan="4">SCPL ERP</th>
                <th rowspan="4" colspan="5">MONTHLY PHYSICAL STOCK</th>
                <th class="top-left" colspan="2">FOR/PRO/NI  DA 22</th>
            </tr>
            <tr>
                <th class="top-left" colspan="2">Rev.No:/Date : 00/24.6.2010</th>
            </tr>
            <tr>
                <th class="top-left" colspan="2">Page No:</th>
            </tr>
            <tr>
                <th class="top-left" colspan="2">Approved By: G.M.</th>
            </tr>
            <tr>
                <th class="title" colspan="8">{{$machine}} -DIPPED STOCK FOR {{$title}} AS ON :- {{date('d-m-Y H:i',strtotime($date))}}</th>
            </tr>
            <tr>
                <th class="space" colspan="8">STATEMENT NO  :- 2 A</th>
            </tr>
            <tr>
                <th style="width:6%;">S.No.</th>
                <th style="width:15%;">UNIQUE CODE</th>
                <th style="width:15%;">TYPE</th>
                <th>FLOOR CODE</th>
                <th style="width:10%;">DOFF NO</th>
                <th style="width:7%;">NO.OF<br>SPLS.</th>
                <th style="width:15%;">D.O.P.</th>
                <th style="width:8%;">NET WT<br>IN KG</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; $total = 0; ?>

            @foreach ($weightLogs as $weightLog)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$weightLog->first()->material}}</td>
                    <td>{{$weightLog->first()->filament}}</td>
                    <td>{{$weightLog->first()->floor_code}}</td>
                    <td>{{$weightLog->first()->doff_no}}</td>
                    <td>{{$weightLog->count()}}</td>
                    <td>{{date('d/m/Y H:i',strtotime($weightLog->first()->doff_date))}}</td>
                    <td>{{$weightLog->sum('material_weight')}}</td>
                </tr>
                <?php $count++; $total += $weightLog->sum('material_weight'); ?>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th></th>
                <th colspan="6">TOTAL</th>
                <th>{{$total}}</th>
            </tr>
        </tfoot>
        <tr>
            <th id="prep-by" colspan="4">PREP .BY</th>
            <th id="verifed" colspan="4">VERIFIED BY</th>
        </tr>
    </table>
</body>
</html>