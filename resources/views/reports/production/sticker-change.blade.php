<html>
    <head>

        <style type="text/css">
          table {
              border-collapse: collapse;
          }

        table, th, td {
            border: 1px solid black;
        }

        th,td{
            height: 10px !important;
            padding: 3px;
        }
        p{
            font-size: 12px;
            margin-top: 0px;
            margin-bottom: 0px;
        }

          html{
              margin: 10px;
              font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
          }

            #table-header>td>p{
                font-weight: 600;
            }
        </style>
    </head>
    <body>
        <table style="width: 100%;">
            <thead>
                <tr>
                    <th colspan="2" rowspan="3"><h5>SCPL ERP</h5></th>
                    <th colspan="14" rowspan="3" style="text-align: center"><h5>Sticker Changes - {{$doff}}</h5></th>
                    <th colspan="4"><p>FORMAT NO : P/R13</p></th>
                </tr>
                <tr>
                    <th colspan="4"><p>APPROVED BY : MD</p></th>
                </tr>
                <tr>
                    <th colspan="4"><p>DEPT : PRODUCTION</p></th>
                </tr>
            </thead>
            <tbody>
                <tr id="table-header">
                    <td colspan="2"><p>Old Doff No</p></td>
                    <td colspan="2"><p>Spl. No</p></td>
                    <td colspan="2"><p>Date</p></td>
                    <td colspan="3"><p>Net. Wt. in Kgs</p></td>
                    <td colspan="3"><p>Reason</p></td>
                    <td colspan="2"><p>Old Doff No</p></td>
                    <td colspan="2"><p>Spl. No</p></td>
                    <td colspan="2"><p>Date</p></td>
                    <td colspan="2"><p>Net. Wt. in Kgs</p></td>
                </tr>
                <?php $beforeTotalWeight = $afterTotalWeight = 0; ?>

                @foreach($spindles as $spindle)
                    <?php
                        $beforeTotalWeight += $spindle['before']['material_weight'];
                        $afterTotalWeight += $spindle['after']['material_weight'];
                    ?>
                    <tr>
                        <td colspan="2"><p>{{$spindle['before']['doff_no']}}</p></td>
                        <td colspan="2"><p>{{$spindle['before']['spindle']}}</p></td>
                        <td colspan="2" style="width: 10%;"><p>{{date('d-m-Y', strtotime($spindle['before']['doff_date']))}}</p></td>
                        <td colspan="3"><p>{{$spindle['before']['material_weight']}}</p></td>
                        <td colspan="3"><p>{{$spindle['before']['reason']}}</p></td>
                        <td colspan="2"><p>{{$spindle['after']['doff_no']}}</p></td>
                        <td colspan="2"><p>{{$spindle['after']['spindle']}}</p></td>
                        @if($spindle['after']['doff_date'] != null)
                            <td colspan="2"><p>{{date('d-m-Y', strtotime($spindle['after']['doff_date']))}}</p></td>
                        @else
                            <td colspan="2"><p></p></td>
                        @endif
                        <td colspan="2"><p>{{$spindle['after']['material_weight']}}</p></td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2"><p><strong>Total</strong></p></td>
                    <td colspan="2"><p></p></td>
                    <td colspan="2"><p></p></td>
                    <td colspan="3"><p><strong>{{$beforeTotalWeight}}</strong></p></td>
                    <td colspan="3"><p></p></td>
                    <td colspan="2"><p></p></td>
                    <td colspan="2"><p></p></td>
                    <td colspan="2"><p></p></td>
                    <td colspan="2"><p><strong>{{$afterTotalWeight}}</strong></p></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>