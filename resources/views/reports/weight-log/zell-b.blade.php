<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ZELL B Weight Log Report</title>

    <style>
        html{
            margin: 10px;
            font-family: "Helvetica";
        }
        h3{
            text-align: center;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            font-size: 12px;
            padding: 0px;
        }


        #header-table>tbody>tr>td>p{
            padding-left: 20px;
            font-weight: bold;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        p>span{
            font-weight: bold;
        }

        #header-table>tbody>tr>td{
            padding: 6px !important;
            height: 17px;
        }

        #detail-table p{
            margin-left: 10px;
        }

        #header-table>tbody>tr>td{
            height: 10px;
        }


        #spindle-table{
            margin-top: 20px;
            border: none !important;
        }

        #spindle-table th{
            padding: 10px;
            border-top: none !important;
        }
        #east-table tbody>tr>td, #west-table tbody>tr>td{
            padding: 3px;
        }

        #east-table tbody>tr>td:first-child{
            border-left: none !important;
        }

        #east-table, #west-table, #spindle-table{
            border: none !important;
        }

        #east-table th:first-child, #west-table{
            border-left: none !important;
        }

        #east-table th:last-child, #west-table th:last-child{
            border-right: none;
        }

        #detail-table>tbody>tr>td{
            padding: 2px;
            height: 1px;
        }


        #east-table>tbody>tr>td:last-child, #west-table>tbody>tr>td:last-child{
            border-right: none;
        }

        #total-table>tbody>tr>td{
            padding: 4px;
            border: none;
        }

        #total-table{
            border:none;
        }

    </style>
</head>
<body>

<table style="width: 100%;" id="header-table">
    <tbody>
    <tr>
        <td style="width: 20%; text-align: center;" rowspan="4" colspan="3"><h3>SCPL ERP</h3></td>
        <td rowspan="4" style="width: 50%; text-align: center" colspan="6"><h2>ZELL B WEIGHT LOG REPORT</h2></td>
        <td colspan="4" style="width: 30%;"><p>FOR/PRO/DB 6A</p></td>
    </tr>
    <tr>
        <td colspan="4"><p>Rev.No/Date : 00/13.6.7</p></td>
    </tr>
    <tr>
        <td colspan="4"><p>Page No : 1</p></td>

    </tr>
    <tr>
        <td colspan="4"><p>Approved By : G.M</p></td>

    </tr>
    </tbody>
</table>
<p style="text-align: center; font-size: 15px; margin-top: 7px; margin-bottom: 7px;">Date From - {{date('d-m-Y H:i:s', strtotime($master->from_date))}} To {{date('d-m-Y H:i:s', strtotime($master->to_date))}}</p>

<table style="width: 100%;" id="detail-table">
    <tr>
        <td style="width: 33%;">
            <p><span>Machine</span>: ZELL B</p>
            <p><span>Count</span>: {{$master->itemMaster->material}}</p>
        </td>

        <td>
            <p><span>Tare</span>: {{$master->tare_weight}}</p>
            <p><span>Doff</span>: {{$master->doff_no}}</p>
        </td>

        <td><p><span>Date & Time</span>: 18-08-2018 15:30:00 To 18-08-2018 16:23:05</p>
            <p><span>Done By</span>: Naveen</p>
        </td>
    </tr>
</table>
<table id="spindle-table" style="width: 100%;">

    <tr>
        <td>
            <table style="width: 100%;" id="east-table">
               <tbody>
                   <tr>
                       <th>Spindle</th>
                       <th>Weight</th>
                       <th colspan="2">Quality</th>
                   </tr>

                   @foreach($master->WeightLogDetails as $details)
                       @if($details->spindle[0] == 'E')

                           <tr>
                               <td>{{$details->spindle}}</td>
                               <td>{{$details->weight}}</td>

                               @if($details->quality_check == 0)
                               <td style="width: 20%;">OK</td>
                               <td style="width: 20%;"></td>
                               @else
                               <td style="width: 20%;"></td>
                               <td style="width: 20%;">Stitches</td>
                               @endif

                           </tr>

                       @endif

                   @endforeach
               </tbody>
            </table>
        </td>


        <td>
            <table style="width: 100%;" id="west-table">
                <tbody>
                    <tr>
                        <th>Spindle</th>
                        <th>Weight</th>
                        <th colspan="2">Quality</th>
                    </tr>
                    @foreach($master->WeightLogDetails as $details)
                        @if($details->spindle[0] == 'W')

                            <tr>
                                <td>{{$details->spindle}}</td>
                                <td>{{$details->weight}}</td>

                                @if($details->quality_check == 0)
                                    <td style="width: 20%;">OK</td>
                                    <td style="width: 20%;"></td>
                                @else
                                    <td style="width: 20%;"></td>
                                    <td style="width: 20%;">Stitches</td>
                                @endif

                            </tr>

                        @endif

                    @endforeach
                </tbody>
            </table>
        </td>
    </tr>
</table>


<table style="width: 50%; margin-top: 10px;" id="total-table">
    <tr>
        <td><strong>Total Entries</strong></td>
        <td>{{$eastEntryCount}}</td>
        <td>{{$westEntryCount}}</td>
    </tr>
    <tr>
        <td><strong>Total Ok Weight</strong></td>
        <td>{{$eastEntryOkWeight}}</td>
        <td>{{$westEntryOkWeight}}</td>
    </tr>
    <tr>
        <td><strong>Total Not Ok Weight</strong></td>
        <td>{{$eastEntryNotOkWeight}}</td>
        <td>{{$westEntryNotOkWeight}}</td>
    </tr>
    <tr>
        <td><strong>Total Weight</strong></td>
        <td>{{$totalWeight}}</td>
        <td></td>
    </tr>
</table>

</body>
</html>
