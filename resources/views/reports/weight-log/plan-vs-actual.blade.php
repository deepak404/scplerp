<html>
<head>
	<meta charset="UTF-8">
	<title>Plan Vs. Actual Report</title>
	<style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 12px;
            padding: 0px;
        }
        .pp-table > thead > tr > th,.pp-table > tbody > tr > td{
			padding: 10px;
        }
        html{
        	margin: 15px;
        }
        .page-break {
		    page-break-after: always;
		}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<thead>
			<tr>
				<th rowspan="3" style="width: 20%;font-size: 15px;">SCPL ERP</th>
				<th rowspan="3" style="width: 50%;font-size: 15px;">PRODUCTION PLAN</th>
				<th style="text-align: left; padding: 5px;">FOR / PRO :KJHAO</th>
			</tr>
			<tr>
				<th style="text-align: left; padding: 5px;">Rev.No.:/Date : 01 / 01.07.2018</th>
			</tr>
			<tr>
				<th style="text-align: left; padding: 5px;">Approved By : G.M.</th>
			</tr>
		</thead>
	</table>
	<div style="text-align: center;">
		<h3>
			<u>TENTATIVE PRODUCTION PLAN FOR {{$machine}} - ({{$month}})</u>
		</h3>
	</div>

	<table class="pp-table" style="width: 100%;">
		<thead>
			<tr>
				<th>DATE</th>
				<th>CODE/FINISH</th>
				<th>CUSTOMER</th>
				<th>Qty. in Kgs</th>
				<th>TYPE</th>
			</tr>
		</thead>
		<tbody class="pp-body">
			@foreach($plan as $value)
			<tr>
				<td>{{$value['date']}}</td>
				<td>{{$value['code']}}</td>
				<td>{{$value['customer']}}</td>
				<td>{{$value['qty']}}</td>
				<td>{{$value['type']}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<div class="page-break"></div>

	<table style="width: 100%;">
		<thead>
			<tr>
				<th rowspan="3" style="width: 20%; font-size: 15px;">SCPL</th>
				<th rowspan="3" style="width: 50%; font-size: 15px;">ACTUAL PRODUCTION</th>
				<th style="text-align: left; padding: 5px;">FOR / PRO :KJHAO</th>
			</tr>
			<tr>
				<th style="text-align: left; padding: 5px;">Rev.No.:/Date : 01 / 01.07.2018</th>
			</tr>
			<tr>
				<th style="text-align: left; padding: 5px;">Approved By : G.M.</th>
			</tr>
		</thead>
	</table>
	<div style="text-align: center;">
		<h3>
			<u>ACTUAL PRODUCTION FOR {{$machine}} - ({{$month}})</u>
		</h3>
	</div>

	<table class="pp-table" style="width: 100%;">
		<thead>
			<tr>
				<th>DATE</th>
				<th>CODE/FINISH</th>
				<th>CUSTOMER</th>
				<th>Qty. in Kgs</th>
				<th>REMARKS</th>
			</tr>
		</thead>
		<tbody class="pp-body">
			@foreach($actual as $value)
			<tr>
				<td>{{$value['date']}}</td>
				<td>{{$value['code']}}</td>
				<td></td>
				<td>{{$value['qty']}}</td>
				<td></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
