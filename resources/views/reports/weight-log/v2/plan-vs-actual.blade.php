<html>
<head>
	<meta charset="UTF-8">
	<title>Plan Vs. Actual Report</title>
	<style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 9px;
            padding: 0px;
        }
        .pp-table > thead > tr > th,.pp-table > tbody > tr > td{
						padding: 10px;
        }
        html{
	        	margin: 10px;
        }
        .page-break {
				    page-break-after: always;
				}
				th,td {
					padding: 5px !important;
				}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<thead>
			<tr>
				<th style="width: 20%;font-size: 15px;">SCPL ERP</th>
				<th style="width: 80%;font-size: 15px;">{{$machine}} - PRODUCTION PLAN Vs. ACTUAL For {{$month}}</th>
			</tr>
		</thead>
	</table>
	{{-- <div style="text-align: center;">
		<h4>
			<u>{{$machine}} - PLAN Vs. ACTUAL For {{$month}}</u>
		</h4>
	</div> --}}
	<table style="width: 100%; border:none;">
		<tr style="border:none;">
			<td style="border:none; vertical-align: text-top;">
				<table class="pp-table" style="width: 100%;">
					<thead>
						<tr>
							<th colspan="5">Plan</th>
						</tr>
						<tr>
							<th>Plan From</th>
							<th>Plan To</th>
							<th>CODE/FINISH</th>
							<th>Qty. in Kgs</th>
							<th>TYPE</th>
						</tr>
					</thead>
					<tbody class="pp-body">
						<?php $sum = 0; foreach ($finalPlan as $startDate => $materials): ?>
							<?php foreach ($materials as $material => $filaments): ?>
								<?php foreach ($filaments as $filament => $value): ?>
									<?php 
										$sum += $value['sum'];
									?>
										<tr>
											<td>{{date('d-m-Y h:i',strtotime($startDate))}}</td>
											<td>{{date('d-m-Y h:i',strtotime($value['end_date']))}}</td>
											<td>{{$material}}</td>
											<td>{{$value['sum']}}</td>
											<td>{{$filament}}</td>
										</tr>
								<?php endforeach; ?>
							<?php endforeach; ?>
						<?php endforeach; ?>						
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th></th>
							<th></th>
							<th>{{$sum}}</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>

			<?php  ?>
			<td style="border:none; vertical-align: text-top;">
				<table class="pp-table" style="width: 100%;">
					<thead>
						<tr>
							<th colspan="5">Actual</th>
						</tr>
						<tr>
							{{--<th>Prod. From</th>--}}
							<th>Start Date</th>
							<th>End Date</th>
							<th>CODE/FINISH</th>
							<th>Qty. in Kgs</th>
							<th>Filament</th>
							{{--<th>REMARKS</th>--}}
						</tr>
					</thead>
					<tbody class="pp-body">
						<?php $sum2 = 0; ?>
						@foreach($actualWeightLog as $data)
								<tr>
									<td>{{date('d-m-Y H:i',strtotime($data['start']))}}</td>
									<td>{{date('d-m-Y H:i',strtotime($data['end_date']))}}</td>
									<td>{{$data['material']}}</td>
									<td>{{$data['sum']}}</td>
									<td>{{implode(',',array_unique($data['filament']))}}</td>
								</tr>
								<?php $sum2 += $data['sum']; ?>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th></th>
							<th></th>
							<th>{{$sum2}}</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</table>


</body>
</html>
