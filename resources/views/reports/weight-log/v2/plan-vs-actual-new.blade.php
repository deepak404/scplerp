<html>
<head>
	<meta charset="UTF-8">
	<title>Plan Vs. Actual Report</title>
	<style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
            font-size: 9px;
            padding: 0px;
        }
        .pp-table > thead > tr > th,.pp-table > tbody > tr > td{
						padding: 10px;
        }
        html{
	        	margin: 10px;
        }
        .page-break {
				    page-break-after: always;
				}
				th,td {
					padding: 5px !important;
				}
	</style>
</head>
<body>
	<table style="width: 100%;">
		<thead>
			<tr>
				<th style="font-size: 15px;" colspan="2">SCPL ERP</th>
				<th style="font-size: 15px;" colspan="8">{{strtoupper($machine)}} - PRODUCTION PLAN Vs. ACTUAL For {{$month}}</th>
			</tr>
			<tr>
				<th colspan="5">Plan</th>
				<th colspan="5">Actual</th>
			</tr>
			<tr>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Code</th>
				<th>Qty</th>
				<th>Filement</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Code</th>
				<th>Qty</th>
				<th>Filement</th>
			</tr>
		</thead>
		<tbody>
			<?php $planTotal = 0; $actualTotal = 0; ?>
			@foreach ($planActualArray as $item)
				<?php $planTotal += $item['plane_qty'] ?? 0;
					$actualTotal += $item['actual_qty'] ?? 0;
					?>
				<tr>
					@if (empty($item['plane_start']))
						<td>-</td>
						<td>-</td>
					@else
						<td>{{date('d-m-Y H:i',strtotime($item['plane_start']))}}</td>
						<td>{{date('d-m-Y H:i',strtotime($item['plane_end']))}}</td>
					@endif
					<td>{{$item['plane_material'] ?? '-'}}</td>
					<td>{{$item['plane_qty'] ?? '-'}}</td>
					<td>{{$item['plane_filement'] ?? '-'}}</td>
					@if (empty($item['actual_start']))
						<td>-</td>
						<td>-</td>
					@else
						<td>{{date('d-m-Y H:i',strtotime($item['actual_start']))}}</td>
						<td>{{date('d-m-Y H:i',strtotime($item['actual_end']))}}</td>
					@endif
					<td>{{$item['actual_material'] ?? '-'}}</td>
					<td>{{$item['actual_qty'] ?? '-'}}</td>
					<td>{{$item['actual_filement'] ?? '-'}}</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<th>Total</th>
				<th></th>
				<th></th>
				<th>{{$planTotal}}</th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th>{{$actualTotal}}</th>
				<th></th>
			</tr>
		</tfoot>
	</table>



</body>
</html>
