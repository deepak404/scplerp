<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Filament Confirmation Report</title>

	<style>
		html{
			margin: 10px;
			font-family: "Helvetica";
		}
		h3{
			text-align: center;
		}
		.table, .th, .td {
		    border: 1px solid black;
		    border-collapse: collapse;
		    text-align: center;
		    font-size: 10px;
		    padding: 2px;
		}
		.bp-table{
			margin-top: 10px;
			width: 40%;
		}
		.page-break {
		    page-break-after: always;
		}
	</style>
</head>
<body>
	<h3> SCPL ERP - FILAMENT CONFIRMATION FOR {{$month}}</h3>
	<?php foreach ($planes as $key => $value): ?>
		<table style="width: 100%; margin-bottom: 10px;">
			<tbody>
				<tr>
					<td style="width: 60%; vertical-align: top;">
					  	<table class="table" style="width: 100%">
					  		<thead>
					  			<tr>
						  			<th class="th" colspan="5">
						  				<?php if ($key == 1): ?>
							  				MACHINE : ZELL "A"
					  					<?php elseif ($key == 2): ?>
							  				MACHINE : ZELL "B"
							  			<?php else: ?>
							  				MACHINE : KIDDE
						  				<?php endif ?>
						  			</th>
					  			</tr>
						  		<tr>
						  			<th class="th">S.No</th>
						  			<th class="th">COUNT</th>
						  			<th class="th">ORDER<br>IN Kgs</th>
						  			<th class="th">PREVIOUS</th>
						  			<th class="th">CURRENT</th>
						  		</tr>
					  		</thead>
					  		<tbody>
					  			<?php $count = 1; ?>
					  			<?php foreach ($value as $material => $data): ?>
										<?php foreach ($data as $filament => $info): ?>
								  		<tr>
								  			<td class="td">{{$count}}</td>
								  			<td class="td">{{$material}}</td>
												<td class="td">{{$info['order_quantity']}}</td>
												<td class="td">{{$info['current']}}</td>
												<td class="td">{{$filament}}</td>
								  		</tr>
							  		<?php $count += 1; ?>
										<?php endforeach; ?>
					  			<?php endforeach ?>
					  			<tr>
					  				<td class="td" colspan="2">TOTAL</td>
					  				<td class="td">{{$total[$key]}}</td>
					  				<td class="td"></td>
					  				<td class="td"></td>
					  			</tr>
					  		</tbody>
					  	</table>
					</td>
					<td style="width: 40%; vertical-align: top;">
					  	<table class="table" style="width: 100%">
					  		<tbody>
					  			<tr>
					  				<td class="td" colspan="3">
							  			CONSUMPTION
					  				</td>
					  			</tr>
					  			<tr>
					  				<td class="td">Filament</td>
					  				<td class="td">Previous</td>
					  				<td class="td">Current</td>
					  			</tr>
					  			<?php $current = 0; $change = 0; ?>
					  			<?php foreach ($consumption[$key] as $key2 => $data): ?>
					  				<?php if ($data['change'] != 0): ?>
						  			<tr>
						  				<td class="td">{{$key2}}</td>
						  				<td class="td">{{$data['current']}}</td>
						  				<td class="td">{{$data['change']}}</td>
						  			</tr>
					  				<?php endif ?>
						  			<?php
						  				$current += $data['current'];
						  				$change += $data['change'];
						  			 ?>
					  			<?php endforeach ?>
					  			<tr>
					  				<td></td>
					  				<td class="td">{{$current}}</td>
					  				<td class="td">{{$change}}</td>
					  			</tr>
					  		</tbody>
					  	</table>
					  	<!-- <table class="table bp-table">
					  		<tbody>
					  			<?php foreach ($party[$key] as $key3 => $value): ?>
							  		<tr>
							  			<td class="td">{{\App\BusinessPartner::where('id',$key3)->value('bp_name')}}</td>
							  			<td class="td">{{$value}}</td>
							  		</tr>
					  			<?php endforeach ?>
					  		</tbody>
					  	</table> -->
					</td>
				</tr>
			</tbody>
		</table>
		<?php $arrayKeys = array_keys($planes); ?>
		<?php if (end($arrayKeys) != $key): ?>
			<!-- <div class="page-break"></div> -->
		<?php endif; ?>
	<?php endforeach ?>
	<table style="width: 100%;">
		<tbody>
			<td style="width: 50%;  vertical-align: top;">
				<table class="table" style="width: 100%;">
					<thead>
						<tr>
							<td class="td" colspan="5">
								OVER ALL CUNSUMPTION
							</td>
						</tr>
						<tr>
							<td class="td">TYPE</td>
							<td class="td">Previous</td>
							<td class="td">Current</td>
							<td class="td">Filament<br>stock</td>
							<td class="td">Transit Stock</td>
						</tr>
					</thead>
					<tbody>
						<?php
							$overAllCurrent = 0;
							$overAllChange = 0;
						 ?>
						<?php foreach ($overAllConsuption as $key4=>$data): ?>
							<?php if ($data['change'] != 0): ?>
							<tr>
								<td class="td">{{$key4}}</td>
								<td class="td">{{$data['current']}}</td>
								<td class="td">{{$data['change']}}</td>
								<td class="td"></td>
								<td class="td"></td>
							</tr>
							<?php endif ?>
							<?php
								$overAllChange += $data['change'];
								$overAllCurrent += $data['current'];
							 ?>
						<?php endforeach ?>
						<tr>
							<td class="td">Total</td>
							<td class="td">{{$overAllCurrent}}</td>
							<td class="td">{{$overAllChange}}</td>
							<td class="td"></td>
							<td class="td"></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 50%; vertical-align: top;">
<!-- 				<table class="table" style="width: 50%;">
					<tbody>
						<tr>
							<td class="td" colspan="2">
								NOTE
							</td>
						</tr>
						<tr>
							<td class="td">S</td>
							<td class="td" style="text-align: left; padding-left: 5px;">SK 10 A</td>
						</tr>
						<tr>
							<td class="td">AK</td>
							<td class="td" style="text-align: left; padding-left: 5px;">KOLAN ARAMIDE</td>
						</tr>
						<tr>
							<td class="td">Y</td>
							<td class="td" style="text-align: left; padding-left: 5px;">Guxi - HMLS - 240 Fila</td>
						</tr>
						<tr>
							<td class="td">GTH</td>
							<td class="td" style="text-align: left; padding-left: 5px;">Cuxi - HT- 1300Dr</td>
						</tr>
						<tr>
							<td class="td">LS</td>
							<td class="td" style="text-align: left; padding-left: 5px;">Guxi - HTLS yarn</td>
						</tr>
						<tr>
							<td class="td">U</td>
							<td class="td" style="text-align: left; padding-left: 5px;">UNIFULL - HMLS</td>
						</tr>
						<tr>
							<td class="td">D</td>
							<td class="td" style="text-align: left; padding-left: 5px;">PERFORMANCE</td>
						</tr>
					</tbody>
				</table> -->
			</td>
		</tbody>
	</table>
</body>
</html>
