<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tentative Production Plan</title>


	<style>
		html{
				margin: 10px;
		}
		body{
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
		h3{
			text-align: center;
		}
		table, th, td {
		    border: 1px solid black;
		    border-collapse: collapse;
		    text-align: center;
		    font-size: 12px;
		    padding: 5px;
		    margin-bottom: 0px;
		}
		.page-break {
		    page-break-after: always;
		}
	</style>
</head>
<body>
	<?php foreach ($master as $key => $value): ?>
		<?php if ($key == 1): ?>
			<h3>SCPL ERP - ZELL "A" TENTATIVE PRODUCTION PLAN - {{$month}}</h3>
		<?php elseif ($key == 2): ?>
			<h3>SCPL ERP - ZELL "B" TENTATIVE PRODUCTION PLAN - {{$month}}</h3>
		<?php elseif ($key == 3): ?>
			<h3>SCPL ERP - KIDDE TENTATIVE PRODUCTION PLAN - {{$month}}</h3>
		<?php endif ?>
		<table style="width: 100%;">
			<thead>
				<tr>
					<th>S.<br>No.</th>
					<th>Count</th>
					<!-- <th>Customer</th> -->
					<th>Order in<br>kgs.</th>
					<th>Cord Wt.</th>
					<th>No.of spls.</th>
					<th>Speed in<br>mpm</th>
					<th>Prod./Hr</th>
					<th>Running Hr.</th>
					<th>No.of<br>changes</th>
					<th>Changes<br>hr.</th>
				</tr>
			</thead>
			<?php
			$totalRunning = 0;
			 ?>
			<tbody>
				<?php $count = 1; ?>
				<?php foreach ($value as $material=>$data): ?>
					<?php
							$prod_hr = round(array_sum($data['prod_hr'])/count($data['prod_hr']),1);
							$runningHr = round(array_sum($data['order_quantity'])/$prod_hr,1);
							$totalRunning += $runningHr;
					 ?>
					<tr>
						<td style="text-align: right;">{{$count}}</td>
						<td style="text-align: left;">{{$material}}</td>
						<!-- <td style="text-align: left;"></td> -->
						<td style="text-align: right;">{{array_sum($data['order_quantity'])}}</td>
						<td style="text-align: right;">{{round(array_sum($data['cord_wt'])/count($data['cord_wt']),2)}}</td>
						<td style="text-align: right;">{{round(array_sum($data['no_of_spls'])/count($data['no_of_spls']),2)}}</td>
						<td style="text-align: right;">{{round(array_sum($data['speed_mpm'])/count($data['speed_mpm']),2)}}</td>
						<td style="text-align: right;">{{$prod_hr}}</td>

						<td style="text-align: right;">{{$runningHr}}</td>
						<td style="text-align: right;">{{array_sum($data['no_of_changes'])}}</td>
						<td style="text-align: right;">{{round(array_sum($data['changes_per_hr']),2)}}</td>
					</tr>
					<?php $count += 1; ?>
				<?php endforeach ?>
				<tr>
					<td colspan="2">Total</td>
					<td style="text-align: right;">{{$total[$key]['order_quantity']}}</td>
					<td colspan="4"></td>
					<td style="text-align: right;">{{$totalRunning}}</td>
					<td style="text-align: right;">{{$total[$key]['no_of_changes']}}</td>
					<td style="text-align: right;">{{$total[$key]['changes_per_hr']}}</td>
				</tr>
			</tbody>
		</table>
		<table style="width: 35%;">
			<tbody>
				<tr>
					<th></th>
					<th>Hrs.</th>
					<th>Days</th>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">Total Running Hrs.</th>
					<td>{{$totalRunning}}</td>
					<td>{{round($totalRunning/24,1)}}</td>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">No. of Changes</th>
					<td>{{$total[$key]['changes_per_hr']}}</td>
					<td>{{round($total[$key]['changes_per_hr']/24,1)}}</td>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">Maintenance</th>
					<?php $maintanance = \App\Machine::where('id',$key)->value('maintanance'); ?>
					<td>{{$maintanance}}</td>
					<td>{{round($maintanance/24,1)}}</td>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">Trial</th>
					<?php $trials = \App\Machine::where('id',$key)->value('trials'); ?>
					<td>{{$trials}}</td>
					<td>{{round($trials/24,1)}}</td>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">Other works</th>
					<?php $other = \App\Machine::where('id',$key)->value('other'); ?>
					<td>{{$other}}</td>
					<td>{{round($other/24,1)}}</td>
				</tr>
				<tr>
					<th style="text-align: left; padding-left: 5px;">Total</th>

					<?php $totalMaster =  $total[$key]['changes_per_hr']+$totalRunning+$maintanance+$other+$trials;?>
					<td>{{round($totalMaster,1)}}</td>
					<td>{{round($totalMaster/24,1)}}</td>
				</tr>
			</tbody>
		</table>
		<?php $arrayKeys = array_keys($master); ?>
		<?php if (end($arrayKeys) != $key): ?>
			<div class="page-break"></div>
		<?php endif; ?>
	<?php endforeach ?>
</body>
</html>
