@extends('layouts.activity')
<style>
    .container{
        margin-top: 100px !important;
    }

    body{
        background-color: #f1f1f1 !important;
    }

    .panel-heading{
        border-bottom: 1px solid gainsboro !important;
    }

    .table>thead>tr>th{
        font-weight: 400 !important;
    }

    .label{
        margin-top: 5px;
        display: inline-block !important;
        padding: .5em .6em .5em !important;
        font-size: 75% !important;
    }

    a:hover{
        cursor: pointer;
    }

    #changes-table th{
        font-weight: 600 !important;
    }

</style>
@section('content')
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Activities</h4>
                        </div>
                        <div class="panel-body">
                           <table class="table">
                                <thead>
                                    <th>User Name</th>
                                    <th>Department</th>
                                    <th>Action</th>
                                    <th>Reason</th>
                                    <th>Time</th>
                                    <th>Activity Info</th>
                                    <th>Changes</th>
                                </thead>
                               <tbody>
                                    @foreach($activities as $activity)
                                        <tr>
                                            <td>{{\App\User::where('id',$activity->user_id)->value('user_name')}}</td>
                                            <td>{{$activity->dept_name}}</td>
                                            @if($activity->activity_type == "edit")
                                                <td><span class="label label-warning">Edit</span></td>
                                                @else
                                                <td><span class="label label-danger">Delete</span></td>
                                            @endif
                                            <td>{{$activity->reason}}</td>
                                            <td>{{date('d-m-Y H:i', strtotime($activity->log_time))}}</td>
                                            @if($activity->activity_type == "delete")
                                                <td style="width: 20%">Click Show changes to see the deleted Entry details.</td>

                                            @else
                                                <td style="width: 20%">{{$activity->log_details}}</td>
                                            @endif
                                            <td><a data-id="{{$activity->id}}" class="show-activity-change">Show Changes</a></td>
                                        </tr>
                                    @endforeach
                               </tbody>
                           </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Changes Log</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table" id="changes-table">
                            <thead>
                                <th style="width: 30%">Changes</th>
                                <th>Previous</th>
                                <th>Updated</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </body>
@endsection

@section('script')
    <script src="{{ url('js/activity-index.js') }}"></script>

@endsection