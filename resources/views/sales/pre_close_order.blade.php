@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/sales-index.css') }}">
    <link rel="stylesheet" href="{{url('responsive-css/sales-order.css')}}">
@endsection

@section('content')
	<section id="sales-head">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <h3>Pending Order</h3>

	                <div class="sales-options">
                    <select name="bp_id" id="bp_id">
                      <option value="">All</option>
                      @foreach ($businessPartners as $businessPartner)
                        @if ($id == $businessPartner->id)
                          <option value="{{$businessPartner->id}}" selected>{{$businessPartner->bp_name}}</option>
                        @else
                          <option value="{{$businessPartner->id}}">{{$businessPartner->bp_name}}</option>                            
                        @endif
                      @endforeach
                    </select>
	                </div>
	            </div>
	        </div>
	    </div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
	</section>


	<section id="sales-table">
    <div class="container-fluid">
      <div class="row">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Business Partner</th>
              <th>Ref No.</th>
              <th>Quality</th>
              <th>Order Quantity</th>
              <th>Dispatch Quantity</th>
              <th>Pending Quantity</th>
              <th>Order Date</th>
							<th>Price (per Kg.)</th>
              <th>Pre Close</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($pendingOrders as $key => $value): ?>
              <tr>
                <td>{{$value->bp_name}}</td>
                <td>{{$value->ref_no}}</td>
                <td>{{$value->material_name}}</td>
                <td style="text-align:center;">{{$value->order_quantity}}</td>
                <td style="text-align:center;">{{$value->dispatch_quantity}}</td>
                <td style="text-align:center;">{{round($value->pending_quantity,1)}}</td>
								<td>{{date('d-m-Y',strtotime($value->customer_date))}}</td>
                <td style="text-align:center;">{{$value->price}}</td>
                <td style="text-align:center;"><i class="material-icons pre-close" data-id="{{$value->id}}">remove_shopping_cart</i></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
	</section>



	<section id="pop-ups">

		<div id="preClose" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 400px !important;">

				<!-- Modal content-->
				<div class="modal-content" style="margin-top:135px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Pre Close Order</h4>
					</div>
					<div class="modal-body">
						<form class="" action="/pre-close-data" method="post">
							@csrf
							<input type="hidden" name="id" value="">
              <strong>
                <p class="bp">JK FEN</p>
                <p class="ref">Ref No:</p>
                <p class="qty">quantity</p>
                <p class="val">1000</p>
                <p class="dated">date</p>
              </strong>
							<input class="text-input" style="width:100%; margin-bottom:10px;" type="text" name="reason" value="" required placeholder="Pre Close Reason">
							<input class="btn btn-primary" type="submit" value="Update">
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
@endsection

@section('script')
<script type="text/javascript">
  $(document).ready(function() {
    $('input[type="text"]').attr('autocomplete','off');
    $('.pre-close').on('click',function() {
      var tr = $(this).parent().parent();
      $('input[name="reason"]').val("");
      $('input[name="id"]').val($(this).data("id"));
      $('.bp').text(tr.children('td').eq(0).text());
      $('.ref').text(tr.children('td').eq(1).text());
      $('.qty').text(tr.children('td').eq(2).text());
      $('.val').text('Pending: '+tr.children('td').eq(5).text());
      $('.dated').text(tr.children('td').eq(6).text());
      $('#preClose').modal('show');
    });
    $('#bp_id').on('change',function(){
      window.location.href = "/pre-close-order/"+$(this).val();
    });
  });

</script>

@endsection
