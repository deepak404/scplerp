@extends('layouts.sales')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .bp-list-card{
            width: 350px;
            height: 340px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .card-holder{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 80vh;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }
    </style>
@endsection

@section('content')
    <section id="pf-head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 card-holder">
                    <div class="bp-list-card">
                        <h3 class="text-center">Customer Order Trend</h3>
                        <form method="GET"  target="_blank">
                            {{--@csrf--}}
                            <div class="form-group">
                                <span for="bp">Customer : </span>
                                <select name="bp" id="bp">
                                    <option value="" selected disabled>Select Customer</option>
                                    <?php foreach (\App\BusinessPartner::all() as $key => $value): ?>
                                    <option value="{{$value['id']}}">{{$value['bp_name']}}</option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary center-block" value="Show Details">
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#bp').on('change', function(){

                var bpId = $(this).find('option:selected').val();
               $(this).closest('form').attr('action','customer-order-trend-report/'+bpId);
            });



        });
    </script>
@endsection