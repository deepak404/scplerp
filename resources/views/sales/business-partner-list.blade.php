@extends('layouts.sales')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/indent-list.css') }}">
@endsection

@section('content')
    <section id="customer-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h3>Customer List</h3>

                    <div class="indent-options">
                        <a href="#" class="btn btn-primary create-customer-btn" data-toggle="modal" data-target="#customer-modal">Create Customer</a>

                    </div>
                </div>

                <div class="col-md-12 table-wrapper">
                    @if($errors->any())
                        <ul style="padding-left: 0px;">
                            @foreach ($errors->all() as $error)
                                <li style="list-style-type: none; color: red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <table class="table ">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>District</th>
                            <th>State</th>
                            <th>Mobile</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>Info</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1; ?>
                            @foreach($businessPartners as $businessPartner)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$businessPartner->bp_name}}</td>
                                    <td>{{$businessPartner->email}}</td>
                                    <td>{{$businessPartner->district}}</td>
                                    <td>{{$businessPartner->state}}</td>
                                    <td>{{$businessPartner->phone_number}}</td>
                                    <td><i class="material-icons edit-customer" data-bpid="{{$businessPartner->id}}">edit</i></td>
                                    <td><i class="material-icons delete-customer" data-bpid="{{$businessPartner->id}}">delete</i></td>
                                    <td><i class="material-icons show-customer" data-bpid="{{$businessPartner->id}}">info</i></td>
                                </tr>
                                <?php $count++; ?>
                                @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <section id="pop-ups">
        <div id="customer-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create Customer</h4>
                    </div>
                    <div class="modal-body row">
                        <form action="/create-customer" method="POST" name="create-customer-form" class="col-md-12 create-customer-form">
                            {{csrf_field()}}
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bp_name">Name</label>
                                        <input type="text" class="text-input" id="bp_name" name="bp_name" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phone_number">Mobile</label>
                                        <input type="text" class="text-input" id="phone_number" name="phone_number" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="text-input" id="email" name="email" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input type="text" class="text-input" id="address" name="address" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pincode">Pin</label>
                                        <input type="text" class="text-input" id="pincode" name="pincode" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="district">District</label>
                                        <input type="text" class="text-input" id="district" name="district" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address">State</label>
                                        <input type="text" class="text-input" id="state" name="state" required>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ar_no">AR No</label>
                                        <input type="text" class="text-input" id="ar_no" name="ar_no" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="code">Code</label>
                                        <input type="text" class="text-input" id="code" name="code" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="contact_person">Contact Person Name</label>
                                        <input type="text" class="text-input" id="contact_person" name="contact_person" required>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="contact_no">Contact Person No</label>
                                        <input type="text" class="text-input" id="contact_no" name="contact_no" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dispatcher">Dispatcher</label>
                                        <input type="text" class="text-input" id="dispatcher" name="dispatcher" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="dispatch_destination">Dispatch Destination</label>
                                        <input type="text" class="text-input" id="dispatch_destination" name="dispatch_destination" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gst_code">GST Code</label>
                                        <input type="text" class="text-input" id="gst_code" name="gst_code" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary" value="Create">
                                    </div>
                                </div>
                            </div>
                        </form>




                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ url('js/indent-list.js') }}"></script>


    <script>
        $(document).ready(function(){
            $('.create-customer-form').on('submit', function(e) {
                e.preventDefault();

                console.log($(this).serialize());
            })
        });
    </script>

@endsection