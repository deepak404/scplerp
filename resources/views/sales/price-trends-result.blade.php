@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.sales' )


@section('css')

    <style>
        .machine-list-card{
            width: 350px;
            height: 400px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .highcharts-legend-item rect{
            display: none;
        }


        .graph-container{
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }

        .graph-card-holder{
            /*background-color: white;*/


        }

        .graph-card{
            height: 420px;
            background-color: white;
            margin: 20px;


        }

        #pf-head{
            margin-top: 80px;
        }


        #client-name{
            margin-left: 40px;
        }

        .highcharts-point{
            /*display: none;*/
        }

    </style>

@endsection

@section('content')

    <section id="pf-head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 card-holder">

                    <h3 id="client-name">{{$clientName}} -  Price Trend</h3>
                    {{--<div class="col-md-6 col-lg-6 col-sm-6 graph-card-holder">--}}
                        {{--<div class="graph-card col-md-12">--}}
                            {{--<div class="graph-container" id="sample-graph">--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    @foreach($results as $materialName => $result)
                    <div class="col-md-6 col-lg-6 col-sm-12 graph-card-holder">
                        <div class="graph-card col-md-12">
{{--                            <h4>{{$materialName}}</h4>--}}

                            <div class="graph-container" id="{{$materialName}}">

                            </div>
                        </div>
                    </div>

                    @endforeach


                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('js/highcharts/highcharts.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/data.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/exporting.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/export-data.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            var results = {!! json_encode($results) !!};


            $.each(results, function(material, data){

                var categories = [];
                var rate = [];
                var quantity = [];
               // console.log(data);

               $.each(data, function(date, price){
                   // console.log(price);
                  categories.push(price[1]);
                  rate.push(price[0]);
                  quantity.push(price[2]);

               });


               // console.log(categories);
               generateGraph(material, categories, rate, quantity)


            });


            function generateGraph(material, categories, rate, quantity){
                console.log(rate, quantity);
                Highcharts.chart(material, {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: material
                    },
                    xAxis: {
                        categories: categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Price Per Kg.'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        formatter : function(){

                            var index = rate.indexOf(this.point.x);
                            var tQuantity = quantity[this.point.x];

                            return ' ' +
                                'Quantity : <strong>' + tQuantity + ' Kgs</strong><br />'+
                                'Rate : <strong> Rs. '+ this.point.y+'/Kg</strong><br/>';
                        },

                        // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        // '<td style="padding:0"><b>{tQuantity} Rs</b></td></tr>',
                        // footerFormat: '</table>',
                        // shared: true,
                        // useHTML: true,


                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0,
                            color : '#003ebb',
                            dataLabels: {
                                enabled: true,
                                crop: false,
                                overflow: 'none'
                            }
                        }
                    },
                    series: [{
                        name: 'Month',
                        data: rate,
                        quantity : quantity,
                    },

                    ]
                });
            }


            $('#from-date, #to-date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd-mm-yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });

            function getKeyByValue(object, value) {
                console.log(object, value)
                return Object.keys(object).find(key => object[key] === value);
            }






        });
    </script>

@endsection
