@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/indent-list.css') }}">
@endsection

@section('content')
	<section id="indent-list">
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <h3>Indent List</h3>

	                <div class="indent-options">
	                    <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date">

	                </div>
	            </div>

	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-wrapper">
					@if($errors->any())
						<ul style="padding-left: 0px;">
							@foreach ($errors->all() as $error)
								<li style="list-style-type: none; color: red;">{{ $error }}</li>
							@endforeach
						</ul>
					@endif
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>S.No</th>
	                            <th>Buyers Reference No/ Order No</th>
	                            <th>Party Name</th>
															<?php if (\Auth::user()->dept_id == 1): ?>
																<th>Edit</th>
																<th>Delete</th>
															<?php endif; ?>
	                            <th>Export</th>
								{{-- <th>Invoice</th> --}}
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $count=1; ?>
	                    	@foreach($indentMaster as $value)
	                        <tr>
	                            <td>{{$count}}</td>
	                            <td>{{$value['order_ref_no']}}</td>
	                            <td>{{$value['businessPartner']['bp_name']}}</td>
															<?php if (\Auth::user()->dept_id == 1): ?>
																@if ($value->invoice->count() > 0)
																	<td>Invoiced</td>
																	<td>Invoiced</td>
																@else
																<td><a href="/edit-indent/{{$value['id']}}" class="edit-indent "><i class="material-icons">edit</i></a></td>
																<td><a href="#" data-id="{{$value['id']}}" class="delete-indent"><i class="material-icons">delete</i></a></td>
																@endif
															<?php endif; ?>
	                            <td><a href="/generate-indent-report/{{$value['id']}}" class="info-indent" target="_blank" data-id="{{$value['id']}}"><i class="material-icons">info</i></a></td>
								{{-- <td><a href="/create-invoice/{{$value['id']}}" class="create-invoice" target="_blank" data-id="{{$value['id']}}">Create Invoice</a></td> --}}
	                        </tr>
	                        <?php $count++; ?>
	                        @endforeach
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</section>
	<div id="confirmationModal" class="modal fade" role="dialog">
		<div class="modal-dialog" style="width: 400px !important;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Confirmation</h4>
				</div>
				<div class="modal-body">
					<h4 class="confirmation-info">Are you surely want to delete this indent ? </h4>
						<div class="confirm-div">
							<input type="button" class="btn btn-primary delete-sale-btn" value="Yes">
							<input type="button" class="btn btn-primary delete-sale-btn" value="No">
						</div>
				</div>
			</div>

		</div>
	</div>
@endsection

@section('script')
<script src="{{ url('js/indent-list.js?v=1.0') }}"></script>

@endsection
