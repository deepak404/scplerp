@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.sales' )

@section('css')

    <style>
        .machine-list-card{
            width: 350px;
            height: 400px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        /*.highcharts-legend-item rect{*/
            /*display: none;*/
        /*}*/


        .graph-container{
            padding-top: 20px;
            padding-bottom: 20px;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }

        .graph-card-holder{
            /*background-color: white;*/


        }

        .graph-card{
            height: 700px;
            background-color: white;
            margin: 20px;


        }

        #pf-head{
            margin-top: 80px;
        }


        #client-name{
            margin-left: 40px;
        }

        .highcharts-point{
            /*display: none;*/
        }

        #order-div{
            height: 150px;
            background-color: white;
            margin-left: 50px;
            margin-right: 50px;
            width: 47%;
        }

        #order-div table{
            vertical-align: middle;
            margin-top: 30px;

        }

    </style>

@endsection

@section('content')

    <section id="pf-head">
        <div class="container-fluid">
            <div class="row">

                @if(\Route::current()->uri == 'customer-dispatch-report')
                <h3 style="margin-left: 3%; margin-bottom: 40px;">Previous Month Dispatch Quantity</h3>
                @else
                    <h3 style="margin-left: 3%; margin-bottom: 40px;">Previous Month Order Quantity</h3>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12" id="order-div">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            @foreach(array_keys($saleOrders) as $year)
                                <th>{{$year}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach(array_values($saleOrders) as $quantity)
                                <td>{{$quantity}} Kgs</td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12 col-md-12 card-holder">



                    <h3 id="client-name">Order Trend</h3>
                    {{--<div class="col-md-6 col-lg-6 col-sm-6 graph-card-holder">--}}
                    {{--<div class="graph-card col-md-12">--}}
                    {{--<div class="graph-container" id="sample-graph">--}}

                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <?php //$highPriorityNames = ['J.K. Fenner India Ltd (MADURAI)','J.K Fenner (India) Ltd., (Patancheru)','Contitech India Pvt Ltd.,',
//                        'OMFA Rubbers Ltd.,','446','OMFA Rubbers Ltd., - KASNA', 'N.K. Enterprises', 'OM Enterprises', 'Volga Transmission Pvt Ltd.,', 'Dharamshila Belting Pvt Ltd.,', 'Gates (India) Pvt Ltd.,'];
                    ?>
                    @foreach($highPriority as $bpName => $result)
                        <div class="col-md-6 col-lg-6 col-sm-12 graph-card-holder">
                            <div class="graph-card col-md-12">
                                <h3 class="text-center">{{$bpName}}</h3>

                                <div class="graph-container" id="{{$bpName}}">

                                </div>
                            </div>
                        </div>
                    @endforeach



                    @foreach($results as $bpName => $result)


                        <div class="col-md-6 col-lg-6 col-sm-12 graph-card-holder">
                            <div class="graph-card col-md-12">
                                <h3 class="text-center">{{$bpName}}</h3>

                                <div class="graph-container" id="{{$bpName}}">

                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('js/highcharts/highcharts.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/data.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/exporting.js')}}"></script>
    <script type="text/javascript" src="{{url('js/highcharts/export-data.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            var results = {!! json_encode($results) !!};
            var highPriority = {!! json_encode($highPriority) !!};

            resultCalculation(results);
            resultCalculation(highPriority);

            function resultCalculation(results){
                $.each(results, function(bpName, data){


                    var categories = [];
                    var quantity = [];
                    var material = bpName;
                    var tempMaterial = [];

                    $.each(data,  function(month, details){

                        categories.push(month);
                        $.each(details, function(material, quantity){
                            tempMaterial.push(material);

                        });

                    });

                    // console.log(tempMaterial)
                    tempMaterial = removeDuplicateArray(tempMaterial)
                    // console.log(tempMaterial);
                    var series = [];
                    $.each(tempMaterial, function(k, v){
                        // console.log(v);
                        var tempQuantity = [];
                        $.each(data,  function(month, details){
                            // console.log(data, month, tempMaterial);
                            if(data[month][v] === undefined){
                                data[month][v] = 0;
                            }

                            tempQuantity.push(data[month][v]);
                        });

                        series.push({name : v, data : tempQuantity})

                    })

                    generateGraph(material, categories, series)



                });
            }

            function generateGraph(material, categories, series){

                if(material == 'Gates (India) Pvt Ltd.,'){
                    var yAxisProperties = {
                        min: 0,
                        max:21000,
                        title: {
                            text: 'Total Order Quantity (kgs)'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    }
                }else{
                    var yAxisProperties = {
                        min: 0,
                        title: {
                            text: 'Total Order Quantity (kgs)'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            }
                        }
                    }
                }

                Highcharts.chart(material, {
                    chart: {
                        type: 'column',
                        height : '600'
                    },

                    title: {
                        text: ''
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: yAxisProperties,
                    legend: {
                        // align: 'right',
                        // x: -30,
                        // verticalAlign: 'top',
                        // y: 25,
                        // floating: true,
                        // backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        // borderColor: '#CCC',
                        // borderWidth: 1,
                        // shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    series: series
                });
            }


            $('#from-date, #to-date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd-mm-yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });


            function removeDuplicateArray(arr) {
                var seen = {};
                var ret_arr = [];
                for (var i = 0; i < arr.length; i++) {
                    if (!(arr[i] in seen)) {
                        ret_arr.push(arr[i]);
                        seen[arr[i]] = true;
                    }
                }
                return ret_arr;

            }



        });
    </script>

@endsection
