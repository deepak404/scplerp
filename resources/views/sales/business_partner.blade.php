@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<style media="screen">
		#bp-head{
			margin: 70px 0px 20px 0px;
		}
		h3, .bp-options{
			display: inline-block !important;
		}
		.bp-options{
			float: right;
			margin-top: 15px;
		}
		.feedback{
			color: #ff3737e0;
			font-weight: 600;
		}
		#bp-table{
			overflow: auto;
			max-height: 80vh;
			max-width: 100vw;
		}
		.material-icons{
			cursor: pointer;
		}
		td{
			vertical-align: middle !important;
		}
	</style>
@endsection

@section('content')
	<section id="bp-head">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <h3>Business Partner</h3>
	                <div class="bp-options">
	                    <button type="button" class="btn btn-primary" id="create-bp">Add Business Partner</button>
	                </div>
	            </div>
	        </div>
	    </div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
	</section>


	<section id="sales-table">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id='bp-table'>
	                <table class="table table-striped table-bordered">
	                    <thead>
	                        <tr>
	                            <th>S.No</th>
	                            <th>Customer <br> Name</th>
	                            <th>GST</th>
	                            <th>Address</th>
	                            <th>State</th>
															<th>District</th>
	                            <th>Code</th>
	                            <th>AR No</th>
	                            <th>Contact Person</th>
	                            <th>Contact No</th>
	                            <th>Dispatcher</th>
	                            <th>Dispatch<br>Destination</th>
	                            <th>Pincode</th>
	                            <th>Email</th>
															<th>Phone</th>
															<th>Edit</th>
	                        </tr>
	                    </thead>
	                    <tbody>
												<?php $count=1; foreach ($businessPartners as $businessPartner): ?>
													<tr>
														<td><strong>{{$count}}</strong></td>
														<td>{{$businessPartner->bp_name}}</td>
														<td>{{$businessPartner->gst_code}}</td>
														<td>{{$businessPartner->address}}</td>
														<td>{{$businessPartner->state}}</td>
														<td>{{$businessPartner->district}}</td>
														<td>{{$businessPartner->code}}</td>
														<td>{{$businessPartner->ar_no}}</td>
														<td>{{$businessPartner->contact_person}}</td>
														<td>{{$businessPartner->contact_no}}</td>
														<td>{{$businessPartner->dispatcher}}</td>
														<td>{{$businessPartner->dispatch_destination}}</td>
														<td>{{$businessPartner->pincode}}</td>
														<td>{{$businessPartner->email}}</td>
														<td>{{$businessPartner->phone_number}}</td>
														<td><i class="material-icons edit-bp" data-id="{{$businessPartner->id}}">edit</i></td>
													</tr>
												<?php $count++; endforeach; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</section>

<section id='pop-ups'>

	<div id="bpModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content col-md-12 col-lg-12 col-sm-12">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Business Partner</h4>
	      </div>
	      <div class="modal-body col-md-12 col-lg-12 col-sm-12">
					<form class="col-md-12 col-lg-12 col-sm-12" id="bp-form" action="#" method="post">
						<input type="hidden" name="id" value="">
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
					    <label for="name">BP Name:</label>
					    <input type="text" class="form-control" id="name" name="bp_name">
							<span class="feedback"></span>
					  </div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="gst">GST:</label>
							<input type="text" class="form-control upper-case" id="gst" name="gst_code">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-12 col-lg-12 col-sm-12">
							<label for="address">Address:</label>
							<input type="text" class="form-control" id="address" name="address">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="state">State:</label>
							<input type="text" class="form-control" id="state" name="state">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="district">District:</label>
							<input type="text" class="form-control" id="district" name="district">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="pincode">PIN:</label>
							<input type="text" class="form-control numeric" id="pincode" name="pincode">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="pincode">Transit Insurance (%)</label>
							<input type="text" class="form-control" id="transit-insurance" name="transit_insurance">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="code">CODE:</label>
							<input type="text" class="form-control upper-case" id="code" name="code">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="ar_no">AR No:</label>
							<input type="text" class="form-control upper-case" id="ar_no" name="ar_no">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="contact_person">Contact Person:</label>
							<input type="text" class="form-control" id="contact_person" name="contact_person">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="contact_no">Contact No:</label>
							<input type="text" class="form-control numeric" id="contact_no" name="contact_no">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="dispatcher">Dispatcher:</label>
							<input type="text" class="form-control" id="dispatcher" name="dispatcher">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="dispatch_destination">Dispatch Destination:</label>
							<input type="text" class="form-control" id="dispatch_destination" name="dispatch_destination">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="email">Email:</label>
							<input type="email" class="form-control" id="email" name="email">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="phone">Phone:</label>
							<input type="text" class="form-control numeric" id="phone" name="phone_number">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="mode_of_transport">Mode of Transport:</label>
							<input type="mode_of_transport" class="form-control" id="mode_of_transport" name="mode_of_transport">
							<span class="feedback"></span>
						</div>
						<div class="form-group col-md-6 col-lg-6 col-sm-12">
							<label for="password">Password:</label>
							<input type="password" class="form-control" id="password" name="password">
							<span class="feedback"></span>
						</div>

						<div class="form-group col-md-12 col-lg-12 col-sm-12">
								<input type="submit" class="btn btn-primary center-block" value="Save">
						</div>
					</form>
	      </div>
	    </div>

	  </div>
	</div>

</section>



@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('input').attr('autocomplete','off');
		$('#create-bp').on('click',function() {
			$('#bp-form').trigger("reset");
			$('input[name="id"]').val('');
			$('#bpModal').modal('show');
		});
		$(document).on('submit', '#bp-form', function(event) {
			event.preventDefault();
			console.log($(this).serialize());
			var formData = $(this).serialize();
			$.ajax({
				type: "POST",
				url: "/create-business-partner",
				data: formData,
				success: function(data) {
					if (data.status) {
						location.reload();
					}else{
						alert(data.msg);
					}
				},
				error: function(xhr) {
						if (xhr.status == 422) {
							errorHandler(xhr.responseJSON.errors);
						}
				},
			});
		})
		$(document).on('input','.upper-case',function(){
        $(this).val($(this).val().toUpperCase());
    });
		$(document).on('input','.numeric' ,function() {
      $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });
		function errorHandler(errors) {
			$.each(errors, function(key,value){
			$("#"+key).next(".feedback").text(value[0]);
			});
		}
		$('.form-control').on('keyup',function(){
			$(this).next(".feedback").text('');
		});
		$(document).on('click', '.edit-bp', function() {
			var formData = 'id='+$(this).data('id');
			$.ajax({
				type: "POST",
				url: "/get-bp",
				data: formData,
				success: function(data) {
					if (data.status) {
						var bp = data.bp;
						$.each(bp, function(key,val) {
							var select = 'input[name="'+key+'"]';
							$(select).val(val);
						});
						$('#bpModal').modal('show');
					}else{
						alert(data.msg);
					}
				},
				error: function(xhr) {
					console.log(xhr);
				},
			});
		});
	});

</script>

@endsection
