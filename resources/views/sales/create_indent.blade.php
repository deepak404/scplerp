@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link rel="stylesheet" href="{{url('responsive-css/generate-indent.css')}}">
@endsection

@section('content')
	<section id="generate-indent">
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 generat-spacing">
	                <h3>Generate Indent</h3>
	                @if ($errors->any())
	                    <div class="alert alert-danger">
	                        <ul>
	                            @foreach ($errors->all() as $error)
	                                <li>{{ $error }}</li>
	                            @endforeach
	                            <li>Please re-create indent with required.</li>
	                        </ul>
	                    </div>
	                @endif
	                <form action="/create-indent" method="POST" id="generate-indent-form" name="generate-indent-form">
	                	@csrf
	                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="customer_name">Customer Name</label>
									<select name="customer_name" id="customer_name" required>
										<option value="0" selected disabled>Select Customer</option>
										@foreach($businessPartner as $value)
										<?php if ($value['bp_name'] != "buffer"): ?>
											<option value="{{$value['id']}}">{{$value['bp_name']}}</option>
										<?php endif; ?>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="quotation-number">Quotation Number</label>
									<input type="text" class="text-input" id="quotation-number" name="quotation-number" value="{{$quotationNo}}" required autocomplete="off">
								</div>
							</div>

						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<label for="quotation-number">Modes/ Terms of Payment</label>
								<input type="text" class="text-input" id="payment-mode" name="payment-mode" required autocomplete="off">
							</div>
						</div>
						<div id="sales-order-list" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
	                        <span style="font-weight: 700; color: #555555">Sales Order List</span>
	                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 checkbox p-lr-zero">
	                        	<p class="info">Select Customer to Create Indent</p>
	                        </div>
	                    </div>


						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 p-lr-zero">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="tax">Indent type</label>
									<select class="text-input" id="indent-type" name="indent-type" required autocomplete="off">
										<option value="ORDER CONFIRMATION">Order Confirmation</option>
										<option value="PROFORMA INVOICE">Proforma Invoice</option>
									</select>
								</div>
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="tax">Delivery Type</label>
									<select class="text-input" id="delivery-type" name="delivery-type" required autocomplete="off">
										<option value="1">Intra State</option>
										<option value="2">Inter State</option>
										<option value="3">Export</option>
									</select>
								</div>
							</div>
						</div>



	                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 p-lr-zero">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="tax">Transit Insurance (%)</label>
									<input type="text" class="text-input" id="tax" name="tax" required autocomplete="off">
								</div>
							</div>

							{{-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="tax">Handling Charges (Rs.)</label>
									<input type="text" class="text-input" id="handling-charges" name="handling-charges" required autocomplete="off">
								</div>
							</div> --}}
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="date">Date</label>
									<input type="text" class="text-input" id="date" name="date" value = "{{date('d-m-Y')}} "required autocomplete="off">
								</div>
							</div>
						</div>

	                   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">

						   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							   <div class="form-group">
								   <label for="reference-no">Reference No/ Order No</label>
								   <input type="text" class="text-input" id="reference-no" name="reference-no" required autocomplete="off">
							   </div>
						   </div>
						   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							   <div class="form-group">
								   <label for="other-reference-no">Other Reference No</label>
								   <input type="text" class="text-input" id="other-reference-no" name="other-reference-no" autocomplete="off">
							   </div>
						   </div>
					   </div>

	                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">


							<div class="col-xs-12 col-sm-4 col-md-4 col-sm-4">
								<div class="form-group">
									<label for="dispatch">Dispatch Through</label>
									<input type="text" class="text-input" id="dispatch" name="dispatch" required autocomplete="off">
								</div>
							</div>

							<div class="col-xs-12 col-sm-4 col-md-4 col-sm-4">
								<div class="form-group">
									<label for="mode_of_transport">Transport Mode</label>
									<input type="text" class="text-input" id="mode_of_transport" name="mode-of-transport"  required autocomplete="off">
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4 col-sm-4" style="vertical-align: top;">
								<div class="form-group">
									<label for="destination">Destination</label>
									<input type="text" class="text-input" id="destination" name="destination" required autocomplete="off">
								</div>
							</div>
						</div>

	                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-lr-zero">

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<label for="terms">Terms of Delivery</label>
									<textarea class="text-input" style="resize: none" name="terms" id="terms" cols="30" rows="10"></textarea>
								</div>
							</div>
						</div>

	                    <div>
	                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
	                            <input type="submit" class="btn btn-primary create-btn" value="Create Indent">
	                        </div>
	                    </div>



	                </form>
	            </div>
	        </div>
	    </div>
	</section>
@endsection

@section('script')
<script src="{{url('js/generate-indent.js')}}"></script>
@endsection
