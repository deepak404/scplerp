@extends('layouts.sales')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/sales-index.css') }}">
    <link rel="stylesheet" href="{{url('responsive-css/sales-order.css')}}">

    <style>
        .container{
            margin-top: 150px;
        }

        h3{
            margin-bottom: 30px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <h3>Preclose Report ( {{date('M-Y', strtotime($startDate))}} - {{date('M-Y', strtotime($endDate))}})</h3>
                <table class="table table-bordered">
                    <thead>
                        <th>S.No</th>
                        <th>Customer Name</th>
                        <th>Order Date</th>
                        <th>Contract No</th>
                        <th>Contract Date</th>
                        <th>Order Qty</th>
                        <th>Dispatch Qty</th>
                        <th>Preclose Qty</th>
                        <th>Preclose Reason</th>
                    </thead>
                    <tbody>
                        <?php $count = 1; ?>
                        @foreach($results as $result)

                            <tr>
                                <td>{{$count++}}</td>
                                <td>{{$result['customer_name']}}</td>
                                <td>{{$result['order_date']}}</td>
                                <td>{{$result['contract_no']}}</td>
                                <td>{{$result['contract_date']}}</td>
                                <td>{{$result['order_qty']}}</td>
                                <td>{{round($result['dispatch_qty'], 2)}}</td>
                                <td>{{round($result['preclose_qty'], 2)}}</td>
                                <td>{{$result['preclose_reason']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')


@endsection
