@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .bp-list-card{
            width: 350px;
            height: 340px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .card-holder{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 80vh;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }
    </style>
@endsection

@section('content')
<section id="pf-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 card-holder">
           <div class="bp-list-card">
                    <h3 class="text-center">Customer Summary</h3>
                    <form method="POST" action="/customer-summary-data" target="_blank">
                        @csrf
                        <div class="form-group">
                            <span for="bp">Customer : </span>
                            <select name="bp" id="bp">
                                <?php foreach ($bp as $key => $value): ?>
                                <option value="{{$value['id']}}">{{$value['bp_name']}}</option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <span for="from-date">From Date : </span>
                            <input type="text" id="from-date" class="text-input" name="from-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="to-date">To Date : </span>
                            <input type="text" id="to-date" class="text-input" name="to-date" autocomplete="off" required>
                        </div> --}}

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Show Details">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=text]').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'dd-mm-yy'
        });

    });
</script>
@endsection