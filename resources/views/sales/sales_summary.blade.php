@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .sales-list-card{
            width: 350px;
            height: 320px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .card-holder{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 80vh;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }
    </style>
@endsection

@section('content')
<section id="pf-head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 card-holder">
                <div class="sales-list-card">
                    <h3 class="text-center">Sales Summary</h3>
                    <form id="sale-form">
                        <div class="form-group">
                            <span for="from-date">From Date : </span>
                            <input type="text" id="from-date" class="text-input" name="from-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="to-date">To Date : </span>
                            <input type="text" id="to-date" class="text-input" name="to-date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Show Details">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=text]').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'dd-mm-yy'
        });

        $('#sale-form').on('submit',function(e){
        	e.preventDefault();
        	var formData = $(this).serialize();
        	$.ajax({
                type: "POST",
                url: "/get-sales-summary",
                data: formData,
                success: function(data, status, xhr) {
                    var order = data.orders
                    var csv = 'Customer Name,Quality,Order (Kgs),Order Date,Customer Expected Date,Date Committed,Date of Dispatch,Dispatch Qty Kgs,Pending Qty Kgs\n';
                    order.forEach(function(row) {
                            csv += row.join(',');
                            csv += "\n";
                    });
                    var hiddenElement = document.createElement('a');
                    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                    hiddenElement.target = '_blank';
                    hiddenElement.download = 'sales-summary.csv';
                    hiddenElement.click();
                },
                error: function(xhr, status, error) {
                    console.log('error');
                },
            });
        });
    });
</script>
@endsection