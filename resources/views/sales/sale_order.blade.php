@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ url('css/sales-index.css') }}">
    <link rel="stylesheet" href="{{url('responsive-css/sales-order.css')}}">
@endsection

@section('content')
	<section id="sales-head">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                <h3>Sales Order</h3>
	                <span> On: {{$filter}}</span>
	                <div class="sales-options">
	                    <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date" autocomplete="off">
	                    <button type="button" class="btn btn-primary" id="create-sale">Create Sale Order</button>
	                </div>
	            </div>
	        </div>
	    </div>
			@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
	</section>


	<section id="sales-table">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<p id="order-details-info"><span>This Month Order : {{$totalOrders}}</span> <span>This Month Pending : {{$pendingOrders}}</span> <span>This Month Dispatched : {{$dispatchedOrders}}</span> <span>Over All Pending : {{$overAllPending}}</span></p>
	                <div class="table-wrapper">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>S.No</th>
	                            <th>Customer <br> Name</th>
	                            <th>Quality</th>
	                            <th>Order <br>(Kgs)</th>
	                            {{-- <th>Last Month <br> Pending (kgs)</th>
	                            <th>Total Orders <br> (kgs)</th> --}}
								<th>Order Date</th>
	                            <th>Customer Expected <br> Date</th>
	                            <th>Date Committed</th>
	                            <th>SCPL <br> Confirmation date</th>
	                            <th>Date of <br>Dispatch</th>
	                            <th>Dispatch Qty. <br>Kgs</th>
	                            <th>Pending Qty. <br>Kgs</th>
	                            <th>Edit</th>
	                            <th>Delete</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php $count = 1; ?>
	                    	@foreach($saleOrder as $value)
												<?php if ($value->businessPartner->bp_name != "buffer"): ?>
													<tr>
														<td>{{$count}}</td>
														<td>{{$value->businessPartner->bp_name}}</td>
														<td>{{$value->material_name}}</td>
														<td>{{$value->order_quantity}}</td>
														{{-- <td>{{$value->last_month_pending}}</td>
														<td>{{$value->total_order}}</td> --}}
														<td>{{date('d-m-Y',strtotime($value->order_date))}}</td>
														@if(is_null($value->customer_date))
														<td></td>
														@else
														<td>{{date('d-m-Y',strtotime($value->customer_date))}}</td>
														@endif

														@if(is_null($value->committed_date))
														<td style="text-align: center;"><i class="material-icons add-date" data-id="{{$value->id}}" data-date="">calendar_today</i></td>
														@else
														<td style="text-align: center;">
															<?php foreach (explode(",",$value->committed_date) as $key => $date): ?>
																<?php if ((count(explode(",",$value->committed_date))-1) == $key): ?>
																	<strong>{{$date}}</strong><br>
																	<?php else: ?>
																		{{$date}}<br>
																<?php endif; ?>
															<?php endforeach; ?>
															<i class="material-icons add-date" data-id="{{$value->id}}" data-date="">calendar_today</i>
														</td>

														@endif

														@if(is_null($value->scpl_confirmation_date))
														<td></td>
														@else
														<td>
															<?php foreach (explode(",",$value->scpl_confirmation_date) as $key => $date): ?>
																<?php if ((count(explode(",",$value->scpl_confirmation_date))-1) == $key): ?>
																	<strong>{{$date}}</strong><br>
																	<?php else: ?>
																		{{$date}}<br>
																<?php endif; ?>
															<?php endforeach; ?>
														</td>
														@endif

														@if(is_null($value->dispatch_date))
														<td></td>
														@else
														<td>{{date('d-m-Y',strtotime($value->dispatch_date))}}</td>
														@endif

														<td>{{$value->dispatch_quantity}}</td>
														<td>{{round($value->pending_quantity,1)}}</td>
														<?php if ($value->edit_state == '1'): ?>
															<td><a><i class="material-icons" style="color: #91919133 !important;">edit</i></a></td>
														<?php else: ?>
															<td><a class="edit-sales" data-id="{{$value->id}}"><i class="material-icons">edit</i></a></td>
														<?php endif ?>
														<?php if ($value->delete_state == '1'): ?>
															<td><a><i class="material-icons" style="color: #91919133 !important;">delete</i></a></td>
														<?php else: ?>
															<td><a class="delete-sales" data-id="{{$value->id}}"><i class="material-icons">delete</i></a></td>
														<?php endif ?>
													</tr>
													<?php $count++; ?>
												<?php endif; ?>
	                        @endforeach
	                    </tbody>
	                </table>
	           		</div>
	            </div>
	        </div>
	    </div>
	</section>



	<section id="pop-ups">
	    <div id="sales-modal" class="modal fade" role="dialog">
	        <div class="modal-dialog">

	            <!-- Modal content-->
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <h4 class="modal-title">Sales Order form</h4>
	                </div>
	                <div class="modal-body row">

						<form action="#" id="add-sales-form" name="add-sales-form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h4 id="total-sale-qty">Total Quantity : <span></span></h4>
							</div>
	                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label for="customer_name">Customer Name</label>
									<select name="customer_name" id="customer_name" required>
										<option value="" selected disabled></option>
										@foreach($businessPartner as $value)
											<option value="{{$value['id']}}">{{$value['bp_name']}}</option>
										@endforeach
									</select><span class="feedback"></span>
								</div>
							</div>

							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<div class="form-group">
									<label for="order_date">Order Date</label>
									<input type="text" class="text-input" id="order_date" name="order_date" autocomplete="off"><span class="feedback"></span>
								</div>
							</div>
							<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
								<div class="form-group">
									<label for="total_order">No of items</label>
									<input type="text" class="text-input" id="total_order" name="total_order" required autocomplete="off"><span class="feedback"></span>
								</div>
							</div>

							<div class="order-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div class="form-group">
										<label for="material">Material</label>
										<select name="material" class="material" required>
											@foreach($itemMaster as $value)
												<option value="{{$value['id']}}">{{$value['descriptive_name']}}</option>
											@endforeach
										</select><span class="feedback"></span>
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-6 col-lg-3">
									<div class="form-group">
										<label for="weight">Weight (Kgs)</label>
										<input type="text" class="text-input weight" name="weight" required autocomplete="off"><span class="feedback"></span>
									</div>
								</div>

								<div class="col-xs-12 col-sm-3 col-md-6 col-lg-3">
									<div class="form-group">
										<label for="customer_date">Customer Date</label>
										<input type="text" class="text-input customer-date" name="customer_date" autocomplete="off" required autocomplete="off"><span class="feedback"></span>
										<a class="delete-sale-item"><i class="material-icons">delete</i></a>

									</div>
								</div>
							</div>

	                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="form-group">
									<input type="submit" class="btn btn-primary" id="submit-sale-form" value="Create Order">
								</div>
							</div>

	                    </form>
	                </div>
	            </div>

	        </div>
	    </div>
	    <div id="sales-modal-update" class="modal fade" role="dialog">
	        <div class="modal-dialog" style="width: 400px !important;">

	            <!-- Modal content-->
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal">&times;</button>
	                    <h4 class="modal-title">Sales Order form</h4>
	                </div>
	                <div class="modal-body">
						<form action="#" id="update-sales-form" name="add-sales-form">
	                    	<input type="hidden" name="id" id="update_id" value="">
	                        <div class="form-group">
	                            <label for="customer_name_update">Customer Name</label>
	                            <select name="customer_name_update" id="customer_name_update" required readonly="readonly">
	                            	@foreach($businessPartner as $value)
		                                <option value="{{$value->id}}">{{$value->bp_name}}</option>
																@endforeach
	                            </select><span class="feedback"></span>
	                        </div>

	                        <div class="form-group">
	                            <label for="material_update">Material</label>
	                            <select name="material_update" id="material_update" required>
	                            	@foreach($itemMaster as $value)
		                                <option value="{{$value['id']}}">{{$value['material']}}</option>
		                            @endforeach
	                            </select><span class="feedback"></span>
	                        </div>

	                        <div class="form-group">
	                            <label for="weight">Weight (Kgs)</label>
	                            <input type="text" class="text-input" id="weight_update" name="weight_update" required autocomplete="off"><span class="feedback"></span>
	                        </div>

	                        <div class="form-group">
	                            <label for="order_date">Order Placement Date</label>
	                            <input type="text" class="text-input" id="order_update" name="order_update" required autocomplete="off"><span class="feedback"></span>
	                        </div>

	                        <div class="form-group">
	                            <label for="customer_date_update">Customer Date</label>
	                            <input type="text" class="text-input" id="customer_date_update" name="customer_date_update" required autocomplete="off"><span class="feedback"></span>
							</div>

	                        <div class="form-group">
	                            <input type="submit" class="btn btn-primary" id="submit-sale-form" value="Create Order">
	                        </div>

	                    </form>
	                </div>
	            </div>

	        </div>
	    </div>


		<div id="confirmationModal" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 400px !important;">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Confirmation</h4>
					</div>
					<div class="modal-body">
						<h4 class="confirmation-info">Are you surely want to delete this sale Order ? </h4>
						<p style="color: red;">NOTE : The sale order will be removed from the indent if the indent is already generated. </p>
							<div class="confirm-div">
								<input type="button" class="btn btn-primary delete-sale-btn" value="Yes">
								<input type="button" class="btn btn-primary delete-sale-btn" value="No">
							</div>
					</div>
				</div>

			</div>
		</div>

		<div id="updateDate" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 400px !important;">

				<!-- Modal content-->
				<div class="modal-content" style="margin-top:135px;">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Committed Date</h4>
					</div>
					<div class="modal-body">
						<form class="" action="/update-date-committed" method="post">
							@csrf
							<input type="hidden" name="id" value="">
							<input class="text-input committed-date" style="width:100%; margin-bottom:10px;" type="date" name="committed_date" value="" required>
							<input type="number" class="text-input" name="quantity" style="width:100%; margin-bottom:10px;" required placeholder="Quantity">
							<select name="type" id="" style="width:100%; margin-bottom:10px;">
								<option value="">None</option>
								<option value="S">split</option>
								<option value="D">deviation</option>
							</select>
							<input class="btn btn-primary" type="submit" value="Update">
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
@endsection

@section('script')
<script src="{{ url('js/sales-index.js?v=0.2') }}"></script>

@endsection
