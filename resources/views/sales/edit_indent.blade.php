@extends('layouts.sales')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link rel="stylesheet" href="{{url('responsive-css/generate-indent.css')}}">
    <style>
    	.order-input{
    		background-color: white !important;
    	}
    	#edit-form{
    		margin: 0px;
    		padding: 0px;
    	}
    	#data-input > .form-group > div > .inline-label{
    		font-weight: normal !important;
    	}
    	#data-input > .form-group{
    		margin-bottom: 20px !important;
    	}


    </style>
@endsection

@section('content')
	<section id="generate-indent">
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 generat-spacing">
	                <h3>Update Indent</h3>
	                @if ($errors->any())
	                    <div class="alert alert-danger">
	                        <ul>
	                            @foreach ($errors->all() as $error)
	                                <li>{{ $error }}</li>
	                            @endforeach
	                            <!-- <li>Please re-create indent with required.</li> -->
	                        </ul>
	                    </div>
	                @endif
	                <form action="/update-indent" method="POST" id="edit-indent-form" name="generate-indent-form">
	                	@csrf
	                	<input type="hidden" name="indent_id" value="{{$indent['id']}}">
	                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                        <div class="form-group">
	                            <label for="customer_name">Customer Name</label>
	                            <input type="text" class="text-input" id="customer_name" value="{{$indent->businessPartner['bp_name']}}" disabled style="background-color: #b1b1b133;">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                        <div class="form-group">
	                            <label for="quotation-number">Quotation Number</label>
	                            <input type="text" class="text-input" id="quotation-number" name="quotation-number" value="{{$indent['quotation_number']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div id="sales-order-list" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                        <p style="font-weight: 700; color: #555555" class="">Sales Order List</p>
	                        <div class="checkbox ">
	                        	@foreach($indent->indentDetails as $info)
	                        	<div class="form-group  col-md-12">
	                                <div class="order-list col-xs-12 col-sm-3 col-md-3 col-lg-3 list-spacing order-list-name" >
	                                	<a href="/delete-indent-detail/{{$info['id']}}"><i class="material-icons material-btn">clear</i></a>
	                                    <input type="hidden" name="detail_id[]" value="{{$info['id']}}">
	                                    <span>{{$info->saleOrder->material_name}}</span>
	                                </div>
	                                <div class="order-list col-xs-12 col-sm-3 col-md-3 col-lg-3  list-spacing">
	                                    <label for="due_date" class="inline-label">Due on</label>
	                                    <input type="text" class="order-input due_date" name="due_date[]" value="{{$info['due_on']}}" autocomplete="off" required>
	                                </div>
	                                <div class="order-list col-xs-12 col-sm-3 col-md-3 col-lg-3 list-spacing" >
	                                    <label for="quantity" class="inline-label">Quantity (Kgs)</label>
	                                    <input type="text" class="order-input quantity" name="quantity[]" value="{{$info['quantity']}}" autocomplete="off" value="" required>
	                                </div>
	                                <div class="order-list col-xs-12 col-sm-3 col-md-3 col-lg-3 list-spacing" >
	                                    <label for="rate" class="inline-label">Rate</label>
	                                    <input type="text" class="order-input rate" name="rate[]" value="{{$info['rate_per_kg']}}" autocomplete="off" required>
	                                </div>
	                            </div>
	                        	@endforeach
	                        	<div class="order-list col-xs-12 col-sm-12 col-md-12 col-lg-12 btn-spacing" >
	                        		<a href="#" id="add-order" data-bpid="{{$indent['bp_id']}}" data-masterid="{{$indent['id']}}" class="btn-add">Add Order</a>
	                        	</div>
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                        <div class="form-group">
	                            <label for="tax">Transit Insurance</label>
	                            <input type="text" class="text-input" id="tax" name="tax" value="{{$indent['transit_insurance']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                        <div class="form-group">
	                            <label for="date" class="date-text">Date</label>
	                            <input type="text" class="text-input" id="date" name="date" value="{{$indent['dated_on']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                        <div class="form-group">
	                            <label for="reference-no">Reference No/ Order No</label>
	                            <input type="text" class="text-input" id="reference-no" name="reference-no" value="{{$indent['order_ref_no']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
	                        <div class="form-group">
	                            <label for="other-reference-no">Other Reference No</label>
	                            <input type="text" class="text-input" id="other-reference-no" name="other-reference-no" value="{{$indent['other_ref_no']}}" autocomplete="off">
	                        </div>
	                    </div>


	                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                        <div class="form-group">
	                            <label for="dispatch">Dispatch Through</label>
	                            <input type="text" class="text-input" id="dispatch" name="dispatch" value="{{$indent['dispatcher']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	                        <div class="form-group">
	                            <label for="destination">Destination</label>
	                            <input type="text" class="text-input" id="destination" name="destination" value="{{$indent['destination']}}" required autocomplete="off">
	                        </div>
	                    </div>

	                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                        <div class="form-group">
	                            <label for="terms">Terms of Delivery</label>
	                            <textarea class="text-input" style="resize: none" name="terms" id="terms" cols="30" rows="10">{{$indent['terms_of_delivery']}}</textarea>
	                        </div>
	                    </div>

	                    <div>
	                        <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                            <input type="submit" class="btn btn-primary update-btn " value="Update Indent">
	                        </div>
	                    </div>

	                </form>
	            </div>
	        </div>
	    </div>
	</section>
	<div id="add-order-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Order To Indent</h4>
	      </div>
	      <div class="modal-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form action="/add-indent-detail" method="POST" class="col-sm-12" id="edit-form">
				@csrf
				<div id="data-input">

				</div>
			</form>
	      </div>
	    </div>

	  </div>
	</div>
@endsection

@section('script')
<script src="{{url('js/generate-indent.js')}}"></script>
@endsection
