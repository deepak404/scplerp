@extends( \Auth::user()->dept_id == '0'  ?  'layouts.admin' : 'layouts.sales' )


@section('css')

    <style>
        .machine-list-card{
            width: 350px;
            height: 400px;
            box-shadow: 0px 0px 5px #cecece;
            /*text-align: center;*/
            background-color: white;
        }

        .card-holder{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 80vh;
        }

        .form-group span{
            width: 80px;
            display: inline-block;
            margin-left: 30px;
        }

        form{
            margin-top: 50px;
        }

        input[type="submit"]{
            width: 150px;
            margin-top: 30px;
        }

        h3{
            margin-top: 40px;
        }

        body{
            background-color: #f5f5f5;
        }

        form input[type="text"], form select{
            width: 170px !important;
            height: 35px !important;
            background-color: #f7f7f7 !important;
        }

    </style>

@endsection

@section('content')

    <section id="pf-head">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 card-holder">
                    <div class="machine-list-card">
                        <h3 class="text-center">Price Trends</h3>
                        <form action="/show-price-trends" method="POST" target="_blank">
                            {{csrf_field()}}

                            <div class="form-group">
                                <span for="filter-date">Client</span>
                                <select name="client" id="client">
                                    {{--<option value="everyone">Everyone</option>--}}
                                    @foreach(\App\BusinessPartner::all() as $bp)
                                        <option value="{{$bp->id}}">{{$bp->bp_name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <span for="filter-date">From Date</span>
                                <input type="text" class="text-input" name="from-date" id="from-date" required autocomplete="off">
                            </div>

                            <div class="form-group">
                                <span for="filter-date">To Date</span>
                                <input type="text" class="text-input" name="to-date" id="to-date" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary center-block" value="Show Details">
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#from-date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd-mm-yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
            $('#to-date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd-mm-yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth + 1, 0));
                }
            });
            $('#from-date, #to-date').focus(function(){
                $(".ui-datepicker-calendar").hide();
            });
        });
    </script>

@endsection
