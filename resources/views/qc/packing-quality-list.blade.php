<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="css/weight-log-report.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="responsive-css/purchase-material-clearance.css">
    <style>
        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            min-height: 100px;
            max-height: auto;
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            padding-left: 5%;
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10%;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            width: 10%;
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }
    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Menu<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance-qc">Filament Lot Clearance</a></li>
                                <li><a href="/quality-chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/pm-clearance-list">Packing Material Clearance</a></li>
                                <li><a href="/ds-clearance-list">Dip Solution Clearance</a></li>
                            </ul>
                        </li>

                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Packing<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/pm-clearance-list">Packing Clearance List</a></li>
                                <li><a href="/packing-clearance-updated-list">Updated Packing Clearance</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
</section>

<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Packing Material Clearance</h3>
                {{--<button class="btn btn-primary pull-right" data-target="#createPackingModal" data-toggle="modal">Create Lot</button>--}}
            </div>

            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-md-12" id="packing-clearance-div">
                @if($packageClearance->count() != 0)
                @foreach($packageClearance as $clearance)
                    <div class="clearance-list">
                        <div class="lot-no inline-div">
                            <p class="list-header">LOT No</p>
                            <p class="list-details">{{$clearance->packing_lot_no}}</p>
                        </div>

                        <div class="type inline-div">
                            <p class="list-header">Supplier</p>
                            <p class="list-details">{{$clearance->supplier->name}}</p>
                        </div>

                        <div class="weight inline-div">
                            <p class="list-header">Item Name</p>
                            <p class="list-details">{{$clearance->item}}</p>
                        </div>

                        <div class="date inline-div">
                            <p class="list-header">Quantity Received</p>
                            <p class="list-details">{{$clearance->quantity_received}}</p>
                        </div>

                        <div class="invoice-date inline-div">
                            <p class="list-header">Lot No</p>
                            <p class="list-details">{{$clearance->packing_lot_no}}</p>
                        </div>

                        <div class="denier inline-div">
                            <p class="list-header">Gate Date</p>
                            <p class="list-details">{{$clearance->gate_date}}</p>
                        </div>

                        <div class="status inline-div">
                            <p class="list-header">Status</p>
                            @if($clearance->status == 0)
                                <p class="list-details blue-text"><b>on Process</b></p>
                            @elseif($clearance->status == 1)
                                <p class="list-details green"><b>Cleared</b></p>
                            @else
                                <p class="list-details red"><b>Rejected</b></p>
                            @endif
                        </div>

                        <div class="action inline-div">
                            <!--<a href="/packing-clearance-report/{{$clearance->id}}" style="pointer-events: none" target="_blank" class="download">Download</a> -->
                            <a href="/update-packing-material-lot/{{$clearance->id}}" target="_blank" class="update-chemical edit">Update</a>
                        </div>
                    </div>

                @endforeach
                @else
                    <h4 class="text-center">Package Clearance Lot not Available</h4>
                @endif

            </div>
        </div>
    </div>
</section>

<section id="pop-ups">
    <div id="createPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Package Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-packaging-lot" method="POST" class="col-md-12" id="create-packing">
                        {{csrf_field()}}
                        <input type="hidden" name="form-type" value="create">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-name">Item Name</label>
                                <select class="input-field" name="item" id="item-name">
                                    @foreach($packagingMaterial as $material)
                                        <option value="{{$material}}">{{$material}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier-name">Supplier Name</label>
                                <input type="text" id="supplier-name" name="supplier_name" value="Thiru VenkidaVasan packaging" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="packing-lot-no">Lot No</label>
                                <input type="text" id="packing-lot-no" value="CB04/01" name="packing_lot_no" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-ordered">Quantity Order</label>
                                <input type="text" id="quantity-ordered" name="quantity_ordered" value="100" required></div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="po">PO</label>
                                <input type="text" id="po" name="po" value="ST-PO/0145/2018-19" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="grn">GRN</label>
                                <input type="text" id="grn" name="grn" value="ST-GRN/0564/2018-19" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-received">Quantity Received</label>
                                <input type="text" id="quantity-received" name="quantity_received" value="100" required>
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Package Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-packaging-lot" method="POST" class="col-md-12" id="create-packing">
                        {{csrf_field()}}

                        <input type="hidden" name="form-type" value="update">
                        <input type="hidden" name="packing-id" id="packing-id">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-name">Item Name</label>
                                <select class="input-field" name="item" id="item-name">
                                    @foreach($packagingMaterial as $material)
                                        <option value="{{$material}}">{{$material}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label for="supplier-name">Supplier Name</label>
                                <input type="text" id="supplier-name" name="supplier_name"  required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="packing-lot-no">Lot No</label>
                                <input type="text" id="packing-lot-no" name="packing_lot_no" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-ordered">Quantity Order</label>
                                <input type="text" id="quantity-ordered" name="quantity_ordered" required></div>
                        </div>


                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="po">PO</label>
                                <input type="text" id="po" name="po"  required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="grn">GRN</label>
                                <input type="text" id="grn" name="grn" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-received">Quantity Received</label>
                                <input type="text" id="quantity-received" name="quantity_received" required>
                            </div>
                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

{{--@endsection--}}


{{--@section('script')--}}
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.edit-packaging-lot').on('click', function(){

            // console.log($(this).data('id'));
            $.ajax({
                type:'POST',
                url:'/find-packing-material-lot',
                data:'id='+$(this).data('id'),
                success:function(data){
                    var modal = $('#editPackingModal');

                    modal.find('#supplier-name').val(data.data.supplier_name);
                    modal.find('#packing-lot-no').val(data.data.packing_lot_no);
                    modal.find('#quantity-ordered').val(data.data.quantity_ordered);
                    modal.find('#po').val(data.data.po);
                    modal.find('#grn').val(data.data.grn);
                    modal.find('#quantity-received').val(data.data.quantity_received);
                    modal.find('#packing-id').val(data.data.id);

                    modal.find("#item-name").val(data.data.item).change();

                },
                error:function(xhr){
                    console.log(xhr.status);
                }
            });

            $('#editPackingModal').modal('show');
        });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    });
</script>
{{--@endsection--}}
