<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="responsive-css/purchase-filament-clearance.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            /*height: 100px;*/
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            /*padding-left: 5%;*/
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10% ;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }

        .filament-row input, .filament-row select {
            margin: 0px auto 20px;
        }
        .form-spacing{
                    margin: 0px;
            padding: 0px;
        }

        .modal-body {
        padding: 5px;
        }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 5px !important;
            background-color: #ffffff;
        }

        select{
          height: 32px !important;
        }

        .action {
             display: inline !important;
             vertical-align: middle;
             margin-top: 10px !important;
        }

    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Raw Materials<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance">Filament Clearance</a></li>
                                <li><a href="/chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/packaging-material-clearance">Packing Material Clearance</a></li>
                                <li><a href="/ds-clearance-list">Dip Solution Clearance</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>

<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Filament Clearance</h3>
                <button class="btn btn-primary pull-right" id="create-memo">Create Filament Lot</button>
            </div>
            <div class="col-md-12" id="filament-clearance-div">
                @foreach($lots as $lot)
                <div class="clearance-list">
                    <div class="lot-no inline-div">
                        <p class="list-header">LOT No</p>
                        <p class="list-details">{{$lot['lot_batch_no']}}</p>
                    </div>

                    <div class="type inline-div">
                        <p class="list-header">Type</p>
                        <p class="list-details">{{$lot['type']}}</p>
                    </div>

                    <div class="weight inline-div">
                        <p class="list-header">Net Weight</p>
                        <p class="list-details">{{$lot['net_wt']}} Kgs.</p>
                    </div>

                    <div class="date inline-div">
                        <p class="list-header">Arrival Date</p>
                        <p class="list-details">{{$lot['arrival_date']}}</p>
                    </div>

                    <div class="invoice-date inline-div">
                        <p class="list-header">Pallets Count</p>
                        <p class="list-details">{{$lot['pallet_count']}}</p>
                    </div>

                    <div class="denier inline-div">
                        <p class="list-header">Denier</p>
                        <p class="list-details">{{$lot['denier']}}</p>
                    </div>

                    <div class="status inline-div">
                        <p class="list-header">Status</p>
                        @if(is_null($lot['clearance_status']))
                        <p class="list-details blue-text"><b>on Process</b></p>
                        @elseif($lot['clearance_status'] == 1)
                        <p class="list-details green"><b>Cleared</b></p>
                        @else
                        <p class="list-details red"><b>Rejected</b></p>
                        @endif
                    </div>

                    <div class="action inline-div">
                        <?php if (!is_null($lot['clearance_status'])): ?>
                            <a href="/filament-clearance-report-one/{{$lot['id']}}" target="_blank" class="download">Download</a>
                            <?php else: ?>
                        <?php endif ?>
                        <?php if (is_null($lot['edit_delete'])): ?>
                            <a class="edit" data-id="{{$lot['id']}}">Edit</a>
                            <a class="delete btn btn-primary" href="/delete-filament-memo/{{$lot['id']}}">Delete</a>
                        <?php endif ?>
                    </div>

                </div>
                @endforeach

            </div>
        </div>
    </div>
</section>

<section id="pop-ups">
    <div id="createFilamentModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Filament Lot</h4>
                </div>
                <div class="modal-body row">
                    <form action="#" id="filament-form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-spacing">
                        <div class="col-md-12 filament-row">
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="lot-no">Lot No.</label>
                              <input type="text" id="lot-no" name="lot_batch_no" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="gate-no">Gate Entry No.</label>
                              <input type="text" id="gate-no" name="gate_entry_no" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="gate-date">Gate Entry Date</label>
                              <input type="text" id="gate-date" name="gate_entry_date" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="supplier_name">Supplier Name</label>
                              <select name="supplier_id" id="supplier_name">
                                <?php foreach ($suppliers as $supplier): ?>
                                  <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                <?php endforeach; ?>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-12 filament-row">
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="type">SCPL Type</label>
                            <select name="type" id="type" required>
                              <option value="" disabled>Types</option>
                              @foreach($filaments as $key=>$filament)
                              <option value="{{$key}}">{{$key}}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="supplier_type">Purchase Type</label>
                            <select name="supplier_type" id="supplier_type" required>
                              <option value="imported">Imported</option>
                              <option value="indigenous">Indigenous</option>
                            </select>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="material_code">Material Code</label>
                            <input type="text" name="material_code" id="material-code" value="" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="denier">Denier</label>
                            <input type="text" id="denier" name="denier" required>
                          </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 filament-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="weight">Invoice Quntity -Nett</label>
                              <input type="text" id="weight" name="net_wt" required>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="tube-color">Paper Tube Colour</label>
                                <input type="text" id="tube-color" name="tube_color" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="tube-id">Paper Tube ID</label>
                                <input type="text" id="tube-id" name="tube_id" required></div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                              <label for="pallet-no">Total No of Pallets / Box</label>
                              <input type="text" id="pallet-no" name="pallet_count" required></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 filament-row">
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="inv-no">Invoice No.</label>
                            <input type="text" id="inv-no" name="inv_no" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="inv-date">Invoice Date</label>
                            <input type="text" id="inv-date" name="inv_date" required>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <label for="doa">Date of Arrival</label>
                            <input type="text" id="doa" name="arrival_date" required>
                          </div>
                          <div class="col-xs- col-sm-6 col-md-3 col-lg-3">
                            <label for="supplier_filament_type">Supplier Type</label>
                            <input type="text" id="supplier_filament_type" name="supplier_filament_type">
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 filament-row">
                            <div class="col-xs- col-sm-12 col-md-12 col-lg-12">
                                <label for="note">Note</label>
                                <input type="text" id="note" name="note">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 filament-row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn btn-primary" value="Create Filament Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="{{url('/js/filament-clearance.js')}}"></script>

</body>
</html>
