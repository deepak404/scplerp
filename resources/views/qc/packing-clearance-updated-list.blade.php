<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="css/weight-log-report.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <script src="gulpfile.js"></script>

    <style>
        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            min-height: 100px;
            max-height: auto;
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            padding-left: 5%;
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10%;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }
    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Menu<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance-qc">Filament Lot Clearance</a></li>
                                <li><a href="/quality-chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/pm-clearance-list">Packing Material Clearance</a></li>
                                <li><a href="/ds-clearance-list">Dip Solution Clearance</a></li>
                            </ul>
                        </li>

                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Packing<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/pm-clearance-list">Packing Clearance List</a></li>
                                <li><a href="/packing-clearance-updated-list">Updated Packing Clearance</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>
<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Packing Material Clearance</h3>
            </div>

            <div class="col-md-12" id="filament-clearance-div">

                @foreach($packingClearance as $clearance)
                    <div class="clearance-list">
                        <div class="lot-no inline-div">
                            <p class="list-header">LOT No</p>
                            <p class="list-details">{{$clearance->packing_lot_no}}</p>
                        </div>

                        <div class="type inline-div">
                            <p class="list-header">Supplier</p>
                            <p class="list-details">{{$clearance->supplier_name}}</p>
                        </div>

                        <div class="weight inline-div">
                            <p class="list-header">Item Name</p>
                            <p class="list-details">{{$clearance->item}}</p>
                        </div>

                        <div class="date inline-div">
                            <p class="list-header">Quantity Received</p>
                            <p class="list-details">{{$clearance->quantity_received}}</p>
                        </div>

                        <div class="invoice-date inline-div">
                            <p class="list-header">PO</p>
                            <p class="list-details">{{$clearance->po}}</p>
                        </div>

                        <div class="denier inline-div">
                            <p class="list-header">GRN</p>
                            <p class="list-details">{{$clearance->grn}}</p>
                        </div>

                        <div class="status inline-div">
                            <p class="list-header">Status</p>
                            @if($clearance->status == 0)
                                <p class="list-details blue-text"><b>on Process</b></p>
                            @elseif($clearance->status == 1)
                                <p class="list-details green"><b>Cleared</b></p>
                            @else
                                <p class="list-details red"><b>Rejected</b></p>
                            @endif
                        </div>

                        <div class="action inline-div">
                            <a href="/packing-clearance-report/{{$clearance->id}}" target="_blank" class="download">Download</a>
                            <a href="/edit-updated-packing-details/{{$clearance->id}}" target="_blank" class="update-chemical edit">Edit Updated Details</a>
                        </div>
                    </div>

                @endforeach
                {{--<div class="clearance-list">--}}
                {{--<div class="lot-no inline-div">--}}
                {{--<p class="list-header">LOT No</p>--}}
                {{--<p class="list-details">81</p>--}}
                {{--</div>--}}

                {{--<div class="type inline-div">--}}
                {{--<p class="list-header">Supplier</p>--}}
                {{--<p class="list-details">Western India Chemicals</p>--}}
                {{--</div>--}}

                {{--<div class="weight inline-div">--}}
                {{--<p class="list-header">Chemical Name</p>--}}
                {{--<p class="list-details">Formaldehyde</p>--}}
                {{--</div>--}}

                {{--<div class="date inline-div">--}}
                {{--<p class="list-header">Chemical Code</p>--}}
                {{--<p class="list-details">21200</p>--}}
                {{--</div>--}}

                {{--<div class="invoice-date inline-div">--}}
                {{--<p class="list-header">Man. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="denier inline-div">--}}
                {{--<p class="list-header">Exp. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="status inline-div">--}}
                {{--<p class="list-header">Status</p>--}}
                {{--<p class="list-details blue-text"><b>on Process</b></p>--}}
                {{--</div>--}}

                {{--<div class="action inline-div">--}}
                {{--<a href="#" class="download">Download</a>--}}
                {{--<a href="#" class="edit">Edit</a>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--<div class="clearance-list">--}}
                {{--<div class="lot-no inline-div">--}}
                {{--<p class="list-header">LOT No</p>--}}
                {{--<p class="list-details">81</p>--}}
                {{--</div>--}}

                {{--<div class="type inline-div">--}}
                {{--<p class="list-header">Supplier</p>--}}
                {{--<p class="list-details">Western India Chemicals</p>--}}
                {{--</div>--}}

                {{--<div class="weight inline-div">--}}
                {{--<p class="list-header">Chemical Name</p>--}}
                {{--<p class="list-details">Formaldehyde</p>--}}
                {{--</div>--}}

                {{--<div class="date inline-div">--}}
                {{--<p class="list-header">Chemical Code</p>--}}
                {{--<p class="list-details">21200</p>--}}
                {{--</div>--}}

                {{--<div class="invoice-date inline-div">--}}
                {{--<p class="list-header">Man. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="denier inline-div">--}}
                {{--<p class="list-header">Exp. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="status inline-div">--}}
                {{--<p class="list-header">Status</p>--}}
                {{--<p class="list-details blue-text"><b>on Process</b></p>--}}
                {{--</div>--}}

                {{--<div class="action inline-div">--}}
                {{--<a href="#" class="download">Download</a>--}}
                {{--<a href="#" class="edit">Edit</a>--}}
                {{--</div>--}}

                {{--</div>--}}
                {{--<div class="clearance-list">--}}
                {{--<div class="lot-no inline-div">--}}
                {{--<p class="list-header">LOT No</p>--}}
                {{--<p class="list-details">81</p>--}}
                {{--</div>--}}

                {{--<div class="type inline-div">--}}
                {{--<p class="list-header">Supplier</p>--}}
                {{--<p class="list-details">Western India Chemicals</p>--}}
                {{--</div>--}}

                {{--<div class="weight inline-div">--}}
                {{--<p class="list-header">Chemical Name</p>--}}
                {{--<p class="list-details">Formaldehyde</p>--}}
                {{--</div>--}}

                {{--<div class="date inline-div">--}}
                {{--<p class="list-header">Chemical Code</p>--}}
                {{--<p class="list-details">21200</p>--}}
                {{--</div>--}}

                {{--<div class="invoice-date inline-div">--}}
                {{--<p class="list-header">Man. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="denier inline-div">--}}
                {{--<p class="list-header">Exp. Date</p>--}}
                {{--<p class="list-details">31/10/2018</p>--}}
                {{--</div>--}}

                {{--<div class="status inline-div">--}}
                {{--<p class="list-header">Status</p>--}}
                {{--<p class="list-details blue-text"><b>on Process</b></p>--}}
                {{--</div>--}}

                {{--<div class="action inline-div">--}}
                {{--<a href="#" class="download">Download</a>--}}
                {{--<a href="#" class="edit">Edit</a>--}}
                {{--</div>--}}

                {{--</div>--}}

            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">

</script>
</body>
</html>
