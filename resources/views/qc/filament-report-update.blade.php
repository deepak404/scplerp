<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <style>


        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }


        .filament-test-result, .tested-pallet-entry{
            box-shadow: 0px 0px 5px gainsboro;
            padding: 0px !important;
            margin-bottom: 40px;
        }

        .tested-pallet-entry{
            margin-top: 40px;

        }

        .table>tbody>tr>td{
            padding: 15px;
            vertical-align: middle;
        }

        .table>tbody>tr{
            border-bottom: 1px solid #d6d6d6;
        }

        .table>tbody>tr:last-child{
            border-bottom: none;
        }

        .table{
            margin-bottom: 0px;
        }

        .material-icons{
            vertical-align: middle;
        }

        label{
            margin-bottom: 0px;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        .final-report{
            margin: 0px auto 50px;
            padding: 0px;
        }

        .final-report>div{
            padding-left: 0px;
        }

        .final-report label{
            margin-bottom: 10px;
        }

        select{
            height: 26px;
        }

        .table>thead>tr>th{
            padding: 15px !important;
        }

        #pallet-count-entry{
            margin: 20px auto;
        }

        #pallet-count-entry span{
            font-weight: bold;
        }
        .delete,.add_info{
            float: right;
        }
        .add_info{
            margin-right: 10px;
        }
        .tested-pallet>tr>td>input{
            width: 140px;
        }
        .modal-dialog{
            width: 80%;
        }
        .modal-body{
            overflow-y: auto;
            max-height: 60vh;
        }

    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a class="active-menu" href="/">Filament Lot Clearance</a></li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>
<section id="filament-clearance">
    <div class="container">
      <h3>Select parameters for MEMO</h3>
        <div class="row" style="margin-top: 25px;">
            <div class="col-md-12">
                <form action="/filament-report-update" method="post" id="report-update">
                  @csrf
                    <div class="col-md-12 box-shadow filament-test-result">
                        <table class="table">
                            <thead>
                            <tr>
                                <th><i class="material-icons">
                                    indeterminate_check_box
                                </i></th>
                                <th>Parameters</th>
                                <th>Actual Result</th>
                                <th>SCPL Specification</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <input type="hidden" name="id" value="{{$id}}">
                                <?php $count=0; ?>
                        @foreach($finalResult as $key=>$test)
                         <tr>
                            <td><label for="ft-{{$count}}"><i class="material-icons">check_box_outline_blank</i></label>
                                <input type="checkbox" id="ft-{{$count}}" name="parameter[]" value="{{$key}}"></td>
                            <td>{{$key}}</td>
                            <td><input type="hidden" name="actual[]" value="{{$test['avg']}}" disabled>{{$test['avg']}}</td>
                            <td><input type="hidden" name="scpl[]" value="{{$test['scpl']}}" disabled>{{$test['scpl']}}</td>
                            <td><input type="hidden" name="status[]" value="{{$test['status']}}" disabled>{{$test['status']}}</td>
                        </tr>
                        <?php $count++; ?>
                        @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="btn btn-primary center-block" value="Update">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
      </script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
</script>

<script src="{{url('/js/filament-quality-update.js')}}">
</script>

</body>
</html>
