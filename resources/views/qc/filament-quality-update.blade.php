@extends('layouts.quality')

@section('css')
<link rel="stylesheet" href="{{url('responsive-css/qc-filament-quality-update.css')}}">

    <style>


        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 120px;
        }


        .filament-test-result, .tested-pallet-entry{
            box-shadow: 0px 0px 5px gainsboro;
            padding: 0px !important;
            margin-bottom: 40px;
        }

        .tested-pallet-entry{
            margin-top: 20px;
        }
         .tested-pallet-entry{
            overflow-x: scroll;
        }

        .table>tbody>tr>td{
            padding: 15px;
            vertical-align: middle;
        }

        .table>tbody>tr{
            border-bottom: 1px solid #d6d6d6;
        }

        .table>tbody>tr:last-child{
            border-bottom: none;
        }

        .table{
            margin-bottom: 0px;
        }

        .material-icons{
            vertical-align: middle;
        }

        label{
            margin-bottom: 0px;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        .final-report{
            margin: 0px auto 50px;
            padding: 0px;
        }

        .final-report>div{
            padding-left: 0px;
        }

        .final-report label{
            margin-bottom: 10px;
        }

        select{
            height: 26px;
        }

        .table>thead>tr>th{
            padding: 15px !important;
        }

        #pallet-count-entry{
            margin: 20px auto;
        }

        #pallet-count-entry span{
            font-weight: bold;
        }
        body
        {
            counter-reset: Serial;           /* Set the Serial counter to 0 */
        }
        .tested-pallet>tr td:first-child:before
        {
          counter-increment: Serial;      /* Increment the Serial counter */
          content: counter(Serial)"."; /* Display the counter */
        }
        .delete,.add_info{
            float: right;
        }
        .add_info{
            margin-right: 10px;
        }
        .tested-pallet>tr>td>input{
            width: 140px;
        }
        .modal-dialog{
            width: 80%;
        }
        .modal-body{
            overflow-y: auto;
            max-height: 60vh;
        }
        .btn-div{
            text-align: center;
            padding: 20px;
        }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }
        .final-report input, .final-report select{
            width: 80%;
        }
        #quality-update-form{
            margin-top: 20px;
        }
        .btn-spacing{
            padding:0px 50px !important; 
        }
        .final-report-spacing input{
            margin-bottom: 30px;
        }

    </style>
@endsection
@section('content')
<section id="filament-clearance">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <h3>Lot Specification</h3>
            </div>


            <form action="#" id="quality-update-form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <input type="hidden" name="id" value="{{$id}}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  p-lr-zero final-report final-report-spacing">
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <label for="tested-by">Tested By</label>
                        <input type="text" id="tested-by" name="tested-by" value="{{$filamentLot['tested_by']}}" required>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <label for="tested-date">Tested Date</label>
                        <input type="text" id="tested-date" name="tested-date" autocomplete="off" value="{{$filamentLot['test_date']}}" required>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <label for="tested-arrival-date">Tested Arrival Date</label>
                        <input type="text" id="tested-arrival-date" name="tested-arrival-date" autocomplete="off" value="{{$filamentLot['sample_date']}}" required>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <label for="final-result">Result</label>
                        <select name="final-result" id="final-result">
                            <?php if ($filamentLot['sample_date'] == 1): ?>
                                <option value="1" selected>Cleared</option>
                                <option value="2">Rejected</option>
                            <?php elseif ($filamentLot['sample_date'] == 2): ?>
                                <option value="1">Cleared</option>
                                <option value="2" selected>Rejected</option>
                            <?php else: ?>      
                                <option value="1">Cleared</option>
                                <option value="2">Rejected</option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>

                <h3>Tested Pallet</h3>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  p-lr-zero" id="pallet-count-entry">
                    <span for="tested-pallet-count">No of Tested Pallets : &nbsp;</span>
                    <?php  if (!is_null($testedPalletData)): ?>
                    <input type="text" id="tested-pallet-count" name="tested-pallet-count" data-count="1">
                        <?php else: ?>
                    <input type="text" id="tested-pallet-count" name="tested-pallet-count" data-count="0">
                    <?php endif ?>

                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  tested-pallet-entry">
                    <table class="table col-xs-12 col-sm-12 col-md-12 col-lg-12 table-width">
                        <thead>
                            <tr>
                                <th>Sl.No</th>
                                <th>P.S No</th>
                                <th>Code</th>
                                <th>Pallet No</th>
                                <th>Lea Weight</th>
                                <th>Actual Denier</th>
                            </tr>
                        </thead>
                        <tbody class="tested-pallet">
                            <?php if (!is_null($testedPalletData)): ?>
                            <?php $count = 1; ?>
                            @foreach($testedPalletData->ps_no as $key => $palletData)
                            <tr>
                                <td></td>
                                <td><input type="text" name="ps-no[]" value="{{$palletData}}"></td>
                                <td><input type="text" name="code[]" value="{{$testedPalletData->code[$key]}}"></td>
                                <td><input type="text" name="pallet-no[]" value="{{$testedPalletData->pallet_no[$key]}}"></td>
                                <td><input type="text" name="weight[]" value="{{$testedPalletData->weight[$key]}}"></td>
                                <td><input type="text" name="denier[]" value="{{$testedPalletData->denier[$key]}}"><i class="material-icons delete">delete</i><i class="material-icons add_info" data-id="{{$key}}" data-val="{{$testedPalletData->result[$key]}}">playlist_add</i></td>
                                <input type="hidden" name="result[]" id="result-{{$key}}" value="{{$testedPalletData->result[$key]}}">
                            </tr>
                            <?php $count += 1; ?>
                            @endforeach
                            <?php else: ?>
                            <tr>
                                <td></td>
                                <td><input type="text" name="ps-no[]"></td>
                                <td><input type="text" name="code[]"></td>
                                <td><input type="text" name="pallet-no[]"></td>
                                <td><input type="text" name="weight[]"></td>
                                <td><input type="text" name="denier[]"><i class="material-icons delete">delete</i><i class="material-icons add_info" data-id="1">playlist_add</i></td>
                                <input type="hidden" name="result[]" id="result-0">
                            </tr>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                    <input type="submit" class="btn btn-primary center-block btn-spacing" value="Update">

                </div>
            </form>

        </div>
    </div>
</section>

<!-- Modal -->
<div id="parameterModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
        <form action="#" id="parameter-form" >
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th><i class="material-icons">
                                indeterminate_check_box
                            </i></th>
                            <th>Parameters (UOM)</th>
                            <th>Actual Result</th>
                            <th>SCPL Specification</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($filamentTest as $test)
                        <tr>
                            <td><label for="ft-{{$test['id']}}"><i class="material-icons">
                                check_box_outline_blank
                            </i></label>
                                <input type="checkbox" id="ft-{{$test['id']}}" name="parameter[]" value="{{$test['parameter']}}"></td>
                            <td>{{$test['parameter']}} ({{$test['unit']}})</td>
                            <td><input type="text" name="actual[]" value="" disabled></td>
                            <td><input type="text" name="scpl[]" value="{{$test['value']}}" disabled></td>
                            <input type="hidden" name="id[]" value="{{$test['id']}}" disabled>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="btn-div">
                <input class="btn btn-primary btn-spacing" type="submit" value="Update" data-id="" id="form-btn">
            </div>
        </form>
    </div>

  </div>
</div>
@endsection
@section('script')
<script src="{{url('/js/filament-quality-update.js')}}"></script>
@endsection
