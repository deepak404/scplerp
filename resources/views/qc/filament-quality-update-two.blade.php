@extends('layouts.quality')

@section('css')
<link rel="stylesheet" href="{{url('responsive-css/qc-filament-quality-update.css')}}">

    <style>


        h4{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 120px;
        }


        .filament-test-result, .tested-pallet-entry{
            box-shadow: 0px 0px 5px gainsboro;
            padding: 0px !important;
            margin-bottom: 40px;
        }

        .tested-pallet-entry{
            margin-top: 20px;
        }
         .tested-pallet-entry{
            overflow-x: scroll;
        }

        .table>tbody>tr>td{
            padding: 15px;
            vertical-align: middle;
        }

        .table>tbody>tr{
            border-bottom: 1px solid #d6d6d6;
        }

        .table>tbody>tr:last-child{
            border-bottom: none;
        }

        .table{
            margin-bottom: 0px;
        }

        .material-icons{
            vertical-align: middle;
        }

        label{
            margin-bottom: 0px;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        .final-report{
            margin: 0px auto 20px;
            padding: 0px;
        }

        .final-report>div{
            padding-left: 0px;
        }

        .final-report label{
            margin-bottom: 10px;
        }

        select{
            height: 26px;
        }

        .table>thead>tr>th{
            padding: 15px !important;
        }

        #pallet-count-entry{
            margin: 20px auto;
        }

        #pallet-count-entry span{
            font-weight: bold;
        }
        body
        {
            counter-reset: Serial;           /* Set the Serial counter to 0 */
        }
        .tested-pallet>tr td:first-child:before
        {
          counter-increment: Serial;      /* Increment the Serial counter */
          content: counter(Serial)"."; /* Display the counter */
        }
        .delete,.add_info{
            float: right;
        }
        .add_info{
            margin-right: 10px;
        }
        .tested-pallet>tr>td>input{
            width: 140px;
        }
        .modal-dialog{
            width: 80%;
        }
        .modal-body{
            overflow-y: auto;
            max-height: 60vh;
        }
        .btn-div{
            /* text-align: center; */
            padding: 20px;
        }
        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }
        .final-report input, .final-report select{
            width: 80%;
        }
        #quality-update-form{
            margin-top: 20px;
        }
        .btn-spacing{
            padding:0px 50px !important;
        }
        .final-report-spacing input{
            margin-bottom: 30px;
        }
        .master-div{
          margin-bottom: 20px;
        }
        #empty-p{
          text-align: center;
          background-color: #0000000d;
          padding: 10px;
          border-radius: 11px;
          margin: 62px 26px 10px;
        }
        .pallet-div{
          border-bottom: 1px solid #d3d3d3;
          padding: 10px;
          margin-bottom: 5px;
        }
        .input-div > input{
          width: 100%;
        }
        .status-note{
          float: right;
          padding: 10px;
          font-weight: 600;
          border-radius: 13px;
          margin-right: 10px;
          display: none;
          position: relative;
          animation: animatebottom 0.5s;
        }
        @keyframes animatebottom {
            from {
                bottom: -50px;
                opacity: 0;
            }
            to {
                bottom: 0;
                opacity: 1;
            }
        }
        .success{
          background-color: #71ea7154;
          color: #0a630a99;
        }
        .error{
          background-color: #ea717154;
          color: #d81e1e99;
        }
        .modal-title{
          padding-bottom: 15px;
        }
    </style>
@endsection
@section('content')
<section id="filament-clearance">
    <div class="container">
        <div class="row master-div">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <h4><strong>Lot Specification</strong></h4>
            </div>


            <form action="#" id="quality-update-form" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <input type="hidden" name="id" value="{{$id}}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  p-lr-zero final-report final-report-spacing">
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <label for="tested-by">Tested By</label>
                        <input type="text" id="tested-by" name="tested-by" value="{{$filamentLot['tested_by']}}" required>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <label for="tested-date">Tested Date</label>
                        <input type="text" id="tested-date" name="tested-date" autocomplete="off" value="{{$filamentLot['test_date']}}" required>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <label for="tested-arrival-date">Tested Arrival Date</label>
                        <input type="text" id="tested-arrival-date" name="tested-arrival-date" autocomplete="off" value="{{$filamentLot['sample_date']}}" required>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                  <input type="submit" class="btn btn-primary center-block btn-spacing" value="Update">
                </div>
            </form>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
              <h4><strong>Tested Pallets</strong></h4>
              <?php if (is_null($filamentLot['tested_by'])): ?>
                <button type="button" id="add-result" class="btn btn-primary btn-spacing pull-right" disabled>Add Result</button>
                <?php else: ?>
                  <button type="button" id="add-result" class="btn btn-primary btn-spacing pull-right">Add Result</button>
              <?php endif; ?>
          </div>
          @if(count($testedPallet) > 0)
          <table class="table">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>P.S.No</th>
                  <th>Code</th>
                  <th>Pallet. No</th>
                  <th>Lea Weight</th>
                  <th>Actual</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php $count=1; foreach ($testedPallet as $value): ?>
                  <tr>
                    <td>{{$count}}</td>
                    <td>{{$value['ps_no']}}</td>
                    <td>{{$value['code']}}</td>
                    <td>{{$value['pallet_no']}}</td>
                    <td>{{$value['lea_weight']}}</td>
                    <td>{{$value['actual']}}</td>
                    <td><i class="material-icons delete-pallet" data-id="{{$value['id']}}">delete</i></td>
                  </tr>
                  <?php $count++; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
            @else
            <p id="empty-p">No Tested pallet to show.</p>
            @endif
        </div>
    </div>
</section>

<!-- Modal -->
<div id="parameterModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tested Pallet</h4>
        <span class="status-note">Successfully Create!!! Add Next Pallet.</span>
      </div>
        <form action="#" id="parameter-form">
          <input type="hidden" name="id" value="{{$id}}">
          <input type="hidden" id="pallet-id" name="pallet-id" value="">
          <div class="col-md-12 pallet-div">
            <div class="col-md-2 input-div">
              <label for="ps_no">P.S.No</label>
              <input type="text" id="ps_no" name="ps_no" autocomplete="off" value="">
            </div>
            <div class="col-md-2 input-div">
              <label for="code">Code</label>
              <input type="text" id="code" name="code" autocomplete="off" value="">
            </div>
            <div class="col-md-2 input-div">
              <label for="pallet_no">Pallet No</label>
              <input type="text" id="pallet_no" name="pallet_no" autocomplete="off" value="" required>
            </div>
            <div class="col-md-2 input-div">
              <label for="lea_weight">Lea Weight</label>
              <input type="text" id="lea_weight" name="lea_weight" autocomplete="off" value="" required>
            </div>
            <div class="col-md-2 input-div">
              <label for="actual">Actual Denier</label>
              <input type="text" id="actual" name="actual-denier" autocomplete="off" value="" required>
            </div>
          </div>
            <div class="modal-body col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th><i class="material-icons">
                                indeterminate_check_box
                            </i></th>
                            <th>Parameters (UOM)</th>
                            <th>Actual Result</th>
                            <th>SCPL Specification</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($filamentTest as $test)
                        <tr>
                            <td><label for="ft-{{$test['id']}}"><i class="material-icons">
                                check_box_outline_blank
                            </i></label>
                                <input type="checkbox" id="ft-{{$test['id']}}" name="parameter[]" value="{{$test['parameter']}}"></td>
                            <td>{{$test['parameter']}} ({{$test['unit']}})</td>
                            <td><input type="text" name="actual[]" value="" autocomplete="off" disabled></td>
                            <td>{{$test['value']}}<input type="hidden" name="scpl[]" value="{{$test['value']}}" disabled></td>
                            <input type="hidden" name="data-id[]" value="{{$test['id']}}" disabled>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="btn-div">
              <input class="btn btn-primary btn-spacing" type="submit" value="Update">
              <input class="btn btn-primary btn-spacing pull-right" id="done-btn" data-id="{{$id}}" type="button" value="Done">
            </div>
        </form>
    </div>

  </div>
</div>
<div id="confirmationModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 400px !important;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <h4 class="confirmation-info"></h4>
        <p style="color: red;" id="note-p"></p>
          <div class="confirm-div">
            <input type="button" class="btn btn-primary confirm-btn" value="Yes">
            <input type="button" class="btn btn-primary confirm-btn" value="No">
          </div>
      </div>
    </div>

  </div>
</div>
@endsection
@section('script')
<script src="{{url('/js/filament-quality-update.js')}}"></script>
@endsection
