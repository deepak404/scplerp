<!DOCTYPE html>
<html>
<head>
    <title>Starter Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    {{--<link rel="stylesheet" href="css/generate-indent.css">--}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="gulpfile.js"></script>

    <style>


        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }


        .chemical-test-result, .tested-pallet-entry{
            box-shadow: 0px 0px 5px gainsboro;
            padding: 0px !important;
            margin-bottom: 40px;
        }

        .tested-pallet-entry{
            margin-top: 40px;

        }

        .table>tbody>tr>td{
            padding: 15px;
            vertical-align: middle;
        }

        .table>tbody>tr{
            border-bottom: 1px solid #d6d6d6;
        }

        .table>tbody>tr:last-child{
            border-bottom: none;
        }

        .table{
            margin-bottom: 0px;
        }

        .material-icons{
            vertical-align: middle;
        }

        label{
            margin-bottom: 0px;
        }


        .material-icons:hover{
            cursor: pointer;
        }


        .final-report{
            margin: 0px auto 50px;
            padding: 0px;
        }

        .final-report>div{
            padding-left: 0px;
        }

        .final-report label{
            margin-bottom: 10px;
        }

        select{
            height: 30px;
        }

        .table>thead>tr>th{
            padding: 15px !important;
        }

        #pallet-count-entry{
            margin: 20px auto;
        }

        #pallet-count-entry span{
            font-weight: bold;
        }

        .table>tbody>tr>td>input{
            width: 70px;
        }

        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }
        .final-report input, .final-report select{
            width: 90%;
        }
        #quality-update-form{
            margin-top: 20px;
        }
        .btn-spacing{
            padding:0px 50px !important;
        }
        .final-report-spacing input{
            margin-bottom: 30px;
        }

        .chemical-test-result{
            overflow-x: scroll;
        }
        .table>tbody>tr>td>select {
            width: 80px;
        }

        #packing-material-form{
            margin-top: 10px;
        }

    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}} " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Menu<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance-qc">Filament Lot Clearance</a></li>
                                <li><a href="/quality-chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/pm-clearance-list">Packing Material Clearance</a></li>
                            </ul>
                        </li>

                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Packing<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/pm-clearance-list">Packing Clearance List</a></li>
                                <li><a href="/packing-clearance-updated-list">Updated Packing Clearance</a></li>
                            </ul>
                        </li>
                        <!--<li class="dropdown" id="nav-dropdown">-->
                        <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"-->
                        <!--data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Invoice Details<i class="material-icons">-->
                        <!--arrow_drop_down-->
                        <!--</i></a>-->
                        <!--<ul class="dropdown-menu">-->
                        <!--<li><a href="#">Generate Indent</a></li>-->
                        <!--<li><a href="#">Indent List</a></li>-->
                        <!--</ul>-->
                        <!--</li>-->

                    </ul>
                </div>
            </div>
        </nav>

    </header>

</section>
<section id="filament-clearance">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Lot Specification</h3>
                <!--<button class="btn btn-primary pull-right" data-target="#createFilamentModal" data-toggle="modal">Create Filament Lot</button>-->
            </div>


            <form action="#" id="dip-solution-form" class="col-md-12">
                <input type="hidden" id="dip-solution-id" value="{{$dipSolution->id}}">
                <div class="col-md-12 p-lr-zero final-report">
                    <div class="col-md-3">
                        <label for="tested-by">Tested By</label>
                        <input type="text" id="tested-by" name="tested-by" required>
                    </div>
                    <div class="col-md-3">
                        <label for="tested-date">Tested Date</label>
                        <input type="text" id="tested-date" name="tested-date" autocomplete="off" required>
                    </div>
                    <div class="col-md-3">
                        <label for="tested-arrival-date">Tested Arrival Date</label>
                        <input type="text" id="tested-arrival-date" name="tested-arrival-date" autocomplete="off" required>
                    </div>
                    <div class="col-md-3">
                        <label for="final-result">Result</label>
                        <select name="final-result" id="final-result" required>
                            <option value="cleared">Cleared</option>
                            <option value="rejected">Rejected</option>
                        </select>
                    </div>
                </div>


                <div class="col-md-12 box-shadow chemical-test-result">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><i class="material-icons">
                                    indeterminate_check_box
                                </i></th>
                            <th>Parameters</th>
                            <th>UOM</th>
                            <th>LSL</th>
                            <th>USL</th>
                            <th>Sample 1</th>
                            <th>Sample 2</th>
                            <th>Sample 3</th>
                            <th>Result</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($dipSolutionData as $property)
                            <tr>
                                <td><label for="{{$property->parameter}}"><i class="material-icons">
                                            check_box_outline_blank
                                        </i></label>
                                    <input type="checkbox" id="{{$property->parameter}}" name="{{$property->parameter}}"></td>
                                <td>{{$property->parameter}}</td>
                                <td>{{$property->uom}}</td>
                                <td>{{$property->lsl}}</td>
                                <td>{{$property->usl}}</td>
                                <td><input type="text"  name="{{$property->parameter}}-sample[]"></td>
                                <td><input type="text"  name="{{$property->parameter}}-sample[]"></td>
                                <td><input type="text"  name="{{$property->parameter}}-sample[]"></td>
                                <td><select name="{{$property->parameter}}-result" id="">
                                        <option value="passed">Passed</option>
                                        <option value="failed">Failed</option>
                                    </select></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>


                <div class="col-md-12">
                    <input type="submit" class="btn btn-primary center-block btn-spacing" value="Update">

                </div>
            </form>

        </div>
    </div>
</section>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script type="text/javascript">
    $(document).ready(function(){


        $('#tested-date, #tested-arrival-date').datepicker({
            dateFormat : 'dd-mm-yy',
        })

        $('#dip-solution-form').on('submit', function(e){
            e.preventDefault();

            // console.log($(this).serialize());

            var parameterSample = {};
            var testedBy = $('#tested-by').val();
            var testedDate = $('#tested-date').val();
            var testArrivalDate = $('#tested-arrival-date').val();
            var finalresult = $('#final-result').val();
            var dipSolutionId = $('#dip-solution-id').val();

            $('input[type = "checkbox"]').each(function(){
                if($(this).prop('checked')){
                    var temp = {};
                    var count = 1;
                    temp['result'] = $(this).parent().parent().find('select').val();
                    temp['uom'] = $(this).parent().parent().find('td:nth-child(3)').text();
                    temp['lsl'] = $(this).parent().parent().find('td:nth-child(4)').text();
                    temp['usl'] = $(this).parent().parent().find('td:nth-child(5)').text();
                    $(this).parent().parent().find('input[type="text"]').each(function(){
                        temp["sample"+count] = ($(this).val());
                        count++;
                    });

                    // var name = ;
                    parameterSample[$(this).attr('name')] = temp;

                }
            });

            var formdata = new FormData();

            console.log(parameterSample);

            formdata.append('id',dipSolutionId);
            formdata.append('type','create');
            formdata.append('tested-by',testedBy);
            formdata.append('tested-date',testedDate);
            formdata.append('test-arrival-date',testArrivalDate);
            formdata.append('final-result',finalresult);
            formdata.append('parameter-sample', JSON.stringify(parameterSample));


            console.log(formdata);

            $.ajax( {

                type: "POST",
                url:"/insert-dip-solution-result",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:formdata,
                cache:!1,
                contentType:!1,
                processData:!1,
                success:function(data) {


                    if(data.message == 'success'){

                        window.location.href = '/packing-clearance-updated-list'
                    }
                    else{

                        alert('Cannot Update the package Right now');
                    }
                },
                error: function(xhr, status, error) {
                }
            });

        });
        $('input[type="checkbox"]').on('change', function(){
            if($(this).prop('checked')){
                $(this).prev().empty().append('<i class="material-icons checkbox_selected">check_box</i>');
            }else{
                $(this).prev().empty().append('<i class="material-icons">check_box_outline_blank</i>');

            }
        });


        $('input[type="checkbox"]').on('change', function(){
            if($(this).prop('checked')){
                console.log($(this).parent().parent().find('input[type="text"]').length);
            }
        });

    });
</script>
</body>
</html>
