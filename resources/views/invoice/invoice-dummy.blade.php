<!DOCTYPE html>
<html>
<head>
    <title>Invoice Table</title>

    <style type="text/css">

        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }

        p{
            margin-top: 2px;
            margin-bottom: 2px;
            font-size: 12px;
        }

        td{
            vertical-align: top;
            height: auto !important;
            padding: 1px 8px;
        }
        html{
            margin: 10px;
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
        }
        #package-heading>td{
            vertical-align: middle;
        }

        #package-content>td>p, #package-heading>td>p{
            text-align: center;
        }

        #package-heading>td>p, .strong>td>p{
            font-weight: 600;
        }

        .no-border > td{
            border : none !important;
            /*border-bottom : none !important;*/
            border-right : 1px solid black !important;
        }

        thead>tr>th{
            border: none !important;
        }

        .align-left>p{
            text-align: left !important;
        }

        .align-right-italic>p{
            text-align: right !important;
            font-style: italic !important;
        }

        .align-right{
            text-align: right !important;
        }



    </style>
</head>
<body>

<?php $fmt = new \NumberFormatter($locale = 'en_IN', \NumberFormatter::DECIMAL);?>
<h4 style="text-align: center; margin-bottom: 0px;">TAX INVOICE</h4>
<p style="text-align: right; margin: 0px; !important;">({{$invoiceName}})</p>

<table style="width: 100%;">
    <tbody>
    <tr>
        <td colspan="10" rowspan="3" style="width: 50%;">
            <p style="font-size: 14px;"><strong>Shakti Cords Pvt. Ltd</strong></p>
            <p>CS 17&18, SIDCO INDUSTRIAL ESTATE</p>
            <p>KAPPALUR</p>
            <p>MADURAI - 625008</p>
            <p>GSTIN/UIN : 33AAHCS3989E1Z4</p>
            <p>PAN : AAHCS3989E</p>
            <p>State Name : Tamil Nadu, Code : 33</p>
            <p>CIN : U17297TN2003PTC051243</p>
            <p>Email : sales@indtextile.com, scpl@indtextile.com</p>
        </td>
        <td colspan="5">
            <p>Invoice No.</p>
            <p><strong>{{$master->invoice_no}}</strong></p>
        </td>
        <td colspan="5">
            <p>Dated</p>
            <p><strong>{{date('d-M-y',strtotime($master->date))}}</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <p>Dispatch Through </p>
            <p><strong>{{$indentMaster->dispatcher}}</strong></p>
        </td>
        <td colspan="5">
            <p>Modes/Terms of Payment</p>
            <p><strong>{{$indentMaster->payment_mode}}</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <p>Buyer's Ref/Order No</p>
            <p><strong>{{$master->indent->order_ref_no}}</strong></p>
        </td>
        <td colspan="5">
            <p>Supplier Ref. No</p>
            <p><strong>{{$master->indent->other_ref_no}}</strong></p>
        </td>
    </tr>


    <!-- First Head ends. -->


            @if($master->bp_id == $master->consignee_id)
            <tr>
                <td colspan="10" rowspan="3" style="width: 50%;">

                <p style="font-size: 14px;"><strong>{{$businessPartner['name']}}</strong></p>
                <p>{{$businessPartner['address']}}</p>
                <p>District : {{$businessPartner['district']}}</p>
                <p>{{$businessPartner['pincode']}}</p>
                <p>GST/UIN : {{$businessPartner['gst']}}</p>
                <p>StateName : {{$businessPartner['state']}}, Code : 033</p>

            </td>
            <td colspan="5">
                <p>Transport Mode</p>
                <p><strong>{{$indentMaster->mode_of_transport}}</strong></p>
            </td>
            <td colspan="5">
                <p>Destination</p>
                <p><strong>{{$indentMaster->destination}}</strong></p>
            </td>
            </tr>
            <tr>
                <td colspan="10" rowspan="2" height="200">
                    <p>Terms of Delivery</p>
                </td>
            </tr>
            <tr></tr>
            @else
                <tr>
                <td colspan="10" rowspan="3" style="width: 50%;">
                <p><strong>Invoice to</strong></p>
                <p style="font-size: 14px;"><strong>{{$businessPartner['name']}}</strong></p>
                <p>{{$businessPartner['address']}}</p>
                <p>District : {{$businessPartner['district']}}</p>
                <p>{{$businessPartner['pincode']}}</p>
                <p>GST/UIN : {{$businessPartner['gst']}}</p>
                <p>StateName : {{$businessPartner['state']}}</p>




                </td>
                <td colspan="5">
                    <p>Transport Mode</p>
                    <p><strong>{{$indentMaster->mode_of_transport}}</strong></p>
                </td>
                <td colspan="5">
                    <p>Destination</p>
                    <p><strong>{{$indentMaster->destination}}</strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="10" rowspan="2" height="200">
                    <p>Terms of Delivery</p>
                </td>
            </tr>
            <tr></tr>


                <tr>
                    <td colspan="10" rowspan="3">
                        <?php $consignee = \App\BusinessPartner::where('id', $master->consignee_id)->get()->toArray()[0]; //dd($consignee); ?>
                        <p style="margin-top: 10px; font-size: 14px;"><small><strong>Consignee</strong></small></p>
                        <p style="font-size: 14px;"><strong>{{$consignee['bp_name']}}</strong></p>
                        <p>{{$consignee['address']}}</p>
                        <p>District : {{$consignee['district']}}</p>
                        <p>{{$consignee['pincode']}}</p>
                        <p>GST/UIN : {{$consignee['gst_code']}}</p>
                        <p>StateName : {{$consignee['state']}}, Code : 033</p>
                    </td>
                    <td colspan="10" rowspan="3">

                    </td>
                </tr>
                <tr></tr>
                <tr></tr>
            @endif



    <!--Buyer Ends-->
    <tr id="package-heading">
        <td><p>S.No</p></td>
        <td colspan=2><p>No &amp; Kind of <br> Pkgs.</p></td>
        <td colspan=8><p>Description of Goods</p></td>
        <td><p>HSN/SAC</p></td>
        <td colspan="2"><p>Quantity (Kgs.)</p></td>
        <td><p>Rate <br>(Rs.)</p></td>
        <td><p>Per</p></td>
        <td colspan=4><p>Amount</p></td>
    </tr>
    <?php $orderCount = 1;
    $totalAmount = 0;
    $totalQuantity = 0;
    $taxAmount= [];

    foreach($orderDetails as $orderDetail){

        if(array_key_exists($orderDetail['hsn'], $taxAmount)){

            $taxAmount[$orderDetail['hsn']]['taxableAmount'] += $orderDetail['rate'] * $orderDetail['weight'];
            $taxAmount[$orderDetail['hsn']]['rate'] = $orderDetail['gst'];

        }else{

            $taxAmount[$orderDetail['hsn']]['taxableAmount'] = $orderDetail['rate'] * $orderDetail['weight'];
            $taxAmount[$orderDetail['hsn']]['rate'] = $orderDetail['gst'];

        }


    }

    foreach ($taxAmount as $hsn => $tax){
        $taxAmount[$hsn]['tax'] = ($tax['rate']/100 * $tax['taxableAmount']);
    }



    $integratedTax = array_sum(array_column($taxAmount, 'tax'));


    $taxHsn = [];
    $hsnInsurance = [];
    $hsnHandlingCharges = [];
    $hsnCgst = [];
    $hsnSgst = [];
    $hsnIgst = [];
    $hsnCgstTaxRate = [];
    $hsnSgstTaxRate = [];
    $hsnIgstTaxRate = [];

    $totalSubAmount = 0;
    ?>

    @foreach($orderDetails as $orderDetail)
    <tr id="package-content" class="no-border">
        <td><p>{{$orderCount}}</p></td>
        <td colspan=2><p>{{$orderDetail['no_box']}}</p></td>
        <td colspan=8 class="align-left"><p><strong>{{$orderDetail['name']}}</strong></p></td>
        <td><p>{{$orderDetail['hsn']}}</p></td>
        <td colspan="2"><p><strong>{{$fmt->format(round($orderDetail['weight'], 2))}}</strong></p></td>
        <td><p>{{$orderDetail['rate']}}</p></td>
        <td><p>kgs</p></td>
        <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($orderDetail['rate'] * $orderDetail['weight'], 2))}}</strong></p></td>
    </tr>
    <?php $orderCount++;
        $taxHsn[$orderDetail['hsn']] = round($orderDetail['rate'] * $orderDetail['weight'], 2);
        $totalQuantity += $orderDetail['weight'];
        $totalAmount += $orderDetail['rate'] * $orderDetail['weight'];
    ?>
    @endforeach

    <tr id="package-content" class="no-border">
        <td><p></p></td>
        <td colspan=2><p></p></td>
        <td colspan=8 class="align-right-italic"><p><strong>Subtotal</strong></p></td>
        <td><p></p></td>
        <td colspan="2"></td>
        <td></td>
        <td></td>
        <td colspan=4 class="align-left" style="border-top: 1px solid black !important; margin-top: 0px !important; "><p><strong>{{$fmt->format(round($totalAmount,2))}}</strong></p></td>
    </tr>

    {{--Packages ends--}}

    <?php

    $cgst = 0;
    $sgst = 0;
    $igst = 0;
    $totalGst = 0;
    $handlingCharges = 0;

    $indentDetails = $indentMaster->indentDetails()->whereIn('material_id',array_keys($master->packageMaster->groupBy('material_id')->toArray()))->get();

    foreach ($indentDetails as $indentDetail){

        if(!array_key_exists($indentDetail->hsn_sac, $taxHsn)){
            $taxHsn[$indentDetail->hsn_sac] = 0;
        }

        if(!array_key_exists($indentDetail->hsn_sac, $hsnInsurance)){
            $hsnInsurance[$indentDetail->hsn_sac] = 0;
        }

        if(!array_key_exists($indentDetail->hsn_sac, $hsnHandlingCharges)){
            $hsnHandlingCharges[$indentDetail->hsn_sac] = 0;
        }

        if(!array_key_exists($indentDetail->hsn_sac, $hsnCgst)){
            $hsnCgst[$indentDetail->hsn_sac] = 0;
        }

        if(!array_key_exists($indentDetail->hsn_sac, $hsnSgst)){
            $hsnSgst[$indentDetail->hsn_sac] = 0;
        }

        if(!array_key_exists($indentDetail->hsn_sac, $hsnIgst)){
            $hsnIgst[$indentDetail->hsn_sac] = 0;
        }

        $packedQuantity = \App\WeightLog::whereIn('package_master_id', $master->packageMaster()->where('material_id', $indentDetail->material_id)->pluck('id')->toArray())->sum('material_weight');

        $moisture = $master->packageMaster()->where('material_id', $indentDetail->material_id)->sum('moisture_weight');
        $packedQuantity += $moisture;

        $itemMaster = $indentDetail->itemMaster;

        $tempHandlingCharges = ($master->handling_charges/$totalAmount) * ($packedQuantity * $indentDetail->rate_per_kg);
        $hsnHandlingCharges[$indentDetail->hsn_sac] += $tempHandlingCharges;
        $handlingCharges += $tempHandlingCharges;
        $insuranceCost = ((($indentMaster->transit_insurance/100) * $totalAmount)/$totalAmount) * ($packedQuantity * $indentDetail->rate_per_kg);
        $hsnInsurance[$indentDetail->hsn_sac] += $insuranceCost;


        if($indentMaster->delivery_type == 1){
            //Within Tamil Nadu

            $cgst += $itemMaster->cgst/100 * (($packedQuantity * $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);
            $sgst += $itemMaster->sgst/100 * (($packedQuantity * $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);

            $hsnCgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->cgst;
            $hsnSgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->sgst;
            $taxHsn[$indentDetail->hsn_sac] += ($cgst + $sgst);
            $hsnCgst[$indentDetail->hsn_sac] = $cgst;
            $hsnSgst[$indentDetail->hsn_sac] = $sgst;
        }else{
            //Outside Tamil Nadu

            $totalGst += $itemMaster->igst/100 * (($packedQuantity* $indentDetail->rate_per_kg) + $tempHandlingCharges + $insuranceCost);
            $taxHsn[$indentDetail->hsn_sac] += $totalGst;

            $hsnIgstTaxRate[$indentDetail->hsn_sac] = $itemMaster->igst;
            $hsnIgst[$indentDetail->hsn_sac] = $igst;

        }



    } //foreach ends

    if($indentMaster->delivery_type == 1){
        $totalGst = $sgst + $cgst;

    }

    if($indentMaster->delivery_type == 1){

        $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
        $totalBillable = $totalAmount + $insuranceCost + $cgst +$sgst + $handlingCharges;

    }else{
        $insuranceCost = ($indentMaster->transit_insurance/100) * $totalAmount;
        $totalBillable = $totalAmount + $insuranceCost + $totalGst + $handlingCharges;
    }

    ?>
    @if($indentMaster->delivery_type == 2 || $indentMaster->delivery_type == 3)
        <?php //dd($indentMaster->transit_insurance); ?>

        @if($handlingCharges != 0)
            <tr id="package-content" class="no-border">
                <td><p></p></td>
                <td colspan=2><p></p></td>
                <td colspan=8 class="align-right-italic"><p><strong>Handling Charges</strong></p></td>
                <td><p></p></td>
                <td colspan="2"></td>
                <td></td>
                <td></td>
                <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($handlingCharges, 2))}}</strong></p></td>
            </tr>
        @endif

        @if($indentMaster->transit_insurance != 0)

                <tr id="package-content" class="no-border">
                <td><p></p></td>
                <td colspan=2><p></p></td>
                <td colspan=8 class="align-right-italic"><p><strong>Interstate Insurance</strong></p></td>
                <td><p></p></td>
                <td colspan="2"></td>
                <td><p>{{$indentMaster->transit_insurance}}</p></td>
                <td><p>%</p></td>
                <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($insuranceCost, 2))}}</strong></p></td>
            </tr>
            @endif
            <tr id="package-content" class="no-border">
                <td><p></p></td>
                <td colspan=2><p></p></td>
                <td colspan=8 class="align-right-italic"><p><strong>Integrated Tax</strong></p></td>
                <td><p></p></td>
                <td colspan="2"></td>
                <td></td>
                <td></td>
                <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($totalGst, 2))}}</strong></p></td>
            </tr>
    @else
        @if($handlingCharges != 0)
            <tr id="package-content" class="no-border">
                <td><p></p></td>
                <td colspan=2><p></p></td>
                <td colspan=8 class="align-right-italic"><p><strong>Handling Charges</strong></p></td>
                <td><p></p></td>
                <td colspan="2"></td>
                <td></td>
                <td></td>
                <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($handlingCharges, 2))}}</strong></p></td>
            </tr>
        @endif
        @if($indentMaster->transit_insurance != 0)
        <tr id="package-content" class="no-border">
            <td><p></p></td>
            <td colspan=2><p></p></td>
            <td colspan=8 class="align-right-italic"><p><strong>Intrastate Insurance</strong></p></td>
            <td><p></p></td>
            <td colspan="2"></td>
            <td><p>{{$indentMaster->transit_insurance}}</p></td>
            <td><p>%</p></td>
            <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($insuranceCost, 2))}}</strong></p></td>
        </tr>
        @endif

        <tr id="package-content" class="no-border">
            <td><p></p></td>
            <td colspan=2><p></p></td>
            <td colspan=8 class="align-right-italic"><p><strong>SGST</strong></p></td>
            <td><p></p></td>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($sgst, 2))}}</strong></p></td>
        </tr>

        <tr id="package-content" class="no-border">
            <td><p></p></td>
            <td colspan=2><p></p></td>
            <td colspan=8 class="align-right-italic"><p><strong>CGST</strong></p></td>
            <td><p></p></td>
            <td colspan="2"></td>
            <td></td>
            <td></td>
            <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($cgst, 2))}}</strong></p></td>
        </tr>
    @endif

    <?php $finalTotal = ceil($totalAmount + $totalGst + $insuranceCost + $handlingCharges); $roundOffValue = round($finalTotal - ($totalAmount + $totalGst + $insuranceCost + $handlingCharges), 2);?>
    <tr id="package-content" class="no-border">
        <td><p></p></td>
        <td colspan=2><p></p></td>
        <td colspan=8 class="align-right-italic"><p><strong>Round Off to Sales</strong></p></td>
        <td><p></p></td>
        <td colspan="2"></td>
        <td></td>
        <td></td>
        <td colspan=4 class="align-left"><p><strong>{{$fmt->format(round($roundOffValue, 2))}}</strong></p></td>
    </tr>

    <tr id="package-content" >
        <td><p></p></td>
        <td colspan=2><p></p></td>
        <td colspan=8 class="align-right-italic"><p><strong>Total</strong></p></td>
        <td><p></p></td>
        <td colspan="2"><p><strong>{{$fmt->format($totalInvoiceWeight)}}</strong></p></td>
        <td></td>
        <td></td>
        <td colspan=4 class="align-left"><p><strong>{{$fmt->format((round(($finalTotal), 2)))}}</strong></p></td>
    </tr>



    <?php

    $totalBillable = $finalTotal;
    $currencyToWords = new App\CurrencyToWords();

    $totalTaxableValue = 0;
    $totalTaxAmount = 0;

    $cgstTotalTaxValue = 0;
    $sgstTotalTaxValue = 0;

    $igstTotalTaxValue = 0;

    $convertedCurrency = $currencyToWords->convertIndianCurrency(($totalBillable - ($totalBillable - floor($totalBillable))));

    if(is_numeric( $totalBillable ) && floor( $totalBillable ) != $totalBillable){
        $decimalValue = $totalBillable - floor($totalBillable);

        $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

        $convertedCurrency = str_replace(' and ','',$convertedCurrency).' rupees and '. $decimalWords. ' paise only.';
    }

    ?>


    <tr>
        <td colspan="20" rowspan="3">
            <p><small>Amount Chargeable (in words.)</small></p>
            <p style="font-size: 14px;"><strong>Rupees {{$convertedCurrency}} only.</strong></p>
        </td>
    </tr>
    <tr></tr>
    <tr></tr>
    {{--Amount Chargeable ends--}}

    {{--Below is for CGST and SGST--}}

    @if($indentMaster->delivery_type == 1)
        <tr class="strong">
            <td rowspan="2" colspan="5"><p>HSN/SAC</p></td>
            <td rowspan="2" colspan="5"><p>Taxable Value</p></td>
            <td colspan="4"><p>Central Tax</p></td>
            <td colspan="4"><p>State Tax</p></td>
            <td colspan="2"><p>Total Tax Amount</p></td>
        </tr>
        <tr class="strong">
            <td colspan="2"><p>Rate (%)</p></td>
            <td colspan="2"><p>Amount</p></td>
            <td colspan="2"><p>Rate (%)</p></td>
            <td colspan="2"><p>Amount</p></td>
            <td colspan="2"></td>
        </tr>

        @foreach($taxAmount as $hsn => $tax)

            <?php
                $amount = $tax['taxableAmount'] + $hsnInsurance[$hsn] + $hsnHandlingCharges[$hsn];
                $totalTaxableValue +=  $amount;
                $totalTaxAmount +=  $taxHsn[$hsn];
                $cgstTaxValue = ($hsnCgstTaxRate[$hsn]/100) * $amount;
                $sgstTaxValue = ($hsnSgstTaxRate[$hsn]/100) * $amount;

                $cgstTotalTaxValue += $cgstTaxValue;
                $sgstTotalTaxValue += $sgstTaxValue;
            ?>
            <tr>
                <td colspan="5"><p>{{$hsn}}</p></td>
                <td colspan="5"><p>{{$fmt->format(round($amount, 2))}}</p></td>
                <td colspan="2"><p>{{$hsnCgstTaxRate[$hsn]}}</p></td>
                <td colspan="2"><p>{{$fmt->format(round($cgstTaxValue, 2))}}</p></td>
                <td colspan="2"><p>{{$hsnSgstTaxRate[$hsn]}}</p></td>
                <td colspan="2"><p>{{$fmt->format(round($sgstTaxValue, 2))}}</p></td>
                <td colspan="2"><p>{{$fmt->format(round($cgstTaxValue + $sgstTaxValue, 2))}}</p></td>
            </tr>
        @endforeach

        <tr class="strong">
            <td colspan="5"><p>Total</p></td>
            <td colspan="5"><p>{{$fmt->format(round($totalTaxableValue, 2))}}</p></td>
            <td colspan="2"></td>
            <td colspan="2"><p>{{$fmt->format(round($cgstTotalTaxValue, 2))}}</p></td>
            <td colspan="2"><p>{{$fmt->format(round($cgstTotalTaxValue, 2))}}</p></td>
            <td colspan="2"><p>{{$fmt->format(round($sgstTotalTaxValue, 2))}}</p></td>
            <td colspan="2"><p>{{$fmt->format(round($sgstTotalTaxValue + $cgstTotalTaxValue, 2))}}</p></td>
        </tr>

        <?php
        $printableTax = $sgstTotalTaxValue + $cgstTotalTaxValue;

        $convertedCurrency = $currencyToWords->convertIndianCurrency(floor($printableTax));

        if(is_numeric( $printableTax ) && floor( $printableTax ) != $printableTax){
            $decimalValue = $printableTax - floor($printableTax);

            $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

            $convertedCurrency = str_replace(' and ','',$convertedCurrency).' and '. $decimalWords. 'paise only.';
        }

        ?>
        <tr>
            <td colspan="12">
                <p><small>Tax Amount in Words :</small></p>
                <p style="font-size: 14px;"><strong>Rupees {{$convertedCurrency}}</strong></p>
            </td>

            <td colspan="8">
                <p>Company's Bank Details</p>
                <p>Bank Name : <strong>IDBI Bank</strong></p>
                <p>Acc Name : <strong>0388655000000028</strong></p>
                <p>Branch & IFSC Code : <strong>Madurai & IBKL0000388</strong></p>

            </td>
        </tr>

        {{--Tax Amount and company details ends--}}


        {{--Declaration--}}

        <tr>
            <td colspan="12">
                <small>Declaration</small>
                <p>Any Statutory variation in any govt levies will be to the account of buyer. Subject to
                    Madurai Jurisdiction only. Certified that the particulars given above are correct and the
                    amount indicated represents the price actually charged and there is no additional
                    consideration flowing directly or indirectly from the buyer.</p>
            </td>

            <td colspan="8" style="vertical-align: bottom">
                <p style="text-align: right;"><strong>For Shakti Cords Pvt. Ltd.</strong></p>
                <p style="text-align: right;"><strong>Authorised Signatory</strong></p>
            </td>
        </tr>
        {{--CGST and SGST Ends--}}

    @else

        {{--IGST Starts below--}}
        <tr>
            <td rowspan="2" colspan="7"><p>HSN/SAC</p></td>
            <td rowspan="2" colspan="7"><p>Taxable Value</p></td>
            <td colspan="4"><p>Integrated Tax</p></td>
            <td colspan="2"><p>Total Tax Amount</p></td>
        </tr>
        <tr>
            <td colspan="2"><p>Rate (%)</p></td>
            <td colspan="2"><p>Amount</p></td>
            <td colspan="2"></td>
        </tr>

        @foreach($taxAmount as $hsn => $tax)
            <?php
            $amount = $tax['taxableAmount'] + $hsnInsurance[$hsn] + $hsnHandlingCharges[$hsn];
            $totalTaxableValue +=  $amount;
            $totalTaxAmount +=  $taxHsn[$hsn];

            $igstTaxValue = ($hsnIgstTaxRate[$hsn]/100) * $amount;

            $igstTotalTaxValue += $igstTaxValue;

            ?>

            <tr>
                <td colspan="7"><p>{{$hsn}}</p></td>
                <td colspan="7"><p>{{$fmt->format(round($amount, 2))}}</p></td>
                <td colspan="2"><p>{{$hsnIgstTaxRate[$hsn]}}</p></td>
                <td colspan="2"><p>{{$fmt->format(round($igstTaxValue, 2))}}</p></td>
                <td colspan="2"><p>{{$fmt->format(round($igstTaxValue, 2))}}</p></td>
            </tr>
        @endforeach

        <tr class="strong">
            <td colspan="7"><p style="text-align: right"><strong>Total</strong></p></td>
            <td colspan="7"><p>{{$fmt->format(round($totalTaxableValue, 2))}}</p></td>
            <td colspan="2"><p></p></td>
            <td colspan="2"><p>{{$fmt->format(round($igstTotalTaxValue, 2))}}</p></td>
            <td colspan="2"><p>{{$fmt->format(round($igstTotalTaxValue, 2))}}</p></td>
        </tr>

        {{--IGST Ends--}}

        <?php

        $printableTax = $igstTotalTaxValue;
        $convertedCurrency = $currencyToWords->convertIndianCurrency(floor($printableTax));

        if(is_numeric( $printableTax ) && floor( $printableTax ) != $printableTax){
            $decimalValue = $printableTax - floor($printableTax);

            $decimalWords = $currencyToWords->convertIndianCurrency(round($decimalValue, 2) * 100);

            $convertedCurrency = str_replace(' and ','',$convertedCurrency).'rupees and '. $decimalWords. 'paise only.';
        }

        ?>
        <tr>
            <td colspan="12">
                <p><small>Tax Amount in Words :</small></p>
                <p style="font-size: 14px;"><strong>{{$convertedCurrency}}</strong></p>
            </td>

            <td colspan="8">
                <p>Company's Bank Details</p>
                <p>Bank Name : <strong>IDBI Bank</strong></p>
                <p>Acc Name : <strong>0388655000000028</strong></p>
                <p>Branch & IFSC Code : <strong>Madurai & IBKL0000388</strong></p>

            </td>
        </tr>

        {{--Tax Amount and company details ends--}}


        {{--Declaration--}}

        <tr>
            <td colspan="12">
                <small>Declaration</small>
                <p>Any Statutory variation in any govt levies will be to the account of buyer. Subject to
                    Madurai Jurisdiction only. Certified that the particulars given above are correct and the
                    amount indicated represents the price actually charged and there is no additional
                    consideration flowing directly or indirectly from the buyer.</p>
            </td>

            <td colspan="8" style="vertical-align: bottom">
                <p style="text-align: right;"><strong>For Shakti Cords Pvt. Ltd.</strong></p>
                <p style="text-align: right;"><strong>Authorised Signatory</strong></p>
            </td>
        </tr>

    @endif



    {{--Tax Amount In Words and Company Bank Details--}}




    </tbody>
</table>
</body>
</html>