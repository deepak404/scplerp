@extends('layouts.invoice')

@section('css')
 <!-- CSS  -->
 <style media="screen">
   h3{
     display: inline-block;
   }
   .download{
     float: right;
     padding: 20px;
   }
   .material-icons:hover{
     color: #003ebb !important;
   }
 </style>
@endsection

@section('content')
  <section id="indent-list">
      <div class="container">
        <h3>Doff Wise Stock (Sound)</h3>
        <a class="download" href="/instock/exp" target="_blank" title="Export"><i class="material-icons">open_in_new</i></a>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width:10%;">CODE</th>
              <th style="">Code Description</th>
              <th style="width:10%;">Info</th>
              <th style="">Quantity</th>
              <th style="width:10%;">No. Packs</th>
              <th style="width:10%;">No. SPL</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($instock as $unique_id => $info): ?>
              <?php foreach ($info as $key => $value): ?>
                <tr>
                  <th>{{$unique_id}}</th>
                  <td>{{$value['material']}}</td>
                  <td>{{$key}}</td>
                  <td>{{$value['weight']}}</td>
                  <td>{{$value['box']}}</td>
                  <td>{{$value['spindle']}}</td>
                </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="3">Total</th>
              <th>{{$total['weight']}}</th>
              <th>{{$total['box']}}</th>
              <th>{{$total['spindle']}}</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </section>
@endsection

@section('script')

@endsection
