@extends('layouts.invoice')

@section('css')
    <link rel="stylesheet" href="{{url('css/invoice-lot.css')}}">

    <style>
        .dropdown-toggle{
            width: 165px;
        }

        .material-div{
            margin: 5px auto;
        }

        .material-div:last-child{
            margin-bottom: 0px !important;
        }

        .weight-noti{
            margin-bottom: 20px;
        }

        #selected-div{
            position: fixed;
            right: 0;
            padding: 20px;
            box-shadow: 0px 0px 5px gainsboro;
            bottom: 0;
            background-color: white;
        }
    </style>
@endsection

@section('content')
    <div id="selected-div">
        <p class="total-selected-boxes">Selected Boxes : <span>0</span></p>
    </div>

    <section id="invoice_lot">
        <div class="container-fluid">
            <div class="row">
                <h3>Create Challan</h3>


                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <form action="/create-challan-details" method="POST"  name="invoice-form" id="invoice-form">

                    <div class="col-md-12" id="invoice-input">
                        {{csrf_field()}}




                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="challan_no">Challan No</label>
                                <input type="text" name="challan_no" id="challan_no" class="text-input" required ">
                                </label>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="reason">Reason</label>
                                <input type="text" name="reason" id="reason" class="text-input" required ">
                                </label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="text" name="date" id="date" class="text-input" required ">
                                </label>
                            </div>
                        </div>

                    </div>





                        <?php //dd($material); ?>
                        <h4>{{$packageMasters[0]->material_name}}</h4>
                        <div class="col-md-12 material-div">
                            <h4 class="weight-noti"><span class="total-net-weight">Net Weight : <span>0</span> Kgs&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="total-gross-weight">Gross Weight : <span>0</span> Kgs</span></h4>
                            <h4 class="selected-boxes"><span>Selected Boxes : </span> <span>0</span></h4>


                            <table class="table">
                                <thead>
                                <th><input type="checkbox" class="select-all-packing"></th>
                                <th>Pack No</th>
                                <th>DOFF. No</th>
                                <th>Net Weight</th>
                                <th>Gross Weight</th>
                                <th>Spindle Count</th>
                                <th>Pack Type</th>
                                </thead>
                                <tbody>

                                @foreach($packageMasters as $packages)
                                    <?php //dd($packages); ?>
                                    <tr>
                                        <td><input type="checkbox" name="packing_details[]" value="{{$packages->id}}" class="packing-details"></td>
                                        <td>{{$packages->case_no}}</td>
                                        <td>{{$packages->doff_no}}</td>
                                        <td class="net-weight">{{$packages->net_weight}}</td>
                                        <td class="gross-weight">{{$packages->gross_weight}}</td>
                                        <td>{{$packages->spindle_count}}</td>
                                        <td>{{$packages->box_type}}</td>
                                    </tr>


                                @endforeach

                            </table>
                        </div>


                    <input type="submit" class="btn btn-primary" value="Add Details">
                </form>
            </div>
        </div>
    </section>


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Production Order Assign</h4>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <td></td>
                            <td>From Date</td>
                            <td>Customer Name</td>
                            <td>Order(Kgs)</td>
                            <td>Quality</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        <tr>
                            <td><input type="radio" name="actual-production"></td>
                            <td>21/2/2018</td>
                            <td>JK Fenner</td>
                            <td>500</td>
                            <td>9x3ss</td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="button" class="btn btn-primary" value="Add Actual Plan">
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')

    <script type="text/javascript">


        $(document).ready(function(){
            $('#invoice-form').on('submit', function(){
                if($('.packing-details:checked').length == 0){
                    alert('Please select packing info to add to the Invoice');
                    return false;
                }else{
                    $('#invoice-form').submit();
                }
            });

            $('#invoice_no').on('input',function() {
                $(this).val($(this).val().replace(/[\/\\]/gi, ''));
            });

            $('#date').datepicker({dateFormat : 'dd-mm-yy'})


            $('.select-all-packing').on('click', function(){



                if($(this).is(':checked')){

                    $(this).closest('.material-div').find('.selected-boxes span:last-child').text(0);
                    $(this).closest('.material-div').find('.total-gross-weight span:first-child').text(0);
                    $(this).closest('.material-div').find('.total-net-weight span:first-child').text(0);

                    $(this).closest('table').find('tbody tr').find('td>input[type="checkbox"]').prop('checked',true).change();

                }else{

                    $(this).closest('table').find('tbody tr').find('td>input[type="checkbox"]').prop('checked',false).change();
                    $(this).closest('.material-div').find('.selected-boxes span:last-child').text(0);
                }

                setTotalSelectedBoxes();

            });


            $('.packing-details').on('change', function(){

                var netWeight = $(this).parent().parent().find('.net-weight').text();
                var grossWeight = $(this).parent().parent().find('.gross-weight').text();
                var selectedBoxes = parseInt($(this).closest('.material-div').find('.selected-boxes').find('span:last-child').text());


                // console.log(selectedBoxes);

                if($(this).is(':checked')){

                    var totalGrossWeight = parseFloat($(this).closest('.material-div').find('.total-gross-weight span:first-child').text()) + parseFloat(grossWeight);
                    var totalNetWeight = parseFloat($(this).closest('.material-div').find('.total-net-weight span:first-child').text()) + parseFloat(netWeight);

                    selectedBoxes++;
                    // console.log(totalSelected++);
                    // $('.total-selected-boxes').find('span:last-child').text(totalSelected++);

                }else{


                    var totalGrossWeight = parseFloat($(this).closest('.material-div').find('.total-gross-weight span:first-child').text()) - parseFloat(grossWeight);
                    var totalNetWeight = parseFloat($(this).closest('.material-div').find('.total-net-weight span:first-child').text()) - parseFloat(netWeight);
                    selectedBoxes--;
                    // console.log(totalSelected--);

                    // $('.total-selected-boxes').find('span:last-child').text(totalSelected--);


                }

                $(this).closest('.material-div').find('.selected-boxes span:last-child').text(selectedBoxes);



                // $('#selected-div').find('span').text(selectedBoxes);

                $(this).closest('.material-div').find('.total-gross-weight span:first-child').text(totalGrossWeight.toFixed(2));
                $(this).closest('.material-div').find('.total-net-weight span:first-child').text(totalNetWeight.toFixed(2));

                setTotalSelectedBoxes();


            })


            function setTotalSelectedBoxes(){
                var totalSelectedBoxes = $(document).find('.packing-details:checked').length;
                // console.log(totalSelectedBoxes)
                $('.total-selected-boxes').find('span:last-child').text(totalSelectedBoxes);

            }


            $('#en_hc').on('change', function(){

                $('#handling-charges').attr('disabled',false);
                if($(this).find('option:selected').val() == "yes"){
                    $('#handling-charges').val('');
                }else{
                    $('#handling-charges').val(0);
                }
            })


            $('#handling-charges').on('keydown', function(e){
                if($('#en_hc').find('option:selected').val() == "no"){
                    e.preventDefault();
                }else{
                    var charCode = (e.which) ? e.which : e.keyCode;
                    console.log(charCode);
                    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
                        return false;
                    }
                    return true;

                }
            });


            $('#invoice_no').on('keydown', function (e) {
                e.preventDefault();
            })




        });



    </script>
@endsection
