@extends( \Auth::user()->dept_id == '6'  ?  'layouts.invoice' : 'layouts.sales' )


@section('css')

<style>
    .machine-list-card{
        width: 350px;
        height: 320px;
        box-shadow: 0px 0px 5px #cecece;
        /*text-align: center;*/
        background-color: white;
    }

    .card-holder{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 80vh;
    }

    .form-group span{
        width: 80px;
        display: inline-block;
        margin-left: 30px;
    }

    form{
        margin-top: 50px;
    }

    input[type="submit"]{
        width: 150px;
        margin-top: 30px;
    }

    h3{
        margin-top: 15px;
    }

    body{
        background-color: #f5f5f5;
    }

    form input[type="text"], form select{
        width: 170px !important;
        height: 35px !important;
        background-color: #f7f7f7 !important;
    }
</style>

@endsection

@section('content')

<section id="pf-head">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 card-holder">
            <div class="machine-list-card">

                @if(\Route::current()->uri == 'dispatch-by-date')
                    <h3 class="text-center">Dispatch By Date</h3>
                <form action="/get-dispatch-date" method="POST" target="_blank">
                    {{csrf_field()}}
                    <div class="form-group">
                      <span for="filter-date">From : </span>
                      <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                      <span for="filter-date">To : </span>
                      <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary center-block" value="Get Report">
                    </div>
                </form>

                @elseif(\Route::current()->uri == 'dispatch-by-material')
                    <h3 class="text-center">Dispatch By Quality</h3>
                    <form action="/get-dispatch-by-material" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <span for="filter-date">From : </span>
                            <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <span for="filter-date">To : </span>
                            <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="filter-date">Customer</span>
                            <select name="material" id="material">
                                @foreach(\App\ItemMaster::all() as $item)
                                    <option value="{{$item->id}}">{{$item->descriptive_name}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Get Report">
                        </div>

                    </form>

                    @elseif(\Route::current()->uri == 'dispatch-by-customer')
                    <h3 class="text-center">Dispatch By Customer</h3>
                    <form action="/get-dispatch-by-customer" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <span for="filter-date">From : </span>
                            <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <span for="filter-date">To : </span>
                            <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <span for="filter-date">Customer</span>
                            <select name="customer" id="customer">
                                @foreach(\App\BusinessPartner::all() as $bp)
                                    <option value="{{$bp->id}}">{{$bp->bp_name}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Get Report">
                        </div>
                    </form>

                @elseif(\Route::current()->uri == 'dispatch-all-customer')
                    <h3 class="text-center">Cumulative Dispatch</h3>
                    <form action="/get-cumulative-dispatch" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="form-group">
                            <span for="filter-date">From : </span>
                            <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <span for="filter-date">To : </span>
                            <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary center-block" value="Get Report">
                        </div>
                    </form>


                @elseif(\Route::current()->uri == 'pre-close-report')
                        <h3 class="text-center">Pre Close Report</h3>
                        <form action="/show-preclose-report" method="POST" target="_blank">
                            {{csrf_field()}}
                            <div class="form-group">
                                <span for="filter-date">From : </span>
                                <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <span for="filter-date">To : </span>
                                <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary center-block" value="Get Report">
                            </div>
                        </form>
                    @endif
            </div>

        </div>
    </div>
</div>
</section>

@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.filter-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd-mm-yy',
        // onClose: function(dateText, inst) {
        //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        // }
    });
    $('#filter-date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });
});
</script>
@endsection
