@extends('layouts.invoice')

@section('css')
    <link rel="stylesheet" href="{{url('css/invoice-lot.css')}}">

    <style>
        .dropdown-toggle{
            width: 165px;
        }

        .material-div{
            margin: 5px auto;
        }

        .material-div:last-child{
            margin-bottom: 0px !important;
        }

        .weight-noti{
            margin-bottom: 20px;
        }

        #selected-div{
            position: fixed;
            right: 0;
            padding: 20px;
            box-shadow: 0px 0px 5px gainsboro;
            bottom: 0;
            background-color: white;
        }
    </style>
@endsection

@section('content')
    <div id="selected-div">
        <p class="total-selected-boxes">Selected Boxes : <span>0</span></p>
    </div>

    <section id="invoice_lot">
        <div class="container-fluid">
            <div class="row">
                <h3>Sales Return</h3>


                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <form action="/create-sales-return" method="POST"  name="sales-return-form" id="sales-return-form">

                    <div class="col-md-12" id="invoice-input">
                        {{csrf_field()}}

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="intimation_by">Intimation by</label>
                                <input type="text" name="intimation_by" id="intimation_by" class="text-input" value="Stores" required>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="invoice_date">Customer</label>
                                <select class="text-input" name="customer" id="customer">
                                    @foreach(\App\BusinessPartner::all() as $bp)
                                        <option value="{{$bp->id}}">{{$bp->bp_name}}</option>
                                    @endforeach
                                </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="customer">Goods Receieved on</label>
                                <input type="text" class="text-input" id="received_on" value="25/10/2018" name="received_on" class = "received_on" required>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="doc_received">Documents Received</label>
                                <input type="text" name="docs_received" id="docs_received" value="invoice from the customer" class="text-input" required>

                                </label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="grn">GRN</label>
                                <input type="text" value="202" class="text-input" id="grn" name="grn" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="shortage">Shortage of Qty</label>
                                <input type="text" class="text-input" value="40" id="shortage" name="shortage" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="reason">Reason for Return</label>
                                <input type="text" class="text-input" value="winding issue" id="reason" name="reason" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="condition">SCPL Supplied Invoice No</label>
                                <input type="text" class="text-input" value="IC-1505"  id="scpl-invoice" name="scpl_invoice" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="condition">Goods Condition</label>
                                <input type="text" class="text-input" id="condition" value="Goodd Condition" name="condition" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="frieght-details">Frieght Details</label>
                                <input type="text" class="text-input" value="jaipur golden" id="frieght-details" name="frieght_details" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="sales-folio">Sales Return Folio</label>
                                <input type="text" class="text-input" id="sales-folio" value="104" name="sales_folio" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="attachment-details">Attached Copy Details</label>
                                <input type="text" class="text-input" id="attachement-details" value="invoice and GRN Only" name="attachment_details" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="credit-note-details">Credit Note Details</label>
                                <input type="text" class="text-input" id="credit-note-details" name="credit_note_details" value="-" required>
                            </div>
                        </div>
                    </div>

                    <div id="invoice-list" class="col-md-12">
                        <h3>Customer Invoices</h3>
                        <p style="margin-left: 15px; font-size: 16px;"><a href="#" class="add-invoice">Add Invoice</a></p>
                        <div class="invoice-parent-holder">
                            <div class="col-md-6">
                                <label>Invoice List</label>
                                <select name="invoices[]" class="invoices-list text-input">

                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Material</label>
                                <select name="material[]" class="material-list text-input">

                                </select>
                            </div>
                        </div>
                    </div>


                    <input type="submit" class="btn btn-primary" value="Add Details">
                </form>
            </div>
        </div>
    </section>


@endsection

@section('script')

    <script type="text/javascript">


        $(document).ready(function(){




           $('#customer').on('change', function(e){
               $.ajax({
                   type:'POST',
                   url:'/get-bp-invoices',
                   data:'bp_id='+$(this).val(),
                   success:function(data){
                        appendInvoices(data)
                   },
                   error:function(xhr){
                       console.log(xhr.status);
                   }
               });

           });

            function appendInvoices(data){
               console.log(data.invoices.length);

               $('.invoices-list').each(function(){
                  $(this).children().remove();
               });

                $('.material-list').each(function(){
                    $(this).children().remove();
                });
               if(data.invoices.length > 0){
                   $('.invoices-list').append('<option disabled selected>Select Invoice</option>')
                   $.each(data.invoices, function(k, v){
                        $('.invoices-list').append('<option value="'+v.id+'">'+v.invoice_no+'</option>')
                    });
               }else{
                   console.log('No Invoices found for the customer');
               }
            }


            $(document).on('change','.invoices-list' ,function (e) {
                var invoice = $(this);
                console.log(invoice);
                $.ajax({
                    type:'POST',
                    url:'/get-invoice-materials',
                    data:'id='+$(this).val(),
                    success:function(data){
                        // appendInvoices(data)
                        invoice.closest('.invoice-parent-holder').find('.material-list').children().remove();
                        $.each(data.materials, function(k,v){
                            invoice.closest('.invoice-parent-holder').find('.material-list').append(
                                '<option value="'+v.material_id+'">'+v.descriptive_name+'</option>'
                            );
                        });
                        // console.log(.children().length);
                    },
                    error:function(xhr){
                        console.log(xhr.status);
                    }
                });
            });


            $('.add-invoice').on('click', function(){

               var existingInvoices =  $('#invoice-list').find('.invoice-parent-holder').length;

               if(existingInvoices > 0){
                    var existingInvoiceList = $('.invoice-parent-holder:first').find('.invoices-list').clone().children();
                   $('#invoice-list').append(
                       ' <div class="invoice-parent-holder">' +
                       '      <div class="col-md-6">' +
                       '           <label>Invoice List</label>' +
                       '           <select name="invoices[]" class="invoices-list text-input">' +
                       '           </select>' +
                       '       </div>' +
                       '       <div class="col-md-6">' +
                       '           <label>Material</label>' +
                       '           <select name="material[]" class="material-list text-input">' +
                       '           </select>' +
                       '       </div>' +
                       '   </div>'
                   );
                    console.log(existingInvoiceList);
                   $(document).find('.invoices-list:last').append(existingInvoiceList);

               }else{
                   $('#invoice-list').append(
                       ' <div class="invoice-parent-holder">' +
                       '      <div class="col-md-6">' +
                       '           <label>Invoice List</label>' +
                       '           <select name="invoices[]" class="invoices-list text-input">' +
                       '           </select>' +
                       '       </div>' +
                       '       <div class="col-md-6">' +
                       '           <label>Material</label>' +
                       '           <select name="material[]" class="material-list text-input">' +
                       '           </select>' +
                       '       </div>' +
                       '   </div>'
                   );
               }

            });
        });



    </script>
@endsection
