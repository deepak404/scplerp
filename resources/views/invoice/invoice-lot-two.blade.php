@extends('layouts.invoice')

@section('css')
<link rel="stylesheet" href="{{url('css/invoice-lot.css')}}">

    <style>
        .dropdown-toggle{
            width: 165px;
        }

        .material-div{
            margin: 5px auto;
        }

        .material-div:last-child{
            margin-bottom: 0px !important;
        }

        .weight-noti{
            margin-bottom: 20px;
        }

        #selected-div{
            position: fixed;
            right: 0;
            padding: 20px;
            box-shadow: 0px 0px 5px gainsboro;
            bottom: 0;
            background-color: white;
        }
    </style>
@endsection

@section('content')
    <div id="selected-div">
        <p class="total-selected-boxes">Selected Boxes : <span>0</span></p>
    </div>

    <section id="invoice_lot">
    <div class="container-fluid">
        <div class="row">
            <h3>Invoice Lot</h3>


            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form action="/create-invoice-details" method="POST"  name="invoice-form" id="invoice-form">

                <div class="col-md-12" id="invoice-input">
                    {{csrf_field()}}


                    <input type="hidden" name="indent_id" id="indent_id" value="{{$indentMaster->id}}" class="text-input" required>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="invoice_no">Invoice No</label>
                                <input type="text" name="invoice_no" id="invoice_no" class="text-input" required value="{{$latestInvoiceNo}}">
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="invoice_date">Date</label>
                                <input type="text" name="invoice_date" id="invoice_date" class="text-input" autocomplete="off" required>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="customer">Customer Name</label>
                            <select name="customer" id="customer">
                                    <option value="{{$indentMaster->bp_id}}">{{\App\BusinessPartner::where('id', $indentMaster->bp_id)->value('bp_name')}}</option>
                            </select>
                            </label>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="invoice_no">Enable Handling Charges</label>
                            <select name="en_hc" id="en_hc">
                                <option value="" selected disabled>select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="handling-charges">Handling Charges</label>
                            <input type="text" name="handling_charges" id="handling-charges" class="text-input" autocomplete="off" required disabled>

                            </label>
                        </div>
                    </div>


                    {{--<div class="col-md-3">--}}
                        {{--<div class="form-group">--}}
                            {{--<label for="material">Material</label>--}}
                            {{--<select name="material" id="material">--}}
                                {{--<option value="0">2X3 SOFT</option>--}}
                            {{--</select>--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>

                @foreach($materialIds as $material)

                    <h4>{{\App\ItemMaster::where('id', $material)->value('descriptive_name')}}</h4>
                    <div class="col-md-12 material-div">
                        <h4 class="weight-noti"><span class="total-net-weight">Net Weight : <span>0</span> Kgs&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="total-gross-weight">Gross Weight : <span>0</span> Kgs</span></h4>
                        <h4 class="selected-boxes"><span>Selected Boxes : </span> <span>0</span></h4>
                        @if(array_key_exists($material, $packed->toArray()))
                            @foreach($packed as $material_id => $packages)

                                @if($material_id == $material)

                                <table class="table">
                                    <thead>
                                    <th><input type="checkbox" class="select-all-packing"></th>
                                    <th>Pack No</th>
                                    <th>DOFF. No</th>
                                    <th>Type</th>
                                    <th>Info</th>
                                    <th>Net Weight</th>
                                    <th>Gross Weight</th>
                                    <th>Spindle Count</th>
                                    <th>Pack Type</th>
                                    </thead>
                                    <tbody>




                                    <?php //dd($packed); ?>

                                    @foreach($packages as $packingDetail)
                                        @if($packingDetail->invoice_status == 0)
                                            <?php  $doff_no = implode(', ',$packingDetail->weightLogs->pluck('doff_no')->toArray()); ?>
                                        <tr>
                                            <td><input type="checkbox" name="packing_details[]" value="{{$packingDetail->id}}" class="packing-details"></td>
                                            <td>{{$packingDetail->case_no}}</td>
                                            <td>{{$doff_no}}</td>
                                            <td>{{$packingDetail->material_type}}</td>
                                            <td><strong>{{$packingDetail->reason}}</strong></td>
                                            <td class="net-weight">{{round($packingDetail->weightLogs->sum('material_weight') + $packingDetail->moisture_weight, 1)}}</td>
                                            <td class="gross-weight">{{round($packingDetail->inset_weight + $packingDetail->box_weight + $packingDetail->weightLogs->sum('total_weight'),1)}}</td>
                                            <td>{{$packingDetail->bobbin_count}}</td>
                                            <td>{{$packingDetail->packing_box}}</td>
                                        </tr>
                                        @endif
                                    @endforeach

                                    </tbody>
                                </table>
                                @endif
                            @endforeach
                        @else
                            <p>No packing Details for this material yet.</p>
                        @endif

                    </div>
                @endforeach

                <input type="submit" class="btn btn-primary" value="Add Details">
            </form>
        </div>
    </div>
</section>


@endsection

@section('script')

<script type="text/javascript">


    $(document).ready(function(){
        $('#invoice-form').on('submit', function(){
            if($('.packing-details:checked').length == 0){
                alert('Please select packing info to add to the Invoice');
                return false;
            }else{
                $('#invoice-form').submit();
            }
        });

        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                // alert('enter pressed da');
            }
        });

        $('#invoice_no').on('input',function() {
          $(this).val($(this).val().replace(/[\/\\]/gi, ''));
        });

        $('#invoice_date').datepicker({dateFormat : 'dd-mm-yy'})


        $('.select-all-packing').on('click', function(){



            if($(this).is(':checked')){

                $(this).closest('.material-div').find('.selected-boxes span:last-child').text(0);
                $(this).closest('.material-div').find('.total-gross-weight span:first-child').text(0);
                $(this).closest('.material-div').find('.total-net-weight span:first-child').text(0);

                $(this).closest('table').find('tbody tr').find('td>input[type="checkbox"]').prop('checked',true).change();

            }else{

                $(this).closest('table').find('tbody tr').find('td>input[type="checkbox"]').prop('checked',false).change();
                $(this).closest('.material-div').find('.selected-boxes span:last-child').text(0);
            }

            setTotalSelectedBoxes();

        });


        $('.packing-details').on('change', function(){

            var netWeight = $(this).parent().parent().find('.net-weight').text();
            var grossWeight = $(this).parent().parent().find('.gross-weight').text();
            var selectedBoxes = parseInt($(this).closest('.material-div').find('.selected-boxes').find('span:last-child').text());


            // console.log(selectedBoxes);

            if($(this).is(':checked')){

                var totalGrossWeight = parseFloat($(this).closest('.material-div').find('.total-gross-weight span:first-child').text()) + parseFloat(grossWeight);
                var totalNetWeight = parseFloat($(this).closest('.material-div').find('.total-net-weight span:first-child').text()) + parseFloat(netWeight);

                selectedBoxes++;
                // console.log(totalSelected++);
                // $('.total-selected-boxes').find('span:last-child').text(totalSelected++);

            }else{


                var totalGrossWeight = parseFloat($(this).closest('.material-div').find('.total-gross-weight span:first-child').text()) - parseFloat(grossWeight);
                var totalNetWeight = parseFloat($(this).closest('.material-div').find('.total-net-weight span:first-child').text()) - parseFloat(netWeight);
                selectedBoxes--;
                // console.log(totalSelected--);

                // $('.total-selected-boxes').find('span:last-child').text(totalSelected--);


            }

            $(this).closest('.material-div').find('.selected-boxes span:last-child').text(selectedBoxes);



            // $('#selected-div').find('span').text(selectedBoxes);

            $(this).closest('.material-div').find('.total-gross-weight span:first-child').text(totalGrossWeight.toFixed(2));
            $(this).closest('.material-div').find('.total-net-weight span:first-child').text(totalNetWeight.toFixed(2));

            setTotalSelectedBoxes();


        })


        function setTotalSelectedBoxes(){
            var totalSelectedBoxes = $(document).find('.packing-details:checked').length;
            // console.log(totalSelectedBoxes)
            $('.total-selected-boxes').find('span:last-child').text(totalSelectedBoxes);

        }


        $('#en_hc').on('change', function(){

            $('#handling-charges').attr('disabled',false);
            if($(this).find('option:selected').val() == "yes"){
                $('#handling-charges').val('');
            }else{
                $('#handling-charges').val(0);
            }
        })


        $('#handling-charges').on('keydown', function(e){
            if($('#en_hc').find('option:selected').val() == "no"){
                e.preventDefault();
            }else{
                var charCode = (e.which) ? e.which : e.keyCode;
                console.log(charCode);
                if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
                    return true;

            }
        });


        $('#invoice_no').on('keydown', function (e) {
            e.preventDefault();
        })




    });



</script>
@endsection
