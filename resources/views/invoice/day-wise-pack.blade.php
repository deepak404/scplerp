
@extends( \Auth::user()->dept_id == '1'  ?  'layouts.sales' : 'layouts.invoice' )
@section('css')

<style>
    .machine-list-card{
        width: 350px;
        height: 320px;
        box-shadow: 0px 0px 5px #cecece;
        /*text-align: center;*/
        background-color: white;
    }

    .card-holder{
        display: flex;
        align-items: center;
        justify-content: center;
        height: 80vh;
    }

    .form-group span{
        width: 80px;
        display: inline-block;
        margin-left: 30px;
    }

    form{
        margin-top: 50px;
    }

    input[type="submit"]{
        width: 150px;
        margin-top: 30px;
    }

    h3{
        margin-top: 15px;
    }

    body{
        background-color: #f5f5f5;
    }

    form input[type="text"], form select{
        width: 170px !important;
        height: 35px !important;
        background-color: #f7f7f7 !important;
    }
</style>

@endsection

@section('content')

<section id="pf-head">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12 card-holder">
            <div class="machine-list-card">
                <?php if (empty($model)): ?>
                  <h3 class="text-center">Day Wise Packed Entry</h3>
                  <form action="/get-day-wise-pack" method="POST" target="_blank">
                    <?php else: ?>
                      <?php if ($model == 'count'): ?>
                        <h3 class="text-center">Quality Wise Packed Entry</h3>
                        <form action="/get-count-wise-pack" method="POST" target="_blank">
                          <?php else: ?>
                            <h3 class="text-center">Quality Packed Entry</h3>
                            <form action="/get-material-pack" method="POST" target="_blank">
                      <?php endif; ?>
                <?php endif; ?>
                    {{csrf_field()}}
                    <div class="form-group">
                      <span for="filter-date">Date : </span>
                      <input type="text" class="text-input filter-date" name="start_date" autocomplete="off" required>
                    </div>
                    <?php if (!empty($model)): ?>
                      <?php if ($model == 'item'): ?>
                        <div class="form-group">
                          <span for="filter-date">To : </span>
                          <input type="text" class="text-input filter-date" name="end_date" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                          <span for="material">Quality : </span>
                          <select class="" name="material" required>
                            <?php foreach (\App\ItemMaster::all()->sortBy('material') as $key => $value): ?>
                              <option value="{{$value->id}}">{{$value->material}}</option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      <?php endif; ?>
                    <?php endif; ?>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary center-block" value="Get Report">
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
</section>

@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.filter-date').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'dd-mm-yy',
        // onClose: function(dateText, inst) {
        //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        // }
    });
    $('#filter-date').focus(function(){
        $(".ui-datepicker-calendar").hide();
    });
});
</script>
@endsection
