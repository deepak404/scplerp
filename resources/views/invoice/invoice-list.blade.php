@extends( \Auth::user()->dept_id == '6'  ?  'layouts.invoice' : 'layouts.sales' )

@section('css')
    <style>
        .dropdown-toggle{
            width: 165px;
        }

        h3{
            display: inline-block;
        }

        .invoice-options{
            display: inline-block;
            float: right;
        }
    </style>
@endsection

@section('content')
    <section id="indent-list">
        <div class="container-fluid" style="margin-top: 70px;">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="margin: 20px auto;">
                    <h3 style="display: inline-block">Invoice List</h3>

                    <div class="invoice-options">
                        <input type="text" name="filter-date" class="text-input" id="filter-date" placeholder="Filter Date">

                    </div>
                </div>

                <div class="col-md-12 table-wrapper">
                    @if($errors->any())
                        <ul style="padding-left: 0px;">
                            @foreach ($errors->all() as $error)
                                <li style="list-style-type: none; color: red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Invoice No</th>
                            <th>Date</th>
                            <th>Party Name</th>
                            {{--<th>Delete</th>--}}
                            <th>Info</th>
                            <th>Package Slip</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php $count=0; ?>
                        @foreach($invoiceList as $invoice)
                            <tr>
                                <td>{{++$count}}</td>
                                <td>{{$invoice->invoice_no}}</td>
                                <td>{{date('d-m-Y', strtotime($invoice->date))}}</td>
                                <td>{{\App\BusinessPartner::where('id', $invoice->bp_id)->value('bp_name')}}</td>
                                {{--<td><a href="delete-invoice/{{$invoice->id}}" class="delete-invoice" data-id="{{$invoice->id}}"><i class="material-icons">delete</i></a></td>--}}
                                <td><a href="#" data-id="{{$invoice->id}}" class="generate-invoice" target="_blank"><i class="material-icons">info</i></a></td>
                                <td><a href="/get-package-slip/{{$invoice->id}}" class="info-indent" target="_blank"><i class="material-icons">receipt</i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <section id="pop-ups">
        <div id="invoice-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Invoice</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/generate-invoice-report" method="POST" target="_blank" id="invoice-form" name="invoice-form">
                            {{csrf_field()}}
                            <input type="hidden" name="invoice_id" id="invoice-id">
                            <div class="form-group">
                                <label for="no_of_copies">No of Copies</label>
                                <input type="text" class="text-input" id="no_of_copies" name="no_of_copies" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" id="submit-sale-form" value="Generate Invoice">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
      $(document).ready(function() {
        $('#filter-date').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'mm-yy',
            onClose: function(dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });

        $('#filter-date').focus(function(){
            $(".ui-datepicker-calendar").hide();
        });

        $(document).on('click','.ui-datepicker-close',function(){
            window.location.href = '/invoice-list/'+$('#filter-date').val();
        });

        $('.generate-invoice').on('click', function(e){

            e.preventDefault();

            $('#invoice-form').find('#invoice-id').val($(this).data('id'));
            $('#invoice-modal').modal('show');


        });

      });
    </script>
@endsection
