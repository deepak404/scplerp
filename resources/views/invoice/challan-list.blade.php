@extends( \Auth::user()->dept_id == '6'  ?  'layouts.invoice' : 'layouts.sales' )

@section('css')
    <style>
        .dropdown-toggle{
            width: 165px;
        }

        h3{
            display: inline-block;
        }

        .invoice-options{
            display: inline-block;
            float: right;
        }
    </style>
@endsection

@section('content')
    <section id="indent-list">
        <div class="container-fluid" style="margin-top: 70px;">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="margin: 20px auto;">
                    <h3 style="display: inline-block">Challan List</h3>
                </div>

                <div class="col-md-12 table-wrapper">
                    @if($errors->any())
                        <ul style="padding-left: 0px;">
                            @foreach ($errors->all() as $error)
                                <li style="list-style-type: none; color: red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Challan No</th>
                            <th>Date</th>
                            <th>Reason</th>
                            <th>List</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0; ?>
                        @foreach($challans as $challan)
                            <tr>
                                <td>{{++$count}}</td>
                                <td>{{$challan->challan_no}}</td>
                                <td>{{date('d-m-Y', strtotime($challan->date))}}</td>
                                <td>{{$challan->reason}}</td>
                                <td><a href="generate-challan-list/{{$challan->challan_no}}" target="_blank"><i class="material-icons">receipt</i></a></td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>


    <section id="pop-ups">
        <div id="invoice-modal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Invoice</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/generate-invoice-report" method="POST" target="_blank" id="invoice-form" name="invoice-form">
                            {{csrf_field()}}
                            <input type="hidden" name="invoice_id" id="invoice-id">
                            <div class="form-group">
                                <label for="no_of_copies">No of Copies</label>
                                <input type="text" class="text-input" id="no_of_copies" name="no_of_copies" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" id="submit-sale-form" value="Generate Invoice">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#filter-date').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'mm-yy',
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });

            $('#filter-date').focus(function(){
                $(".ui-datepicker-calendar").hide();
            });

            $(document).on('click','.ui-datepicker-close',function(){
                window.location.href = '/invoice-list/'+$('#filter-date').val();
            });

            $('.generate-invoice').on('click', function(e){

                e.preventDefault();

                $('#invoice-form').find('#invoice-id').val($(this).data('id'));
                $('#invoice-modal').modal('show');


            });

        });
    </script>
@endsection
