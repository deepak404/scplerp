@extends('layouts.invoice')

@section('css')
    <style>
        .dropdown-toggle{
            width: 165px;
        }


        h3{
            display: inline-block;
        }

        .invoice-options{
            display: inline-block;
            float: right;
        }
    </style>
@endsection

@section('content')
    <section id="indent-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="margin: 20px auto;">
                    <h3 style="display: inline-block">Indent List</h3>
                    <div class="invoice-options">
                        <select class="businessPartner" name="businessPartner">
                          <option value="" selected disabled>Select Business Partner</option>
                          <?php foreach ($businessPartner->sortBy('bp_name') as $key => $value): ?>
                            <option value="{{$value->id}}">{{$value->bp_name}}</option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-12 table-wrapper">
                    @if($errors->any())
                        <ul style="padding-left: 0px;">
                            @foreach ($errors->all() as $error)
                                <li style="list-style-type: none; color: red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Indent No</th>
                          <th>Customer</th>
                          <th>Material</th>
                          <th>Order Qty</th>
                          <th>Price</th>
                          <th>Date</th>
                          <th>Pending Qty</th>
                          <th>Create Invoice</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (!empty($saleOrders)): ?>
                          <?php foreach ($saleOrders->sortBy('material_id') as $key => $value): ?>
                          @if (!empty($value->indentDetails))
                            <tr>
                              <th>{{$value->indentDetails->indentMaster->order_ref_no}}</th>
                              <td>{{$value->businessPartner->bp_name}}</td>
                              <td>{{$value->material_name}}</td>
                              <td>{{$value->order_quantity}}</td>
                              <td>{{$value->indentDetails->rate_per_kg}}</td>
                              <td>{{date('d / m / Y',strtotime($value->indentDetails->indentMaster->dated_on))}}</td>
                              <td>{{$value->pending_quantity}}</td>
                              <td><a href="/create-invoice/{{$value->indentDetails->indentMaster->id}}" class="create-invoice" target="_blank">Create Invoice</a></td>
                            </tr>
                          @endif
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
      $(document).ready(function() {

        $('.businessPartner').on('change',function() {
          window.location.href = '/indent-list-invoice/'+$(this).val();
        });
      });
    </script>
@endsection
