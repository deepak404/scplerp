@extends('layouts.invoice')

@section('css')
    <style>
        .dropdown-toggle{
            width: 165px;
        }


        h3{
            display: inline-block;
        }

        .invoice-options{
            display: inline-block;
            float: right;
        }
        /* .btn-primary {
            height: 38px !important;
            border-radius: 2px !important;
            background-color: #003ebb;
        } */
    </style>
@endsection

@section('content')
    <section id="indent-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="margin: 20px auto;">
                    <h3 style="display: inline-block">Indent List</h3>
                    <div class="invoice-options">
                        <input type="text" name="ref_no" class="text-input"  value="" placeholder="Reference Number">
                        <button type="button" class="btn btn-primary" name="get">Get</button>
                    </div>
                </div>

                <div class="col-md-12 table-wrapper">
                    @if($errors->any())
                        <ul style="padding-left: 0px;">
                            @foreach ($errors->all() as $error)
                                <li style="list-style-type: none; color: red;">{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Indent No</th>
                          <th>Customer</th>
                          <th>Material</th>
                          <th>Order Qty</th>
                          <th>Price</th>
                          <th>Date</th>
                          <th>Pending Qty</th>
                          <th>Create Invoice</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (!empty($indentMasters)): ?>
                          <?php foreach ($indentMasters as $indentMaster): ?>
                            <?php foreach ($indentMaster->indentDetails as $indentDetail): ?>
                              <tr>
                                <th>{{$indentMaster->order_ref_no}}</th>
                                <td>{{$indentMaster->businessPartner->bp_name}}</td>
                                <td>{{$indentDetail->saleOrder->material_name}}</td>
                                <td>{{$indentDetail->quantity}}</td>
                                <td>{{$indentDetail->rate_per_kg}}</td>
                                <td>{{$indentMaster->dated_on}}</td>
                                <td>{{$indentDetail->saleOrder->pending_quantity}}</td>
                                <td><a href="/create-invoice/{{$indentMaster->id}}" class="create-invoice" target="_blank">Create Invoice</a></td>
                              </tr>
                            <?php endforeach; ?>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript">
      $(document).ready(function() {

        $('button[name="get"]').on('click',function() {
          window.location.href = '/get-ref-no/'+$('input[name="ref_no"]').val();
        });
      });
    </script>
@endsection
