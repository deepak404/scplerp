<!DOCTYPE html>
<html>
<head>
    <title>Purchase Chemical Clearance</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="css/generate-indent.css">
    <link rel="stylesheet" href="responsive-css/chemical-clearence.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="gulpfile.js"></script>

    <style>
        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            /*height: 100px;*/
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            /*padding-left: 5%;*/
            margin: 30px auto 15px;
        }

        .lot-no{
            width: 10%;
        }

        .type{
            width: 10%;
        }

        .weight, .date, .invoice-date, .status{
            width: 10%;
        }

        .denier{
            width: 10%;
        }

        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }

        /*        #createChemicalModal .modal-dialog{
                    width: 80% !important;
                }
        */
        .chemical-row{
            margin: 10px auto;
            padding: 0px;
        }

        #note{
            width: 90%;
            height: 30px;
        }

        #create-chemical{
            margin: 0px;
        }
        .modal-content {

            padding: 0px 15px !important;
        }
        .modal-body {
            padding: 5px;
        }
        .blue-text-modal{
            padding: 10px 15px  !important;
            font-weight: 600    !important;
        }


        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px;
            background-color: #ffffff;
        }



        .edit-chemical-clearance:hover{
            cursor: pointer;
        }
    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Raw Materials<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance">Filament Clearance</a></li>
                                <li><a href="/chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/packaging-material-clearance">Packing Material Clearance</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>
<?php $suppliers = App\Supplier::all(); //dd($suppliers); ?>

<section id="filament-clearance">
    <div class="container-fluid">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <h3>Chemical Clearance</h3>
                <button class="btn btn-primary pull-right" data-target="#createChemicalModal" data-toggle="modal">Create Chemical Lot</button>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-md-12" id="filament-clearance-div">

                @foreach($chemicalClearance as $clearance)
                    <div class="clearance-list">
                        <div class="lot-no inline-div">
                            <p class="list-header">LOT No</p>
                            <p class="list-details">{{$clearance->lot_no}}</p>
                        </div>

                        <div class="type inline-div">
                            <p class="list-header">Supplier</p>
                            <p class="list-details">{{$clearance->supplier->name}}</p>
                        </div>

                        <div class="weight inline-div">
                            <p class="list-header">Chemical Name</p>
                            <p class="list-details">{{$clearance->chemical}}</p>
                        </div>

                        <div class="date inline-div">
                            <p class="list-header">Chemical Code</p>
                            <p class="list-details">{{$clearance->chemical_code}}</p>
                        </div>

                        <div class="invoice-date inline-div">
                            <p class="list-header">Man. Date</p>
                            <p class="list-details">{{date('d-m-Y', strtotime($clearance->manufactured_date))}}</p>
                        </div>

                        <div class="denier inline-div">
                            <p class="list-header">Exp. Date</p>
                            <p class="list-details">{{date('d-m-Y', strtotime($clearance->expiry_date))}}</p>
                        </div>

                        <div class="status inline-div">
                            <p class="list-header">Status</p>
                            @if($clearance->status == 0)
                                <p class="list-details blue-text"><b>on Process</b></p>
                            @elseif($clearance->status == 1)
                                <p class="list-details green"><b>Cleared</b></p>
                            @else
                                <p class="list-details red"><b>Rejected</b></p>
                            @endif
                        </div>

                        <div class="action inline-div">
                            @if($clearance->status == 0)
                                <a data-id="{{$clearance->id}}" class="edit edit-chemical-clearance">Edit</a>
                                <a href="/delete-chemical-lot/{{$clearance->id}}" data-id="{{$clearance->id}}" class="delete-chemical-lot btn btn-primary" >Delete</a>
                            @else
                                <a href="/chemical-clearance-report/{{$clearance->id}}" target="_blank" class="download">Download</a>
                                {{--<a href="#" class="edit" disabled>Edit</a>--}}
                                {{--<a href="#" class="delete btn btn-primary" disabled>Delete</a>--}}
                            @endif
                        </div>

                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>

<section id="pop-ups">
    <div id="createChemicalModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Chemical Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-chemical-lot" method="POST" class="col-md-12" id="create-chemical">
                        {{csrf_field()}}
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier">Supplier Name</label>
                                <select name="supplier" id="supplier">
                                    <option disabled selected value>Select</option>

                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" name="supplier" id="supplier" required>--}}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="received-on">Received On</label>
                                <input type="text" name="received_on" id="received-on" class="date-field" autocomplete="off" required>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supply-type">Supply type</label>
                                <select id="supply-type" name="supplier_type">
                                    <option disabled selected value>Select</option>

                                    <option value="imported">Imported</option>
                                    <option value="indigenous">Indigenous</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="chemical-name">Chemical Name</label>
                                <select name="chemical" id="chemical-name" required>
                                    <option disabled selected value>Select</option>
                                    @foreach($chemicals as $chemical)
                                        <?php $cc = \App\Chemicals::where('descriptive_name',$chemical)->value('material_code'); ?>
                                        <option value="{{$chemical}}" data-cc="{{$cc}}">{{$chemical}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="chemical-code">Chemical Code</label>
                                    <input type="text" id="chemical-code" name="chemical_code">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="gate-no">Gate Inward No</label>
                                    <input type="text" id="gate-in-no" name="gate_in_no">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="gate-date">Gate Date</label>
                                    <input type="text" id="gate-date" class="date-field" name="gate_date">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">

                                <p class="blue-text-modal">Lot Details</p>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="lot-no">Lot No</label>
                                    <input type="text" id="lot-no" name="lot_no" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="manufactured-date">Manufactured Date</label>
                                    <input type="text" name="manufactured_date" id="manufactured-date" class="date-field" autocomplete="off" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="total-weight">Invoice Quantity</label>
                                    <input type="text" id="total-weight" name="total_weight" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="expiry-date">Expiry Date</label>
                                    <input type="text" name="expiry_date" id="expiry-date" class="date-field" autocomplete="off" required>
                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="no-of-barrels">No of Package</label>
                                    <input type="text" id="no-of-barrels" name="no_of_packs" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="grn-remarks">GRN Remarks</label>
                                    <input type="text" id="grn-remarks" name="grn_remarks" required>
                                </div>
                            </div>



                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary" value="Create Chemical Lot">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="editChemicalModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Chemical Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/edit-chemical-lot" method="POST" class="col-md-12" id="edit-chemical">
                        {{csrf_field()}}
                        <input type="hidden" name="update-type" value="1">
                        <input type="hidden" name="id" id="id" >
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier">Supplier Name</label>
                                <select name="supplier" id="supplier">
                                    @foreach($suppliers as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" name="supplier" id="supplier" required>--}}
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="received-on">Received On</label>
                                <input type="text" name="received_on" class="date-field" id="u-received-on" autocomplete="off" required>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supply-type">Supply type</label>
                                <select id="supply-type" name="supplier_type">

                                    <option value="imported">Imported</option>
                                    <option value="indigenous">Indigenous</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="chemical-name">Chemical Name</label>
                                <select name="chemical" id="chemical-name" required>
                                    @foreach($chemicals as $chemical)
                                        <?php $cc = \App\Chemicals::where('descriptive_name',$chemical)->value('material_code'); ?>
                                        <option value="{{$chemical}}" data-cc="{{$cc}}">{{$chemical}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="chemical-code">Chemical Code</label>
                                    <input type="text" id="chemical-code" name="chemical_code">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="gate-no">Gate Inward No</label>
                                    <input type="text" id="gate-in-no" name="gate_in_no">
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="gate-date">Gate Date</label>
                                    <input type="text" id="u-gate-date" class="date-field" name="gate_date">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">

                                <p class="blue-text-modal">Lot Details</p>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="lot-no">Lot No</label>
                                    <input type="text" id="lot-no" name="lot_no" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="manufactured-date">Manufactured Date</label>
                                    <input type="text" name="manufactured_date" class="date-field" id="u-manufactured-date" autocomplete="off" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="total-weight">Invoice Quantity</label>
                                    <input type="text" id="total-weight" name="total_weight" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="expiry-date">Expiry Date</label>
                                    <input type="text" name="expiry_date" class="date-field" id="u-expiry-date" autocomplete="off" required>
                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="no-of-barrels">No of Package</label>
                                    <input type="text" id="no-of-barrels" name="no_of_packs" required>
                                </div>

                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                    <label for="grn-remarks">GRN Remarks</label>
                                    <input type="text" id="grn-remarks" name="grn_remarks" required>
                                </div>
                            </div>



                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row p-lr-zero">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary" value="Create Chemical Lot">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmationModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 400px !important;">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h4 class="confirmation-info">Are you surely want to delete this ? </h4>
                    <div class="confirm-div">
                        <input type="button" class="btn btn-primary delete-chemical-lot-btn" value="Yes">
                        <input type="button" class="btn btn-primary delete-chemical-lot-btn" value="No">
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{--<script src="js/sales-index.js"></script>--}}
<script type="text/javascript">
    $(document).ready(function(){
        $('.date-field').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'dd-mm-yy',
            // onClose: function(dateText, inst) {
            //     $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            // }
        });


        $('body').on('focus',".datepicker:not(.hasDatepicker)", function(){
            $(this).datepicker();
        });



        $('.delete-chemical-lot').on('click', function(e){
            e.preventDefault();
            var dataId = $(this).data('id');

            $('#confirmationModal').modal('show');


            $('.delete-chemical-lot-btn').click(function(e){

                if ($(this).val() == 'Yes'){
                    window.location.href = "/delete-chemical-lot/"+dataId;
                } else{
                    $('#confirmationModal').modal('hide');
                }

            })
        });



        $('.edit-chemical-clearance').on('click', function(){

            // $('#')
            $.ajax({
                type: "POST",
                url: "/get-chemical-clearance",
                data: 'id='+$(this).data('id'),
                success: function(data, status, xhr) {

                    var form = $('#edit-chemical');

                    form.find('#id').val(data.id);
                    form.find('#supplier').val(data.supplier_id).change();
                    form.find('#supply-type').val(data.supplier_type).change();
                    form.find('#u-received-on').val(formatDate(data.received_on));
                    form.find('#received-from').val(data.received_from);
                    form.find('#chemical-name').val(data.chemical);
                    form.find('#chemical-code').val(data.chemical_code);
                    form.find('#lot-no').val(data.lot_no);
                    form.find('#u-gate-date').val(formatDate(data.gate_date));
                    form.find('#gate-in-no').val(data.gate_in_no);
                    form.find('#u-manufactured-date').val(formatDate(data.manufactured_date));
                    form.find('#total-weight').val(data.total_weight);
                    form.find('#u-expiry-date').val(formatDate(data.expiry_date));
                    form.find('#no-of-barrels').val(data.no_of_packs);
                    form.find('#no-of-samples').val(data.no_of_samples);
                    form.find('#grn-remarks').val(data.grn_remarks);


                    $('#editChemicalModal').modal('show');
                },
                error: function(xhr, status, error) {
                    if (xhr.status == 422) {

                        errorHandler(xhr.responseJSON.errors)

                    }
                },
            })

        });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });


        $(document).on('change', '#chemical-name',function(){
            console.log($(this).find('option:selected').data('cc'));
            $(this).closest('form').find('#chemical-code').val($(this).find('option:selected').data('cc'));
        });


        function formatDate(date){
            var date = (date).split('-');
            date = date[2]+'-'+date[1]+'-'+date[0];

            console.log(date);

            return date;
        }
    });
</script>

</body>
</html>
