<!-- <!DOCTYPE html> -->
<html>
<head>
    <title>Purchase Packing Material Clearance</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/global.css">
    <link rel="stylesheet" href="responsive-css/purchase-material-clearance.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="gulpfile.js"></script>

    <style>
        .inline-div{
            display: inline-block;
        }

        .clearance-list{
            /*height: 100px;*/
            padding: 20px 15px;
            box-shadow: 0px 0px 5px gainsboro;
            /*padding-left: 5%;*/
            margin: 30px auto 15px;
        }


        .download,.edit,.delete{
            /*width: 10%;*/
            margin: 0px 10px;
        }

        .action{
            vertical-align: middle;
            margin-top: -30px;
        }

        .list-header{
            font-weight: bold;
        }

        .download,.edit{
            margin: 0px 10px;
            padding: 10px;
            background-color: #DDE8FE;
            color: #5D5F61;
            font-weight: bold;
        }

        h3{
            display: inline-block;
            margin-top: 0px;
        }


        .chemical-row{
            margin: 10px auto;
            padding: 0px;
        }

        #filament-clearance{
            margin-top: 50px;
        }

        .chemical-row input, .chemical-row select {
            margin: 0px auto 20px;
        }
        .form-spacing{
                    margin: 0px;
            padding: 0px;
        }

        .modal-body {
        padding: 5px;
        }


        input, select {
            height: auto !important;
            border: 1px solid #d7d7d7 !important;
            padding: 5px !important;
            border-radius: 0px !important;
            background-color: #ffffff;
        }

        #create-chemical{
            margin: 0px;
        }

        .modal-content {
            padding-bottom: 25px;
        }

        select {
            height: 32.5px !important;
        }
    </style>
</head>
<body>
<section id="header">
    <header>

        <div class="container">
        </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/logo.svg " class="nav-logo"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
                        <li class="dropdown" id="nav-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Raw Materials<i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/filament-clearance">Filament Clearance</a></li>
                                <li><a href="/chemical-clearance">Chemical Clearance</a></li>
                                <li><a href="/packaging-material-clearance">Packing Material Clearance</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="active-menu" href="/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

</section>
<section id="filament-clearance">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>Packing Material Clearance</h3>
                <button class="btn btn-primary pull-right" data-target="#createPackingModal" data-toggle="modal">Create Lot</button>
            </div>



           <div class="col-md-12">
               @if ($errors->any())
                   <div class="alert alert-danger">
                       <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                           @endforeach
                       </ul>
                   </div>
               @endif
           </div>

            <div class="col-md-12">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </div>



            <div class="col-md-12" id="packing-clearance-div">

            @foreach($packageClearance as $clearance)
<!--                    --><?php //dd($clearance); ?>

                    <div class="clearance-list">
                    <div class="lot-no inline-div">
                        <p class="list-header">LOT No</p>
                        <p class="list-details">{{$clearance->packing_lot_no}}</p>
                    </div>

                    <div class="type inline-div">
                        <p class="list-header">Supplier</p>
                        <p class="list-details">{{$clearance->supplier->name}}</p>
                    </div>

                    <div class="weight inline-div">
                        <p class="list-header">Item Name</p>
                        <p class="list-details">{{$clearance->item}}</p>
                    </div>

                    <div class="date inline-div">
                        <p class="list-header">Quantity Received</p>
                        <p class="list-details">{{$clearance->quantity_received}}</p>
                    </div>

                    <div class="invoice-date inline-div">
                        <p class="list-header">Lot No</p>
                        <p class="list-details">{{$clearance->packing_lot_no}}</p>
                    </div>

                    <div class="denier inline-div">
                        <p class="list-header">Gate Date</p>
                        <p class="list-details">{{$clearance->gate_date}}</p>
                    </div>

                    <div class="status inline-div">
                        <p class="list-header">Status</p>
                        @if($clearance->status == 0)
                            <p class="list-details blue-text"><b>on Process</b></p>
                        @elseif($clearance->status == 1)
                            <p class="list-details green"><b>Cleared</b></p>
                        @else
                            <p class="list-details red"><b>Rejected</b></p>
                        @endif
                    </div>


                    <div class="action inline-div">
                        @if($clearance->status == 0)
                            <a href="#" class="edit edit-packaging-lot" data-id="{{$clearance->id}}">Edit</a>
                            <a href="/delete-packaging-material-lot/{{$clearance->id}}" class="delete btn btn-primary" >Delete</a>
                        @else
                            <a href="/packing-clearance-report/{{$clearance->id}}" target="_blank" class="download">Download</a>
                            {{--<a href="#" class="edit" disabled>Edit</a>--}}
                            {{--<a href="#" class="delete btn btn-primary" disabled>Delete</a>--}}
                        @endif

                        {{--<a href="#" class="download">Download</a>--}}
                        {{--<a href="#" class="edit">Edit</a>--}}
                    </div>
                </div>

                    @endforeach
            </div>
        </div>
    </div>
</section>
<?php //dd($packagingMaterial); ?>


<section id="pop-ups">
    <div id="createPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create Package Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-packaging-lot" method="POST" class="col-md-12" id="create-packing">
                        {{csrf_field()}}
                        <input type="hidden" name="form-type" value="create">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-name">Item Name</label>
                                <select class="input-field" name="item" id="item-name">
                                    <option disabled="" selected="" value="">Select</option>

                                @foreach($packagingMaterial as $material => $data)

                                        <option value="{{$material}}" data-code="{{$data[0]['code']}}">{{$material}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-code">Item Code</label>
                                <input type="text" name="item_code" id="item-code" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier-name">Supplier Name</label>
                                <select style="width: 100%;" id="supplier-name" name="supplier_name" required>
                                    <option disabled="" selected="" value="">Select</option>
                                    @foreach(\App\Supplier::all() as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supply-type">Supply Type</label>
                                <select id="supply-type" name="supplier_type" required>
                                    <option disabled="" selected="" value="">Select</option>
                                    <option value="imported">Imported</option>
                                    <option value="indigenous">Indigenous</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="goods_received">Goods Received on</label>
                                <input type="text" id="goods_received" name="goods_received" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-ordered">Quantity Order</label>
                                <input type="text" id="quantity-ordered" name="quantity_ordered"  required >
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="gate-in-no">Gate Inward No</label>
                                <input type="text" id="gate-in-no" name="gate_in_no" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="gate-date">Gate Date</label>
                                <input type="text" id="gate-date" name="gate_date" required>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="packing-lot-no">Lot No</label>
                                <input type="text" id="packing-lot-no" name="packing_lot_no" required >
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="invoice-quantity">Invoice Quantity</label>
                                <input type="text" id="invoice-quantity" name="invoice_quantity" required >
                            </div>


                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row ">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="editPackingModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Package Clearance</h4>
                </div>
                <div class="modal-body row">
                    <form action="/create-packaging-lot" method="POST" class="col-md-12" id="create-packing">
                        {{csrf_field()}}
                        <input type="hidden" name="form-type" value="update">
                        <input type="hidden" name="packing-id" id="packing-id">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-name">Item Name</label>
                                <select class="input-field" name="item" id="item-name">
                                    <option disabled="" selected="" value="">Select</option>

                                    @foreach($packagingMaterial as $material => $data)

                                        <option value="{{$material}}" data-code="{{$data[0]['code']}}">{{$material}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="item-code">Item Code</label>
                                <input type="text" name="item_code" id="item-code" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supplier-name">Supplier Name</label>
                                <select style="width: 100%;" id="supplier-name" name="supplier_name" required>
                                    <option disabled="" selected="" value="">Select</option>
                                    @foreach(\App\Supplier::all() as $supplier)
                                        <option value="{{$supplier->id}}">{{$supplier->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="supply-type">Supply Type</label>
                                <select id="supply-type" name="supplier_type" required>
                                    <option disabled="" selected="" value="">Select</option>
                                    <option value="imported">Imported</option>
                                    <option value="indigenous">Indigenous</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="goods_received">Goods Received on</label>
                                <input type="text" id="goods_received" name="goods_received" required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="quantity-ordered">Quantity Order</label>
                                <input type="text" id="quantity-ordered" name="quantity_ordered"  required>
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="gate-in-no">Gate Inward No</label>
                                <input type="text" id="gate-in-no" name="gate_in_no" required >
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="gate-date">Gate Date</label>
                                <input type="text" id="gate-date" name="gate_date" required>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row">
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="packing-lot-no">Lot No</label>
                                <input type="text" id="packing-lot-no" name="packing_lot_no" required >
                            </div>

                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                <label for="invoice-quantity">Invoice Quantity</label>
                                <input type="text" id="invoice-quantity" name="invoice_quantity" required >
                            </div>


                        </div>



                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 chemical-row ">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <input type="submit" class="btn btn-primary" value="Create Lot">
                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

        $('#goods_received, #gate-date').datepicker({
            dateFormat : 'dd-mm-yy',
        });

       $('.edit-packaging-lot').on('click', function(){

           // console.log($(this).data('id'));
           $.ajax({
               type:'POST',
               url:'/find-packing-material-lot',
               data:'id='+$(this).data('id'),
               success:function(data){
                   var modal = $('#editPackingModal');

                   console.log(data.data);

                   modal.find('#supplier-name').val(data.data.supplier_name);
                   modal.find('#item-code').val(data.data.item_code);
                   modal.find('#packing-lot-no').val(data.data.packing_lot_no);
                   modal.find('#quantity-ordered').val(data.data.quantity_ordered);

                   modal.find('#supplier-name').val(data.data.supplier_id).change();
                   modal.find('#supply-type').val(data.data.supplier_type).change();

                   modal.find('#goods_received').val(data.data.received_on);
                   modal.find('#quantity-ordered').val(data.data.quantity_received);
                   modal.find('#gate-in-no').val(data.data.gate_in_no);
                   modal.find('#gate-date').val(data.data.gate_date);
                   modal.find('#invoice-quantity').val(data.data.invoice_quantity);


                   modal.find('#packing-id').val(data.data.id);

                   modal.find("#item-name").val(data.data.item).change();

               },
               error:function(xhr){
                   console.log(xhr.status);
               }
           });

           $('#editPackingModal').modal('show');
       });


       $('#item-name').on('change', function(){
           // console.log('hge');
           console.log($(this).find(':selected').data('code'));
           $('#item-code').val($(this).find(':selected').data('code'));
       });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    });
</script>
</body>
</html>
